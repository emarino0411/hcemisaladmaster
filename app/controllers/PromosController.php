<?php

use Illuminate\Support\Facades\Auth;


class PromosController extends \BaseController
{

    /**
     * Display a listing of the resource.
     * GET /promos
     *
     * @return Response
     */
    public function index()
    {
        if (!isAccessModuleAllowed('promos_view')) {
            return Redirect::to('dashboard');
        }
        $promos = DB::table('promos')
            ->orderBy('promos.updated_at', 'desc')
            ->get();

        return View::make('promos/promos_list', [
            'page_title' => 'HCEMIOS Promos',
            'page_description' => 'Show me the contests :)',
            'level' => 'Promos',
            'sub_level' => 'List',
            'promos' => $promos
        ]);
    }

    /**
     * Show the form for creating a new resource.
     * GET /promos/create
     *
     * @return Response
     */
    public function create()
    {
        //
        if (!isAccessModuleAllowed('promos_add')) {
            return Redirect::to('dashboard');
        }
        return View::make('promos/promos_add', [
            'page_title' => 'HCEMIOS Promos',
            'page_description' => 'Add contests',
            'level' => 'Promos',
            'sub_level' => 'Add New',
            'products' => []
        ]);
    }

    public function updateContest()
    {
        if (!isAccessModuleAllowed('promos_view')) {
            return Redirect::to('dashboard');
        }
        return View::make('promos/promos_update', [
            'page_title' => 'HCEMIOS Promos',
            'page_description' => 'Update Contest',
            'level' => 'Promos',
            'sub_level' => 'Update',
            'products' => [],
            'purchases' => []

        ]);
    }

    public function register_po()
    {
        if (!isAccessModuleAllowed('promos_edit')) {
            return Redirect::to('dashboard');
        }
        return View::make('promos/promos_register_po', [
            'page_title' => 'HCEMIOS Promos',
            'page_description' => 'Register Purchase Order',
            'level' => 'Promos',
            'sub_level' => 'Register PO',
            'products' => [],
            'purchases' => []

        ]);
    }

    public function register_po_by_po()
    {
        if (!isAccessModuleAllowed('promos_edit')) {
            return Redirect::to('dashboard');
        }
        return View::make('promos/promos_register_po_by_po', [
            'page_title' => 'HCEMIOS Promos',
            'page_description' => 'Register Purchase Order',
            'level' => 'Promos',
            'sub_level' => 'Register PO',
            'products' => [],
            'purchases' => []

        ]);
    }

    /**
     * Store a newly created resource in storage.
     * POST /promos
     *
     * @return Response
     */
    public function store()
    {
        if (!isAccessModuleAllowed('promos_add')) {
            return Redirect::to('dashboard');
        }
        $promos = new Promo();
        $promos->title = Input::get('txt_contest_title');
        $promos->description = Input::get('txt_contest_description');
        $promos->from = Input::get('txt_date_from');
        $promos->to = Input::get('txt_date_to');
        $promos->minimum_qualification = Input::get('slct_minimum_requirements');
        $promos->mechanics_1_points = Input::get('txt_set_sold_1');
        $promos->mechanics_1_prize = Input::get('txt_prize_1');
        $promos->mechanics_2_points = Input::get('txt_set_sold_2');
        $promos->mechanics_2_prize = Input::get('txt_prize_2');
        $promos->mechanics_3_points = Input::get('txt_set_sold_3');
        $promos->mechanics_3_prize = Input::get('txt_prize_3');
        $promos->status = 'ACTIVE';
        $promos->save();
    }

    /**
     * Display the specified resource.
     * GET /promos/{id}
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        if (!isAccessModuleAllowed('promos_view')) {
            return Redirect::to('dashboard');
        }
        $promo = DB::table('promos')
            ->where('id', '=', $id)
            ->orderBy('promos.updated_at', 'desc')
            ->get();

        return View::make('promos/promos_view', [
            'page_title' => 'HCEMIOS Promos',
            'page_description' => 'Add contests',
            'level' => 'Promos',
            'sub_level' => 'Add New',
            'promo' => $promo[0]
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     * GET /promos/{id}/edit
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        if (!isAccessModuleAllowed('promos_edit')) {
            return Redirect::to('dashboard');
        }
        $promo = DB::table('promos')
            ->where('id', '=', $id)
            ->orderBy('promos.updated_at', 'desc')
            ->get();

        return View::make('promos/promos_edit', [
            'page_title' => 'HCEMIOS Promos',
            'page_description' => 'Add contests',
            'level' => 'Promos',
            'sub_level' => 'Add New',
            'promo' => $promo[0]
        ]);
    }

    /**
     * Update the specified resource in storage.
     * PUT /promos/{id}
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $promos = Promo::find($id);
        $promos->title = Input::get('txt_contest_title');
        $promos->description = Input::get('txt_contest_description');
        $promos->from = Input::get('txt_date_from');
        $promos->to = Input::get('txt_date_to');
        $promos->minimum_qualification = Input::get('slct_minimum_requirements');
        $promos->mechanics_1_points = Input::get('txt_set_sold_1');
        $promos->mechanics_1_prize = Input::get('txt_prize_1');
        $promos->mechanics_2_points = Input::get('txt_set_sold_2');
        $promos->mechanics_2_prize = Input::get('txt_prize_2');
        $promos->mechanics_3_points = Input::get('txt_set_sold_3');
        $promos->mechanics_3_prize = Input::get('txt_prize_3');
        $promos->status = 'ACTIVE';
        $promos->save();
    }

    /**
     * Remove the specified resource from storage.
     * DELETE /promos/{id}
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function getContestTitle()
    {
        $promos = DB::table('promos')
            ->select(DB::raw('UPPER(title) as text, id as id'))
            ->where(function ($query) {
                $query->where(DB::raw('LOWER(promos.title)'), 'like', '%' . strtolower(Input::get('q')) . '%');
                $query->orWhere(DB::raw('LOWER(promos.description)'), 'like', '%' . strtolower(Input::get('q')) . '%');
            })
            ->get();
        return $promos;
    }

    public function getPO()
    {
        $promos = Promo::find(Input::get('contestId'));

        $purchases = DB::table('purchase')
            ->leftJoin('client', 'client.id', '=', 'purchase.client_client_id')
            ->leftJoin('purchase_item_list', 'purchase.id', '=', 'purchase_item_list.purchase_po_no')
            ->leftJoin('item', 'item.id', '=', 'purchase_item_list.item_id')
            ->select('client.last_name', 'client.first_name', 'purchase.id', 'purchase.purchase_date',
                'purchase.transaction_status', 'purchase.status',
                'item.description',
                DB::raw('SUM(purchase_item_list.item_price * qty) as total_price'))
            ->where('purchase.sponsor_id', '=', Input::get('agentId'))
            ->whereBetween('purchase.purchase_date', array($promos->from, $promos->to))
            ->orderBy('purchase.updated_at', 'desc')
            ->groupBy('purchase.id')
            ->get();

        return $purchases;
    }


    public function getPOByContest()
    {
        $promos = Promo::find(Input::get('contestId'));

        if($promos->minimum_qualification == '1'){
            $purchases = DB::table('purchase')
            ->leftJoin('client', 'client.id', '=', 'purchase.client_client_id')
            ->leftJoin('purchase_item_list', 'purchase.id', '=', 'purchase_item_list.purchase_po_no')
            ->leftJoin('item', 'item.id', '=', 'purchase_item_list.item_id')
            ->leftJoin('promos_qualifier','promos_qualifier.po_no','=','purchase.id')
            ->leftJoin('representative','representative.id','=','purchase.sponsor_id')
            ->leftJoin('representative as assoc','assoc.id','=','purchase.associate_id')
            ->join('multilevel', function ($join) {
                    $join->on('multilevel.agent_id', '=', 'representative.id')
                        ->where('multilevel.is_latest', '=', '1');
                })
            ->select('client.last_name', 'client.first_name', 'purchase.id', 'purchase.purchase_date',
                'purchase.transaction_status', 'purchase.status',
                'item.description','promos_qualifier.po_no',
                DB::raw('UPPER(CONCAT(representative.last_name,", ",representative.first_name)) as sponsor'),
                DB::raw('IFNULL(UPPER(CONCAT(assoc.last_name,", ",assoc.first_name)),"N/A") as assoc'),
                DB::raw('SUM(purchase_item_list.item_price * qty) as total_price'))
            ->whereBetween('purchase.purchase_date', array($promos->from, $promos->to)) 
            ->orderBy('purchase.updated_at', 'desc')
            ->groupBy('purchase.id')
            ->get();

        }else if($promos->minimum_qualification == '2'){
            $purchases = DB::table('purchase')
            ->leftJoin('client', 'client.id', '=', 'purchase.client_client_id')
            ->leftJoin('purchase_item_list', 'purchase.id', '=', 'purchase_item_list.purchase_po_no')
            ->leftJoin('item', 'item.id', '=', 'purchase_item_list.item_id')
            ->leftJoin('promos_qualifier','promos_qualifier.po_no','=','purchase.id')
            ->leftJoin('representative','representative.id','=','purchase.sponsor_id')
            ->leftJoin('representative as assoc','assoc.id','=','purchase.associate_id')
            ->join('multilevel', function ($join) {
                    $join->on('multilevel.agent_id', '=', 'representative.id')
                        ->where('multilevel.is_latest', '=', '1');
                })
            ->where(function ($query) {
                    $query->whereIn(DB::raw('UPPER(multilevel.level_to)'), ['CONSULTANT', 'SENIOR CONSULTANT', 'MANAGER', 'DISTRIBUTOR']);
                    $query->orWhere('multilevel.type', '=', 'Phase II');
                })
            ->select('client.last_name', 'client.first_name', 'purchase.id', 'purchase.purchase_date',
                'purchase.transaction_status', 'purchase.status',
                'item.description','promos_qualifier.po_no',
                DB::raw('UPPER(CONCAT(representative.last_name,", ",representative.first_name)) as sponsor'),
                DB::raw('IFNULL(UPPER(CONCAT(assoc.last_name,", ",assoc.first_name)),"N/A") as assoc'),
                DB::raw('SUM(purchase_item_list.item_price * qty) as total_price'))
            ->whereBetween('purchase.purchase_date', array($promos->from, $promos->to)) 
            ->orderBy('purchase.updated_at', 'desc')
            ->groupBy('purchase.id')
            ->get();


        }else if($promos->minimum_qualification == '3'){
            $purchases = DB::table('purchase')
            ->leftJoin('client', 'client.id', '=', 'purchase.client_client_id')
            ->leftJoin('purchase_item_list', 'purchase.id', '=', 'purchase_item_list.purchase_po_no')
            ->leftJoin('item', 'item.id', '=', 'purchase_item_list.item_id')
            ->leftJoin('promos_qualifier','promos_qualifier.po_no','=','purchase.id')
            ->leftJoin('representative','representative.id','=','purchase.sponsor_id')
            ->leftJoin('representative as assoc','assoc.id','=','purchase.associate_id')
            ->join('multilevel', function ($join) {
                    $join->on('multilevel.agent_id', '=', 'representative.id')
                        ->where('multilevel.is_latest', '=', '1');
                })
            ->where(function ($query) {
                    $query->whereIn(DB::raw('UPPER(multilevel.level_to)'), ['CONSULTANT', 'SENIOR CONSULTANT', 'MANAGER', 'DISTRIBUTOR']);
                })
            ->select('client.last_name', 'client.first_name', 'purchase.id', 'purchase.purchase_date',
                'purchase.transaction_status', 'purchase.status',
                'item.description','promos_qualifier.po_no',
                DB::raw('UPPER(CONCAT(representative.last_name,", ",representative.first_name)) as sponsor'),
                DB::raw('IFNULL(UPPER(CONCAT(assoc.last_name,", ",assoc.first_name)),"N/A") as assoc'),
                DB::raw('SUM(purchase_item_list.item_price * qty) as total_price'))
            ->whereBetween('purchase.purchase_date', array($promos->from, $promos->to)) 
            ->orderBy('purchase.updated_at', 'desc')
            ->groupBy('purchase.id')
            ->get();
            
        }else if($promos->minimum_qualification == '4'){
            $purchases = DB::table('purchase')
            ->leftJoin('client', 'client.id', '=', 'purchase.client_client_id')
            ->leftJoin('purchase_item_list', 'purchase.id', '=', 'purchase_item_list.purchase_po_no')
            ->leftJoin('item', 'item.id', '=', 'purchase_item_list.item_id')
            ->leftJoin('promos_qualifier','promos_qualifier.po_no','=','purchase.id')
            ->leftJoin('representative','representative.id','=','purchase.sponsor_id')
            ->leftJoin('representative as assoc','assoc.id','=','purchase.associate_id')
            ->join('multilevel', function ($join) {
                    $join->on('multilevel.agent_id', '=', 'representative.id')
                        ->where('multilevel.is_latest', '=', '1');
                })
            ->where(function ($query) {
                    $query->whereIn(DB::raw('UPPER(multilevel.level_to)'), ['SENIOR CONSULTANT', 'MANAGER', 'DISTRIBUTOR']);
                })
            ->select('client.last_name', 'client.first_name', 'purchase.id', 'purchase.purchase_date',
                'purchase.transaction_status', 'purchase.status',
                'item.description','promos_qualifier.po_no',
                DB::raw('UPPER(CONCAT(representative.last_name,", ",representative.first_name)) as sponsor'),
                DB::raw('IFNULL(UPPER(CONCAT(assoc.last_name,", ",assoc.first_name)),"N/A") as assoc'),
                DB::raw('SUM(purchase_item_list.item_price * qty) as total_price'))
            ->whereBetween('purchase.purchase_date', array($promos->from, $promos->to)) 
            ->orderBy('purchase.updated_at', 'desc')
            ->groupBy('purchase.id')
            ->get();
            
        }else if($promos->minimum_qualification == '5'){
            
            $purchases = DB::table('purchase')
            ->leftJoin('client', 'client.id', '=', 'purchase.client_client_id')
            ->leftJoin('purchase_item_list', 'purchase.id', '=', 'purchase_item_list.purchase_po_no')
            ->leftJoin('item', 'item.id', '=', 'purchase_item_list.item_id')
            ->leftJoin('promos_qualifier','promos_qualifier.po_no','=','purchase.id')
            ->join('representative','representative.id','=','purchase.sponsor_id')
            ->leftJoin('representative as assoc','assoc.id','=','purchase.associate_id')
            ->join('multilevel', function ($join) {
                    $join->on('multilevel.agent_id', '=', 'representative.id')
                        ->where('multilevel.is_latest', '=', '1');
                })
            ->whereIn('multilevel.level_to', ['MANAGER', 'DISTRIBUTOR'])
            ->where('multilevel.is_latest','=','1')
            ->select('client.last_name', 'client.first_name', 'purchase.id', 'purchase.purchase_date',
                'purchase.transaction_status', 'purchase.status',
                'item.description','promos_qualifier.po_no',
                DB::raw('UPPER(CONCAT(representative.last_name,", ",representative.first_name)) as sponsor'),
                DB::raw('IFNULL(UPPER(CONCAT(assoc.last_name,", ",assoc.first_name)),"N/A") as assoc'),
                DB::raw('SUM(purchase_item_list.item_price * qty) as total_price'))
            ->whereBetween('purchase.purchase_date', array($promos->from, $promos->to)) 
            ->orderBy('purchase.updated_at', 'desc')
            ->groupBy('purchase.id')
            ->get();
            
        }else if($promos->minimum_qualification == '6'){
            $purchases = DB::table('purchase')
            ->leftJoin('client', 'client.id', '=', 'purchase.client_client_id')
            ->leftJoin('purchase_item_list', 'purchase.id', '=', 'purchase_item_list.purchase_po_no')
            ->leftJoin('item', 'item.id', '=', 'purchase_item_list.item_id')
            ->leftJoin('promos_qualifier','promos_qualifier.po_no','=','purchase.id')
            ->leftJoin('representative','representative.id','=','purchase.sponsor_id')
            ->leftJoin('representative as assoc','assoc.id','=','purchase.associate_id')
            ->join('multilevel', function ($join) {
                    $join->on('multilevel.agent_id', '=', 'representative.id')
                        ->where('multilevel.is_latest', '=', '1');
                })
            ->where(function ($query) {
                    $query->whereIn(DB::raw('UPPER(multilevel.level_to)'), ['MANAGER']);
                })
            ->select('client.last_name', 'client.first_name', 'purchase.id', 'purchase.purchase_date',
                'purchase.transaction_status', 'purchase.status',
                'item.description','promos_qualifier.po_no',
                DB::raw('UPPER(CONCAT(representative.last_name,", ",representative.first_name)) as sponsor'),
                DB::raw('IFNULL(UPPER(CONCAT(assoc.last_name,", ",assoc.first_name)),"N/A") as assoc'),
                DB::raw('SUM(purchase_item_list.item_price * qty) as total_price'))
            ->whereBetween('purchase.purchase_date', array($promos->from, $promos->to)) 
            ->orderBy('purchase.updated_at', 'desc')
            ->groupBy('purchase.id')
            ->get();
            
        }else{
            return 'hello';
        }

        
        return $purchases;
    }

    public function registerPoToPromo()
    {

        if(Input::get('agentId')==null){
            DB::table('promos_qualifier')
            ->where('po_no', '=', Input::get('po'))
            ->where('promo_id', '=', Input::get('contestId'))
            ->delete();

        }else{
            DB::table('promos_qualifier')
            ->where('po_no', '=', Input::get('po'))
            ->where('promo_id', '=', Input::get('contestId'))
            ->delete();
        }
        
        $promo_qualifier = new PromoQualifier();
        $promo_qualifier->representative = Input::get('agentId');
        $promo_qualifier->po_no = Input::get('po');
        $promo_qualifier->promo_id = Input::get('contestId');
        $promo_qualifier->user_user_id = Auth::id();
        $promo_qualifier->save();
    }

    public function unregisterPoToPromo()
    {
        DB::table('promos_qualifier')
            ->where('representative', '=', Input::get('agentId'))
            ->where('po_no', '=', Input::get('po'))
            ->where('promo_id', '=', Input::get('contestId'))
            ->delete();

    }

    public function getContestants()
    {

        $purchases = DB::table('promos')
            ->leftJoin('promos_qualifier', 'promos_qualifier.promo_id', '=', 'promos.id')
            ->leftJoin('purchase', 'purchase.id', '=', 'promos_qualifier.po_no')
            ->leftJoin('representative', 'representative.id', '=', 'purchase.sponsor_id')
            ->leftJoin('purchase_item_list', 'purchase.id', '=', 'purchase_item_list.purchase_po_no')
            ->leftJoin('item', 'item.id', '=', 'purchase_item_list.item_id')
            ->select('representative.last_name', 'representative.first_name', 'purchase.id', 'purchase.purchase_date',
                'purchase.transaction_status', 'purchase.status', 'purchase.agent_current_level',
                'purchase_item_list.qty',
                'promos.mechanics_1_points',
                'promos.mechanics_2_points',
                'promos.mechanics_3_points',
                'promos.mechanics_1_prize',
                'promos.mechanics_2_prize',
                'promos.mechanics_3_prize',
                DB::raw('SUM(IF(STRCMP(LOWER(purchase.agent_current_level),"associate"),
                                item.production_points_associate,
                                item.production_points)
                                *qty) as point'),
                'item.description')
            ->where('promos.id', '=', Input::get('contestId'))
            ->groupBy('representative.id')
            ->orderBy('purchase.updated_at', 'desc')
            ->get();

        return $purchases;
    }

    public function getAgentsForPromo()
    {
        $term = Input::get('q');
        $contest_id = Input::get('contest_id');
        $contest = DB::table('promos')
            ->where('id', '=', $contest_id)
            ->get();
        $contest = $contest[0];
        $type = 0;

        if ($contest->minimum_qualification == 1) {
            $minimum_contest_reqt = '';

            $clients = DB::table('representative')
                ->join('multilevel', function ($join) {
                    $join->on('multilevel.agent_id', '=', 'representative.id')
                        ->where('multilevel.is_latest', '=', '1');
                })
                ->where(function ($query) {
                    $query->where(DB::raw('LOWER(representative.last_name)'), 'like', '%' . strtolower(Input::get('q')) . '%');
                    $query->orWhere(DB::raw('LOWER(representative.first_name)'), 'like', '%' . strtolower(Input::get('q')) . '%');
                })
                ->select(DB::raw('UPPER(CONCAT(representative.last_name,", ",representative.first_name)) as text, representative.id as id'))
                ->get();

        } else if ($contest->minimum_qualification == 2) {

            $clients = DB::table('representative')
                ->join('multilevel', function ($join) {
                    $join->on('multilevel.agent_id', '=', 'representative.id')
                        ->where('multilevel.is_latest', '=', '1');
                })
                ->where(function ($query) {
                    $query->where(DB::raw('LOWER(representative.last_name)'), 'like', '%' . strtolower(Input::get('q')) . '%');
                    $query->orWhere(DB::raw('LOWER(representative.first_name)'), 'like', '%' . strtolower(Input::get('q')) . '%');
                })
                ->where(function ($query) {
                    $query->whereIn('multilevel.level_to', ['Consultant', 'Senior Consultant', 'Manager', 'Distributor']);
                    $query->orWhere('multilevel.type', '=', 'Phase II');
                })
                ->select(DB::raw('UPPER(CONCAT(representative.last_name,", ",representative.first_name)) as text, representative.id as id'))
                ->get();

        } else if ($contest->minimum_qualification == 3) {
            $clients = DB::table('representative')
                ->join('multilevel', function ($join) {
                    $join->on('multilevel.agent_id', '=', 'representative.id')
                        ->where('multilevel.is_latest', '=', '1');
                })
                ->where(function ($query) {
                    $query->where(DB::raw('LOWER(representative.last_name)'), 'like', '%' . strtolower(Input::get('q')) . '%');
                    $query->orWhere(DB::raw('LOWER(representative.first_name)'), 'like', '%' . strtolower(Input::get('q')) . '%');
                })
                ->where(function ($query) {
                    $query->whereIn('multilevel.level_to', ['Consultant', 'Senior Consultant', 'Manager', 'Distributor']);
                })
                ->select(DB::raw('UPPER(CONCAT(representative.last_name,", ",representative.first_name)) as text, representative.id as id'))
                ->get();
        } else if ($contest->minimum_qualification == 4) {

            $clients = DB::table('representative')
                ->join('multilevel', function ($join) {
                    $join->on('multilevel.agent_id', '=', 'representative.id')
                        ->where('multilevel.is_latest', '=', '1');
                })
                ->where(function ($query) {
                    $query->where(DB::raw('LOWER(representative.last_name)'), 'like', '%' . strtolower(Input::get('q')) . '%');
                    $query->orWhere(DB::raw('LOWER(representative.first_name)'), 'like', '%' . strtolower(Input::get('q')) . '%');
                })
                ->where(function ($query) {
                    $query->whereIn('multilevel.level_to', ['Senior Consultant', 'Manager', 'Distributor']);
                })
                ->select(DB::raw('UPPER(CONCAT(representative.last_name,", ",representative.first_name)) as text, representative.id as id'))
                ->get();

        } else if ($contest->minimum_qualification == 5) {

            $clients = DB::table('representative')
                ->join('multilevel', function ($join) {
                    $join->on('multilevel.agent_id', '=', 'representative.id')
                        ->where('multilevel.is_latest', '=', '1');
                })
                ->where(function ($query) {
                    $query->where(DB::raw('LOWER(representative.last_name)'), 'like', '%' . strtolower(Input::get('q')) . '%');
                    $query->orWhere(DB::raw('LOWER(representative.first_name)'), 'like', '%' . strtolower(Input::get('q')) . '%');
                })
                ->where(function ($query) {
                    $query->whereIn('multilevel.level_to', ['Manager', 'Distributor']);
                })
                ->select(DB::raw('UPPER(CONCAT(representative.last_name,", ",representative.first_name)) as text, representative.id as id'))
                ->get();
        } else if ($contest->minimum_qualification == 6) {
            $clients = DB::table('representative')
                ->join('multilevel', function ($join) {
                    $join->on('multilevel.agent_id', '=', 'representative.id')
                        ->where('multilevel.is_latest', '=', '1');
                })
                ->where(function ($query) {
                    $query->where(DB::raw('LOWER(representative.last_name)'), 'like', '%' . strtolower(Input::get('q')) . '%');
                    $query->orWhere(DB::raw('LOWER(representative.first_name)'), 'like', '%' . strtolower(Input::get('q')) . '%');
                })
                ->where(function ($query) {
                    $query->whereIn('multilevel.level_to', ['Distributor']);
                })
                ->select(DB::raw('UPPER(CONCAT(representative.last_name,", ",representative.first_name)) as text, representative.id as id'))
                ->get();
        } else {
            $clients = DB::table('representative')
                ->join('multilevel', function ($join) {
                    $join->on('multilevel.agent_id', '=', 'representative.id')
                        ->where('multilevel.is_latest', '=', '1');
                })
                ->where(function ($query) {
                    $query->where(DB::raw('LOWER(representative.last_name)'), 'like', '%' . strtolower(Input::get('q')) . '%');
                    $query->orWhere(DB::raw('LOWER(representative.first_name)'), 'like', '%' . strtolower(Input::get('q')) . '%');
                })
                ->where(function ($query) {
                    $query->whereIn('multilevel.level_to', ['Distributor']);
                })
                ->select(DB::raw('UPPER(CONCAT(representative.last_name,", ",representative.first_name)) as text, representative.id as id'))
                ->get();
        }


        return $clients;
    }


    public function viewPurchase()
    {
        $id = Input::get('po');
        $purchase = Purchase::find($id);
        $purchase_order = DB::table('purchase')
            ->leftJoin('client', 'client.id', '=', 'purchase.client_client_id')
            ->leftJoin('purchase_item_list', 'purchase.id', '=', 'purchase_item_list.purchase_po_no')
            ->leftJoin('item', 'item.id', '=', 'purchase_item_list.item_id')
            ->leftJoin('promos_qualifier','promos_qualifier.po_no','=','purchase.id')
            ->leftJoin('representative','representative.id','=','purchase.sponsor_id')
            ->leftJoin('representative as assoc','assoc.id','=','purchase.associate_id')
            ->select('client.last_name', 'client.first_name', 'purchase.id', 'purchase.purchase_date',
                'purchase.transaction_status', 'purchase.status',
                'item.description','promos_qualifier.po_no','promos_qualifier.representative',
                DB::raw('UPPER(CONCAT(representative.last_name,", ",representative.first_name)) as sponsor'),
                DB::RAW('UPPER(assoc.current_level) as level_assoc'),
                DB::RAW('UPPER(representative.current_level) as level_sponsor'),
                'purchase.purchase_order_no',
                'representative.id as sponsor_id',
                'assoc.id as assoc_id',
                'representative.representative_code as sponsor_code',
                'assoc.representative_code as assoc_code',
                DB::raw('IFNULL(UPPER(CONCAT(assoc.last_name,", ",assoc.first_name)),"N/A") as assoc'),
                DB::raw('IFNULL(UPPER(CONCAT(client.last_name,", ",client.first_name)),"N/A") as client_name'),
                DB::raw('SUM(purchase_item_list.item_price * qty) as total_price'))
            ->where('purchase.id', '=', $id) 
            ->orderBy('purchase.updated_at', 'desc')
            ->groupBy('purchase.id')
            ->get();

        if (!$purchase_order) {
            return 'ERROR';
        }

        $purchase_item_list = DB::table('purchase_item_list')
                            ->join('item','item.id','=','purchase_item_list.item_id')
                            ->select('item.description','qty','gift',DB::raw('item_price*qty as total_price'),'production_points','production_points_associate')
                            ->where('purchase_item_list.purchase_po_no', '=', $id)
                            ->groupBy('purchase_item_list.item_id')                         
                            ->get();

        $total_price = DB::table('purchase_item_list')
            ->select(DB::raw('SUM(item_price*qty) as price'))
            ->where('purchase_po_no', '=', $purchase->id)
            ->get();

        return 
        [
            'purchase_order' => $purchase_order,
            'purchase_list' => $purchase_item_list
        ];

    }

}