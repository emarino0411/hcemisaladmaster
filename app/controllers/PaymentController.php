<?php

class PaymentController extends \BaseController
{
    public function showList()
    {
        if (!isAccessModuleAllowed('payments_view')) {
            return Redirect::to('/dashboard');
        }

        $payments = DB::table('payments as p1')
            ->join('purchase', 'purchase.id', '=', 'p1.po_no')
            ->join('client as c1', 'purchase.client_client_id', '=', 'c1.id')
            ->select(DB::raw('p1.type,purchase.purchase_order_no as po_no,p1.remittance_date,
                    p1.type,p1.check_number,p1.bank,p1.branch,
                    p1.amount,p1.status,p1.remarks,CONCAT(c1.last_name,", ",c1.first_name) as client'))
            ->get();

        return View::make('payments/payments_list', [
            'page_title' => 'HCEMIOS Payments',
            'page_description' => 'Will it be check or cash? :)',
            'level' => 'Payments',
            'sub_level' => 'List',
            'icon' => 'fa-money',
            'payments' => $payments
        ]);
    }

    public function addNew()
    {
        if (!isAccessModuleAllowed('payments_add')) {
            return Redirect::to('/dashboard');
        }
        return View::make('payments/payments_add', [
            'page_title' => 'HCEMIOS Payments',
            'page_description' => 'Now accepting cash and check!:)',
            'level' => 'Payments',
            'sub_level' => 'Add New Payment',
            'icon' => 'fa-money'
        ]);
    }

    public function viewPayments($id = '')
    {
        if (!isAccessModuleAllowed('payments_view')) {
            return Redirect::to('/dashboard');
        }

        if ($id == '') {
            $client   = null;
            $purchase = null;
        } else {
            $purchase = Purchase::where('id', $id)->first();
            $client = Client::where('id', $purchase->client_client_id)->first();
        }

        return View::make('payments/payments_view', [
            'page_title' => 'HCEMIOS Payments',
            'page_description' => 'Now accepting cash and check!:)',
            'level' => 'Payments',
            'sub_level' => 'Add New Payment',
            'icon' => 'fa-money',
            'client' => $client,
            'purchase' => $purchase
        ]);
    }

    public function getClientPO()
    {
        $po_no = DB::table('purchase')
            ->where('purchase.client_client_id', '=', Input::get('client_id'))
            ->select(DB::raw('IF(purchase.purchase_order_no="",CONCAT("[TEMPORARY-",purchase.id,"]"),purchase.purchase_order_no) as text'), 'purchase.id')
            ->get();
        return $po_no;
    }

    public function getClientPayment()
    {
        $payments = Payment::where('payments.po_no', '=', Input::get('po_id'))
            ->join('purchase', 'purchase.id', '=', 'payments.po_no')
            ->select('purchase.purchase_order_no', 'payments.remittance_date',
                'payments.type', 'payments.check_number', 'payments.check_date',
                'payments.deposit_date1', 'payments.deposit_date2', 'payments.bank',
                'payments.branch', 'payments.amount', 'payments.status', 'payments.remarks',
                'payments.id')
            ->get();
        return $payments;
    }

    public function addClientPayment()
    {
        if (!isAccessModuleAllowed('payments_add')) {
            return Redirect::to('/dashboard');
        }

        $payment_type = Input::get('payment_type');
        if ($payment_type == 'cash') {
            $payment = new Payment();
            $payment->po_no = Input::get('po_id');
            $payment->type = $payment_type;
            $payment->amount = Input::get('cash_amount');
            $payment->status = 'Good';
            $payment->remarks = Input::get('cash_remarks');
            $payment->remittance_date = Input::get('date_received');
            $payment->user_id = Auth::id();
            $payment->save();
        } else {
            $payment = new Payment();
            $payment->po_no = Input::get('po_id');
            $payment->type = $payment_type;
            $payment->remittance_date = Input::get('date_received');
            $payment->check_number = Input::get('check_no');
            $payment->bank = Input::get('bank');
            $payment->branch = Input::get('branch');
            $payment->check_date = Input::get('check_date');
            $payment->deposit_date1 = Input::get('check_date');
            $payment->status = 'For Deposit';
            $payment->amount = Input::get('check_amount');
            $payment->user_id = Auth::id();
            $payment->remarks = Input::get('remarks');
            $payment->save();
        }
        $payments = Payment::where('po_no', '=', Input::get('po_id'))->get();
        return $payments;
    }

    public function getTransactionDetails()
    {
        $id = Input::get('transaction_id');
        $transaction_detail = PurchaseItem::where('purchase_po_no', '=', $id)
            ->select(DB::raw('SUM(item_price*qty) as total_price'))
            ->get();
        return $transaction_detail;
    }

    public function setTransactionPaid()
    {
        $id = Input::get('transaction_id');
        $transaction = Purchase::find($id);
        $transaction->status = 'Paid';
        $transaction->save();
    }

    public function removePayments()
    {
        $data = Payment::whereIn('id', Input::get('remove_items'))
            ->where('user_id', '=', Auth::id())
            ->select(DB::raw('SUM(amount) as total, type'))
            ->groupBy('type')
            ->get();

        Payment::whereIn('id', Input::get('remove_items'))->where('user_id', '=', Auth::id())->delete();
        return $data;
    }

    public function updatePaymentStatus()
    {

        DB::table('payments')
            ->whereIn('id', Input::get('update_items'))
            ->update(array('status' => Input::get('status')));

        $payments = Payment::where('po_no', '=', Input::get('po_id'))->get();

        if (count($payments) == 0) {
            $po_list = DB::table('payments')
                ->whereIn('id', Input::get('update_items'))
                ->select('po_no')
                ->get();

            $po = array_pluck($po_list, 'po_no');

            if (Input::get('status') != "For Deposit" && Input::get('status') != "Good") {
                DB::table('purchase')
                    ->whereIn('id', $po)
                    ->update(array('status' => "With Payment Problem"));

            } else {
                DB::table('purchase')
                    ->whereIn('id', $po)
                    ->update(array('status' => "Good"));
            }
        } else {
            if (Input::get('status') != "For Deposit" && Input::get('status') != "Good") {
                DB::table('purchase')
                    ->where('id', '=', Input::get('po_id'))
                    ->update(array('status' => "With Payment Problem"));

            }
        }


        // return $po_list;
    }

    public function getPaymentDetail()
    {
        $payments = Payment::where('payments.id', '=', Input::get('id'))->get();

        return $payments;
    }

    public function updateDepositDate()
    {
        $payments = Payment::find(Input::get('id'));

        if ($payments->status == 'Daif') {
            DB::table('payments')
                ->where('id', '=', Input::get('id'))
                ->update(array('deposit_date1' => Input::get('new_deposit_date')));
        } else if ($payments->status == 'Daif2') {
            DB::table('payments')
                ->where('id', '=', Input::get('id'))
                ->update(array('deposit_date2' => Input::get('new_deposit_date')));
        } else if ($payments->status == 'On Hold') {
            DB::table('payments')
                ->where('id', '=', Input::get('id'))
                ->update(array('deposit_date1' => Input::get('new_deposit_date')));
        }
        return $payments;


    }

    public function updatePaymentRemarks()
    {
        $payments = Payment::find(Input::get('id'));

        if (Input::get('new_date') != "") {
            DB::table('payments')
                ->where('id', '=', Input::get('id'))
                ->update(array('remittance_date' => Input::get('new_date')));
        }

        if (Input::get('remarks') != "") {
            DB::table('payments')
                ->where('id', '=', Input::get('id'))
                ->update(array('remarks' => Input::get('remarks')));
        }


    }

    public function checkForDeposit()
    {

        if (!isAccessModuleAllowed('checks_for_deposit')) {
            return Redirect::to('dashboard');
        }

        return View::make('payments/payments_check_for_deposit', [
            'purchases' => [],
            'page_title' => 'HCEMIOS Payments',
            'page_description' => 'Checks For Deposit',
            'level' => 'Payments',
            'sub_level' => 'Checks For Deposit',
            'icon' => 'fa-bank'
        ]);
    }

    public function getCheckForDeposit()
    {
        $date_from = Input::get('_date_from');
        $date_to = Input::get('_date_to');

        return DB::table('payments')
            ->join('purchase', 'purchase.id', '=', 'payments.po_no')
            ->join('client', 'client.id', '=', 'purchase.client_client_id')
            ->where('payments.status', '=', 'For Deposit')
            ->where(function ($query) {
                $query->whereBetween('check_date', [Input::get('_date_from'), Input::get('_date_to')]);
            })
            ->orWhere(function ($query) {
                $query->whereBetween('deposit_date1', [Input::get('_date_from'), Input::get('_date_to')]);
            })
            ->orWhere(function ($query) {
                $query->whereBetween('deposit_date2', [Input::get('_date_from'), Input::get('_date_to')]);
            })
            ->orderBy('payments.check_date', 'payments.deposit_date1', 'payments.deposit_date2')
            ->select('payments.id', 'payments.remittance_date', 'client.first_name', 'client.last_name', 'payments.check_number', 'payments.check_date', 'payments.deposit_date1', 'payments.deposit_date2', 'payments.bank', 'payments.branch', 'payments.amount', 'payments.status', 'purchase.purchase_order_no', 'payments.remarks')
            ->get();
    }


}
