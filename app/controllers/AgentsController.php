<?php

class AgentsController extends \BaseController
{

    public function index()
    {
        if(!isAccessModuleAllowed('agents_view')){
            return Redirect::to('dashboard');
        }

        $agents = DB::table('representative')
            ->select('representative.id','representative.status','representative.last_name','representative.first_name','representative.representative_code',
                'representative.current_level')
            // ->leftJoin('multilevel','multilevel.agent_id','=','representative.id')
            //  ->where('multilevel.is_latest','=','1')
            ->get();


        $page_title = 'HCEMIOS Agents';
        $page_description = 'The hardworking agents of Saladmaster :)';
        $level = 'Agents';
        $sub_level = 'List';
        $icon = 'fa fa-users';

        return View::make('agents/agents_list', compact('agents', 'page_title', 'page_description', 'level', 'sub_level', 'icon', 'roles'));
    }

    public function addAgent()
    {
        if(!isAccessModuleAllowed('agents_add')){
            return Redirect::to('dashboard');
        }

        $page_title = 'HCEMIOS Agents';
        $page_description = 'Add an Agent';
        $level = 'Agents';
        $sub_level = 'Add New';
        $icon = 'fa fa-users';

        $municipalities = DB::table('rcitymun')
                ->join('rprov', 'rcitymun.provcode', '=', 'rprov.provcode')
                ->select(DB::raw('concat(cityname," [",provname,"]") as cityname'), 'rcitymun.citycode')
                ->orderBy('cityname', 'asc')
                ->orderBy('provname', 'asc')
                ->get();

        $provinces = DB::table('rprov')
                ->select('provcode', 'provname')
                ->orderBy('provname', 'asc')
                ->get();

        return View::make('agents/agents_add', compact('page_title', 'municipalities', 'provinces', 'page_description', 'level', 'sub_level', 'icon', 'municipalities', 'provinces'));
    }

    public function create()
    {
        if(!isAccessModuleAllowed('agents_add')){
            return Redirect::to('dashboard');
        }

        $existing_name = Agent::where('last_name','=',Input::get('txt_last_name'))
            ->where('first_name','=',Input::get('txt_first_name'))
            ->get();

        if(count($existing_name)>0)
        {
            return 'ERROR_EXISTING_AGENT';
        }

        $existing_username = Agent::where('representative_code', 'like', substr(Input::get('txt_first_name'), 0, 1) . Input::get('txt_last_name') . '%')
                ->get();

        $number_of_existing_users = count($existing_username);
        if (count($existing_username) > 0) {
            $number_of_existing_users++;
            $rep_code = substr(Input::get('txt_first_name'), 0, 1) . Input::get('txt_last_name') . $number_of_existing_users;
        } else {
            $rep_code = substr(Input::get('txt_first_name'), 0, 1) . Input::get('txt_last_name');
        }

        $agent = new Agent();
        $agent->last_name = Input::get('txt_last_name');
        $agent->first_name = Input::get('txt_first_name');
        $agent->representative_code = $rep_code;
        $agent->sponsor_id = Input::get('txt_agent_sponsor');
        $agent->sponsor_date = Input::get('txt_agent_sponsor_date');
        $agent->birthdate = Input::get('txt_agent_birthdate');
        $agent->civil_status = Input::get('slct_civil_status');
        $agent->street = Input::get('txt_street');
        $agent->brgy = Input::get('txt_brgy');
        $agent->city = Input::get('slct_town');
        $agent->province = Input::get('slct_province');
        $agent->gender = Input::get('rdo_gender');
        $agent->phone1 = Input::get('txt_agent_landline');
        $agent->phone2 = Input::get('txt_agent_mobile');
        $agent->tin_no = Input::get('txt_tin');
        $agent->sss_no = Input::get('txt_sss');
        $agent->philhealth_no = Input::get('txt_philhealth');
        $agent->pagibig_no = Input::get('txt_pagibig');
        $agent->can_drive = Input::get('rdo_can_drive');
        $agent->own_car = Input::get('rdo_own_car');
        $agent->previous_employer = Input::get('txt_employer_name');
        $agent->p_employer_telno = Input::get('txt_employer_contact');
        $agent->p_employer_addr = Input::get('txt_employer_address');
        $agent->p_employer_email = Input::get('txt_employer_email');
        $agent->spouse_last_name = Input::get('txt_spouse_last_name');
        $agent->spouse_first_name = Input::get('txt_spouse_first_name');
        $agent->no_of_dependents = Input::get('slct_dependents');
        $agent->email_address = Input::get('txt_agent_email');
        $agent->current_level = Input::get('agent_level');
        $agent->wsp_percentage = Input::get('txt_wsp_percenage');
        $agent->account_balance_percentage = Input::get('txt_account_balance');
        $agent->first_presentation = Input::get('first_presentation');
        $agent->first_sale = Input::get('first_sale');
        $agent->phase1_start = Input::get('phase_one_start');
        $agent->phase1_end = Input::get('phase_one_end');
        $agent->phase2_start = Input::get('phase_two_start');
        $agent->phase2_end = Input::get('phase_two_end');
        $agent->bom = Input::get('bom');

        $agent->save();

        DB::table('multilevel')
            ->where('agent_id','=',$agent->id)
            ->update(array('is_latest' => 0));


        $multilevel = new Multilevel();
        $multilevel->agent_id = $agent->id;
        $multilevel->effective_date = date('Y-m-d');
        $multilevel->type='INIT';
        $multilevel->level_to=$agent->current_level;
        $multilevel->level_from=$agent->current_level;
        $multilevel->is_latest=1;
        $multilevel->save();

    }

    public function viewProfile($id, $edit_mode = '')
    {
        if(!isAccessModuleAllowed('agents_view')){
            return Redirect::to('dashboard');
        }

        if ((!isAccessModuleAllowed('agents_edit')) && $edit_mode != "") {
            return Redirect::to('dashboard');
        }

        $agent = Agent::find($id);
        $sponsor = Agent::find($agent->sponsor_id);
        
        $agent->_city = DB::table('rcitymun')
                ->join('rprov', 'rcitymun.provcode', '=', 'rprov.provcode')
                ->select('cityname', 'provname', 'rcitymun.citycode')
                ->where('rcitymun.citycode','=',$agent->city)
                ->orderBy('cityname', 'asc')
                ->orderBy('provname', 'asc')
                ->get();

        $agent->_province = DB::table('rprov')
                ->select('provcode', 'provname')
                ->where('rprov.provcode','=',$agent->province)
                ->orderBy('provname', 'asc')
                ->get();

        $page_title = 'HCEMIOS Agents';
        $page_description = 'View Agent Details';
        $level = 'Agents';
        $sub_level = 'View Profile';
        $icon = 'fa fa-users';

        $municipalities = DB::table('rcitymun')
                ->join('rprov', 'rcitymun.provcode', '=', 'rprov.provcode')
                ->select(DB::raw('concat(cityname," [",provname,"]") as cityname'), 'provname', 'rcitymun.citycode')
                ->orderBy('cityname', 'asc')
                ->orderBy('provname', 'asc')
                ->get();

        $provinces = DB::table('rprov')
                ->select('provcode', 'provname')
                ->orderBy('provname', 'asc')
                ->get();

        Session::put('agent_id', $id);

        $purchases = DB::table('purchase')
                ->leftJoin('client', 'client.id', '=', 'purchase.client_client_id')
                ->leftJoin('purchase_item_list', 'purchase.id', '=', 'purchase_item_list.purchase_po_no')
                ->select('client.last_name', 'client.first_name', 'purchase.id', 'purchase.purchase_date',
                        'purchase.transaction_status', 'purchase.status', 'purchase.purchase_order_no',
                        'purchase.fast_track',
                        'purchase.sponsor_id',
                        'purchase.associate_id',
                        DB::raw('SUM(purchase_item_list.item_price * qty) as total_price'))
                ->where(function($query)
                {
                    $query->where('purchase.sponsor_id', '=', Session::get('agent_id'))
                          ->orWhere('purchase.associate_id', '=', Session::get('agent_id'));
                })
                ->orderBy('purchase.updated_at', 'desc')
                ->groupBy('purchase.id')
                ->get();

        $client_purchase = [];
        $ctr = 0;
        foreach($purchases as $purchase){
            if($purchase->associate_id == $id && $purchase->fast_track == 'Yes')
            {
                $client_purchase[$ctr] = $purchase;
                $ctr++;
            }
            if($purchase->sponsor_id == $id && $purchase->fast_track=='No')
            {
                $client_purchase[$ctr] = $purchase;   
                $ctr++;
            }
        }
        $purchases = $client_purchase;


        $wsp_commissions = DB::table('purchase')
                ->leftJoin('client', 'client.id', '=', 'purchase.client_client_id')
                ->leftJoin('commissions', 'commissions.po_no', '=', 'purchase.id')
                ->leftJoin('purchase_item_list', 'purchase.id', '=', 'purchase_item_list.purchase_po_no')
                ->select('client.last_name', 'client.first_name', 'commissions.id','commissions.voucher_no',
                        'commissions.wsp_percentage', 'purchase.purchase_date', 'purchase.transaction_status', 'commissions.status', 'commissions.due_date', 'commissions.account_balance_percentage',
                        DB::raw('(SUM(purchase_item_list.item_price * qty) * 0.83) * (commissions.percentage /100) * (commissions.account_balance_percentage / 100) as balance_deductions'),
                        DB::raw('(SUM(purchase_item_list.item_price * qty) * 0.83)  * (commissions.percentage / 100) * (commissions.wsp_percentage / 100) as wsp_commission'))
                ->where('commissions.representative_id', '=', $id)

                ->orderBy('purchase.updated_at', 'desc')
                ->groupBy('purchase.id')
                ->get();

        $total_balance = DB::table('purchase')
                ->leftJoin('commissions', 'commissions.po_no', '=', 'purchase.id')
                ->leftJoin('purchase_item_list', 'purchase.id', '=', 'purchase_item_list.purchase_po_no')
                ->leftJoin('cash_advance', 'cash_advance.agent_id', '=', 'commissions.representative_id')
                ->select(DB::raw('SUM(cash_advance.amount) as amount'),
                        DB::raw('SUM(cash_advance.amount) - SUM(purchase_item_list.item_price * qty/commissions.account_balance_percentage) as total_cash_advance'))
                ->where('cash_advance.agent_id', '=', $id)
                ->where('commissions.representative_id', '=', $id)
                ->get();
        $total_balance = $total_balance[0];
        // set view
        return View::make('agents/agents_profile', compact('provinces', 'municipalities', 'agent', 'page_title',
                'page_description', 'level', 'sub_level', 'icon', 'edit_mode', 'purchases', 'wsp_commissions', 'id','total_balance','sponsor'));
    }

    public function editAgent()
    {

    }

    public function update()
    {
        if(!isAccessModuleAllowed('agents_edit')){
            return Redirect::to('dashboard');
        }

        $agent = Agent::find(Input::get('txt_agent_id'));
        $old_level = $agent->current_level;

        $agent->last_name = Input::get('txt_last_name');
        $agent->first_name = Input::get('txt_first_name');
        $agent->representative_code = substr($agent->first_name, 0, 1) . $agent->last_name;
        $agent->sponsor_id = Input::get('txt_agent_sponsor');
        $agent->sponsor_date = Input::get('txt_agent_sponsor_date');
        $agent->birthdate = Input::get('txt_agent_birthdate');
        $agent->civil_status = Input::get('slct_civil_status');
        $agent->street = Input::get('txt_street');
        $agent->brgy = Input::get('txt_brgy');
        $agent->city = Input::get('slct_town');
        $agent->province = Input::get('slct_province');
        $agent->gender = Input::get('rdo_gender');
        $agent->phone1 = Input::get('txt_agent_landline');
        $agent->phone2 = Input::get('txt_agent_mobile');
        $agent->tin_no = Input::get('txt_tin');
        $agent->sss_no = Input::get('txt_sss');
        $agent->philhealth_no = Input::get('txt_philhealth');
        $agent->pagibig_no = Input::get('txt_pagibig');
        $agent->can_drive = Input::get('rdo_can_drive');
        $agent->own_car = Input::get('rdo_own_car');
        $agent->previous_employer = Input::get('txt_employer_name');
        $agent->p_employer_telno = Input::get('txt_employer_contact');
        $agent->p_employer_addr = Input::get('txt_employer_address');
        $agent->p_employer_email = Input::get('txt_employer_email');
        $agent->spouse_last_name = Input::get('txt_spouse_last_name');
        $agent->spouse_first_name = Input::get('txt_spouse_first_name');
        $agent->no_of_dependents = Input::get('slct_dependents');
        $agent->email_address = Input::get('txt_agent_email');
        $agent->current_level = Input::get('agent_level');
        $agent->wsp_percentage = Input::get('txt_wsp_percenage');
        $agent->account_balance_percentage = Input::get('txt_account_balance');
        $agent->first_presentation = Input::get('first_presentation');
        $agent->first_sale = Input::get('first_sale');
        $agent->bom = Input::get('bom');
        $agent->phase1_start = Input::get('phase_one_start');
        $agent->phase2_start = Input::get('phase_two_start');
        $agent->phase1_end = Input::get('phase_one_end');
        $agent->phase2_end = Input::get('phase_two_end');
        $agent->save();

        $is_existing = DB::table('multilevel')
                            ->where('agent_id','=',$agent->id)
                            ->get();
                            
        if(count($is_existing)==0){

            DB::table('multilevel')
                ->where('agent_id','=',$agent->id)
                ->update(array('is_latest' => 0));

            $multilevel = new Multilevel();
            $multilevel->agent_id = $agent->id;
            $multilevel->effective_date = date('Y-m-d');
            $multilevel->type='INIT';
            $multilevel->level_to=$agent->current_level;
            $multilevel->level_from=$agent->current_level;
            $multilevel->level_from=$agent->current_level;
            $multilevel->is_latest=1;

            $multilevel->save();
        }else{
            DB::table('multilevel')
                ->where('agent_id','=',$agent->id)
                ->update(array('is_latest' => 0));

            $multilevel = new Multilevel();
            $multilevel->agent_id = $agent->id;
            $multilevel->effective_date = date('Y-m-d');
            $multilevel->level_to=$agent->current_level;
            $multilevel->level_from=$old_level;
            $multilevel->is_latest=1;

            $multilevel->save();
        }
        
    }

    public function delete()
    {
        if(!isAccessModuleAllowed('agents_delete')){
            return Redirect::to('dashboard');
        }

        $agent = Agent::find(Input::get('userId'));
        $agent->status = 'DEACTIVATED';
        $agent->update();
        if ($agent->update()) {
            return 'USER_DELETED';
        } else {
            return 'Error';
        }
    }

    public function activate()
    {
        if(!isAccessModuleAllowed('agents_delete')){
            return Redirect::to('dashboard');
        }

        $agent = Agent::find(Input::get('userId'));
        $agent->status = 'ACTIVATED';
        $agent->update();
        if ($agent->update()) {
            return 'USER_ACTIVATED';
        } else {
            return 'Error';
        }
    }

    public function getWsp()
    {
        $wsp_commissions = DB::table('purchase')
                ->leftJoin('client', 'client.id', '=', 'purchase.client_client_id')
                ->leftJoin('commissions', 'commissions.po_no', '=', 'purchase.id')
                ->leftJoin('purchase_item_list', 'purchase.id', '=', 'purchase_item_list.purchase_po_no')
                ->select('commissions.wsp_percentage', 'commissions.status', 'commissions.due_date', 'commissions.wsp_percentage', 'commissions.id',
                        DB::raw('SUM(purchase_item_list.item_price * qty)/commissions.wsp_percentage as wsp_commission'))
                ->where('commissions.representative_id', '=', Input::get('id'))
                ->whereBetween('commissions.due_date', array(Input::get('from'), Input::get('to')))
                ->orderBy('purchase.updated_at', 'desc')
                ->groupBy('purchase.id')
                ->get();
        return $wsp_commissions;
    }

    public function getSales()
    {
        Session::put('agent_id', Input::get('id'));

        $purchases = DB::table('purchase')
                ->leftJoin('client', 'client.id', '=', 'purchase.client_client_id')
                ->leftJoin('purchase_item_list', 'purchase.id', '=', 'purchase_item_list.purchase_po_no')
                ->select('client.last_name', 'client.first_name', 'purchase.id', 'purchase.purchase_date',
                        'purchase.transaction_status', 'purchase.status', 'purchase.purchase_order_no',
                        'purchase.fast_track',
                        'purchase.sponsor_id',
                        'purchase.associate_id',
                        DB::raw('SUM(purchase_item_list.item_price * qty) as total_price'))
                ->where(function($query)
                {
                    $query->where('purchase.sponsor_id', '=', Session::get('agent_id'))
                          ->orWhere('purchase.associate_id', '=', Session::get('agent_id'));
                })
                ->whereBetween('purchase.purchase_date', array(Input::get('from'), Input::get('to')))
                ->orderBy('purchase.updated_at', 'desc')
                ->groupBy('purchase.id')
                ->get();

        $client_purchase = [];
        $ctr = 0;
        foreach($purchases as $purchase){
            if(($purchase->associate_id == $id && $purchase->fast_track == 'Yes') || ($purchase->sponsor_id == $id))
            {
                $client_purchase[$ctr] = $purchase;
            }
            $ctr++;
        }
        $purchases = $client_purchase;

        // $purchases = DB::table('purchase')
        //         ->leftJoin('client', 'client.id', '=', 'purchase.client_client_id')
        //         ->leftJoin('purchase_item_list', 'purchase.id', '=', 'purchase_item_list.purchase_po_no')
        //         ->select('client.last_name', 'client.first_name', 'purchase.id', 'purchase.purchase_date',
        //                 'purchase.transaction_status', 'purchase.status',
        //                 DB::raw('SUM(purchase_item_list.item_price * qty) as total_price'))
        //         ->where('purchase.sponsor_id', '=', Input::get('id'))
                
        //         ->orderBy('purchase.updated_at', 'desc')
        //         ->get();

        return $purchases;
    }

    public function getAccountBalancePayments(){
        $wsp_commissions = DB::table('purchase')
                ->leftJoin('client', 'client.id', '=', 'purchase.client_client_id')
                ->leftJoin('commissions', 'commissions.po_no', '=', 'purchase.id')
                ->leftJoin('purchase_item_list', 'purchase.id', '=', 'purchase_item_list.purchase_po_no')
                ->select('client.last_name', 'client.first_name', 'commissions.id',
                        'commissions.wsp_percentage', 'purchase.purchase_date', 'purchase.transaction_status', 'commissions.status', 'commissions.due_date', 'commissions.account_balance_percentage',
                        DB::raw('SUM(purchase_item_list.item_price * qty)/commissions.account_balance_percentage as balance_deductions'),
                        DB::raw('SUM(purchase_item_list.item_price * qty)/commissions.wsp_percentage as wsp_commission'))
                ->where('commissions.representative_id', '=', Input::get('id'))
                ->whereBetween('commissions.due_date', array(Input::get('from'), Input::get('to')))
                ->orderBy('purchase.updated_at', 'desc')
                ->groupBy('purchase.id')
                ->get();

        return $wsp_commissions;
    }

    public function uploadImage(){
        $user_id = Input::get('txt_agent_id');

        if (Input::hasFile('avatar'))
        {
            $file = Input::file('avatar');
            @unlink('public/uploads/'.$user_id.'_avatar.'.$file->getClientOriginalExtension());
            $file->move('public/uploads', $user_id.'_avatar.'.$file->getClientOriginalExtension());

            //$image = Image::make(sprintf('public/uploads/%s', $user_id.'_avatar.'.$file->getClientOriginalExtension()))->resize(120, 120)->save();

            $user = Agent::find($user_id);
            $user->profile_picture = '/uploads/'.$user_id.'_avatar.'.$file->getClientOriginalExtension();
            $user->save();

            return Redirect::action('AgentsController@viewProfile', [$user_id]);

        }else{

        }
    }

}
