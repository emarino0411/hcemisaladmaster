<?php

class SalesOverrideController extends \BaseController {

	public function index(){

        return View::make('sales_override/sales_override_index', [
            'purchases' => [],
            'page_title' => 'HCEMIOS Sales Override',
            'page_description' => 'Manager\'s and Distributor\'s Override',
            'level' => 'Override',
            'sub_level' => 'Get Sales Override',
            'icon' => 'fa-money'
        ]);
	}
}