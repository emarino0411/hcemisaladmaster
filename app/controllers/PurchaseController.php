<?php


class PurchaseController extends \BaseController
{

    public function index()
    {
        if (!isAccessModuleAllowed('purchase_view')) {
            return Redirect::to('dashboard');
        }

        $purchases = DB::table('purchase')
            ->leftJoin('client', 'client.id', '=', 'purchase.client_client_id')
            ->join('representative', 'representative.id', '=', 'purchase.sponsor_id')
            ->select(DB::raw('CONCAT(representative.last_name,", ",representative.first_name) as sponsor'),
                'purchase.purchase_order_no', 'client.last_name', 'client.first_name', 'purchase.id', 'purchase.purchase_date', 'purchase.transaction_status', 'purchase.status', 'purchase.remarks')
            //     ->where('purchase.status','<>','Incomplete')
            ->orderBy('purchase.updated_at', 'desc')
            ->get();

        return View::make('purchases/purchase_list', [
            'purchases' => $purchases,
            'page_title' => 'HCEMIOS Purchase',
            'page_description' => 'Are we having a new Purchase Order Today? :)',
            'level' => 'Purchase',
            'sub_level' => 'List',
            'icon' => 'fa-shopping-cart'
        ]);
    }

    public function addNew()
    {

        if (!isAccessModuleAllowed('purchase_add')) {
            return Redirect::to('dashboard');
        }

        // PurchaseItem::where('status', '=', '2')->where('user_user_id', '=', Auth::id())->update(array('status' => 1));

        // PurchaseItem::where('status', '=', '0')->where('user_user_id', '=', Auth::id())->delete();

        // Purchase::where('status', '=', 'Incomplete')->where('user_user_id', '=', Auth::id())->delete();

        $agents = Agent::all();
        $products = Product::where('type', '=', 'Single');
        $purchase_id = "";

        return View::make('purchases/purchase_add', [
            'page_title' => 'HCEMIOS Purchase',
            'page_description' => 'Yes we are having a new Purchase Order Today!:)',
            'level' => 'Purchase',
            'sub_level' => 'New Purchase Order',
            'icon' => 'fa-shopping-cart',
            'agents' => $agents,
            'products' => $products,
            'purchase_id' => $purchase_id
        ]);

    }

    public function createPOItem()
    {
        if (!isAccessModuleAllowed('purchase_add')) {
            return Redirect::to('dashboard');
        }

        $update = false;
        if (Input::get('hdn_po_no') != "") {
            $purchase_order = Purchase::find(Input::get('hdn_po_no'));
        } else {
            $purchase_order = null;
        }


        if (!$purchase_order || $purchase_order == null) {
            $purchase_order = new Purchase();
            //$purchase_order->id = Input::get('hdn_po_no');
            $purchase_order->client_client_id = Input::get('slct_client')[0];
            $purchase_order->po_no = Input::get('txt_po_no');
            $purchase_order->purchase_date = Input::get('txt_purchase_date');
            $purchase_order->terms = Input::get('slct_terms');
            $purchase_order->delivery_date = Input::get('txt_delivery_date');
            $purchase_order->total_amount = 0;
            $purchase_order->associate_id = Input::get('slct_associate');
            $purchase_order->sponsor_id = Input::get('slct_sponsor');
            $purchase_order->consultant_id = Input::get('slct_consultant');
            $purchase_order->fast_track = Input::get('is_fast_track');
            $purchase_order->status = Input::get('slct_trans_status');
            $purchase_order->remarks = Input::get('txtarea_remarks');
            $purchase_order->user_user_id = Auth::id();
            $purchase_order->transaction_status = 'Pending';

            if (Input::get('slct_sponsor') != "") {
                $sponsor = Agent::find(Input::get('slct_sponsor'));
                $date = Input::get('txt_purchase_date');

                $current_level = DB::table('multilevel')
                    ->where('agent_id', '=', $sponsor->id)
                    ->where('effective_date', '<=', $date)
                    ->orderBy('effective_date', 'DESC')
                    ->get();

                if (count($current_level) == 0) {
                    $current_level = DB::table('multilevel')
                        ->where('agent_id', '=', $sponsor->id)
                        ->orderBy('effective_date', 'DESC')
                        ->get();

                }

                $sponsor = Agent::find($sponsor->id);

                if ($current_level->type == 'INIT') {

                }


                $purchase_order->agent_current_level = strtolower($current_level[0]->level_to);
            }
            $purchase_order_no = Input::get('purchase_order_no');

            // check if existing purchase order no
            $is_existing = DB::table('purchase')
                ->where('purchase_order_no', '=', $purchase_order_no)
                ->get();

            if ($is_existing != null && $purchase_order_no != "") {
                return 'PURCHASE_ORDER_NO_EXISTING';
            }

            $purchase_order->purchase_order_no = $purchase_order_no;

            $purchase_order->save();
        }

        $purchase_item_list = PurchaseItem::where('item_id', '=', Input::get('slct_purchase_item'))
            ->where('purchase_po_no', '=', $purchase_order->id)
            ->where('gift', '=', Input::get('is_gift'))
            ->first();

        if (!$purchase_item_list) {
            $purchase_item_list = new PurchaseItem();
            $purchase_item_list->gift = Input::get('is_gift');
            $item = Product::find(Input::get('slct_purchase_item'));

            if (strtolower($purchase_order->agent_current_level) == 'associate') {
                $points = $item->production_points_associate;
            } else {
                $points = $item->production_points;
            }

            if (Input::get('is_gift') == 'Yes') {
                $purchase_item_list->item_price = 0;
                $purchase_item_list->points = 0;
            } else {
                $purchase_item_list->item_price = $item->retail_price;
                $purchase_item_list->points = $points;
            }

            $purchase_item_list->item_id = Input::get('slct_purchase_item');
            $purchase_item_list->purchase_type = Input::get('slct_purchase_type');
            $purchase_item_list->purchase_po_no = $purchase_order->id;
            $purchase_item_list->qty = Input::get('txt_qty');
            $purchase_item_list->user_user_id = Auth::id();
            $purchase_item_list->save();

        } else {

            $purchase_item_list->qty = $purchase_item_list->qty + Input::get('txt_qty');
            $purchase_item_list->item_price = $purchase_item_list->item_price;
            $purchase_item_list->save();
            $update = true;
        }
        return [$purchase_item_list, $update, $purchase_order];

    }

    public function savePurchaseOrder()
    {

        if (!isAccessModuleAllowed('purchase_add')) {
            return Redirect::to('dashboard');
        }

        $purchase_order = Purchase::find(Input::get('hdn_po_no'));

        $purchase_order->po_no = Input::get('txt_po_no');


        if (is_array(Input::get('slct_client'))) {
            $purchase_order->client_client_id = Input::get('slct_client')[0];
        } else {
            $purchase_order->client_client_id = Input::get('slct_client');
        }


        $purchase_order->purchase_date = Input::get('txt_purchase_date');
        $purchase_order->terms = Input::get('slct_terms');
        $purchase_order->delivery_date = Input::get('txt_delivery_date');
        $purchase_order->total_amount = 0;

        if (Input::get('slct_associate') != null) {
            $purchase_order->associate_id = Input::get('slct_associate');
        }
        if (Input::get('slct_sponsor') != null) {
            $purchase_order->sponsor_id = Input::get('slct_sponsor');
        }
        if (Input::get('slct_consultant') != null) {
            $purchase_order->consultant_id = Input::get('slct_consultant');
        }

        if (Input::get('slct_sponsor') != "" && Input::get('slct_sponsor') != null) {
            $sponsor = Agent::find(Input::get('slct_sponsor'));

            $date = Input::get('txt_purchase_date');
            $current_level = DB::table('multilevel')
                ->where('agent_id', '=', $sponsor->id)
                ->where('effective_date', '<=', $date)
                ->orderBy('effective_date', 'DESC')
                ->get();
            if (count($current_level) == 0) {
                $current_level = DB::table('multilevel')
                    ->where('agent_id', '=', $sponsor->id)
                    ->orderBy('effective_date', 'DESC')
                    ->get();
            }

            $purchase_order->agent_current_level = strtolower($current_level[0]->level_to);
        }


        $purchase_order->fast_track = Input::get('is_fast_track');
        if ($purchase_order->status != "" && $purchase_order->status != null) {
            // do nothing
        } else {
            $purchase_order->status = 'Completed';
        }
        $purchase_order->status = Input::get('slct_trans_status');

        $purchase_order->remarks = Input::get('txtarea_remarks');
        $purchase_order->user_user_id = Auth::id();
        $purchase_order->transaction_status = 'Approved';

        $purchase_order_no = Input::get('purchase_order_no');

        // check if existing purchase order no
        $is_existing = DB::table('purchase')
            ->where('purchase_order_no', '=', $purchase_order_no)
            ->where('purchase.id', '<>', $purchase_order->id)
            ->get();

        if ($is_existing != null && $purchase_order_no != "") {
            return 'PURCHASE_ORDER_NO_EXISTING';
        }

        $purchase_order->purchase_order_no = $purchase_order_no;


        $purchase_order->save();

        DB::table('purchase_item_list')
            ->where('purchase_po_no', '=', Input::get('hdn_po_no'))
            ->update(array('status' => 1));
    }

    public function searchClient()
    {
        $term = Input::get('q');

        $clients = Client::where(DB::raw('LOWER(last_name)'), 'like', '%' . strtolower($term) . '%')
            ->orWhere(DB::raw('LOWER(first_name)'), 'like', '%' . strtolower($term) . '%')
            ->select(DB::raw('UPPER(CONCAT(last_name,", ",first_name)) as text, id as id'))
            ->get();
        return $clients;
    }

    public function searchPO()
    {

        $po = DB::table('bookings')
            ->select('po_no')
            ->whereNotNull('po_no')
            ->get();

        $po_list = array_pluck($po, 'po_no');

        $term = Input::get('q');

        $po = Purchase::where('purchase_order_no', 'like', '%' . strtolower($term) . '%')
            ->select('purchase_order_no as text', 'purchase_order_no as id')
            ->whereNotIn('purchase_order_no', $po_list)
            ->get();

        return $po;
    }

    public function viewPurchase($id)
    {
        $agents = Agent::all();
        $products = Product::where('type', '=', 'Single');
        $purchase = Purchase::all()->last();

        $purchase_order = Purchase::find($id);
        if (!$purchase_order) {
            return 'ERROR';
        }

        $purchase_item_list = PurchaseItem::where('purchase_po_no', '=', $purchase_order->id)->where('status', '=', '1')
            ->get();

        $total_price = DB::table('purchase_item_list')
            ->select(DB::raw('SUM(item_price*qty) as price'))
            ->where('purchase_po_no', '=', $purchase_order->id)
            ->get();

        $client = Client::find($purchase_order->client_client_id);
        $client_name = strtoupper($client->last_name) . ',' . strtoupper($client->first_name);
        if ($purchase_order->consultant_id != 0) {
            $consultant = Agent::find($purchase_order->consultant_id);
            $consultant_name = strtoupper($consultant->last_name) . ',' . strtoupper($consultant->first_name);
        } else {
            $consultant_name = 'N/A';
        }
        if ($purchase_order->associate_id != 0) {
            $associate = Agent::find($purchase_order->associate_id);
            $associate_name = strtoupper($associate->last_name) . ',' . strtoupper($associate->first_name);
        } else {
            $associate_name = 'N/A';
        }
        if ($purchase_order->sponsor_id != 0) {
            $sponsor = Agent::find($purchase_order->sponsor_id);
            $sponsor_name = strtoupper($sponsor->last_name) . ',' . strtoupper($sponsor->first_name);
        } else {
            $sponsor_name = 'N/A';
        }


        return View::make('purchases/purchase_view', [
            'page_title' => 'HCEMIOS Purchase',
            'page_description' => 'View Purchase Order Detail',
            'level' => 'Purchase',
            'sub_level' => 'View Purchase Order',
            'icon' => 'fa-shopping-cart',
            'agents' => $agents,
            'products' => $products,
            'purchase_order' => $purchase_order,
            'purchase_id' => $purchase_order->id,
            'purchase_list' => $purchase_item_list,
            'client' => $client_name,
            'total_price' => $total_price[0]->price,
            'consultant_name' => $consultant_name,
            'associate_name' => $associate_name,
            'sponsor_name' => $sponsor_name
        ]);

    }

    public function editPurchase($id)
    {

        if (!isAccessModuleAllowed('purchase_edit')) {
            return Redirect::to('dashboard');
        }

        $agents = Agent::all();
        $products = Product::where('type', '=', 'Single');
        $purchase = Purchase::all()->last();

        $purchase_order = Purchase::find($id);
        if (!$purchase_order) {
            return 'ERROR';
        }

        PurchaseItem::where('purchase_po_no', '=', $purchase_order->id)->where('status', '=', '2')->update(array('status' => 1));

        $purchase_item_list = PurchaseItem::where('purchase_po_no', '=', $purchase_order->id)
            ->get();

        $total_price = DB::table('purchase_item_list')
            ->select(DB::raw('SUM(item_price*qty) as price'))
            ->where('purchase_po_no', '=', $purchase_order->id)
            ->get();

        $client = Client::find($purchase_order->client_client_id);
        $client_name = strtoupper($client->last_name) . ',' . strtoupper($client->first_name);

        if ($purchase_order->consultant_id != 0) {
            $consultant = Agent::find($purchase_order->consultant_id);
            $consultant_name = strtoupper($consultant->last_name) . ',' . strtoupper($consultant->first_name);
        } else {
            $consultant_name = '';
        }

        if ($purchase_order->associate_id != 0) {
            $associate = Agent::find($purchase_order->associate_id);
            $associate_name = strtoupper($associate->last_name) . ',' . strtoupper($associate->first_name);
        } else {
            $associate_name = '';
        }

        if ($purchase_order->sponsor_id != 0) {
            $sponsor = Agent::find($purchase_order->sponsor_id);
            $sponsor_name = strtoupper($sponsor->last_name) . ',' . strtoupper($sponsor->first_name);
        } else {
            $sponsor_name = '';
        }

        $commission = Commission::where('po_no', $id)->get();

        return View::make('purchases/purchase_edit', [
            'page_title' => 'HCEMIOS Purchase',
            'page_description' => 'Edit Purchase Order Detail',
            'level' => 'Purchase',
            'sub_level' => 'Edit Purchase Order',
            'icon' => 'fa-shopping-cart',
            'agents' => $agents,
            'products' => $products,
            'purchase_order' => $purchase_order,
            'purchase_id' => $purchase_order->id,
            'purchase_list' => $purchase_item_list,
            'client' => $client_name,
            'total_price' => $total_price[0]->price,
            'consultant_name' => $consultant_name,
            'associate_name' => $associate_name,
            'sponsor_name' => $sponsor_name,
            'commission' => $commission
        ]);
    }

    public function updatePurchase()
    {
        if (!isAccessModuleAllowed('purchase_edit')) {
            return Redirect::to('dashboard');
        }

        $purchase_order = Purchase::find(Input::get('hdn_po_no'));
        $purchase_order->client_client_id = Input::get('slct_client');
        $purchase_order->purchase_date = Input::get('txt_purchase_date');
        $purchase_order->terms = Input::get('slct_terms');
        $purchase_order->delivery_date = Input::get('txt_delivery_date');
        $purchase_order->total_amount = 0;
        $purchase_order->associate_id = Input::get('slct_associate');
        $purchase_order->sponsor_id = Input::get('slct_sponsor');
        $purchase_order->consultant_id = Input::get('slct_consultant');
        $purchase_order->fast_track = Input::get('is_fast_track');
        $purchase_order->status = Input::get('slct_trans_status');
        $purchase_order->remarks = Input::get('txtarea_remarks');
        $purchase_order->user_user_id = Auth::id();
        $purchase_order->transaction_status = 'Approved';
        $purchase_order->status = 'Completed';

        $purchase_order_no = Input::get('purchase_order_no');

        // check if existing purchase order no
        $is_existing = DB::table('purchase')
            ->where('purchase_order_no', '=', $purchase_order_no)
            ->where('purchase.id', '<>', $purchase_order->id)
            ->get();

        if ($is_existing != null && $purchase_order_no != "") {
            return 'PURCHASE_ORDER_NO_EXISTING';
        }

        $purchase_order->purchase_order_no = $purchase_order_no;

        $purchase_order->save();
    }

    public function cancelPurchase()
    {
        $purchase_order = Purchase::find(Input::get('hdn_po_no'));
        $purchase_order->status = 'Cancelled';
        $purchase_order->transaction_status = 'Cancelled';
        $purchase_order->save();
    }

    public function reactivatePurchase()
    {
        $purchase_order = Purchase::find(Input::get('hdn_po_no'));
        $purchase_order->status = 'Incomplete';
        $purchase_order->transaction_status = 'Approved';
        $purchase_order->save();
    }

    public function discardChanges()
    {
        $removed_item = PurchaseItem::where('status', '=', '2');
        $removed_item->status = 1;
        $removed_item->save();

        PurchaseItem::where('status', '=', '0')
            ->delete();
    }

    public function removeItems()
    {
        PurchaseItem::whereIn('id', Input::get('remove_items'))
            //            ->where('user_user_id', '=', Auth::id())
            ->delete();

        // PurchaseItem::whereIn('id', Input::get('remove_items'))
        //             ->where('user_user_id', '=', Auth::id())
        //             ->where('status', '=', '1')
        //             ->update(['status' => '2']);
    }

    public function getProductDetail()
    {

        return Product::find(Input::get('id'))->get();

    }

    public function updatePurchaseStatus()
    {

        DB::table('purchase')
            ->whereIn('id', Input::get('update_items'))
            ->update(array('status' => Input::get('status')));

        if (Input::get('status') == 'For Pull Out') {
            return DB::table('purchase')
                ->join('commissions', 'commissions.po_no', '=', 'purchase.id')
                ->join('representative', 'representative.id', '=', 'commissions.representative_id')
                ->select('representative.last_name', 'representative.first_name', 'commissions.po_no', 'commissions.id as commission_id', 'representative.id as agent_id', 'commissions.received_commission', 'commissions.due_date')
                ->whereIn('purchase.id', Input::get('update_items'))
                ->where('commissions.status', '=', 'RELEASED')
                ->get();

        } else {
            return [];
        }

    }

    public function addPullOutCommissionToCashAdvance()
    {

        $agents = Input::get('_agents');
        $po_no = Input::get('_po_no');
        $commission_id = Input::get('_commission_id');
        $amount = Input::get('_amount');
        $user_action = Input::get('_user_action');

        for ($ctr = 0; $ctr < count($agents); $ctr++) {

            AgentCashAdvance::where('agent_id', '=', $agents[$ctr])
                ->where('commission_id', '=', $commission_id[$ctr])
                ->delete();

            $cash_advance = new AgentCashAdvance();
            $cash_advance->type = $user_action[$ctr];
            $cash_advance->amount = $amount[$ctr];
            $cash_advance->agent_id = $agents[$ctr];
            $cash_advance->commission_id = $commission_id[$ctr];
            $cash_advance->date = date('Y-m-d');
            $cash_advance->save();

        }


    }

    public function searchPurchases()
    {
        $purchases = DB::table('purchase')
            ->leftJoin('client', 'client.id', '=', 'purchase.client_client_id')
            ->join('representative', 'representative.id', '=', 'purchase.sponsor_id')
            ->select(DB::raw('CONCAT(representative.last_name,", ",representative.first_name) as sponsor'),
                'purchase.purchase_order_no', 'client.last_name', 'client.first_name', 'purchase.id', 'purchase.purchase_date', 'purchase.transaction_status', 'purchase.status', 'purchase.remarks')
            ->orderBy('purchase.updated_at', 'desc');

        if (Input::get('startDate') != '' && Input::get('endDate') != '') {
            $purchases->whereBetween('purchase.purchase_date', [Input::get('startDate'), Input::get('endDate')]);
        }
        if (Input::get('client') != '') {
            $purchases->where('client.id', '=', Input::get('client'));
        }
        if (Input::get('po_no') != '') {
            $purchases->where('purchase.purchase_order_no', 'like', Input::get('po_no').'%');
        }

        return $purchases->get();
    }

}
