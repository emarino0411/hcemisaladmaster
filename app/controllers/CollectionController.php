<?php

class CollectionController extends \BaseController
{

    public function index()
    {
        if (!isAccessModuleAllowed('collections_view')) {
            return Redirect::to('dashboard');
        }

        $purchases = DB::table('purchase')
            ->leftJoin('client', 'client.id', '=', 'purchase.client_client_id')
            ->join('representative', 'representative.id', '=', 'purchase.sponsor_id')
            ->select(DB::raw('CONCAT(representative.last_name,", ",representative.first_name) as sponsor'),
                'purchase.purchase_order_no', 'client.last_name', 'client.first_name', 'purchase.id', 'purchase.purchase_date', 'purchase.transaction_status', 'purchase.status', 'purchase.remarks')
            //     ->where('purchase.status','<>','Incomplete')
            ->orderBy('purchase.updated_at', 'desc')
            ->get();

        // if (!isset($_GET['approved'])) {
        //     PurchaseItem::where('status', '=', '2')->where('user_user_id', '=', Auth::id())->update(array('status' => 1));

        //     PurchaseItem::where('status', '=', '0')->where('user_user_id', '=', Auth::id())->delete();
        // }


        return View::make('collections/collections_list', [
            'purchases' => $purchases,
            'page_title' => 'HCEMIOS Collections',
            'page_description' => 'We add notes and comments about collections done by our sales team.',
            'level' => 'Collections',
            'sub_level' => 'List',
            'icon' => 'fa-calculator'
        ]);
    }

    public function add_collection_update($po)
    {
        if (!isAccessModuleAllowed('collections_add')) {
            return Redirect::to('/dashboard');
        }

        $purchase = Purchase::where('id',$po)->first();

        $client = Client::where('id',$purchase->client_client_id)->first();


       $collectiondetails = DB::table('purchase')
            ->join('client', 'client.id', '=', 'purchase.client_client_id')
            ->leftjoin('rcitymun', 'client.city', '=', 'rcitymun.citycode')
            ->leftjoin('rprov', 'client.province', '=', 'rprov.provcode')
            ->select('purchase.purchase_order_no',
                'purchase.id',
                 'client.last_name',
                 'client.first_name',
                 'client.phone1',
                 'client.phone2',
                 'client.email_address',
                DB::raw('CONCAT(client.street,client.brgy) as client_street'),
                DB::raw('IFNULL(rcitymun.cityname,"N/A") as cityname'),
                DB::raw('IFNULL(rprov.provname,"N/A") as provname'))
            ->where('purchase.id', '=', $po)
            ->get();


        $purchaselistdetails = DB::table('purchase')
            ->join('purchase_item_list', 'purchase_item_list.purchase_po_no', '=', 'purchase.id')
            ->join('item', 'item.id', '=', 'purchase_item_list.item_id')
            ->select('item.description', 'purchase.remarks','purchase_date','purchase.terms')
            ->where('purchase.id', '=', $po)
            ->get();

        $sponsordetails= DB::table('purchase')
            ->join('representative', 'representative.id', '=', 'purchase.sponsor_id')
            ->select('representative.last_name', 'representative.first_name')
            ->where('purchase.id', '=', $po)
            ->get();

        $presenterdetails= DB::table('purchase')
            ->join('representative', 'representative.id', '=', 'purchase.consultant_id')
            ->select('representative.last_name', 'representative.first_name')
            ->where('purchase.id', '=', $po)
            ->get();

        return View::make('collections/collections_add', [
            'page_title' => 'HCEMIOS Collections',
            'page_description' => 'Add collection updates ',
            'level' => 'Collections',
            'sub_level' => 'Add New Collection Update',
            'icon' => 'fa-calculator',
            'client' =>$client,
            'collectiondetails'=>$collectiondetails,
            'purchaselistdetails'=>$purchaselistdetails,
            'sponsordetails'=>$sponsordetails,
            'presenterdetails'=>$presenterdetails,
            'purchase'=>$purchase

        ]);
    }

    public function save_collection_update()
    {
        $collection = new Collection();
        $collection->representative_id = Auth::user()->id;
        $collection->po_no = Input::get('po_id');
        $collection->update = nl2br(Input::get('update'));
        $collection->update_date = date('Y-m-d');
        $collection->save();
    }

    public function get_collections_lists()
    {
        return $collections = DB::table('collections')
            ->join('users', 'users.id', '=', 'collections.representative_id')
            ->where('po_no', '=', Input::get('po_id'))
            ->get();
    }

public function get_collection_details()
    {

    }

}