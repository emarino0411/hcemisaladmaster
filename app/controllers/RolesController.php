<?php

class RolesController extends \BaseController
{

    public function index()
    {
        if(!isAccessModuleAllowed('roles_view')){
            return Redirect::to('dashboard');
        }

        $page_title = 'HCEMIOS User Roles';
        $page_description = 'Access Granted!';
        $level = 'User Roles';
        $sub_level = 'List';
        $icon = 'fa fa-key';

        $roles = Role::all()->toArray();
        return View::make('roles/roles_list', compact('page_title', 'page_description', 'level', 'sub_level', 'icon', 'roles'));
    }

    public function create()
    {
        if(!isAccessModuleAllowed('roles_add')){
            return Redirect::to('dashboard');
        }

        $is_role_existing = Role::where('name', '=', Input::get('name'))->count();

        if ($is_role_existing == 0) {
            $role = new Role();
            $role->name = Input::get('name');
            $role->description = Input::get('description');
            $role->save();
            return 'ROLE_SAVED';
        } else {
            return 'ROLE_EXISTING';
        }
    }

    public function update()
    {
        $get_role_id = Role::where('name', '=', Input::get('role_name'))->get()->first();
        $role = Role::find($get_role_id['id']);
        if ($role) {
            /*modules*/
            $role->purchase_view = $this->transformStatus(Input::get('purchase_view'));
            $role->purchase_add = $this->transformStatus(Input::get('purchase_add'));
            $role->purchase_edit = $this->transformStatus(Input::get('purchase_edit'));
            $role->purchase_delete = $this->transformStatus(Input::get('purchase_delete'));
            $role->payments_view = $this->transformStatus(Input::get('payments_view'));
            $role->payments_add = $this->transformStatus(Input::get('payments_add'));
            $role->payments_edit = $this->transformStatus(Input::get('payments_edit'));
            $role->payments_delete = $this->transformStatus(Input::get('payments_delete'));
            $role->bookings_view = $this->transformStatus(Input::get('bookings_view'));
            $role->bookings_add = $this->transformStatus(Input::get('bookings_add'));
            $role->bookings_edit = $this->transformStatus(Input::get('bookings_edit'));
            $role->bookings_delete = $this->transformStatus(Input::get('bookings_delete'));
            $role->agents_view = $this->transformStatus(Input::get('agents_view'));
            $role->agents_add = $this->transformStatus(Input::get('agents_add'));
            $role->agents_edit = $this->transformStatus(Input::get('agents_edit'));
            $role->agents_delete = $this->transformStatus(Input::get('agents_delete'));
            $role->clients_view = $this->transformStatus(Input::get('clients_view'));
            $role->clients_add = $this->transformStatus(Input::get('clients_add'));
            $role->clients_edit = $this->transformStatus(Input::get('clients_edit'));
            $role->clients_delete = $this->transformStatus(Input::get('clients_delete'));
            $role->users_view = $this->transformStatus(Input::get('users_view'));
            $role->users_add = $this->transformStatus(Input::get('users_add'));
            $role->users_edit = $this->transformStatus(Input::get('users_edit'));
            $role->users_delete = $this->transformStatus(Input::get('users_delete'));
            $role->roles_view = $this->transformStatus(Input::get('roles_view'));
            $role->roles_add = $this->transformStatus(Input::get('roles_add'));
            $role->roles_edit = $this->transformStatus(Input::get('roles_edit'));
            $role->roles_delete = $this->transformStatus(Input::get('roles_delete'));
            $role->promos_view = $this->transformStatus(Input::get('promos_view'));
            $role->promos_add = $this->transformStatus(Input::get('promos_add'));
            $role->promos_edit = $this->transformStatus(Input::get('promos_edit'));
            $role->promos_delete = $this->transformStatus(Input::get('promos_delete'));
            $role->reports_view = $this->transformStatus(Input::get('reports_view'));
            $role->reports_add = $this->transformStatus(Input::get('reports_add'));
            $role->reports_edit = $this->transformStatus(Input::get('reports_edit'));
            $role->reports_delete = $this->transformStatus(Input::get('reports_delete'));

            $role->products_view = $this->transformStatus(Input::get('products_view'));
            $role->products_add = $this->transformStatus(Input::get('products_add'));
            $role->products_edit = $this->transformStatus(Input::get('products_edit'));
            $role->products_delete = $this->transformStatus(Input::get('products_delete'));


            $role->multilevel_view = $this->transformStatus(Input::get('multilevel_view'));
            $role->multilevel_update = $this->transformStatus(Input::get('multilevel_update'));

            
            $role->collections_add = $this->transformStatus(Input::get('collections_add'));
            $role->collections_view = $this->transformStatus(Input::get('collections_view'));

            /*Commissions*/
            $role->distribute_commission = $this->transformStatus(Input::get('distribute_commission'));
            $role->print_voucher = $this->transformStatus(Input::get('print_voucher'));
            $role->print_check = $this->transformStatus(Input::get('print_check'));
            $role->view_commissions = $this->transformStatus(Input::get('view_commission'));
            $role->update_commissions = $this->transformStatus(Input::get('update_commission'));
            /*Dashboard*/
            $role->dashboard_new_po = $this->transformStatus(Input::get('new_po'));
            $role->dashboard_new_agent = $this->transformStatus(Input::get('new_agent'));
            $role->dashboard_bookings = $this->transformStatus(Input::get('bookings'));
            $role->dashboard_due_payments = $this->transformStatus(Input::get('payment'));
            $role->dashboard_approved_commissions = $this->transformStatus(Input::get('approved_commissions'));
            $role->dashboard_promo_qualifiers = $this->transformStatus(Input::get('promo_qualifiers'));
            $role->dashboard_for_pull_out = $this->transformStatus(Input::get('pull_out'));
            $role->dashboard_checks_follow_up = $this->transformStatus(Input::get('follow_up'));
            $role->dashboard_calendar = $this->transformStatus(Input::get('calendar'));
            $role->dashboard_orders = $this->transformStatus(Input::get('orders'));
            $role->dashboard_top_agents = $this->transformStatus(Input::get('top_agents'));



            $role->checks_for_deposit = $this->transformStatus(Input::get('checks_for_deposit'));
            $role->commissions_for_release = $this->transformStatus(Input::get('commissions_for_release'));
            $role->on_hold_commissions = $this->transformStatus(Input::get('on_hold_commissions'));
            $role->sales_by_agent = $this->transformStatus(Input::get('sales_by_agent'));
            $role->total_sales_by_month = $this->transformStatus(Input::get('total_sales_by_month'));
            $role->contest_status = $this->transformStatus(Input::get('contest_status'));
            $role->total_sales_incomplete_payment = $this->transformStatus(Input::get('total_sales_incomplete_payment'));
            $role->list_of_checks_by_status = $this->transformStatus(Input::get('list_of_checks_by_status'));
            $role->list_of_clients_for_month = $this->transformStatus(Input::get('list_of_clients_for_month'));
            $role->client_purchases = $this->transformStatus(Input::get('list_of_clients_for_month'));
            $role->top_selling_products = $this->transformStatus(Input::get('top_selling_products'));
            $role->top_seller_sponsor = $this->transformStatus(Input::get('top_seller_sponsor'));
            
            $role->save();
            return 'ROLE_SAVED';
        } else {
            return 'INVALID_ROLE';
        }
    }

    public function getRoleConfig()
    {
        $get_role_id = Role::where('name', '=', Input::get('role_name'))->get()->first();
        $role = Role::find($get_role_id['id']);
        if ($role) {
            return $role;
        } else {
            return 'INVALID_ROLE';
        }
    }

    public function transformStatus($status)
    {
        if ($status == 'Yes' || $status == 'true') {
            return 1;
        } else {
            return 0;
        }
    }


}
