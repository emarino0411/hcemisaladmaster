<?php

class CommissionsController extends \BaseController
{
    public function addNew()
    {
        return View::make('commissions/commissions_distribute', [
            'page_title' => 'HCEMIOS Commissions',
            'page_description' => 'Let us distribute the commissions :)',
            'level' => 'Commissions',
            'sub_level' => 'Distribute Commissions',
            'icon' => 'fa-rub'
        ]);
    }

    public function getClientPurchaseDetails()
    {
        $po_no = Input::get('po_id');

        $purchase_detail = DB::table('purchase as a')
            ->join('client as b', 'a.client_client_id', '=', 'b.id')
            ->select(DB::raw('a.po_no,b.last_name,b.first_name,a.purchase_date,a.terms,a.fast_track,
                    a.delivery_date,a.transaction_status,a.purchase_order_no'))
            ->where('a.id', '=', $po_no)
            ->get();

        return $purchase_detail;
    }

    public function getPurchasedItems()
    {

        $po_no = Input::get('po_id');
        $purchased_items = DB::table('purchase_item_list as a')
            ->join('item as b', 'a.item_id', '=', 'b.id')
            ->select('a.item_price', 'b.description', 'a.qty', 'a.gift', DB::raw('SUM(a.item_price*a.qty) as total_price'))
            ->where('purchase_po_no', '=', $po_no)
            ->groupBy('a.id')
            ->get();
        return $purchased_items;
    }

    public function getAgentCommissions()
    {
        $po_no = Input::get('po_id');

        $commissions = DB::table('commissions')
            ->where('po_no', '=', $po_no)
            ->get();

        $associate = 'N/A';
        $associate_percentage = 0;
        $is_associate_voucher_printed = '';

        $consultant = 'N/A';
        $consultant_percentage = 0;
        $is_consultant_voucher_printed = '';

        $sponsor = 'N/A';
        $sponsor_percentage = 0;
        $is_sponsor_voucher_printed = '';

        $cook = 'N/A';
        $cook_percentage = 0;
        $is_cook_voucher_printed = '';

        $driver = 'N/A';
        $driver_percentage = 0;
        $is_driver_voucher_printed = '';

        if (count($commissions) > 0) {
            foreach ($commissions as $commission) {

                $agent = DB::table('representative')
                    ->where('id', '=', $commission->representative_id)
                    ->get();
                if (count($agent) == 0) {
                    continue;
                }
                $agent = $agent[0];

                if ($commission->type == 'associate') {
                    $associate = $agent;
                    $associate_percentage = $commission->percentage;
                    $is_associate_voucher_printed = $commission->status;
                }
                if ($commission->type == 'presenter') {
                    $consultant = $agent;
                    $consultant_percentage = $commission->percentage;
                    $is_consultant_voucher_printed = $commission->status;
                }
                if ($commission->type == 'sponsor') {
                    $sponsor = $agent;
                    $sponsor_percentage = $commission->percentage;
                    $is_sponsor_voucher_printed = $commission->status;
                }
                if ($commission->type == 'cook') {
                    $cook = $agent;
                    $cook_percentage = $commission->percentage;
                    $is_cook_voucher_printed = $commission->status;
                }
                if ($commission->type == 'driver') {
                    $driver = $agent;
                    $driver_percentage = $commission->percentage;
                    $is_driver_voucher_printed = $commission->status;
                }

            }


            $data = [
                'associate' => $associate,
                'associate_percentage' => $associate_percentage,
                'associate_voucher' => $is_associate_voucher_printed,
                'consultant' => $consultant,
                'consultant_percentage' => $consultant_percentage,
                'presenter_voucher' => $is_consultant_voucher_printed,
                'sponsor' => $sponsor,
                'sponsor_percentage' => $sponsor_percentage,
                'sponsor_voucher' => $is_sponsor_voucher_printed,
                'cook' => $cook,
                'cook_percentage' => $cook_percentage,
                'cook_voucher' => $is_cook_voucher_printed,
                'driver' => $driver,
                'driver_percentage' => $driver_percentage,
                'driver_voucher' => $is_driver_voucher_printed,
            ];

        } else {
            $purchase_detail = DB::table('purchase as a')
                ->where('a.id', '=', $po_no)
                ->get();

            $purchase_detail = $purchase_detail[0];

            if ($purchase_detail->associate_id != 0 && $purchase_detail->associate_id != null) {
                $associate = DB::table('representative')
                    ->where('id', '=', $purchase_detail->associate_id)
                    ->get();
                $associate = $associate[0];
            } else {
                $associate = 'N/A';
            }

            if ($purchase_detail->consultant_id != 0 && $purchase_detail->consultant_id != null) {
                $consultant = DB::table('representative')
                    ->where('id', '=', $purchase_detail->consultant_id)
                    ->get();
                $consultant = $consultant[0];
            } else {
                $consultant = 'N/A';
            }

            if ($purchase_detail->sponsor_id != 0 && $purchase_detail->sponsor_id != null) {
                $sponsor = DB::table('representative')
                    ->where('id', '=', $purchase_detail->sponsor_id)
                    ->get();
                $sponsor = $sponsor[0];
            } else {
                $sponsor = 'N/A';
            }

            // sponsor and no associate
            if ($associate == 'N/A') {
                $associate_percentage = 0;

                //sponsor
                if ($sponsor == 'N/A') {
                    $sponsor_percentage = 0;
                } else {
                    if ($sponsor->current_level == 'consultant') {
                        $sponsor_percentage = 15;
                    } else if ($sponsor->current_level == 'senior consultant') {
                        $sponsor_percentage = 20;
                    } else if ($sponsor->current_level == 'manager') {
                        $sponsor_percentage = 25;
                    } else if ($sponsor->current_level == 'distributor') {
                        $sponsor_percentage = 25;
                    } else {
                        $sponsor_percentage = 0;
                    }
                }

                //presenter
                if ($consultant == 'N/A') {
                    $consultant_percentage = 0;
                } else {
                    if ($consultant->current_level == 'consultant') {
                        $consultant_percentage = 4;
                    } else if ($consultant->current_level == 'senior consultant') {
                        $consultant_percentage = 4;
                    } else if ($consultant->current_level == 'manager') {
                        $consultant_percentage = 5;
                    } else if ($consultant->current_level == 'distributor') {
                        $consultant_percentage = 5;
                    } else {
                        $consultant_percentage = 0;
                    }
                }

                $sponsor_percentage = $sponsor_percentage - $consultant_percentage;


                if ($purchase_detail->terms == 'cod') {
                    $sponsor_percentage += 4;
                }


            } else { // sponsor with associate
                $associate_percentage = 10;
                //sponsor
                if ($sponsor == 'N/A') {
                    $sponsor_percentage = 0;
                } else {

                    if ($sponsor->current_level == 'consultant') {
                        $sponsor_percentage = 10;

                    } else if ($sponsor->current_level == 'senior consultant') {
                        $sponsor_percentage = 10;

                    } else if ($sponsor->current_level == 'manager') {
                        $sponsor_percentage = 12;


                    } else if ($sponsor->current_level == 'distributor') {
                        $sponsor_percentage = 12;

                    } else {
                        $sponsor_percentage = 0;
                    }

                    if ($purchase_detail->terms == 'cod') {
                        $associate_percentage += 2;
                    }

                }

                //presenter
                if ($consultant == 'N/A') {
                    $consultant_percentage = 0;
                } else {
                    if ($consultant->current_level == 'consultant') {
                        $consultant_percentage = 4;
                    } else if ($consultant->current_level == 'senior consultant') {
                        $consultant_percentage = 4;
                    } else if ($consultant->current_level == 'manager') {
                        $consultant_percentage = 5;
                    } else if ($consultant->current_level == 'distributor') {
                        $consultant_percentage = 5;
                    } else {
                        $consultant_percentage = 0;
                    }
                }

                $sponsor_percentage = $sponsor_percentage - $consultant_percentage;
            }
            $data = [
                'associate' => $associate,
                'associate_percentage' => $associate_percentage,
                'consultant' => $consultant,
                'consultant_percentage' => $consultant_percentage,
                'sponsor' => $sponsor,
                'sponsor_percentage' => $sponsor_percentage,
            ];
        }


        return $data;
    }

    public function saveCommissions()
    {
        $inputs = Input::all();

        Commission::where('po_no', '=', $inputs['txt_po'])->delete();

        if ($inputs['txt_sponsor_id'] != '0') {
            $sponsor = new Commission();
            $sponsor->po_no = $inputs['txt_po'];
            $sponsor->representative_id = $inputs['txt_sponsor_id'];
            $sponsor->percentage = $inputs['txt_sponsor_percentage'];
            $sponsor->type = 'sponsor';
            $sponsor->status = 'DISTRIBUTED';
            $sponsor->save();
        }
        if ($inputs['txt_consultant_id'] != '0') {
            $consultant = new Commission();
            $consultant->po_no = $inputs['txt_po'];
            $consultant->representative_id = $inputs['txt_consultant_id'];
            $consultant->percentage = $inputs['txt_consultant_percentage'];
            $consultant->type = 'presenter';
            $consultant->status = 'DISTRIBUTED';
            $consultant->save();
        }
        if ($inputs['txt_associate_id'] != '0') {
            $associate = new Commission();
            $associate->po_no = $inputs['txt_po'];
            $associate->representative_id = $inputs['txt_associate_id'];
            $associate->percentage = $inputs['txt_associate_percentage'];
            $associate->type = 'associate';
            $associate->status = 'DISTRIBUTED';
            $associate->save();
        }

        if ($inputs['txt_cook_id'] != '') {
            $cook = new Commission();
            $cook->po_no = $inputs['txt_po'];
            $cook->representative_id = $inputs['txt_cook_id'];
            $cook->percentage = $inputs['txt_cook_percentage'];
            $cook->type = 'cook';
            $cook->status = 'DISTRIBUTED';
            $cook->save();
        }

        if ($inputs['txt_driver_id'] != '') {
            $driver = new Commission();
            $driver->po_no = $inputs['txt_po'];
            $driver->representative_id = $inputs['txt_driver_id'];
            $driver->percentage = $inputs['txt_driver_percentage'];
            $driver->type = 'driver';
            $driver->status = 'DISTRIBUTED';
            $driver->save();
        }

    }

    public function voucher($id='')//omm id
    {


        if ($id == '') {
            $client   = null;
            $purchase = null;
            $agent = null;
            $commission = null;
        } else {
            $commission = Commission::where('id',$id)->first();
            $purchase = Purchase::where('id', $commission->po_no)->first();
            $client = Client::where('id', $purchase->client_client_id)->first();
            $agent = Agent::where('id',$commission->representative_id)->first();
        }

        $name = User::find(Auth::id())->get();

        return View::make('commissions/commissions_vouchers', [
            'page_title' => 'HCEMIOS Commissions',
            'page_description' => 'Who will have the voucher for today? :)',
            'level' => 'Commissions',
            'sub_level' => 'Create Voucher',
            'icon' => 'fa-rub',
            'name' => $name[0],
            'commission' => $commission,
            'purchase' => $purchase,
            'client' => $client,
            'agent' => $agent,
        ]);
    }

    public function getAgentsList()
    {
        $po_no = Input::get('po_id');

        $commissions = DB::table('commissions as c')
            ->join('representative as r', 'r.id', '=', 'representative_id')
            ->select('c.percentage', 'c.wsp_percentage', 'c.status',
                'r.wsp_percentage as agent_wsp',
                'r.account_balance_percentage as agent_account_balance',
                'c.id', 'r.first_name', 'r.last_name', 'c.type',
                'c.due_date', 'c.check_no', 'c.voucher_no', 'c.check_date',
                'c.other_deduction', 'c.other_deduction_amount',
                'c.remarks',
                'c.voucher_no', 'c.due_date', 'c.check_date',
                'c.account_balance_percentage')
            ->where('c.po_no', '=', $po_no)
            ->orderBy('c.percentage', 'desc')
            ->get();

        return $commissions;
    }

    public function saveVoucherCommissions()
    {
        $commission_agent = Commission::where('po_no', '=', Input::get('po_no'))
            ->where('id', '=', Input::get('agent_id'));

        if (Input::get('status')) {
            $status = Input::get('status');
        } else {
            $status = 'DISTRIBUTED';
        }

        if (Input::get('_check_date')) {
            $check_date = Input::get('_check_date');
        } else {
            $check_date = null;
        }

        $commission_agent->update(array(
            'due_date' => Input::get('release_date'),
            'wsp_percentage' => Input::get('wsp_percentage'),
            'account_balance_percentage' => Input::get('account_balance_percentage'),
            'other_deduction' => Input::get('other_deduction'),
            'other_deduction_amount' => Input::get('other_deduction_amount'),
            'received_commission' => Input::get('received_commission'),
            'user_user_id' => Input::get('user_user_id'),
            'status' => $status,
            'check_date' => $check_date,
            'check_no' => Input::get('check_no'),
            'voucher_no' => Input::get('voucher_no'),
            'remarks' => Input::get('remarks'),
            'bank_name' => Input::get('bank')
        ));
        return $commission_agent->get();
    }

    public function printVoucher($id)
    {
        $commissions_details = DB::table('commissions')
            ->join('representative', 'representative.id', '=', 'commissions.representative_id')
            ->join('purchase', 'purchase.id', '=', 'commissions.po_no')
            ->where('commissions.id', '=', $id)
            ->get();
        $wsp = DB::table('commissions')
            ->join('representative', 'representative.id', '=', 'commissions.representative_id')
            ->join('purchase', 'purchase.id', '=', 'commissions.po_no')
            ->select('commissions.wsp_percentage', 'commissions.account_balance_percentage')
            ->where('commissions.id', '=', $id)
            ->get();
        $purchase = DB::table('commissions')
            ->join('representative', 'representative.id', '=', 'commissions.representative_id')
            ->join('purchase', 'purchase.id', '=', 'commissions.po_no')
            ->select('purchase.id', 'purchase.terms',
                'purchase.purchase_order_no',
                'purchase.purchase_date')
            ->where('commissions.id', '=', $id)
            ->get();

        $purchase_item_list = DB::table('purchase')
            ->join('purchase_item_list', 'purchase_item_list.purchase_po_no', '=', 'purchase.id')
            ->join('item', 'item.id', '=', 'purchase_item_list.item_id')
            ->select('item.description', 'item.item_code')
            ->where('purchase_item_list.gift', '=', 'No')
            ->where('purchase.id', '=', $purchase[0]->id)
            ->get();

        $total_price = DB::table('purchase_item_list')
            ->where('purchase_po_no', '=', $purchase[0]->id)
            ->select(DB::raw('SUM(item_price*qty) as total_price'))
            ->get();

        $user = Auth::user();


        $client = DB::table('client')
            ->join('purchase', 'purchase.client_client_id', '=', 'client.id')
            ->join('commissions', 'commissions.po_no', '=', 'purchase.id')
            ->where('commissions.id', '=', $id)
            ->get();

        return View::make('commissions/commissions_printVoucherForm', [
            'commissions' => $commissions_details[0],
            'total_price' => $total_price[0]->total_price,
            'total_commission' => $total_price[0]->total_price * .83,
            'wsp' => $wsp[0]->wsp_percentage,
            'account_balance_percentage' => $wsp[0]->account_balance_percentage,
            'user' => $user,
            'purchase' => $purchase[0],
            'client' => $client[0],
            'purchase_item_list' => $purchase_item_list
        ]);
    }


    public function printVoucherValue($id)
    {
        $commissions_details = DB::table('commissions')
            ->join('representative', 'representative.id', '=', 'commissions.representative_id')
            ->join('purchase', 'purchase.id', '=', 'commissions.po_no')
            ->where('commissions.id', '=', $id)
            ->get();

        $wsp = DB::table('commissions')
            ->join('representative', 'representative.id', '=', 'commissions.representative_id')
            ->join('purchase', 'purchase.id', '=', 'commissions.po_no')
            ->select('commissions.wsp_percentage', 'commissions.account_balance_percentage')
            ->where('commissions.id', '=', $id)
            ->get();

        $purchase = DB::table('commissions')
            ->join('representative', 'representative.id', '=', 'commissions.representative_id')
            ->join('purchase', 'purchase.id', '=', 'commissions.po_no')
            ->select('purchase.id', 'purchase.terms',
                'purchase.purchase_order_no',
                'purchase.purchase_date')
            ->where('commissions.id', '=', $id)
            ->get();

        $purchase_item_list = DB::table('purchase')
            ->join('purchase_item_list', 'purchase_item_list.purchase_po_no', '=', 'purchase.id')
            ->join('item', 'item.id', '=', 'purchase_item_list.item_id')
            ->select('item.description', 'item.item_code')
            ->where('purchase_item_list.gift', '=', 'No')
            ->where('purchase.id', '=', $purchase[0]->id)
            ->get();

        $total_price = DB::table('purchase_item_list')
            ->where('purchase_po_no', '=', $purchase[0]->id)
            ->select(DB::raw('SUM(item_price*qty) as total_price'))
            ->get();

        $user = Auth::user();


        $client = DB::table('client')
            ->join('purchase', 'purchase.client_client_id', '=', 'client.id')
            ->join('commissions', 'commissions.po_no', '=', 'purchase.id')
            ->where('commissions.id', '=', $id)
            ->get();

        return View::make('commissions/commissions_printVoucherValues', [
            'commissions' => $commissions_details[0],
            'total_price' => $total_price[0]->total_price,
            'total_commission' => ($total_price[0]->total_price * .83),
            'wsp' => $wsp[0]->wsp_percentage,
            'account_balance_percentage' => $wsp[0]->account_balance_percentage,
            'user' => $user,
            'purchase' => $purchase[0],
            'client' => $client[0],
            'purchase_item_list' => $purchase_item_list
        ]);

    }


    public function printCheck($id)
    {
        $commissions_details = DB::table('commissions')
            ->join('representative', 'representative.id', '=', 'commissions.representative_id')
            ->join('purchase', 'purchase.id', '=', 'commissions.po_no')
            ->where('commissions.id', '=', $id)
            ->get();
        $wsp = DB::table('commissions')
            ->join('representative', 'representative.id', '=', 'commissions.representative_id')
            ->join('purchase', 'purchase.id', '=', 'commissions.po_no')
            ->select('commissions.wsp_percentage', 'commissions.account_balance_percentage')
            ->where('commissions.id', '=', $id)
            ->get();
        $purchase = DB::table('commissions')
            ->join('representative', 'representative.id', '=', 'commissions.representative_id')
            ->join('purchase', 'purchase.id', '=', 'commissions.po_no')
            ->select('purchase.id', 'purchase.terms',
                'purchase.purchase_order_no',
                'purchase.purchase_date')
            ->where('commissions.id', '=', $id)
            ->get();

        $purchase_item_list = DB::table('purchase')
            ->join('purchase_item_list', 'purchase_item_list.purchase_po_no', '=', 'purchase.id')
            ->join('item', 'item.id', '=', 'purchase_item_list.item_id')
            ->select('item.description', 'item.item_code')
            ->where('purchase_item_list.gift', '=', 'No')
            ->where('purchase.id', '=', $purchase[0]->id)
            ->get();

        $total_price = DB::table('purchase_item_list')
            ->where('purchase_po_no', '=', $purchase[0]->id)
            ->select(DB::raw('SUM(item_price*qty) as total_price'))
            ->get();

        $user = Auth::user();


        $client = DB::table('client')
            ->join('purchase', 'purchase.client_client_id', '=', 'client.id')
            ->join('commissions', 'commissions.po_no', '=', 'purchase.id')
            ->where('commissions.id', '=', $id)
            ->get();

        return View::make('commissions/commissions_printCheck', [
            'commissions' => $commissions_details[0],
            'total_price' => $total_price[0]->total_price,
            'total_commission' => $total_price[0]->total_price * .83,
            'wsp' => $wsp[0]->wsp_percentage,
            'account_balance_percentage' => $wsp[0]->account_balance_percentage,
            'user' => $user,
            'purchase' => $purchase[0],
            'client' => $client[0],
            'purchase_item_list' => $purchase_item_list
        ]);

    }

    public function commissionForRelease()
    {
        if (!isAccessModuleAllowed('commissions_for_release')) {
            return Redirect::to('dashboard');
        }

        return View::make('commissions/commissions_commission_for_release', [
            'purchases' => [],
            'page_title' => 'HCEMIOS Commissions',
            'page_description' => 'Commissions For Release',
            'level' => 'Reports',
            'sub_level' => 'Commissions For Release',
            'icon' => 'fa-rub'
        ]);
    }

    public function getCommissionForRelease()
    {
        $date_from = Input::get('_date_from');
        $date_to = Input::get('_date_to');

        return DB::table('commissions')
            ->join('representative', 'representative.id', '=', 'commissions.representative_id')
            ->join('purchase', 'purchase.id', '=', 'commissions.po_no')
            ->join('client', 'client.id', '=', 'purchase.client_client_id')
            ->where(function ($query) {
                $query->whereBetween('due_date', [Input::get('_date_from'), Input::get('_date_to')]);
            })
            ->where(function ($query) {
                $query->where('purchase.status', '<>', 'Cancelled')
                    ->orWhere('purchase.status', '<>', 'Pulled Out')
                    ->orWhere('purchase.status', '<>', 'For Pull Out');
            })
            ->orderBy('purchase.purchase_date')
            ->select('commissions.due_date', 'representative.last_name', 'representative.first_name', 'commissions.id', 'commissions.status', 'commissions.received_commission', 'commissions.id', 'client.last_name as client_last',
                'client.first_name as client_first', 'purchase.purchase_order_no', 'commissions.check_no', 'commissions.voucher_no')
            ->get();
    }


    public function updateCommissionsStatus()
    {

        DB::table('commissions')
            ->whereIn('id', Input::get('update_items'))
            ->update(array('status' => Input::get('status')));

    }

    public function commissionlist()
    {
        if (!isAccessModuleAllowed('purchase_view')) {
            return Redirect::to('dashboard');
        }

        $commissions = DB::table('commissions')
            ->leftJoin('purchase', 'purchase.id', '=', 'commissions.po_no')
            ->join('representative', 'representative.id', '=', 'commissions.representative_id')
            ->join('client', 'client.id', '=', 'purchase.client_client_id')
            ->select(DB::raw('CONCAT(representative.last_name,", ",representative.first_name) as agent'),
                DB::raw('CONCAT(client.first_name,", ",client.last_name) as client'),
                'purchase.purchase_order_no','purchase.id','commissions.status', 'commissions.voucher_no', 'commissions.check_no', 'commissions.check_date', 'commissions.due_date', 'commissions.remarks',
                'commissions.id', 'commissions.received_commission')
            //     ->where('purchase.status','<>','Incomplete')
            ->orderBy('purchase.updated_at', 'desc')
            ->get();


        return View::make('commissions/commission_list', [
            'commissions' => $commissions,
            'page_title' => 'HCEMIOS Commissions',
            'page_description' => 'Which commissions shall we release today? :)',
            'level' => 'Commissions',
            'sub_level' => 'List',
            'icon' => 'fa-shopping-cart'
        ]);
    }

}