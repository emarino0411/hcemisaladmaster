<?php


class UserController extends \BaseController
{

    public function index()
    {

        if(!isAccessModuleAllowed('users_view')){
            return Redirect::to('dashboard');
        }

        $users = User::all()->toArray();

        $page_title = 'HCEMIOS Users';
        $page_description = 'We Control Everything';
        $level = 'Users';
        $sub_level = 'List';
        $icon = 'fa fa-street-view';

        return View::make('users/users_list', compact('users', 'page_title', 'page_description', 'level', 'sub_level', 'icon', 'roles'));
    }

    public function addUser()
    {
        if(!isAccessModuleAllowed('users_add')){
            return Redirect::to('dashboard');
        }

        $page_title = 'HCEMIOS Users';
        $page_description = 'Adding another User?';
        $level = 'Users';
        $sub_level = 'Add New';
        $icon = 'fa fa-street-view';

        $roles = Role::all()->toArray();

        $municipalities = DB::table('rcitymun')
                ->join('rprov', 'rcitymun.provcode', '=', 'rprov.provcode')
                ->select(DB::raw('concat(cityname," [",provname,"]") as cityname'), 'rcitymun.provcode')
                ->orderBy('cityname', 'asc')
                ->orderBy('provname', 'asc')
                ->get();

        $provinces = DB::table('rprov')
                ->select('provcode', 'provname')
                ->orderBy('provname', 'asc')
                ->get();
        return View::make('users/users_add', compact('roles', 'brgy', 'municipalities', 'provinces',
                'page_title', 'page_description', 'level', 'sub_level', 'icon', 'roles'));
    }

    public function viewProfile($id, $edit_mode = '')
    {
        if(!isAccessModuleAllowed('users_view')){
            return Redirect::to('dashboard');
        }

        if ((!isAccessModuleAllowed('users_edit')) && $edit_mode != "") {
            return Redirect::to('dashboard');
        }

        // get user details
        $roles = Role::all()->toArray();
        $user = User::find($id);

        $page_title = 'HCEMIOS Users';
        $page_description = 'Checking out our Users?';
        $level = 'Users';
        $sub_level = 'View Profile:' . $user->first_name . ' ' . $user->last_name;
        $icon = 'fa fa-street-view';

        // get user role name
        $user_role = DB::table('roles')->where('id','=',$user->user_role)->get();

        $user->user_role = $user_role[0]->name;

        $municipalities = DB::table('rcitymun')
                ->join('rprov', 'rcitymun.provcode', '=', 'rprov.provcode')
                ->select(DB::raw('concat(cityname," [",provname,"]") as cityname'), 'provname', 'rcitymun.provcode','citycode')
                ->orderBy('cityname', 'asc')
                ->orderBy('provname', 'asc')
                ->get();

        $provinces = DB::table('rprov')
                ->select('provcode', 'provname')
                ->orderBy('provname', 'asc')
                ->get();

        // set view
        return View::make('users/users_profile', compact('provinces', 'municipalities', 'user', 'page_title',
                'page_description', 'level', 'sub_level', 'icon', 'roles', 'edit_mode'));
    }

    public function create()
    {
        if(!isAccessModuleAllowed('users_add')){
            return Redirect::to('dashboard');
        }

        $is_email_duplicate = User::where('email', '=', Input::get('user_email'))->count();
        if ($is_email_duplicate > 0) {
            return 'EMAIL_EXISTING';
        }
        $user = new User();
        $user->email = Input::get('user_email');
        $user->password = Hash::make((Input::get('user_password')));
        $user->last_name = Input::get('user_last_name');
        $user->first_name = Input::get('user_first_name');
        $user->gender = Input::get('user_gender');
        $user->civil_status = Input::get('user_civil_status');
        $user->birthdate = Input::get('user_birthdate');
        $user->landline = Input::get('user_landline');
        $user->mobile = Input::get('user_mobile');
        $user->street = Input::get('user_street');
        $user->brgy = Input::get('user_brgy');
        $user->city = Input::get('user_town');
        $user->province = Input::get('user_province');
        $user->user_role = Input::get('user_role');
        $user->status = 'ACTIVE';
        if ($user->save()) {
            return 'USER_SAVED';
        } else {
            return 'data';
        }
    }

    public function update()
    {
        if(!isAccessModuleAllowed('users_edit')){
            return Redirect::to('dashboard');
        }

        $get_user_id = User::where('email', '=', Input::get('old_email'))->get()->first();

        $user = User::find($get_user_id->id);

        $is_email_duplicate = User::where('email', '=', Input::get('user_email'))
                ->where('id', '!=', $get_user_id->id)
                ->count();

        if ($is_email_duplicate > 0) {
            return 'EMAIL_EXISTING';
        }
        $user->email = Input::get('user_email');
        $user->last_name = Input::get('user_last_name');
        $user->first_name = Input::get('user_first_name');
        $user->gender = Input::get('user_gender');
        $user->civil_status = Input::get('user_civil_status');
        $user->birthdate = Input::get('user_birthdate');
        $user->landline = Input::get('user_landline');
        $user->mobile = Input::get('user_mobile');
        $user->street = Input::get('user_street');
        $user->brgy = Input::get('user_brgy');
        $user->city = Input::get('user_town');
        $user->province = Input::get('user_province');
        $user->user_role = Input::get('user_role');
        $user->update();
        if ($user->update()) {
            return 'USER_UPDATED';
        } else {

            return 'Error';
        }

    }

    public function delete()
    {
        if(!isAccessModuleAllowed('users_delete')){
            return Redirect::to('dashboard');
        }

        $user = User::find(Input::get('userId'));
        $user->status = 'DEACTIVATED';
        $user->update();
        if ($user->update()) {
            return 'USER_DELETED';
        } else {
            return 'Error';
        }
    }

    public function activate()
    {
        if(!isAccessModuleAllowed('users_delete')){
            return Redirect::to('dashboard');
        }

        $user = User::find(Input::get('userId'));
        $user->status = 'ACTIVE';
        $user->update();
        if ($user->update()) {
            return 'USER_ACTIVATED';
        } else {
            return 'Error';
        }
    }

    public function viewLogs()
    {

    }

    public function getLogin()
    {
        return View::make('login');
    }

    public function postLogin()
    {
        if (Auth::attempt(array('email' => Input::get('email'), 'password' => Input::get('password')))) {
            $user = User::find(Auth::id());

//            Session::put('name', $user->first_name.' '.$user->last_name);
            Session::put('name', $user->first_name.' '.$user->last_name);
            if( $user->gender == 'Female' ){
                Session::put('img', '/admin_girl.png');
            }else{
                Session::put('img', '/admin_boy.png');
            }

            return Redirect::to('/dashboard')->with('message', 'You are now logged in!');
        } else {

            return Redirect::to('users/login')
                    ->with('message', 'Your ussername/password combination was incorrect')
                    ->withInput();
        }
    }

    public function getLogout() {
        Auth::logout();
        return Redirect::to('users/login')->with('message', 'Your are now logged out!');
    }


}
