<?php

class ReportController extends \BaseController
{

    public function checkForDeposit()
    {

        if (!isAccessModuleAllowed('checks_for_deposit')) {
            return Redirect::to('dashboard');
        }

        return View::make('report/report_check_for_deposit', [
            'purchases' => [],
            'page_title' => 'HCEMIOS Reports',
            'page_description' => 'Checks For Deposit',
            'level' => 'Reports',
            'sub_level' => 'Checks For Deposit',
            'icon' => 'fa-bank'
        ]);
    }

    public function getCheckForDeposit()
    {
        $date_from = Input::get('_date_from');
        $date_to = Input::get('_date_to');

        return DB::table('payments')
            ->join('purchase', 'purchase.id', '=', 'payments.po_no')
            ->join('client', 'client.id', '=', 'purchase.client_client_id')
            ->where(function ($query) {
                $query->whereBetween('check_date', [Input::get('_date_from'), Input::get('_date_to')])
                    ->whereIn('payments.status', ['For Deposit', 'Daif', 'Daif2']);
            })
            ->orWhere(function ($query) {
                $query->whereBetween('deposit_date1', [Input::get('_date_from'), Input::get('_date_to')])
                    ->whereIn('payments.status', ['For Deposit', 'Daif', 'Daif2']);
            })
            ->orWhere(function ($query) {
                $query->whereBetween('deposit_date2', [Input::get('_date_from'), Input::get('_date_to')])
                    ->whereIn('payments.status', ['For Deposit', 'Daif', 'Daif2']);
            })
            ->orderBy('purchase.purchase_date')
            ->select('payments.remittance_date', 'client.first_name', 'client.last_name', 'payments.check_number', 'payments.check_date', 'payments.deposit_date1', 'payments.deposit_date2', 'payments.bank', 'payments.branch', 'payments.amount', 'payments.status', 'purchase.purchase_order_no')
            ->get();
    }

    public function commissionForRelease()
    {
        if (!isAccessModuleAllowed('commissions_for_release')) {
            return Redirect::to('dashboard');
        }

        return View::make('report/report_commission_for_release', [
            'purchases' => [],
            'page_title' => 'HCEMIOS Reports',
            'page_description' => 'Commissions For Release',
            'level' => 'Reports',
            'sub_level' => 'Commissions For Release',
            'icon' => 'fa-rub'
        ]);
    }

    public function getCommissionForRelease()
    {
        $date_from = Input::get('_date_from');
        $date_to = Input::get('_date_to');

        return DB::table('commissions')
            ->join('representative', 'representative.id', '=', 'commissions.representative_id')
            ->join('purchase', 'purchase.id', '=', 'commissions.po_no')
            ->where(function ($query) {
                $query->whereBetween('due_date', [Input::get('_date_from'), Input::get('_date_to')]);
            })
            ->where(function ($query) {
                $query->where('purchase.status', '<>', 'Cancelled')
                    ->orWhere('purchase.status', '<>', 'Pulled Out')
                    ->orWhere('purchase.status', '<>', 'For Pull Out');
            })
            // ->where('commissions.status','=','FOR RELEASE')
            ->orderBy('purchase.purchase_date')
            ->select('commissions.due_date', 'representative.last_name', 'representative.first_name', 'commissions.id', 'commissions.status', 'commissions.received_commission', 'purchase.purchase_order_no')
            ->get();
    }

    public function onHoldCommission()
    {
        if (!isAccessModuleAllowed('on_hold_commissions')) {
            return Redirect::to('dashboard');
        }

        return View::make('report/report_commission_on_hold', [
            'purchases' => [],
            'page_title' => 'HCEMIOS Reports',
            'page_description' => 'On Hold Commissions',
            'level' => 'Reports',
            'sub_level' => 'On Hold Commissions',
            'icon' => 'fa-rub'
        ]);
    }

    public function getOnHoldCommission()
    {
        $date = Input::get('_date');

        return DB::table('commissions')
            ->join('representative', 'representative.id', '=', 'commissions.representative_id')
            ->join('purchase', 'purchase.id', '=', 'commissions.po_no')
            // ->where(function($query)
            //          {
            //              $query->where('due_date','=',Input::get('_date'));
            //          })
            // ->where(function($query)
            //          {
            //              $query->where('purchase.status','=','Cancelled')
            //              	  ->orWhere('purchase.status','=','Pulled Out')
            //              	  ->orWhere('purchase.status','=','For Pull Out');
            //          })
            ->where('commissions.status', '=', 'HOLD')
            ->orderBy('purchase.purchase_date')
            ->select('commissions.due_date', 'representative.last_name', 'representative.first_name', 'commissions.id', 'commissions.status', 'commissions.received_commission', 'purchase.status as po_status', 'purchase.purchase_order_no')
            ->get();
    }

    public function salesByAgent()
    {
        if (!isAccessModuleAllowed('sales_by_agent')) {
            return Redirect::to('dashboard');
        }

        return View::make('report/report_sales_by_agent', [
            'purchases' => [],
            'page_title' => 'HCEMIOS Reports',
            'page_description' => 'Agent Sales',
            'level' => 'Reports',
            'sub_level' => 'Agent Sales',
            'icon' => 'fa-rub'
        ]);
    }

    public function getSalesByAgent()
    {
        $date_from = Input::get('_date_from');
        $date_to = Input::get('_date_to');
        $agent = Input::get('_agent');

        Session::put('agent_id', $agent);

        $agent_sales = DB::table('purchase')
            ->join('representative', 'representative.id', '=', 'purchase.sponsor_id')
            ->join('client', 'client.id', '=', 'purchase.client_client_id')
            ->join('rcitymun', 'client.city', '=', 'rcitymun.citycode')
            ->join('rprov', 'client.province', '=', 'rprov.provcode')
            ->join('purchase_item_list', 'purchase_item_list.purchase_po_no', '=', 'purchase.id')
            ->join('item', 'item.id', '=', 'purchase_item_list.item_id')
            ->where(function ($query) {
                $query->where('purchase.status', '<>', 'Cancelled')
                    ->orWhere('purchase.status', '<>', 'Pulled Out')
                    ->orWhere('purchase.status', '<>', 'For Pull Out');
            })
            ->whereBetween('purchase.purchase_date', [$date_from, $date_to])
            ->select(
                DB::raw("IF(purchase.purchase_order_no = '','[TEMPORARY]',purchase.purchase_order_no) as purchase_order_number"),
                'client.city',
                'client.province',
                'purchase.id',
                'purchase.purchase_date',
                'purchase.status',
                'client.last_name',
                'client.first_name',
                'purchase.sponsor_id',
                'purchase.associate_id',
                'purchase.remarks',
                'purchase.fast_track',
                'purchase_item_list.gift',
                DB::raw('CONCAT(client.street,client.brgy) as client_street'),
                DB::raw('IFNULL(rcitymun.cityname,"N/A") as cityname'),
                DB::raw('IFNULL(rprov.provname,"N/A") as provname'),
                DB::raw('SUM(item_price*qty) as total_price'),

                DB::raw('0 as total_points'),
                DB::raw('
                    SUM(
					    IF(STRCMP(LOWER(purchase_item_list.gift),"yes") = 0,
						    0,
							item.production_points_associate
             			)
                        *qty
					)as total_points_assoc'),

                DB::raw('
                    SUM(
					    IF(STRCMP(LOWER(purchase_item_list.gift),"yes") = 0,
						    0,
							item.production_points
             			)
                        *qty
					)as total_points_sponsor'),

                DB::raw('STRCMP(LOWER(purchase_item_list.gift),"no") as is_gift'),
                DB::raw('STRCMP(LOWER(purchase.fast_track),"yes") as is_fast_track'),
                DB::raw('STRCMP(LOWER(purchase.agent_current_level),"associate") as current_level')
            )
            // ,
            // 	DB::raw('SUM(IF(STRCMP(LOWER(purchase_item_list.gift),"yes"),
            // 						0,
            // 						IF(STRCMP(LOWER(purchase.agent_current_level),"associate"),
            // 						item.production_points_associate,
            // 						item.production_points)
            // 						),
            // 					)
            // 					*qty) as total_points'))
            ->where(function ($query) {
                $query->where('purchase.sponsor_id', '=', Session::get('agent_id'))
                    ->orWhere('purchase.associate_id', '=', Session::get('agent_id'));
            })
            ->orderBy('purchase.purchase_date')
            ->groupBy('purchase.id')
            ->get();

        $client_purchase = [];

        $ctr = 0;
        foreach ($agent_sales as $purchase) {
            if ($purchase->associate_id == $agent && $purchase->fast_track == 'Yes') {
                $purchase->total_points = $purchase->total_points_assoc;
                $client_purchase[$ctr] = $purchase;
                $ctr++;
            }
            if ($purchase->sponsor_id == $agent && $purchase->fast_track == 'No') {
                $purchase->total_points = $purchase->total_points_sponsor;
                $client_purchase[$ctr] = $purchase;
                $ctr++;
            }

        }
        $agent_sales = $client_purchase;

        $client_purchase_items = DB::table('purchase')
            ->join('representative', 'representative.id', '=', 'purchase.sponsor_id')
            ->join('purchase_item_list', 'purchase_item_list.purchase_po_no', '=', 'purchase.id')
            ->join('item', 'item.id', '=', 'purchase_item_list.item_id')
            ->where(function ($query) {
                $query->where('purchase.status', '<>', 'Cancelled')
                    ->orWhere('purchase.status', '<>', 'Pulled Out')
                    ->orWhere('purchase.status', '<>', 'For Pull Out');
            })
            ->whereBetween('purchase.purchase_date', [$date_from, $date_to])
            ->select(
                'purchase.id',
                'item.description')
            ->where(function ($query) {
                $query->where('purchase.sponsor_id', '=', Session::get('agent_id'))
                    ->orWhere('purchase.associate_id', '=', Session::get('agent_id'));
            })
            ->where('purchase_item_list.gift', '=', 'No')
            ->get();

        return [$agent_sales, $client_purchase_items];
    }


    public function totalSalesByMonth()
    {
        if (!isAccessModuleAllowed('total_sales_by_month')) {
            return Redirect::to('dashboard');
        }
        return View::make('report/report_sales_by_month', [
            'purchases' => [],
            'page_title' => 'HCEMIOS Reports',
            'page_description' => 'Monthly Sales',
            'level' => 'Reports',
            'sub_level' => 'Monthly Sales',
            'icon' => 'fa-calendar'
        ]);
    }

    public function getTotalSalesByMonth()
    {
        $date_from = Input::get('_date_from');
        $date_to = Input::get('_date_to');

        // $date_from = $year.'-'.$month.'-1';
        // $date_to=$year.'-'.$month.'-'.cal_days_in_month(CAL_GREGORIAN,$month,$year);

        $sponsor_sales = DB::table('purchase')
            ->join('representative', function ($join) {
                $join->on('representative.id', '=', 'purchase.sponsor_id');
            })
            ->join('client', 'client.id', '=', 'purchase.client_client_id')
            ->join('purchase_item_list', 'purchase_item_list.purchase_po_no', '=', 'purchase.id')
            ->join('item', 'item.id', '=', 'purchase_item_list.item_id')
            ->where(function ($query) {
                $query->where('purchase.status', '<>', 'Cancelled')
                    ->orWhere('purchase.status', '<>', 'Pulled Out')
                    ->orWhere('purchase.status', '<>', 'For Pull Out');
            })
            ->whereBetween('purchase.purchase_date', [$date_from, $date_to])
            ->where('purchase.fast_track', '=', 'No')
            ->select(
                DB::raw("count(DISTINCT purchase_item_list.purchase_po_no) as purchase_order_count"),
                DB::raw("IF(purchase.purchase_order_no = '','[TEMPORARY]',purchase.purchase_order_no) as purchase_order_number"),
                'representative.id as representative_id',
                'purchase.purchase_date',
                'purchase.status',
                'client.last_name',
                'client.first_name',
                'purchase.sponsor_id',
                'purchase.associate_id',
                'purchase.fast_track',
                'representative.first_name',
                'representative.first_name as assoc_first_name',
                'representative.last_name',
                'representative.last_name as assoc_last_name',
                DB::raw('SUM(item_price*qty) as total_price'),
                DB::raw('0 as total_points'),
                DB::raw('
                    SUM(
					    IF(STRCMP(LOWER(purchase_item_list.gift),"yes") = 0,
						    0,
							item.production_points
             			)
                        *qty
					)as total_points')
            )
            ->orderBy('purchase.purchase_date')
            ->groupBy('representative.id')
            ->get();

        $associate_sales = DB::table('purchase')
            ->join('representative', function ($join) {
                $join->on('representative.id', '=', 'purchase.associate_id');
            })
            ->join('client', 'client.id', '=', 'purchase.client_client_id')
            ->join('purchase_item_list', 'purchase_item_list.purchase_po_no', '=', 'purchase.id')
            ->join('item', 'item.id', '=', 'purchase_item_list.item_id')
            ->where(function ($query) {
                $query->where('purchase.status', '<>', 'Cancelled')
                    ->orWhere('purchase.status', '<>', 'Pulled Out')
                    ->orWhere('purchase.status', '<>', 'For Pull Out');
            })
            ->whereBetween('purchase.purchase_date', [$date_from, $date_to])
            ->where('purchase.fast_track', '=', 'Yes')
            ->select(
                DB::raw("count(DISTINCT purchase_item_list.purchase_po_no) as purchase_order_count"),
                DB::raw("IF(purchase.purchase_order_no = '','[TEMPORARY]',purchase.purchase_order_no) as purchase_order_number"),
                'representative.id as representative_id',
                'purchase.purchase_date',
                'purchase.status',
                'client.last_name',
                'client.first_name',
                'purchase.sponsor_id',
                'purchase.associate_id',
                'purchase.fast_track',
                'representative.first_name',
                'representative.first_name as assoc_first_name',
                'representative.last_name',
                'representative.last_name as assoc_last_name',
                DB::raw('SUM(item_price*qty) as total_price'),
                DB::raw('0 as total_points'),
                DB::raw('
                    SUM(
					    IF(STRCMP(LOWER(purchase_item_list.gift),"yes") = 0,
						    0,
							item.production_points
             			)
                        *qty
					)as total_points')
            )
            ->orderBy('purchase.purchase_date')
            ->groupBy('representative.id')
            ->get();


        $client_purchase = [];

        $ctr = 0;
        foreach ($associate_sales as $purchase) {
            $client_purchase[$ctr] = $purchase;
            $ctr++;
        }
        foreach ($sponsor_sales as $purchase) {
            $client_purchase[$ctr] = $purchase;
            $ctr++;
        }

        return $client_purchase;
    }

    public function contestStatus()
    {
        if (!isAccessModuleAllowed('contest_status')) {
            return Redirect::to('dashboard');
        }
        return View::make('report/report_contest_status', [
            'purchases' => [],
            'page_title' => 'HCEMIOS Reports',
            'page_description' => 'Contest Status',
            'level' => 'Reports',
            'sub_level' => 'Contest Status',
            'icon' => 'fa-rub'
        ]);
    }

    public function getContestStatus()
    {
        $contest_id = Input::get('contest_id');

        $promo = Promo::find($contest_id);

        return DB::table('promos_qualifier')
            ->join('promos', 'promos.id', '=', 'promos_qualifier.promo_id')
            ->join('purchase', 'purchase.id', '=', 'promos_qualifier.po_no')
            ->join('representative', 'representative.id', '=', 'promos_qualifier.representative')
            ->join('purchase_item_list', 'purchase_item_list.purchase_po_no', '=', 'purchase.id')
            ->join('item', 'item.id', '=', 'purchase_item_list.item_id')
            ->where('promos.id', '=', $contest_id)
            ->whereBetween('purchase.purchase_date', [$promo->from, $promo->to])
            ->where(function ($query) {
                $query->where('purchase.status', '<>', 'Cancelled')
                    ->orWhere('purchase.status', '<>', 'Pulled Out')
                    ->orWhere('purchase.status', '<>', 'For Pull Out');
            })
            ->select(
                'promos.mechanics_1_points',
                'promos.mechanics_2_points',
                'promos.mechanics_3_points',
                'promos.mechanics_1_prize',
                'promos.mechanics_2_prize',
                'promos.mechanics_3_prize',
                'representative.last_name',
                'representative.first_name',
                DB::raw('SUM(item_price*qty) as total_price'),
                DB::raw('SUM(IF(STRCMP(LOWER(purchase.agent_current_level),"associate"),
								item.production_points_associate,
								item.production_points)
								*qty) as total_points'))
            ->orderBy('purchase.purchase_date')
            ->groupBy('representative.id')
            ->get();
    }

    public function totalSalesIncompletePayment()
    {
        if (!isAccessModuleAllowed('total_sales_incomplete_payment')) {
            return Redirect::to('dashboard');
        }
        return View::make('report/report_sales_incomplete_payment', [
            'purchases' => [],
            'page_title' => 'HCEMIOS Reports',
            'page_description' => 'List of Accounts for Follow Up',
            'level' => 'Reports',
            'sub_level' => 'List of Accounts for Follow Up',
            'icon' => 'fa-rub'
        ]);
    }

    public function getTotalSalesIncompletePayment()
    {
        $date_from = Input::get('_date_from');
        $date_to = Input::get('_date_to');

        // $date_from = $year.'-'.$month.'-1';
        // $date_to=$year.'-'.$month.'-'.cal_days_in_month(CAL_GREGORIAN,$month,$year);

        if ($date_from == "" || $date_from == null) {
            $date_from = '1990-01-01';
            $date_to = date('Y-m-d');
        }
        Session::put('date_from', $date_from);
        Session::put('date_to', $date_to);

        $incomplete_payment = [];

        $incomplete_purchases = DB::table('purchase')
            ->join('representative', 'representative.id', '=', 'purchase.sponsor_id')
            ->leftJoin('representative as presenter', 'presenter.id', '=', 'purchase.consultant_id')
            ->join('payments', 'payments.po_no', '=', 'purchase.id')
            ->join('client', 'client.id', '=', 'purchase.client_client_id')

            ->where(function ($query) {
                $query
                    ->where('payments.status', '=', 'Good')
                    ->orWhere('payments.status', '=', 'For Deposit');
            })
            ->where('purchase.status', '!=', 'Cancelled')
            ->whereBetween('purchase.purchase_date', [Session::get('date_from'), Session::get('date_to')])
            ->select(
                'representative.last_name',
                'representative.first_name',
                'presenter.last_name as presenter_last_name',
                'presenter.first_name as presenter_first_name',
                'client.last_name as client_last_name',
                'client.first_name as client_first_name',
                DB::raw('SUM(payments.amount) as total_cleared_payment'),
                'payments.amount',
                'purchase.status',
                'purchase.id',
                'purchase.purchase_order_no',
                'purchase.purchase_date',
                'purchase.remarks')
            ->groupBy('purchase.id')
            ->get();

        $incomplete_po = array_pluck($incomplete_purchases, 'id');

        $incomplete_purchase_price = DB::table('purchase')
            ->join('purchase_item_list', 'purchase_item_list.purchase_po_no', '=', 'purchase.id')
            ->join('item', 'item.id', '=', 'purchase_item_list.item_id')
            ->whereIn('purchase.id', $incomplete_po)
            ->select(
                DB::raw('SUM(item_price*qty) as amount'),
                'purchase.id',
                'purchase.purchase_order_no')
            ->groupBy('purchase.id')
            ->get();


        $purchase = DB::table('purchase')
            ->join('representative', 'representative.id', '=', 'purchase.sponsor_id')
            ->leftJoin('representative as presenter', 'presenter.id', '=', 'purchase.consultant_id')
            ->join('payments', 'payments.po_no', '=', 'purchase.id')
            ->join('client', 'client.id', '=', 'purchase.client_client_id')
            // ->whereNotIn('payments.status',['Good','For Deposit'])
            ->where('payments.status', '!=', 'Good')
            ->where('payments.status', '!=', 'For Deposit')
            ->where('payments.status', '!=', 'Replaced')
            ->where('payments.status', '!=', 'Cancelled')
            ->whereNotIn('purchase.id',$incomplete_po)
            ->whereBetween('purchase.purchase_date', [Session::get('date_from'), Session::get('date_to')])
            ->select(
                'representative.last_name',
                'representative.first_name',
                'presenter.last_name as presenter_last_name',
                'presenter.first_name as presenter_first_name',
                'client.last_name as client_last_name',

                'client.first_name as client_first_name',

                // DB::raw('IFNULL(payments.remarks,"N/A") as remarks'),
                DB::raw('SUM(payments.amount) as total_problem_amount'),

                //'payments.status as total_problem_amount',
                'payments.amount',
                'purchase.status',
                'purchase.id',
                'purchase.purchase_order_no',
                'purchase.purchase_date',
                'purchase.remarks')
            ->groupBy('purchase.id')
            ->orderBy('purchase.id')
            ->get();

        $purchase_ids = array_pluck($purchase, 'id');

        $purchase_price = DB::table('purchase')
            ->join('purchase_item_list', 'purchase_item_list.purchase_po_no', '=', 'purchase.id')
            ->join('item', 'item.id', '=', 'purchase_item_list.item_id')
            ->whereIn('purchase.id', $purchase_ids)
            ->select(
                DB::raw('SUM(item_price*qty) as amount'),
                'purchase.id')
            ->groupBy('purchase.id')
            ->get();



        return [$purchase, $purchase_price, $incomplete_purchases, $incomplete_purchase_price];
    }

    public function listOfCheckByStatus()
    {
        if (!isAccessModuleAllowed('list_of_checks_by_status')) {
            return Redirect::to('dashboard');
        }
        return View::make('report/report_list_of_checks_by_status', [
            'purchases' => [],
            'page_title' => 'HCEMIOS Reports',
            'page_description' => 'List of Checks By Status',
            'level' => 'Reports',
            'sub_level' => 'List of Checks By Status',
            'icon' => 'fa-bank'
        ]);
    }

    public function getListOfCheckByStatus()
    {
        $date_from = Input::get('_date_from');
        $date_to = Input::get('_date_to');
        $check_status = Input::get('check_status');

        if ($check_status != 'all') {
            return DB::table('payments')
                ->join('purchase', 'purchase.id', '=', 'payments.po_no')
                ->join('client', 'client.id', '=', 'purchase.client_client_id')
                ->where(function ($query) {
                    $query->whereBetween('check_date', [Input::get('_date_from'), Input::get('_date_to')])
                        ->where('payments.status', '=', Input::get('check_status'));
                })
                ->orWhere(function ($query) {
                    $query->whereBetween('deposit_date1', [Input::get('_date_from'), Input::get('_date_to')])
                        ->where('payments.status', '=', Input::get('check_status'));
                })
                ->orWhere(function ($query) {
                    $query->whereBetween('deposit_date2', [Input::get('_date_from'), Input::get('_date_to')])
                        ->where('payments.status', '=', Input::get('check_status'));
                })
                ->orderBy('purchase.purchase_date')
                ->select('payments.remittance_date', 'client.first_name', 'client.last_name', 'payments.check_number', 'payments.check_date', 'payments.deposit_date1', 'payments.deposit_date2', 'payments.bank', 'payments.branch', 'payments.amount', 'payments.status', 'purchase.purchase_order_no')
                ->get();
        } else {
            return DB::table('payments')
                ->join('purchase', 'purchase.id', '=', 'payments.po_no')
                ->join('client', 'client.id', '=', 'purchase.client_client_id')
                ->where(function ($query) {
                    $query->whereBetween('check_date', [Input::get('_date_from'), Input::get('_date_to')]);
                })
                ->orWhere(function ($query) {
                    $query->whereBetween('deposit_date1', [Input::get('_date_from'), Input::get('_date_to')]);
                })
                ->orWhere(function ($query) {
                    $query->whereBetween('deposit_date2', [Input::get('_date_from'), Input::get('_date_to')]);
                })
                ->orderBy('purchase.purchase_date')
                ->select('payments.remittance_date', 'client.first_name', 'client.last_name', 'payments.check_number', 'payments.check_date', 'payments.deposit_date1', 'payments.deposit_date2', 'payments.bank', 'payments.branch', 'payments.amount', 'payments.status', 'purchase.purchase_order_no')
                ->get();
        }

    }

    public function listOfClient()
    {
        if (!isAccessModuleAllowed('list_of_clients_for_month')) {
            return Redirect::to('dashboard');
        }
        return View::make('report/report_client_of_the_month', [
            'purchases' => [],
            'page_title' => 'HCEMIOS Reports',
            'page_description' => 'Monthly Client',
            'level' => 'Reports',
            'sub_level' => 'Monthly Client',
            'icon' => 'fa-bank'
        ]);
    }

    public function getListOfClient()
    {
        $date_from = Input::get('_date_from');
        $date_to = Input::get('_date_to');

        return DB::table('purchase')
            ->join('representative', 'representative.id', '=', 'purchase.sponsor_id')
            ->join('client', 'client.id', '=', 'purchase.client_client_id')
            ->join('purchase_item_list', 'purchase_item_list.purchase_po_no', '=', 'purchase.id')
            ->join('item', 'item.id', '=', 'purchase_item_list.item_id')
            ->whereBetween('purchase.purchase_date', [$date_from, $date_to])
            ->select(
                DB::raw("IF(purchase.purchase_order_no = '','[TEMPORARY]',purchase.purchase_order_no) as purchase_order_number"),

                'purchase.purchase_date',
                'purchase.status',
                'client.last_name',
                'client.first_name',

                'representative.last_name as sponsor_last_name',
                'representative.first_name as sponsor_first_name',
                DB::raw('SUM(item_price*qty) as total_price'),
                DB::raw('SUM(IF(STRCMP(LOWER(purchase.agent_current_level),"associate"),
								item.production_points_associate,
								item.production_points)
								*qty) as total_points'))
            ->orderBy('purchase.purchase_date')
            ->groupBy('purchase.id')
            ->get();
    }

    public function topSellingProducts()
    {
        if (!isAccessModuleAllowed('top_selling_products')) {
            return Redirect::to('dashboard');
        }
        return View::make('report/report_top_product', [
            'purchases' => [],
            'page_title' => 'HCEMIOS Reports',
            'page_description' => 'Top Selling Products',
            'level' => 'Reports',
            'sub_level' => 'Top Selling Products',
            'icon' => 'fa-bank'
        ]);
    }

    public function getTopSellingProduct()
    {
        $date_from = Input::get('_date_from');
        $date_to = Input::get('_date_to');

        return DB::table('purchase_item_list')
            ->join('purchase', 'purchase.id', '=', 'purchase_item_list.purchase_po_no')
            ->join('item', 'item.id', '=', 'purchase_item_list.item_id')
            ->whereBetween('purchase.purchase_date', [$date_from, $date_to])
            ->select('item.item_code',
                'item.description',
                DB::raw('count(purchase_item_list.item_id) as purchase_count'))
            ->orderBy('purchase_count', 'desc')
            ->groupBy('item.id', 'item.description')
            ->get();
    }

    public function topSeller()
    {
        if (!isAccessModuleAllowed('top_seller_sponsor')) {
            return Redirect::to('dashboard');
        }
        return View::make('report/report_top_sellers', [
            'purchases' => [],
            'page_title' => 'HCEMIOS Reports',
            'page_description' => 'Top Sellers / Top Sponsors',
            'level' => 'Reports',
            'sub_level' => 'Top Sellers / Top Sponsors',
            'icon' => 'fa-bank'
        ]);
    }

    public function getTopSeller()
    {
        $date_from = Input::get('_date_from');
        $date_to = Input::get('_date_to');

        return DB::table('purchase')
            ->join('representative', function ($join) {
                $join->on('representative.id', '=', 'purchase.sponsor_id')
                    ->orOn('representative.id', '=', 'purchase.associate_id');
            })
            ->join('purchase_item_list', 'purchase_item_list.purchase_po_no', '=', 'purchase.id')
            ->join('item', 'item.id', '=', 'purchase_item_list.item_id')
            ->whereBetween('purchase.purchase_date', [$date_from, $date_to])
            ->select(
                'representative.last_name',
                'representative.first_name',
                'representative.current_level',
                'representative.representative_code',
                DB::raw('UPPER(representative.current_level) as current_level'),
                DB::raw('count(DISTINCT purchase.id) as purchase_count'),
                DB::raw('SUM(IF(
									purchase_item_list.gift="Yes",
									0,
									(
										IF(
											STRCMP(LOWER(purchase.agent_current_level),"associate"),
											item.production_points_associate,
											item.production_points
										)*qty
									)
								)
							) as total_points')

            )
            //->where('purchase_item_list.purchase_po_no','=','purchase.id')
            ->groupBy('purchase.sponsor_id')
            ->orderBy('total_points', DB::raw('FIELD("current_level","Distributor","Manager","Senior Consultant","Consultant","Associate")'))
            ->get();

        //return [$points,$purchase];
    }


    public function clientPurchases()
    {
        if (!isAccessModuleAllowed('client_purchases')) {
            return Redirect::to('dashboard');
        }
        return View::make('report/report_client_purchases', [
            'purchases' => [],
            'page_title' => 'HCEMIOS Reports',
            'page_description' => 'Client Purchase Report',
            'level' => 'Reports',
            'sub_level' => 'Client Purchase Report',
            'icon' => 'fa-shopping-cart'
        ]);
    }

    public function getClientPurchases()
    {
        $date_from = Input::get('_date_from');
        $date_to = Input::get('_date_to');
        $client_id = Input::get('client');
        // $date_from = $year.'-'.$month.'-1';
        // $date_to=$year.'-'.$month.'-'.cal_days_in_month(CAL_GREGORIAN,$month,$year);

        if ($date_from == "" || $date_from == null) {
            $date_from = '1990-01-01';
            $date_to = date('Y-m-d');
        }

        Session::put('date_from', $date_from);
        Session::put('date_to', $date_to);

        $purchase = DB::table('purchase')
            ->join('representative', 'representative.id', '=', 'purchase.sponsor_id')
            ->leftJoin('representative as presenter', 'presenter.id', '=', 'purchase.consultant_id')
            ->join('client', 'client.id', '=', 'purchase.client_client_id')
            ->whereBetween('purchase.purchase_date', [$date_from, $date_to])
            ->where('purchase.client_client_id', '=', $client_id)
            ->select(
                'representative.last_name',
                'representative.first_name',
                'presenter.last_name as presenter_last_name',
                'presenter.first_name as presenter_first_name',
                'client.last_name as client_last_name',
                'client.first_name as client_first_name',

                'purchase.status',
                'purchase.id',
                'purchase.purchase_order_no',
                'purchase.purchase_date')
            ->groupBy('purchase.id')
            ->get();

        $purchase_price = DB::table('purchase')
            ->join('representative', 'representative.id', '=', 'purchase.sponsor_id')
            ->leftJoin('representative as presenter', 'presenter.id', '=', 'purchase.consultant_id')
            ->join('client', 'client.id', '=', 'purchase.client_client_id')
            ->join('purchase_item_list', 'purchase_item_list.purchase_po_no', '=', 'purchase.id')
            ->join('item', 'item.id', '=', 'purchase_item_list.item_id')
            ->where('purchase.client_client_id', '=', $client_id)
            ->whereBetween('purchase.purchase_date', [$date_from, $date_to])
            ->select(
                'representative.last_name',
                'representative.first_name',
                'presenter.last_name as presenter_last_name',
                'presenter.first_name as presenter_first_name',
                'client.last_name as client_last_name',
                'client.first_name as client_first_name',

                DB::raw('SUM(item_price*qty) as amount'),

                'purchase.id',
                'purchase.purchase_order_no',
                'purchase.purchase_date')
            ->groupBy('purchase.id')
            ->get();

        return [$purchase, $purchase_price];
    }


}