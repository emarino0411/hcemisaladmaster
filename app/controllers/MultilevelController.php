<?php

class MultilevelController extends \BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */

    public function index()
    {
        if (!isAccessModuleAllowed('multilevel_view')) {
            return Redirect::to('dashboard');
        }

        $agents = DB::table('representative')
            ->select('representative.id', 'representative.status', 'representative.last_name', 'representative.first_name', 'representative.representative_code',
                'representative.current_level')
            ->get();

        $page_title = 'HCEMIOS Multilevel and Promotions';
        $page_description = 'Going Up!';
        $level = 'Multilevel and Promotions';
        $sub_level = 'Agents List';
        $icon = 'fa fa-users';

        return View::make('multilevel/multilevel_promotion_agents', compact('agents', 'page_title', 'page_description', 'level', 'sub_level', 'icon', 'roles'));
    }


    public function addNew()
    {

    }

    public function savePromotion()
    {
        if (!isAccessModuleAllowed('multilevel_update')) {
            return Redirect::to('dashboard');
        }
        //check if there are same date promotion
        $is_existing = DB::table('multilevel')
            ->where(function ($query) {
                $query->where('agent_id', '=', Input::get('agent_id'));
                $query->where('level_from', '=', Input::get('level_from'));
                $query->where('level_to', '=', Input::get('level_to'));
            })
            ->orWhere(function ($query) {
                $query->where('agent_id', '=', Input::get('agent_id'));
                $query->where('level_to', '=', Input::get('level_to'));
                $query->where('type', '=', 'INIT');
            })
            ->orWhere(function ($query) {
                $query->where('agent_id', '=', Input::get('agent_id'));
                $query->where('effective_date', '=', Input::get('effectivity_date'));
            })
            ->get();

        if (count($is_existing) > 0) {
            return 'PROMOTION_EXISTING';
        }

        // get latest promotion
        $latest = DB::table('multilevel')
            ->where('effective_date', '<', Input::get('effectivity_date'))
            ->where('agent_id', '=', Input::get('agent_id'))
            ->where('type', '!=', 'INIT')
            ->orderBy('updated_at', 'DESC')
            ->get();

        if (count($latest) > 0) {
            if (Input::get('level_to') == 'consultant' && $latest[0]->level_to != 'associate') {
                return 'INVALID_PROMOTION';
            } else if (Input::get('level_to') == 'senior consultant' && $latest[0]->level_to != 'consultant') {
                return 'INVALID_PROMOTION';
            } else if (Input::get('level_to') == 'distributor' && $latest[0]->level_to != 'senior consultant') {
                return 'INVALID_PROMOTION';
            } else if (Input::get('level_to') == 'manager' && $latest[0]->level_to != 'distributor') {
                return 'INVALID_PROMOTION';
            } else {

            }
        }


        DB::table('multilevel')
            ->where('agent_id', '=', Input::get('agent_id'))
            ->update(array('is_latest' => 0));

        $new_promotion = new Multilevel();
        $new_promotion->effective_date = Input::get('effectivity_date');
        $new_promotion->agent_id = Input::get('agent_id');
        $new_promotion->approved_by = Input::get('approved_by');
        $new_promotion->level_from = Input::get('level_from');
        $new_promotion->level_to = Input::get('level_to');
        $new_promotion->is_latest = 1;
        $new_promotion->save();

        DB::table('purchase')
            ->where('purchase_date', '>', $new_promotion->effective_date)
            ->update(array('agent_current_level' => lower($new_promotion->level_to)));
    }

    public function savePhase()
    {
        if (!isAccessModuleAllowed('multilevel_update')) {
            return Redirect::to('dashboard');
        }

        $new_promotion = new Multilevel();
        $new_promotion->type = Input::get('phase');
        $new_promotion->agent_id = Input::get('agent_id');
        $new_promotion->approved_by = Input::get('approved_by');
        $new_promotion->start_date = Input::get('phase_start');
        $new_promotion->end_date = Input::get('phase_end');
        $new_promotion->save();
        return $new_promotion;

    }

    public function history($id, $edit_mode = '')
    {
        if (!isAccessModuleAllowed('multilevel_view')) {
            return Redirect::to('dashboard');
        }

        if ((!isAccessModuleAllowed('agents_edit')) && $edit_mode != "") {
            return Redirect::to('dashboard');
        }

        $agent = Agent::find($id);

        $agent->city = DB::table('rcitymun')
            ->join('rprov', 'rcitymun.provcode', '=', 'rprov.provcode')
            ->select('cityname', 'provname', 'rcitymun.citycode')
            ->where('rcitymun.citycode', '=', $agent->city)
            ->orderBy('cityname', 'asc')
            ->orderBy('provname', 'asc')
            ->get();

        $agent->province = DB::table('rprov')
            ->select('provcode', 'provname')
            ->where('rprov.provcode', '=', $agent->province)
            ->orderBy('provname', 'asc')
            ->get();

        $agent->sponsor = DB::table('representative')
            ->select('representative.last_name as sponsor_last', 'representative.first_name as sponsor_first')
            ->where('representative.id', '=', $agent->sponsor_id)
            ->get();

        $agent->sponsor = (count($agent->sponsor) == 0) ? ["sponsor_last"=>"","sponsor_first"=>"N/A"] :
            ["sponsor_last"=>$agent->sponsor[0]->sponsor_last,"sponsor_first"=>$agent->sponsor[0]->sponsor_first];

        $first_booking = DB::table('bookings')
            ->join('booking_reps', 'booking_reps.booking_id', '=', 'bookings.id')
            ->where('booking_reps.representative', '=', $agent->id)
            ->orderBy('bookings.booking_date', 'ASC')
            ->take(1)
            ->get();

        $associate_bookings = DB::table('bookings')
            ->join('booking_reps', 'booking_reps.booking_id', '=', 'bookings.id')
            ->leftJoin('purchase','purchase.purchase_order_no','=','bookings.po_no')
            // ->leftJoin('purchase_item_list','purchase_item_list.purchase_po_no','=','purchase.id')
            // ->leftJoin('item','item.id','=','purchase_item_list.item_id')
            ->select('bookings.id as booking_id','bookings.booking_date','purchase.purchase_order_no','purchase.remarks')
            ->where('booking_reps.representative', '=', $agent->id)
            ->where('bookings.status','=','COOKED')
            ->orderBy('bookings.booking_date', 'ASC')
            ->get();

        $po_no_list = array_pluck($associate_bookings,'purchase_order_no');

        $sold_items = DB::table('bookings')
            ->join('purchase','purchase.purchase_order_no','=','bookings.po_no')
            ->join('purchase_item_list','purchase_item_list.purchase_po_no','=','purchase.id')
            ->join('item','item.id','=','purchase_item_list.item_id')
            ->select('item.description','purchase.purchase_order_no','purchase.remarks')
            ->whereIn('purchase.purchase_order_no', $po_no_list)
            ->where('purchase_item_list.gift','=','NO')

            ->get();

        if(count($first_booking) == 0){
            $agent->first_presentation = $agent->first_presentation == null ? "N/A" : $agent->first_presentation;
        }else{
            $agent->first_presentation = $agent->first_presentation == null ? $first_booking[0]->booking_date : $agent->first_presentation;
        }


        $page_title = 'HCEMIOS Multilevel and Promotions';
        $page_description = 'Going Up!';
        $level = 'Multilevel and Promotions';
        $sub_level = 'Agents Details';
        $icon = 'fa fa-users';

        $municipalities = DB::table('rcitymun')
            ->join('rprov', 'rcitymun.provcode', '=', 'rprov.provcode')
            ->select(DB::raw('concat(cityname," [",provname,"]") as cityname'), 'provname', 'rcitymun.citycode')
            ->orderBy('cityname', 'asc')
            ->orderBy('provname', 'asc')
            ->get();

        $provinces = DB::table('rprov')
            ->select('provcode', 'provname')
            ->orderBy('provname', 'asc')
            ->get();

        $multilevels = DB::table('multilevel')
            ->where('agent_id', '=', $id)
            ->where(function ($query) {
                $query->whereNull('type')
                    ->orWhere('type', '=', 'INIT');
            })
            ->get();

        $multilevels_associate = DB::table('multilevel')
            ->where('agent_id', '=', $id)
            ->where('type', '!=', 'INIT')
            ->get();

        // set view
        return View::make('multilevel/multilevel_promotion_history', compact('provinces', 'municipalities', 'agent', 'page_title',
            'page_description', 'level', 'sub_level', 'icon', 'edit_mode', 'purchases', 'wsp_commissions', 'id',
            'total_balance', 'multilevels', 'multilevels_associate','associate_bookings','sold_items'));
    }

    public function getSales()
    {
        $sales = [];
        $level = Input::get('level');
        $date_from = Input::get('date_from');
        $date_to = Input::get('date_to');
        $agent_id = Input::get('agent_id');
        $group = [];

        // get the current level of the agent
        $current_level = DB::table('multilevel')
            ->where('agent_id', '=', $agent_id)
            ->where('effective_date', '<', date('Y-m-d'))
            ->orderBy('updated_at', 'desc')
            ->limit(1)
            ->get();

        if ($current_level == null) {
            $agent = DB::table('representative')
                ->where('id', '=', $agent_id)
                ->get();
            $current_level = $agent[0]->current_level;
        } else {
            $current_level = $current_level[0]->level_to;
        }

        Session::put('agent_id', Input::get('agent_id'));

        $personal_sales = DB::table('purchase')
            ->leftJoin('representative', 'representative.id', '=', 'purchase.sponsor_id')
            ->leftJoin('purchase_item_list', 'purchase.id', '=', 'purchase_item_list.purchase_po_no')
            ->leftJoin('item', 'item.id', '=', 'purchase_item_list.item_id')
            ->select(DB::raw('SUM(IF("' . $current_level . '"="associate",item.production_points_associate,item.production_points)*purchase_item_list.qty) as point'),
                'purchase.fast_track',
                'purchase.sponsor_id',
                'purchase.associate_id'
            )
            ->where(function ($query) {
                $query->where('purchase.sponsor_id', '=', Session::get('agent_id'))
                    ->orWhere('purchase.associate_id', '=', Session::get('agent_id'));
            })
            ->whereBetween('purchase.purchase_date', [$date_from, $date_to])
            ->orderBy('purchase.updated_at', 'desc')
            ->get();

        $client_purchase = [];
        $ctr = 0;
        foreach ($personal_sales as $purchase) {
            if (($purchase->associate_id ==  Input::get('agent_id') && $purchase->fast_track == 'Yes') || ($purchase->sponsor_id == $id)) {
                $client_purchase[$ctr] = $purchase;
            }
            $ctr++;
        }
        $personal_sales = $client_purchase;

        $sales['personal'] = array_pluck($personal_sales, 'point')[0];
        if ($personal_sales == null) {
            $sales['personal'] = 0;
        }
        $ctr = 0;
        $total_points = 0;
        if ($level == 'ALL') {
            $group = DB::table('representative')
                ->where('sponsor_id', '=', $agent_id)
                ->get();
            $dealers = array_pluck($group, 'id');

            if (count($group) > 0) {

                while ($group != null) {

                    Session::put('dealers', $dealers);

                    $group_sales = DB::table('purchase')
                        ->leftJoin('representative', 'representative.id', '=', 'purchase.sponsor_id')
                        ->leftJoin('purchase_item_list', 'purchase.id', '=', 'purchase_item_list.purchase_po_no')
                        ->leftJoin('item', 'item.id', '=', 'purchase_item_list.item_id')
                        ->leftJoin('multilevel', function ($join) {
                            $join->on('multilevel.agent_id', '=', 'purchase.sponsor_id')->where('multilevel.is_latest', '=', '1');
                        })
                        ->select(DB::raw('SUM(IF(multilevel.level_to="associate",item.production_points_associate,item.production_points)*purchase_item_list.qty) as point'),
                            'purchase.fast_track',
                            'purchase.sponsor_id',
                            'purchase.associate_id'
                        )
                        ->whereIn('purchase.sponsor_id', $dealers)
                        ->where(function ($query) {
                            $query->whereIn('purchase.sponsor_id', '=', Session::get('dealers'))
                                ->orWhereIn('purchase.associate_id', '=', Session::get('dealers'));
                        })
                        ->whereBetween('purchase.purchase_date', [$date_from, $date_to])
                        ->orderBy('purchase.updated_at', 'desc')
                        ->get();

                    $client_purchase = [];
                    $ctr = 0;
                    foreach ($group_sales as $purchase) {
                        if (($purchase->associate_id == $id && $purchase->fast_track == 'Yes') || ($purchase->sponsor_id == $id)) {
                            $client_purchase[$ctr] = $purchase;
                        }
                        $ctr++;
                    }
                    $group_sales = $client_purchase;

                    if ($group_sales) {
                        $total_points += array_sum(array_pluck($group_sales, 'point'));
                    }

                    $group = DB::table('representative')
                        ->whereIn('sponsor_id', $dealers)
                        ->get();

                    $dealers = array_pluck($group, 'id');
                }

            }

            $sales['group'] = $total_points;
        } else if ($level > 0) {

            $group = DB::table('representative')
                ->where('sponsor_id', '=', $agent_id)
                ->get();
            $dealers = array_pluck($group, 'id');

            if (count($group) > 0) {

                while ($ctr < $level) {

                    Session::put('dealers', $dealers);

                    $group_sales = DB::table('purchase')
                        ->leftJoin('representative', 'representative.id', '=', 'purchase.sponsor_id')
                        ->leftJoin('purchase_item_list', 'purchase.id', '=', 'purchase_item_list.purchase_po_no')
                        ->leftJoin('item', 'item.id', '=', 'purchase_item_list.item_id')
                        ->leftJoin('multilevel', function ($join) {
                            $join->on('multilevel.agent_id', '=', 'purchase.sponsor_id')->where('multilevel.is_latest', '=', '1');
                        })
                        ->select(DB::raw('SUM(IF(multilevel.level_to="associate",item.production_points_associate,item.production_points)*purchase_item_list.qty) as point'),
                            'purchase.fast_track',
                            'purchase.sponsor_id',
                            'purchase.associate_id'
                        )
                        ->whereIn('purchase.sponsor_id', $dealers)
                        ->where(function ($query) {
                            $query->whereIn('purchase.sponsor_id', '=', Session::get('dealers'))
                                ->orWhereIn('purchase.associate_id', '=', Session::get('dealers'));
                        })
                        ->whereBetween('purchase.purchase_date', [$date_from, $date_to])
                        ->orderBy('purchase.updated_at', 'desc')
                        ->get();

                    $client_purchase = [];
                    $ctr = 0;
                    foreach ($group_sales as $purchase) {
                        if (($purchase->associate_id == $id && $purchase->fast_track == 'Yes') || ($purchase->sponsor_id == $id)) {
                            $client_purchase[$ctr] = $purchase;
                        }
                        $ctr++;
                    }
                    $group_sales = $client_purchase;

                    if ($group_sales) {
                        $total_points += array_sum(array_pluck($group_sales, 'point'));
                    }

                    $group = DB::table('representative')
                        ->whereIn('sponsor_id', $dealers)
                        ->get();

                    $dealers = array_pluck($group, 'id');
                    $ctr++;
                }

            }

            $sales['group'] = $total_points;

        } else {
            $sales['group'] = 0;
        }

        $sales['total'] = $sales['personal'] + $sales['group'];


        return $sales;
    }

    public function getAssociateSales()
    {
        $sales = [];
        $date_from = Input::get('date_from');
        $date_to = Input::get('date_to');
        $agent_id = Input::get('agent_id');
        $group = [];

        // get the current level of the agent
        $current_level = DB::table('multilevel')
            ->where('agent_id', '=', $agent_id)
            ->where('effective_date', '<', date('Y-m-d'))
            ->orderBy('updated_at', 'desc')
            ->limit(1)
            ->get();

        if ($current_level == null) {
            $agent = DB::table('representative')
                ->where('id', '=', $agent_id)
                ->get();
            $current_level = $agent[0]->current_level;
        } else {
            $current_level = $current_level[0]->level_to;
        }


        Session::put('agent_id', Input::get('agent_id'));

        $personal_sales = DB::table('purchase')
            ->leftJoin('representative', 'representative.id', '=', 'purchase.sponsor_id')
            ->leftJoin('purchase_item_list', 'purchase.id', '=', 'purchase_item_list.purchase_po_no')
            ->leftJoin('item', 'item.id', '=', 'purchase_item_list.item_id')
            ->select(DB::raw('SUM(IF("' . $current_level . '"="associate",item.production_points_associate,item.production_points)*purchase_item_list.qty) as point'),
                'purchase.fast_track',
                'purchase.sponsor_id',
                'purchase.associate_id'
            )
            ->where(function ($query) {
                $query->where('purchase.sponsor_id', '=', Session::get('agent_id'))
                    ->orWhere('purchase.associate_id', '=', Session::get('agent_id'));
            })
            ->whereBetween('purchase.purchase_date', [$date_from, $date_to])
            ->orderBy('purchase.updated_at', 'desc')
            ->get();

        $client_purchase = [];
        $ctr = 0;
        foreach ($purchases as $purchase) {
            if (($purchase->associate_id == $id && $purchase->fast_track == 'Yes') || ($purchase->sponsor_id == $id)) {
                $client_purchase[$ctr] = $purchase;
            }
            $ctr++;
        }
        $personal_sales = $client_purchase;


        $sales['sales'] = array_pluck($personal_sales, 'point')[0];

        if ($sales['sales'] == null) {
            $sales['sales'] = 0;
        }
        return $sales;
    }

    public function getPhaseDetails()
    {
        $id = Input::get('id');

        $details = DB::table('multilevel')
            ->where('id', '=', $id)
            ->get();

        return $details;
    }


}
