<?php

class BookingsController extends \BaseController
{

    public function calendarView()
    {
        if (!isAccessModuleAllowed('bookings_view')) {
            return Redirect::to('dashboard');
        }

        $bookings = DB::table('bookings')
            ->join('booking_reps', 'booking_reps.booking_id', '=', 'bookings.id')
            ->join('representative', function($join)
            {
                $join->on('representative.id', '=', 'booking_reps.representative')
                     ->where('booking_reps.type', '=', 'consultant');
            })
            ->select(DB::raw('bookings.id,
                  concat(

                      bookings.id,
                      ":",
                      event_name,
                      " | ",
                      UPPER(last_name),
                      IF(fast_track!="false"," | FT",""),
                      IF(remarks!="",CONCAT(" |", UPPER(bookings.remarks)),"")

                  ) as title,
                  booking_date,
                  concat(booking_date," ",booking_time) as start,
                  concat(booking_date," ",booking_time_end) as end,
                  color as backgroundColor,
                  color as borderColor
                  '))
            //->where('booking_reps.type', '=', 'consultant')
            ->get();

        return View::make('bookings/bookings_calendar', [
            'page_title' => 'HCEMIOS Bookings',
            'page_description' => 'Plan your work. work your plan',
            'level' => 'Bookings',
            'sub_level' => 'Calendar',
            'icon' => 'fa-calendar',
            'bookings' => $bookings
        ]);
    }

    public function create()
    {
        $booking = new Booking();
        $booking->event_name = Input::get('event_name');
        $booking->street = Input::get('street');
        $booking->city = Input::get('city');
        $booking->province = Input::get('province');
        $booking->status = 'Pending';
        $booking->client = Input::get('client');
        $booking->color = Input::get('color');
        $booking->remarks = Input::get('remarks');
        $booking->contact = Input::get('contact');
        $booking->time = Input::get('time');
        $booking->status = Input::get('status');
        $booking->fast_track = Input::get('fast_track');
        $booking->telethon = Input::get('telethon');
        if (Input::get('booking_date') == "") {
            $booking->booking_date = date('Y-m-d');

        } else {
            $booking->booking_date = Input::get('booking_date');
        }

        if (Input::get('booking_time') == "") {
            $booking->booking_time = date('H:i:s');
        } else {
            $booking->booking_time = Input::get('booking_time');
        }

        $booking->save();

        $representative = Input::get('representative');
        if ($representative != "") {
            $booking_rep = new BookingRepresentative();
            $booking_rep->booking_id = $booking->id;
            $booking_rep->representative = $representative;
            $booking_rep->type = 'presenter';

            $booking_rep->save();
        }

        $booker = Input::get('booker');
        if ($booker != "") {
            $booking_rep = new BookingRepresentative();
            $booking_rep->booking_id = $booking->id;
            $booking_rep->representative = $booker;
            $booking_rep->type = 'associate';
            $booking_rep->save();
        }

        $consultant = Input::get('consultant');
        if ($consultant != "") {
            $booking_rep = new BookingRepresentative();
            $booking_rep->booking_id = $booking->id;
            $booking_rep->representative = $consultant;
            $booking_rep->type = 'consultant';
            $booking_rep->save();
            $consultant_data = Agent::where('id', '=', $consultant)->get();
        }
        $consultant_data = $consultant_data[0];

        $assistant = Input::get('assistant');
        if ($assistant != "") {
            $booking_rep = new BookingRepresentative();
            $booking_rep->booking_id = $booking->id;
            $booking_rep->representative = $assistant;
            $booking_rep->type = 'assistant';
            $booking_rep->save();
        }

        if ($booking->fast_track=='true') {
            $data = '#' . $booking->id . ': ' . $booking->event_name . ' | ' . ucwords($consultant_data->last_name) . ' | FT' . " | ".ucwords($booking->remarks);
        } else {
            $data = '#' . $booking->id . ': ' . $booking->event_name . ' | ' . ucwords($consultant_data->last_name) . ' | ' . ucwords($booking->remarks);
        }

        return $data;
    }

    public function updateDate()
    {
        $booking = Booking::find(Input::get('id'));
        $booking->booking_date = Input::get('booking_date');
        $booking->booking_time = Input::get('booking_time');
        $booking->booking_time_end = Input::get('booking_time_end');
        $booking->update();
    }

    public function getBookingDetails()
    {
        $booking_id = Input::get('bookingId');
        $booking = DB::table('bookings')
            ->leftJoin('booking_reps', 'bookings.id', '=', 'booking_reps.booking_id')
            ->join('representative', 'representative.id', '=', 'booking_reps.representative')
            ->select('bookings.event_name', 'bookings.street', 'bookings.city', 'bookings.client', 'bookings.province', 'bookings.booking_date', 'bookings.booking_time', 'bookings.time',
                'representative.last_name', 'representative.first_name', 'bookings.po_no', 'bookings.remarks', 'bookings.status')
            ->where('bookings.id', '=', $booking_id)
            ->where('booking_reps.type', '=', 'consultant')
            ->get();

        $agents = DB::table('booking_reps')
            ->join('representative', 'representative.id', '=', 'booking_reps.representative')
            ->select('booking_reps.type', 'representative.last_name', 'representative.first_name')
            ->where('booking_reps.booking_id', '=', $booking_id)
            ->get();
        $result = [$booking, $agents];
        return $result;
    }

    public function cancelBooking()
    {
        $booking = Booking::find(Input::get('bookingId'));
        $booking_rep = BookingRepresentative::where('booking_id', '=', $booking->id);

        $booking->delete();
        $booking_rep->delete();
    }

    public function cancelBulkBooking(){
        $booking = Booking::whereIn('id',Input::get('update_items'));
        $booking_rep = BookingRepresentative::whereIn('id',Input::get('update_items'));

        $booking->delete();
        $booking_rep->delete();
    }

    public function cookedBulkBooking(){
        DB::table('bookings')
            ->whereIn('id',Input::get('update_items'))
            ->update(array('status' => 'Cooked'));
    }

    public function searchRepresentative()
    {
        $term = Input::get('q');
        $type = Input::get('type');

        $consultant = Input::get('const');

        $assistant = Input::get('asst');
        $presenter = Input::get('booker');
        $sponsor = Input::get('spons');

        if ($type == 'consultant') {
            $clients = Agent::where(function ($query) {
                $query->where(DB::raw('LOWER(last_name)'), 'like', '%' . strtolower(Input::get('q')) . '%')
                    ->orWhere(DB::raw('LOWER(first_name)'), 'like', '%' . strtolower(Input::get('q')) . '%');
            })
                ->whereNotIn('id', [$assistant, $presenter, $sponsor])
                ->select(DB::raw('UPPER(CONCAT(last_name,", ",first_name)) as text, id as id'))
                ->get();
        } else if ($type == 'assistant') {
            $clients = Agent::where(function ($query) {
                $query->where(DB::raw('LOWER(last_name)'), 'like', '%' . strtolower(Input::get('q')) . '%')
                    ->orWhere(DB::raw('LOWER(first_name)'), 'like', '%' . strtolower(Input::get('q')) . '%');
            })
                ->whereNotIn('id', [$consultant, $presenter, $sponsor])
                ->select(DB::raw('UPPER(CONCAT(last_name,", ",first_name)) as text, id as id'))
                ->get();
        } else if ($type == 'presenter') {
            $clients = Agent::where(function ($query) {
                $query->where(DB::raw('LOWER(last_name)'), 'like', '%' . strtolower(Input::get('q')) . '%')
                    ->orWhere(DB::raw('LOWER(first_name)'), 'like', '%' . strtolower(Input::get('q')) . '%');
            })
                ->whereNotIn('id', [$consultant, $assistant, $sponsor])
                ->select(DB::raw('UPPER(CONCAT(last_name,", ",first_name)) as text, id as id'))
                ->get();
        } else if ($type == 'sponsor') {
            $clients = Agent::where(function ($query) {
                $query->where(DB::raw('LOWER(last_name)'), 'like', '%' . strtolower(Input::get('q')) . '%')
                    ->orWhere(DB::raw('LOWER(first_name)'), 'like', '%' . strtolower(Input::get('q')) . '%');
            })
                ->whereNotIn('id', [$consultant, $assistant, $presenter])
                ->select(DB::raw('UPPER(CONCAT(last_name,", ",first_name)) as text, id as id'))
                ->get();
        } else {
            $clients = Agent::where(DB::raw('LOWER(last_name)'), 'like', '%' . strtolower($term) . '%')
                ->orWhere(DB::raw('LOWER(first_name)'), 'like', '%' . strtolower($term) . '%')
                ->select(DB::raw('UPPER(CONCAT(last_name,", ",first_name)) as text, id as id'))
                ->get();
        }


        return $clients;
    }

    public function searchConsultantSponsor()
    {
        $clients = Agent::where('current_level', '!=', 'associate')
            ->where(function ($query) {
                $query->where(DB::raw('LOWER(last_name)'), 'like', '%' . strtolower(Input::get('q')) . '%')
                    ->orWhere(DB::raw('LOWER(first_name)'), 'like', '%' . strtolower(Input::get('q')) . '%');
            })
            ->where('id', '!=', Input::get('not'))
            ->select(DB::raw('UPPER(CONCAT(last_name,", ",first_name)) as text, id as id'))
            ->get();
        return $clients;
    }

    public function searchAssociate()
    {
        $clients = Agent::where('current_level', '=', 'associate')
            ->where(function ($query) {
                $query->where(DB::raw('LOWER(last_name)'), 'like', '%' . strtolower(Input::get('q')) . '%')
                    ->orWhere(DB::raw('LOWER(first_name)'), 'like', '%' . strtolower(Input::get('q')) . '%');
            })
            ->select(DB::raw('UPPER(CONCAT(last_name,", ",first_name)) as text, id as id'))
            ->get();
        return $clients;
    }

    public function searchRepresentativeForCookDriver()
    {
        $clients = Agent::where(function ($query) {
            $query->where(DB::raw('LOWER(last_name)'), 'like', '%' . strtolower(Input::get('q')) . '%')
                ->orWhere(DB::raw('LOWER(first_name)'), 'like', '%' . strtolower(Input::get('q')) . '%');
        })
            ->where('id', '!=', Input::get('not'))
            ->select(DB::raw('UPPER(CONCAT(last_name,", ",first_name)) as text, id as id'))
            ->get();
        return $clients;
    }

    public function searchRepresentativeNotInPurchase($id)
    {
        $representatives = Purchase::where('id', '=', $id)
            ->select('associate_id', 'sponsor_id', 'consultant_id')
            ->get();

        foreach ($representatives as $representative) {
            $clients = Agent::where('id', '<>', $representative->associate_id)
                ->where(function ($query) {
                    $query->where(DB::raw('LOWER(last_name)'), 'like', '%' . strtolower(Input::get('q')) . '%')
                        ->orWhere(DB::raw('LOWER(first_name)'), 'like', '%' . strtolower(Input::get('q')) . '%');
                })
                ->where('id', '<>', $representative->sponsor_id)
                ->where('id', '<>', $representative->consultant_id)
                ->select(DB::raw('UPPER(CONCAT(last_name,", ",first_name)) as text, id as id'))
                ->get();
            return $clients;
        }


    }

    public function index()
    {
        $bookings = DB::table('bookings')
            ->select(DB::raw('bookings.id,
                                      concat("#",bookings.id,":",bookings.event_name) as title,
                                      bookings.booking_date,
                                      bookings.booking_time,
                                      IF(bookings.fast_track="False","No","Yes") as fast_track,
                                      bookings.remarks ,
                                      bookings.status ,
                                      bookings.telethon,
                                      bookings.booking_time,
                                      bookings.booking_time_end,
                                      bookings.street,
                                      bookings.city,
                                      bookings.province,
                                      concat(bookings.booking_date," ",bookings.booking_time) as start,
                                      concat(bookings.booking_date," ",bookings.booking_time_end) as end,
                                      bookings.color as backgroundColor,
                                      bookings.color as borderColor,
                                      IFNULL(bookings.po_no,"N/A") as po_no
                                      '))
            ->get();

        $agents = DB::table('bookings')
            ->select(DB::raw('bookings.id,
                              IFNULL(representative.last_name,"N/A") as last_name,
                              IFNULL(representative.first_name,"N/A") as first_name,
                              booking_reps.type
                              ')
            )
            ->leftJoin('booking_reps', 'bookings.id', '=', 'booking_reps.booking_id')
            ->join('representative', 'representative.id', '=', 'booking_reps.representative')
            ->get();

        return View::make('bookings/bookings_list', [
            'page_title' => 'HCEMIOS Bookings',
            'page_description' => 'Here are our schedules for today? :)',
            'level' => 'Bookings',
            'sub_level' => 'List',
            'icon' => 'fa-calendar',
            'bookings' => $bookings,
            'agents' => $agents
        ]);
    }

    public function updateBookingDetails()
    {
        $booking = Booking::find(Input::get('bookingId'));
        $booking->remarks = Input::get('remarks');
        $booking->status = Input::get('status');
        $booking->po_no = Input::get('po_no');
        $booking->save();
    }

}
