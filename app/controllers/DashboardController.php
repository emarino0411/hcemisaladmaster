<?php

class DashboardController extends \BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

        $agents_count = Agent::where('status', '=', 'ACTIVE')->count();
        $po_count = Purchase::where('transaction_status', '=', 'Approved')->count();
        $bookings_count = Booking::where('booking_date', '=', date('Y-m-d'))->count();
        $due_payment = Payment::where('check_date','<=', date('Y-m-d'))->where('status','=','For Deposit')->count();
        $approved_commissions = Commission::where('status','=','DISTRIBUTED')->count();
        $promo_qualifiers = 0;
        $checks_for_follow_up = Payment::where('check_date','<',date('Y-m-d'))->count();
        $for_pull_out = Purchase::where('status', '=', 'For Pull Out')->count();

        $purchases = DB::table('purchase')
                ->leftJoin('client', 'client.id', '=', 'purchase.client_client_id')
                ->select('purchase.purchase_order_no','client.last_name', 'client.first_name', 'purchase.id', 'purchase.purchase_date', 'purchase.transaction_status', 'purchase.status')
                ->take(10)
                ->get();

        $bookings = DB::table('bookings')
                ->select(DB::raw('id,
                                      concat("#",id,":",event_name) as title,
                                      booking_date,
                                      concat(booking_date," ",booking_time) as start,
                                      concat(booking_date," ",booking_time_end) as end,
                                      color as backgroundColor,
                                      color as borderColor
                                      '))
                ->get();

        return View::make('dashboard', [
                'page_title' => 'HCEMIOS Dashboard',
                'page_description' => 'Control Panel',
                'level' => 'Dashboard',
                'sub_level' => 'Home',
                'agents_count' => $agents_count,
                'po_count' => $po_count,
                'bookings_count' => $bookings_count,
                'due_payments' => $due_payment,
                'approved_commissions' => $approved_commissions,
                'promo_qualifiers' => $promo_qualifiers,
                'checks_for_follow_up' => $checks_for_follow_up,
                'for_pull_out' => $for_pull_out,
                'bookings' => $bookings,
            'purchases'=>$purchases
        ]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }


}
