<?php

class ProductsController extends \BaseController
{

    public function index()
    {
        if(!isAccessModuleAllowed('products_view')){
            return Redirect::to('dashboard');
        }

        $page_title = 'HCEMIOS Products';
        $page_description = 'Products and Sets';
        $level = 'Products and Sets';
        $sub_level = 'List';
        $icon = 'fa fa-cutlery';

        $products = Product::all();

        return View::make('products/products_list', compact('products', 'page_title',
                'page_description', 'level', 'sub_level', 'icon', 'roles', 'edit_mode'));
    }

    public function addProduct()
    {
        if(!isAccessModuleAllowed('products_add')){
            return Redirect::to('dashboard');
        }

        $page_title = 'HCEMIOS Products';
        $page_description = 'Products and Sets';
        $level = 'Products and Sets';
        $sub_level = 'Add New Products';
        $icon = 'fa fa-cutlery';


        return View::make('products/products_add', compact('page_title',
                'page_description', 'level', 'sub_level', 'icon', 'edit_mode'));

    }

    public function create()
    {

        if(!isAccessModuleAllowed('products_add')){
            return Redirect::to('dashboard');
        }

        $product = new Product();
        $product->item_code = Input::get('item_code');
        $product->description = Input::get('description');
        $product->retail_price = Input::get('retail_price');
        $product->inventory_price = Input::get('inventory_price');
        $product->category = Input::get('category');
        $product->type = Input::get('product_type');
        $product->product_type = Input::get('product_type');
        $product->production_points = Input::get('production_points');
        $product->production_points_associate = Input::get('production_points_associate');
        $product->spv = Input::get('spv');


        $product->save();
        $this->index();
    }

    public function viewProduct($id, $edit_mode = '')
    {
        if(!isAccessModuleAllowed('products_view')){
            return Redirect::to('dashboard');
        }

        if ((!isAccessModuleAllowed('products_edit')) && $edit_mode != "") {
            return Redirect::to('dashboard');
        }

        $page_title = 'HCEMIOS Products';
        $page_description = 'Just checking out our products';
        $level = 'Products and Sets';
        $sub_level = 'View Product Details';
        $icon = 'fa fa-cutlery';
        $product = Product::find($id);

        return View::make('products/products_profile', compact('product', 'page_title',
                'page_description', 'level', 'sub_level', 'icon', 'edit_mode'));
    }

    public function update()
    {
        $product = Product::find(Input::get('id'));
        $product->item_code = Input::get('item_code');
        $product->description = Input::get('description');
        $product->retail_price = Input::get('retail_price');
        $product->inventory_price = Input::get('inventory_price');
        $product->category = Input::get('category');
        $product->type = Input::get('product_type');

        $product->production_points = Input::get('production_points');
        $product->production_points_associate = Input::get('production_points_associate');
        $product->spv = Input::get('spv');

        $product->save();
    }

    public function delete()
    {
        $user = Product::find(Input::get('productId'));
        $user->status = 'DEACTIVATED';
        $user->update();
        if ($user->update()) {
            return 'PRODUCT_DELETED';
        } else {
            return 'Error';
        }
    }

    public function activate()
    {
        $user = Product::find(Input::get('productId'));
        $user->status = 'ACTIVE';
        $user->update();
        if ($user->update()) {
            return 'PRODUCT_ACTIVATED';
        } else {
            return 'Error';
        }
    }

    public function getProductsByType()
    {

        return $products = Product::where(DB::raw('LOWER(description)'), 'like', '%' . strtolower(Input::get('q')) . '%')
                ->orWhere(DB::raw('LOWER(item_code)'), 'like', '%' . strtolower(Input::get('q')) . '%')
                ->select(DB::raw('UPPER(description) as text, id as id'))
                ->get();

    }


}
