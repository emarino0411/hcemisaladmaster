<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('purchases/searchPO', 'PurchaseController@searchPO');

Route::get('/', function () {
    return Redirect::to('users/login');
});
Route::get('/chat', function () {
    return View::make('chat');
});

Route::get('login', ['as' => 'login', function () {
    return Redirect::to('users/login');
}]);

/*
LOGIN
*/
/* User Authentication */
Route::get('users/login', 'UserController@getLogin');
Route::post('users/login', 'UserController@postLogin');
Route::get('users/logout', 'UserController@getLogout');

Route::group(array('before' => 'auth'), function () {
    Route::get('users/register', 'UserController@getRegister');
    Route::post('users/register', 'UserController@postRegister');

    /*
    Dashboard
    */
    Route::get('home', function () {
        return Redirect::to('dashboard');
    });
    Route::get('dashboard', 'DashboardController@index');

    Route::get('purchases', 'PurchaseController@index');
    Route::get('purchases/addNew', 'PurchaseController@addNew');
    Route::post('purchases/createPOItem', 'PurchaseController@createPOItem');
    Route::get('purchases/createPOItem', 'PurchaseController@createPOItem');
    Route::get('purchases/savePO', 'PurchaseController@savePurchaseOrder');
    Route::post('purchases/savePO', 'PurchaseController@savePurchaseOrder');
    Route::get('purchases/searchClient', 'PurchaseController@searchClient');
    Route::get('purchases/viewPurchaseDetails/{id}', 'PurchaseController@viewPurchase');
    Route::get('purchases/editPurchase/{id}', 'PurchaseController@editPurchase');
    Route::post('purchases/updatePurchase/{id}', 'PurchaseController@updatePurchase');
    Route::post('purchases/cancelPurchase/{id}', 'PurchaseController@cancelPurchase');
    Route::post('purchases/reactivatePurchase/{id}', 'PurchaseController@reactivatePurchase');
    Route::post('purchases/discardChanges/{id}', 'PurchaseController@discardChanges');
    Route::get('purchases/removeItems', 'PurchaseController@removeItems');
    Route::post('purchases/getProductDetail', 'PurchaseController@getProductDetail');
    Route::get('purchases/updatePurchaseStatus', 'PurchaseController@updatePurchaseStatus');
    Route::get('purchases/addPullOutCommissionToCashAdvance', 'PurchaseController@addPullOutCommissionToCashAdvance');
    Route::get('purchases/searchPurchases', 'PurchaseController@searchPurchases');


    Route::get('payments', 'PaymentController@showList');
    Route::get('payments/addNew', 'PaymentController@addNew');
    Route::get('payments/viewPayments', 'PaymentController@viewPayments');
    Route::get('payments/viewPayments/{id}', 'PaymentController@viewPayments');
    Route::get('payments/getClientPO', 'PaymentController@getClientPO');
    Route::get('payments/getClientPayment', 'PaymentController@getClientPayment');
    Route::get('payments/addClientPayment', 'PaymentController@addClientPayment');
    Route::get('payments/getTransactionDetails', 'PaymentController@getTransactionDetails');
    Route::get('payments/setTransactionPaid', 'PaymentController@setTransactionPaid');
    Route::get('payments/removePayments', 'PaymentController@removePayments');
    Route::get('payments/updatePaymentStatus', 'PaymentController@updatePaymentStatus');
    Route::get('payments/getPaymentDetail', 'PaymentController@getPaymentDetail');
    Route::get('payments/updateDepositDate', 'PaymentController@updateDepositDate');    
    Route::get('payments/updatePaymentRemarks', 'PaymentController@updatePaymentRemarks');    
    Route::get('payments/checksForDeposit', 'PaymentController@checkForDeposit'); 
    Route::get('payments/getCheckForDeposit', 'PaymentController@getCheckForDeposit'); 


    Route::get('collections', 'CollectionController@index'); 
    Route::get('collections/addCollectionsUpdate/{po}', 'CollectionController@add_collection_update');
    Route::get('collections/saveCollectionsUpdate', 'CollectionController@save_collection_update'); 
    Route::get('collections/getCollectionsList', 'CollectionController@get_collections_lists');

    Route::get('commissions/list', 'CommissionsController@commissionlist');
    Route::get('commissions', 'CommissionsController@addNew');
    Route::get('commissions/getClientPurchaseDetails', 'CommissionsController@getClientPurchaseDetails');
    Route::get('commissions/getPurchasedItems', 'CommissionsController@getPurchasedItems');
    Route::get('commissions/getAgentCommissions', 'CommissionsController@getAgentCommissions');
    Route::get('commissions/saveCommissions', 'CommissionsController@saveCommissions');
    Route::get('commissions/voucher', 'CommissionsController@voucher');
    Route::get('commissions/voucher/{id}', 'CommissionsController@voucher');
    Route::get('commissions/getAgentsList', 'CommissionsController@getAgentsList');
    Route::get('commissions/saveVoucherCommissions', 'CommissionsController@saveVoucherCommissions');
    Route::get('commissions/printVoucher/{id}', 'CommissionsController@printVoucher');
    Route::get('commissions/printVoucherValue/{id}', 'CommissionsController@printVoucherValue');
    Route::get('commissions/printCheck/{id}', 'CommissionsController@printCheck');

    Route::get('commissions/commissionForRelease', 'CommissionsController@commissionForRelease');
    Route::get('commissions/getCommissionForRelease', 'CommissionsController@getCommissionForRelease');
    Route::get('commissions/updateCommissionsStatus', 'CommissionsController@updateCommissionsStatus');
    

    Route::get('promos', 'PromosController@index');
    Route::get('promos/addNew', 'PromosController@create');
    Route::get('promos/update', 'PromosController@updateContest');
    Route::get('promos/register_po', 'PromosController@register_po');
    Route::get('promos/register_po_by_po', 'PromosController@register_po_by_po');
    Route::get('promos/getPOByContest', 'PromosController@getPOByContest');
    
    Route::post('promos/store', 'PromosController@store');
    Route::get('promos/show/{id}', 'PromosController@show');
    Route::get('promos/edit/{id}', 'PromosController@edit');
    Route::post('promos/update/{id}', 'PromosController@update');
    Route::get('promos/getContestTitle', 'PromosController@getContestTitle');
    Route::get('/promos/getPO', 'PromosController@getPO');
    Route::get('/promos/registerPoToPromo', 'PromosController@registerPoToPromo');
    Route::get('/promos/unregisterPoToPromo', 'PromosController@unregisterPoToPromo');
    Route::get('/promos/getContestants', 'PromosController@getContestants');
    Route::get('/promos/getAgentsForPromo', 'PromosController@getAgentsForPromo');
    Route::get('/promos/viewPurchaseDetails', 'PromosController@viewPurchase');

    Route::get('reports', function () {
        return View::make('reports/reports', [
                'page_title' => 'HCEMIOS Purchase',
                'page_description' => 'Are we having a new Purchase Order Today? :)',
                'level' => 'Purchase',
                'sub_level' => 'List'
        ]);
    });

    /*
     * Bookings
     * */
    Route::get('bookings', 'BookingsController@calendarView');
    Route::get('bookings/create', 'BookingsController@create');
    Route::post('bookings/updateDate', 'BookingsController@updateDate');

    Route::get('bookings/list', 'BookingsController@index');
    Route::get('bookings/addNew', 'BookingsController@addNew');
    Route::post('bookings/addNew', 'BookingsController@create');
    Route::get('bookings/viewProfile/{id}', 'BookingsController@viewProfile');
    Route::get('bookings/editProfile/{id}/{edit}', 'BookingsController@viewProfile');
    Route::post('bookings/update', 'BookingsController@update');
    Route::post('bookings/delete', 'BookingsController@delete');
    Route::post('bookings/activate', 'BookingsController@activate');
    Route::post('bookings/getBookingDetails', 'BookingsController@getBookingDetails');
    Route::post('bookings/cancelBooking', 'BookingsController@cancelBooking');
    Route::post('bookings/cancelBulkBooking', 'BookingsController@cancelBulkBooking');
    Route::post('bookings/cookedBulkBooking', 'BookingsController@cookedBulkBooking');
    Route::post('bookings/updateBookingDetails', 'BookingsController@updateBookingDetails');
    Route::get('bookings/searchRepresentative', 'BookingsController@searchRepresentative');
    Route::get('bookings/searchConsultantSponsor', 'BookingsController@searchConsultantSponsor');
    Route::get('bookings/searchAssociate', 'BookingsController@searchAssociate');
    Route::get('bookings/searchRepresentativeNotInPurchase/{id}', 'BookingsController@searchRepresentativeNotInPurchase');
    Route::get('bookings/searchCookDriver', 'BookingsController@searchRepresentativeForCookDriver');
    /*
     * Clients
     * */
    Route::get('clients', 'ClientsController@index');
    Route::get('clients/addNew', 'ClientsController@addNew');
    Route::post('clients/addNew', 'ClientsController@create');
    Route::get('clients/viewProfile/{id}', 'ClientsController@viewProfile');
    Route::get('clients/editProfile/{id}/{edit}', 'ClientsController@viewProfile');
    Route::post('clients/update', 'ClientsController@update');
    Route::post('clients/delete', 'ClientsController@delete');
    Route::post('clients/activate', 'ClientsController@activate');
    Route::post('clients/uploadImage', 'ClientsController@uploadImage');
    /*
     * Products
     * */
    Route::get('products', 'ProductsController@index');
    Route::get('products/addNew', 'ProductsController@addProduct');
    Route::post('products/create', 'ProductsController@create');
    Route::get('products/viewProduct/{id}', 'ProductsController@viewProduct');
    Route::post('products/update', 'ProductsController@update');
    Route::get('products/editProfile/{id}/{edit_mode}', 'ProductsController@viewProduct');
    Route::post('products/delete', 'ProductsController@delete');
    Route::post('products/activate', 'ProductsController@activate');
    Route::get('products/getProductsByType', 'ProductsController@getProductsByType');
    /*
     * Agents
     * */
    Route::get('agents', 'AgentsController@index');
    Route::get('agents/addNew', 'AgentsController@addAgent');
    Route::get('agents/viewProfile/{id}', 'AgentsController@viewProfile');
    Route::get('agents/editProfile/{id}/{edit}', 'AgentsController@viewProfile');
    Route::post('agents/addNew', 'AgentsController@create');
    Route::post('agents/update', 'AgentsController@update');
    Route::post('agents/delete', 'AgentsController@delete');
    Route::post('agents/activate', 'AgentsController@activate');
    Route::get('agents/getWsp', 'AgentsController@getWsp');
    Route::get('agents/getSales', 'AgentsController@getSales');
    Route::get('agents/getAccountBalancePayments', 'AgentsController@getAccountBalancePayments');
    Route::post('agents/uploadImage', 'AgentsController@uploadImage');
    /*
     * Refs
     * */
    Route::post('provinces', 'RefAddressController@loadProvince');
    Route::post('municipalities', 'RefAddressController@loadMunicipality');
    /*
     * Users Management
     * */
    Route::get('users', 'UserController@index');
    Route::get('users/addUser', 'UserController@addUser');
    Route::get('users/viewProfile/{id}', 'UserController@viewProfile');
    Route::get('users/editProfile/{id}/{edit}', 'UserController@viewProfile');
    Route::post('users/addUser', 'UserController@create');
    Route::post('users/update', 'UserController@update');
    Route::post('users/delete', 'UserController@delete');
    Route::post('users/activate', 'UserController@activate');


    /*
     * Roles Route
     * */
    Route::get('roles', 'RolesController@index');
    Route::post('roles/addNew', 'RolesController@create');
    Route::post('roles/update', 'RolesController@update');
    Route::get('roles/update', 'RolesController@update');
    Route::post('roles/getConfig', 'RolesController@getRoleConfig');


    Route::get('multilevel','MultilevelController@index');
    Route::get('multilevel/addNew','MultilevelController@addNew');
    
    Route::get('multilevel/savePhase','MultilevelController@savePhase');
    Route::get('multilevel/history/{id}','MultilevelController@history');

    Route::get('reports/checkForDeposit','ReportController@checkForDeposit');
    Route::get('reports/getCheckForDeposit','ReportController@getCheckForDeposit');

    Route::get('reports/commissionForRelease','ReportController@commissionForRelease');
    Route::get('reports/getCommissionForRelease','ReportController@getCommissionForRelease');

    Route::get('reports/onHoldCommission','ReportController@onHoldCommission');
    Route::get('reports/getOnHoldCommission','ReportController@getOnHoldCommission');

    Route::get('reports/salesByAgent','ReportController@salesByAgent');
    Route::get('reports/getSalesByAgent','ReportController@getSalesByAgent');

    Route::get('reports/totalSalesByMonth','ReportController@totalSalesByMonth');
    Route::get('reports/getTotalSalesByMonth','ReportController@getTotalSalesByMonth');

    Route::get('reports/contestStatus','ReportController@contestStatus');
    Route::get('reports/getContestStatus','ReportController@getContestStatus');

    Route::get('reports/totalSalesIncompletePayment','ReportController@totalSalesIncompletePayment');
    Route::get('reports/getTotalSalesIncompletePayment','ReportController@getTotalSalesIncompletePayment');

    Route::get('reports/listOfCheckByStatus','ReportController@listOfCheckByStatus');
    Route::get('reports/getListOfCheckByStatus','ReportController@getListOfCheckByStatus');

    Route::get('reports/listOfClient','ReportController@listOfClient');
    Route::get('reports/getListOfClient','ReportController@getListOfClient');

    Route::get('reports/topSellingProducts','ReportController@topSellingProducts');
    Route::get('reports/getTopSellingProduct','ReportController@getTopSellingProduct');

    Route::get('reports/topSeller','ReportController@topSeller');
    Route::get('reports/getTopSeller','ReportController@getTopSeller');

    Route::get('reports/clientPurchases','ReportController@clientPurchases');
    Route::get('reports/getClientPurchases','ReportController@getClientPurchases');


    Route::get('sales_override','SalesOverrideController@index');



});
Route::filter('auth', function () {
    if (Auth::guest()) return Redirect::guest('login');
});
Route::get('multilevel/savePromotion','MultilevelController@savePromotion');
Route::get('multilevel/getSales','MultilevelController@getSales');
Route::get('multilevel/getAssociateSales','MultilevelController@getAssociateSales');
Route::get('multilevel/getPhaseDetails','MultilevelController@getPhaseDetails');

Route::get('voucher',function(){
    return View::make('chat');
});