<?php
use Illuminate\Database\Seeder;

/**
 * Created by PhpStorm.
 * User: grg021
 * Date: 2/26/15
 * Time: 2:17 PM
 */
class UserSeeder extends Seeder
{

    public function run()
    {

        DB::table('users')->delete();

        User::create(
                array(
                        'email' => 'admin@admin.com',
                        'password'=>Hash::make('password'),
                        'user_role' => 'Admin',
                        'status' => 'Active'
                )
        );


    }

}