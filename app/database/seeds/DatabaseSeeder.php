<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call('RefBrgySeeder');
        $this->call('RefCitySeeder');
        $this->call('RefProvSeeder');
        $this->call('RefRegionSeeder');
        $this->call('UserSeeder');

        Model::reguard();
    }
}
