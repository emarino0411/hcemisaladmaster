<?php

use Illuminate\Database\Migrations\Migration;

class CreateCheckTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('check', function($table) {
            $table->increments('id');
            $table->string('check_no', 45)->nullable();
            $table->string('bank', 45)->nullable();
            $table->string('branch', 45)->nullable();
            $table->string('date', 45)->nullable();
            $table->decimal('amount', 5, 0)->nullable();
            $table->string('payee', 45)->nullable();
            $table->string('category', 45)->nullable();
            $table->integer('check_voucher_cv_no')->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('check');
    }

}