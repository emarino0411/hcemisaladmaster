<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRprovTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('rprov', function(Blueprint $table)
		{
            $table->increments('id');
            $table->string('regcode', 2);
            $table->string('provcode', 4);
            $table->string('provname', 100);
            $table->string('nscb_prov_code', 9);
            $table->string('nscb_prov_name', 100);
            $table->string('addedby', 50);
            $table->integer('UserLevelID');
            $table->dateTime('dateupdated');
            $table->char('status',1);

            $table->index('regcode');
            $table->index('provcode');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('rprov');
	}

}
