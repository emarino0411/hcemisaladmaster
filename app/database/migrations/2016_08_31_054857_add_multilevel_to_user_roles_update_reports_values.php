<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMultilevelToUserRolesUpdateReportsValues extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('roles', function(Blueprint $table)
		{
			$table->integer('checks_for_deposit')->default(0);
			$table->integer('commissions_for_release')->default(0);
			$table->integer('on_hold_commissions')->default(0);
			$table->integer('sales_by_agent')->default(0);
			$table->integer('total_sales_by_month')->default(0);
			$table->integer('contest_status')->default(0);
			$table->integer('total_sales_incomplete_payment')->default(0);
			$table->integer('list_of_checks_by_status')->default(0);
			$table->integer('list_of_clients_for_month')->default(0);
			$table->integer('top_selling_products')->default(0);
			$table->integer('top_seller_sponsor')->default(0);
			
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
