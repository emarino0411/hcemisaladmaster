<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function($table) {
            $table->increments('id');
            $table->string('event_name', 45)->nullable();
            $table->string('street', 45)->nullable();
            $table->string('city', 45)->nullable();
            $table->string('province', 45)->nullable();
            $table->string('client', 45)->nullable();
            $table->string('booking_date', 45)->nullable();
            $table->string('booking_time', 45)->nullable();
            $table->string('booking_time_end', 45)->nullable();
            $table->string('status', 45)->nullable();
            $table->string('color', 45)->default('Active');
            $table->string('contact', 45)->nullable();
            $table->string('remarks', 45)->nullable();


            $table->timestamp('updated_at')->nullable();
            $table->timestamp('created_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bookings');
    }
}
