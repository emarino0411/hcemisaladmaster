<?php

use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function($table) {
            $table->increments('id');
            $table->integer('po_no')->index();
            $table->string('remittance_date')->nullable();
            $table->string('type', 45)->nullable();
            $table->string('check_number', 45)->nullable();
            $table->string('bank', 45)->nullable();
            $table->string('branch', 45)->nullable();
            $table->string('amount', 45)->nullable();
            $table->string('status', 45)->nullable();
            $table->string('deposit_date1', 45)->nullable();
            $table->string('return_date1', 45)->nullable();
            $table->string('deposit_date2', 45)->nullable();
            $table->string('return_date2', 45)->nullable();
            $table->integer('replaced_by');
            $table->integer('replacement_for')->nullable();
            $table->string('remarks', 45)->nullable();
            $table->integer('user_id')->index();

            $table->timestamp('updated_at')->nullable();
            $table->timestamp('created_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('payments');
    }

}