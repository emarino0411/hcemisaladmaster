<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCollectionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		
		Schema::create('collections', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('po_no')->nullable();
			$table->string('representative_id')->nullable();
			$table->string('update')->nullable();
			$table->string('update_date')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('collections');
	}

}
