<?php

use Illuminate\Database\Migrations\Migration;

class CreateClientTable extends Migration
{

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('client', function ($table) {
      $table->increments('id');
      $table->string('last_name', 45);
      $table->string('first_name', 45);
      $table->string('middle_name', 45)->nullable();
      $table->string('birthdate', 45)->nullable();
      $table->string('civil_status', 45)->nullable();
      $table->string('gender', 45)->nullable();
      $table->string('street', 45);
      $table->string('brgy', 45);
      $table->string('city', 45);
      $table->string('province', 45)->nullable();
      $table->string('phone1', 45);
      $table->string('phone2', 45)->nullable();
      $table->string('email_address', 45)->nullable();
      $table->string('valid_id_no', 45)->nullable();
      $table->string('valid_id_type', 45)->nullable();
      $table->string('business_name', 45)->nullable();
      $table->string('business_street', 45)->nullable();
      $table->string('business_brgy', 45)->nullable();
      $table->string('business_city', 45)->nullable();
      $table->string('business_province', 45)->nullable();
      $table->string('business_landline', 45)->nullable();
      $table->string('business_mobile', 45)->nullable();
      $table->string('business_email', 45)->nullable();
      $table->string('valid_signature', 45)->nullable();
      $table->string('status', 45)->default('ACTIVE');
      $table->string('occupation', 45)->nullable();
      $table->timestamp('updated_at')->nullable();
      $table->timestamp('created_at')->nullable();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::drop('client');
  }

}