<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('bookings', function(Blueprint $table)
		{
			$table->string('time', 45)->nullable();
			$table->string('fast_track', 45)->nullable();
			$table->string('telethon', 45)->nullable();
			$table->string('type', 45)->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('bookings', function(Blueprint $table)
		{
//			$table->string('time', 45)->nullable();
//			$table->string('fast_track', 45)->nullable();
//			$table->string('telethon', 45)->nullable();
		});

	}

}
