<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMultilevelTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('multilevel', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('agent_id')->nullable();
			$table->string('effective_date')->nullable();
			$table->string('type')->nullable();
			$table->string('start_date')->nullable();
			$table->string('end_date')->nullable();
			$table->string('approved_by')->nullable();
			$table->string('level_from')->nullable();
			$table->string('level_to')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('multilevel');
	}

}
