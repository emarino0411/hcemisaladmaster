<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePromosQualifierTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('promos_qualifier', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('representative')->nullable();
			$table->string('po_no')->nullable();
			$table->string('promo_id')->nullable();
			$table->string('user_user_id')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('promos_qualifier');
	}

}
