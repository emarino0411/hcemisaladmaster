<?php

use Illuminate\Database\Migrations\Migration;

class CreateAuditlogTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('audit_log', function($table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('ref_table', 45)->nullable();
            $table->string('transaction_desc', 45)->nullable();
            $table->string('ref_id', 45)->nullable();
            $table->string('remarks', 45)->nullable();
            $table->dateTime('transaction_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('audit_log');
    }

}