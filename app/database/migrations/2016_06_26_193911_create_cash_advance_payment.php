<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCashAdvancePayment extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cash_advance_payment', function (Blueprint $table) {
            $table->increments('id');
            $table->string('amount')->nullable();
            $table->string('date_paid')->nullable();
            $table->string('receipt_check_no')->nullable();
            $table->string('agent_id')->nullable();
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cash_advance_payment');
    }

}
