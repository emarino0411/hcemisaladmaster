<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePromosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('promos', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title')->nullable();
			$table->string('description')->nullable();
			$table->string('from')->nullable();
			$table->string('to')->nullable();
			$table->string('minimum_qualification')->nullable();
			$table->string('mechanics_1_points')->nullable();
			$table->string('mechanics_1_prize')->nullable();
			$table->string('mechanics_2_points')->nullable();
			$table->string('mechanics_2_prize')->nullable();
			$table->string('mechanics_3_points')->nullable();
			$table->string('mechanics_3_prize')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('promos');
	}

}
