@extends('admin_template')

@section('additional_header')

        <!-- DataTables -->
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.min.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker-bs3.css")}}'>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/css/jasny-bootstrap.min.css">


@endsection

@section('content')
        <!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-3">

            <!-- Profile Image -->
            <div class="box box-primary">
                <div class="box-body box-profile">
                    <a href="#" data-toggle="modal" data-target="#upload_image">
                        @if($agent->profile_picture!="" && $agent->profile_picture!= null )
                            <img class="profile-user-img img-responsive img-circle"
                                 src="{{ URL::to('/public') }}{{$agent->profile_picture}}"
                                 alt="User profile picture"
                                 id="user_image">
                        @else
                            <img class="profile-user-img img-responsive img-circle"
                                 src="{{ asset("/bower_components/AdminLTE/dist/img/user2-160x160.jpg") }}"
                                 alt="User profile picture"
                                 id="user_image">
                        @endif

                    </a>

                    <h3 class="profile-username text-center">{{ucwords($agent->first_name)}} {{ucwords($agent->last_name)}}</h3>

                    <p class="text-muted text-center">Agent</p>
                    <hr>

                    <p class="text-muted text-center"><b>Agent Code</b><br>{{$agent->representative_code}}</p>
                    <hr>
                    @if($agent->city != null)
                        <p class="text-muted text-center"><b>Address</b><br>{{$agent->street}} {{$agent->brgy}}
                            , {{$agent->_city[0]->cityname}}
                            , {{$agent->_province[0]->provname}}</p>
                        <hr>
                    @else
                        <p class="text-muted text-center"><b>Address</b><br>N/A
                        <hr>
                    @endif
                    @if($agent->phone1 == "" && $agent->phone2=="")
                        <p class="text-muted text-center"><b>Contact Information</b><br>{{$agent->phone1}}
                            /{{$agent->phone2}}</p>
                        <hr>
                    @else
                        <p class="text-muted text-center"><b>Contact Information</b><br>N/A</p>
                        <hr>
                    @endif
                    <p class="text-muted text-center"><b>User Status</b><br>{{$agent->status}}</p>
                    @if(isAccessModuleAllowed('agents_edit'))
                        <hr>
                        <center>
                            <button class="btn btn-primary" id="btn_update">Update Information</button>
                        </center>
                    @endif
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

        </div>
        <!-- /.col -->
        <div class="col-md-9">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#profile" data-toggle="tab">Agent Information</a></li>
                    <li><a href="#sales" data-toggle="tab">Sales</a></li>
                    <li><a href="#wsp" data-toggle="tab">WSP</a></li>
                    <li><a href="#account_balance" data-toggle="tab">Account Balance Payment</a></li>
                    <li><a href="#group" data-toggle="tab">Group</a></li>
                </ul>
                <div class="tab-content">
                    <div class="active tab-pane" id="profile">

                        <div class="row">
                            <div class="col-md-12">
                                <div class="">
                                    <div class="box-header">
                                        <h3 class="box-title"><i class="fa fa-user"></i> Personal Information</h3>
                                    </div>
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                        <div class="row">

                                            <div class="col-md-12">

                                                <form class="form-horizontal">
                                                    <!-- Account Balance -->
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="txt_last_name" class="col-sm-2 control-label">
                                                                Last Name</label>

                                                            <div class="col-sm-4">
                                                                <input type="text" style="background-color:white"
                                                                       class="form-control pull-right"
                                                                       value="{{$agent->last_name}}" id="txt_last_name">
                                                                <input type="hidden" style="background-color:white"
                                                                       class="form-control pull-right"
                                                                       value="{{$agent->id}}" id="txt_agent_id">
                                                            </div>
                                                            <label for="txt_first_name" class="col-sm-2 control-label">
                                                                First Name</label>

                                                            <div class="col-sm-4">
                                                                <input type="text" style="background-color:white"
                                                                       class="form-control pull-right"
                                                                       value="{{$agent->first_name}}"
                                                                       id="txt_first_name">
                                                            </div>

                                                        </div>
                                                    </div>


                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="rdo_gender" class="col-sm-4 control-label">
                                                                Gender</label>

                                                            <div class="col-sm-8">
                                                                <label style="margin-top:2%;margin-left:2%">
                                                                    <input type="radio" name="rdo_gender" value="Male"
                                                                           class="minimal" @if($agent->gender=='Male') {{'checked'}} @endif>
                                                                    Male
                                                                </label>
                                                                <label style="margin-top:2%;margin-left:2%">
                                                                    <input type="radio" name="rdo_gender" value="Female"
                                                                           class="minimal" @if($agent->gender=='Female') {{'checked'}} @endif>
                                                                    Female
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="txt_agent_birthdate"
                                                                   class="col-sm-4 control-label"> BirthDate</label>

                                                            <div class="col-sm-8">

                                                                <input type="text"
                                                                       class="form-control pull-right birthdate"
                                                                       value="{{$agent->birthdate}}"
                                                                       style="background-color:white"
                                                                       id="txt_agent_birthdate">

                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="slct_civil_status"
                                                                   class="col-sm-4 control-label"> Civil Status</label>

                                                            <div class="col-sm-8">
                                                                <select id="slct_civil_status"
                                                                        class="form-control select2"
                                                                        style="width: 100%;height:100%;background-color:white">
                                                                    <option @if($agent->civil_status=='Single') selected @endif>
                                                                        Single
                                                                    </option>
                                                                    <option @if($agent->civil_status=='Married') selected @endif>
                                                                        Married
                                                                    </option>
                                                                    <option @if($agent->civil_status=='Widowed') selected @endif>
                                                                        Widowed
                                                                    </option>
                                                                    <option @if($agent->civil_status=='Separated') selected @endif>
                                                                        Separated
                                                                    </option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="slct_dependents" class="col-sm-4 control-label">
                                                                Dependents</label>

                                                            <div class="col-sm-8">
                                                                <select id="slct_dependents"
                                                                        class="form-control select2"
                                                                        style="width: 100%;height:100%;background-color:white">
                                                                    <option @if($agent->no_of_dependents=='0') selected
                                                                            @endif value="0">0
                                                                    </option>
                                                                    <option @if($agent->no_of_dependents=='1') selected
                                                                            @endif value="1">1
                                                                    </option>
                                                                    <option @if($agent->no_of_dependents=='2') selected
                                                                            @endif value="2">2
                                                                    </option>
                                                                    <option @if($agent->no_of_dependents=='3') selected
                                                                            @endif value="3">3
                                                                    </option>
                                                                    <option @if($agent->no_of_dependents=='4') selected
                                                                            @endif value="4">4
                                                                    </option>
                                                                    <option @if($agent->no_of_dependents=='5') selected
                                                                            @endif value="5">5
                                                                    </option>
                                                                    <option @if($agent->no_of_dependents=='6') selected
                                                                            @endif value="6">6
                                                                    </option>
                                                                    <option @if($agent->no_of_dependents=='7') selected
                                                                            @endif value="7">7
                                                                    </option>
                                                                    <option @if($agent->no_of_dependents=='8') selected
                                                                            @endif value="8">8
                                                                    </option>
                                                                    <option @if($agent->no_of_dependents=='9') selected
                                                                            @endif value="9">9
                                                                    </option>
                                                                    <option @if($agent->no_of_dependents=='10') selected
                                                                            @endif value="10">10
                                                                    </option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="can_drive" class="col-sm-4 control-label"> Can
                                                                Drive</label>

                                                            <div class="col-sm-8">
                                                                <label style="margin-top:2%;margin-left:2%">
                                                                    <input type="radio"
                                                                           @if($agent->can_drive=='Yes') {{'checked'}} @endif name="rdo_can_drive"
                                                                           value="Yes" class="minimal" checked>
                                                                    Yes
                                                                </label>
                                                                <label style="margin-top:2%;margin-left:2%">
                                                                    <input type="radio"
                                                                           @if($agent->can_drive=='No') {{'checked'}} @endif name="rdo_can_drive"
                                                                           value="No" class="minimal">
                                                                    No
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="own_car" class="col-sm-4 control-label"> Owns a
                                                                Car</label>

                                                            <div class="col-sm-8">
                                                                <label style="margin-top:2%;margin-left:2%">
                                                                    <input type="radio"
                                                                           @if($agent->own_car=='No') {{'checked'}} @endif name="rdo_own_car"
                                                                           value="Yes" class="minimal" checked>
                                                                    Yes
                                                                </label>
                                                                <label style="margin-top:2%;margin-left:2%">
                                                                    <input type="radio"
                                                                           @if($agent->own_car=='No') {{'checked'}} @endif name="rdo_own_car"
                                                                           value="No" class="minimal">
                                                                    No
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <hr style="margin-bottom:1%">
                                                    </div>

                                                    <div class="col-md-12">
                                                        <h4 class="box-title"><i class="fa fa-user"></i> Spouse
                                                            Information</h4>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="txt_spouse_last_name"
                                                                   class="col-sm-2 control-label"> Last Name</label>

                                                            <div class="col-sm-4">
                                                                <input type="text" style="background-color:white"
                                                                       class="form-control pull-right"
                                                                       value="{{$agent->spouse_last_name}}"
                                                                       id="txt_spouse_last_name">
                                                            </div>
                                                            <label for="txt_spouse_first_name"
                                                                   class="col-sm-2 control-label"> First Name</label>

                                                            <div class="col-sm-4">
                                                                <input type="text" style="background-color:white"
                                                                       class="form-control pull-right"
                                                                       value="{{$agent->spouse_first_name}}"
                                                                       id="txt_spouse_first_name">
                                                            </div>

                                                        </div>
                                                    </div>


                                                    <div class="col-md-12">
                                                        <hr style="margin-bottom:1%">
                                                    </div>

                                                    <div class="col-md-12">
                                                        <h4 class="box-title"><i class="fa fa-phone"></i> Contact
                                                            Information</h4>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="txt_street" class="col-sm-2 control-label">
                                                                Street</label>

                                                            <div class="col-sm-10">

                                                                <input type="text" class="form-control pull-right"
                                                                       style="background-color:white"
                                                                       value="{{$agent->street}}" id="txt_street">

                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="txt_brgy" class="col-sm-4 control-label">
                                                                Barangay</label>

                                                            <div class="col-sm-8">
                                                                <input type="text" class="form-control pull-right"
                                                                       style="background-color:white"
                                                                       value="{{$agent->brgy}}" id="txt_brgy">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="slct_town" class="col-sm-4 control-label">
                                                                Municipality</label>

                                                            <div class="col-sm-8">

                                                                <select id="slct_town" class="form-control select2"
                                                                        style="width: 100%;height:100%;background-color:white">
                                                                    @foreach($municipalities as $municipality)
                                                                        <option
                                                                                value="{{$municipality->citycode}}" @if($agent->city == $municipality->citycode){{'selected'}} @endif>{{$municipality->cityname}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="slct_province" class="col-sm-4 control-label">
                                                                Province</label>

                                                            <div class="col-sm-8">
                                                                <select id="slct_province" class="form-control select2"
                                                                        style="width: 100%;height:100%;background-color:white">
                                                                    @foreach($provinces as $province)
                                                                        <option
                                                                                value="{{$province->provcode}}" @if($agent->province == $province->provcode){{'selected'}} @endif>
                                                                            {{$province->provname}}
                                                                        </option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="txt_agent_landline"
                                                                   class="col-sm-4 control-label"> Landline</label>

                                                            <div class="col-sm-8">
                                                                <input type="text" class="form-control pull-right"
                                                                       value="{{$agent->phone1}}"
                                                                       style="background-color:white"
                                                                       id="txt_agent_landline">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="txt_agent_mobile"
                                                                   class="col-sm-4 control-label"> Mobile</label>

                                                            <div class="col-sm-8">
                                                                <input type="text" class="form-control pull-right"
                                                                       value="{{$agent->phone2}}"
                                                                       style="background-color:white"
                                                                       id="txt_agent_mobile">
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="txt_agent_email" class="col-sm-4 control-label">
                                                                Email</label>

                                                            <div class="col-sm-8">
                                                                <input type="text" class="form-control pull-right"
                                                                       value="{{$agent->email_address}}"
                                                                       style=" background-color:white"
                                                                       id="txt_agent_email">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <hr style="margin-bottom:1%">
                                                    </div>

                                                    <div class="col-md-12">
                                                        <h4 class="box-title"><i class="fa fa-user"></i> Sponsor
                                                            Information</h4>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="txt_agent_sponsor"
                                                                   class="col-sm-4 control-label"> Sponsor</label>

                                                            <div class="col-sm-8">
                                                                <!-- <input type="text" class="form-control pull-right"
                                                                       value="{{$agent->sponsor_id}}"
                                                                       style="width:100%; background-color:white"
                                                                       id="txt_agent_sponsor"> -->
                                                                @if($sponsor==null)
                                                                    <select id="txt_agent_sponsor"
                                                                            class="form-control pull-right">
                                                                    </select>
                                                                @else
                                                                    <select id="txt_agent_sponsor"
                                                                            class="form-control pull-right">
                                                                        <option value="{{$agent->sponsor_id}}">{{$sponsor->first_name}} {{$sponsor->last_name}}</option>
                                                                    </select>
                                                                @endif


                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="txt_agent_sponsor_date"
                                                                   class="col-sm-4 control-label "> Sponsor Date</label>

                                                            <div class="col-sm-8">

                                                                <input type="text"
                                                                       class="form-control pull-right birthdate"
                                                                       value="{{$agent->sponsor_date}}"
                                                                       style="background-color:white"
                                                                       id="txt_agent_sponsor_date">

                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <hr style="margin-bottom:1%">
                                                    </div>

                                                    <div class="col-md-12">
                                                        <h4 class="box-title"><i class="fa fa-briefcase"></i> HCEMI
                                                            Agent Info</h4>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="agent_level" class="col-sm-4 control-label">
                                                                Level</label>

                                                            <div class="col-sm-8">
                                                                <select class="form-control" id="agent_level">


                                                                    <option @if($agent->current_level == 'associate')
                                                                            selected
                                                                            @endif
                                                                            value="associate">Associate
                                                                    </option>
                                                                    <option @if($agent->current_level == 'consultant')
                                                                            selected
                                                                            @endif
                                                                            value="consultant">Consultant
                                                                    </option>
                                                                    <option @if($agent->current_level == 'senior consultant')
                                                                            selected
                                                                            @endif
                                                                            value="senior consultant">Senior Consultant
                                                                    </option>
                                                                    <option @if($agent->current_level == 'distributor')
                                                                            selected
                                                                            @endif
                                                                            value="distributor">Distributor
                                                                    </option>
                                                                    <option @if($agent->current_level == 'manager')
                                                                            selected
                                                                            @endif
                                                                            value="manager">Manager
                                                                    </option>

                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="txt_wsp_percenage"
                                                                   class="col-sm-4 control-label "> WSP
                                                                Percentage</label>

                                                            <div class="col-sm-8">

                                                                <input type="text" class="form-control pull-right"
                                                                       value="{{$agent->wsp_percentage}}"
                                                                       style="background-color:white"
                                                                       id="txt_wsp_percenage">

                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="txt_account_balance"
                                                                   class="col-sm-4 control-label "> Account
                                                                Balance</label>

                                                            <div class="col-sm-8">

                                                                <input type="text" class="form-control pull-right"
                                                                       value="{{$agent->account_balance_percentage}}"
                                                                       style="background-color:white"
                                                                       id="txt_account_balance">

                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="txt_first_presentation"
                                                                   class="col-sm-4 control-label "> First
                                                                Presentation</label>

                                                            <div class="col-sm-8">

                                                                <input type="text"
                                                                       class="form-control pull-right birthdate"
                                                                       value="{{$agent->first_presentation}}"
                                                                       style="background-color:white"
                                                                       id="txt_first_presentation">

                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="txt_first_sale" class="col-sm-4 control-label ">
                                                                First Sale</label>

                                                            <div class="col-sm-8">

                                                                <input type="text"
                                                                       class="form-control pull-right birthdate"
                                                                       value="{{$agent->first_sale}}"
                                                                       style="background-color:white"
                                                                       id="txt_first_sale">

                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="txt_bom_date" class="col-sm-4 control-label ">
                                                                BOM Date</label>

                                                            <div class="col-sm-8">

                                                                <input type="text"
                                                                       class="form-control pull-right birthdate"
                                                                       value="{{$agent->bom}}"
                                                                       style="background-color:white" id="txt_bom_date">

                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="txt_bom_date" class="col-sm-4 control-label ">
                                                                Phase I</label>

                                                            <div class="col-sm-8">

                                                                <input type="text"
                                                                       class="form-control pull-right"
                                                                       value="{{$agent->phase1_start}} to {{$agent->phase1_end}}"
                                                                       style="background-color:white"
                                                                       id="txt_phase_one">

                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="txt_bom_date" class="col-sm-4 control-label ">
                                                                Phase II</label>

                                                            <div class="col-sm-8">

                                                                <input type="text"
                                                                       class="form-control pull-right"
                                                                       value="{{$agent->phase2_start}} to {{$agent->phase2_end}}"
                                                                       style="background-color:white"
                                                                       id="txt_phase_two">

                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div class="col-md-12">
                                                        <hr style="margin-bottom:1%">
                                                    </div>

                                                    <div class="col-md-12">
                                                        <h4 class="box-title"><i class="fa fa-credit-card"></i>
                                                            Government Contributions ID</h4>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="txt_sss" class="col-sm-2 control-label">
                                                                SSS</label>

                                                            <div class="col-sm-4">
                                                                <input type="text" style="background-color:white"
                                                                       class="form-control pull-right"
                                                                       value="{{$agent->sss_no}}" id="txt_sss">
                                                            </div>
                                                            <label for="txt_tin" class="col-sm-2 control-label"> TIN
                                                                ID</label>

                                                            <div class="col-sm-4">
                                                                <input type="text" class="form-control pull-right"
                                                                       value="{{$agent->tin_no}}"
                                                                       id="txt_tin">
                                                            </div>

                                                        </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="txt_pagibig" class="col-sm-2 control-label">
                                                                PAGIBIG NO</label>

                                                            <div class="col-sm-4">
                                                                <input type="text" class="form-control pull-right"
                                                                       value="{{$agent->pagibig_no}}"
                                                                       id="txt_pagibig">
                                                            </div>
                                                            <label for="txt_philhealth" class="col-sm-2 control-label">
                                                                PHILHEALTH NO</label>

                                                            <div class="col-sm-4">
                                                                <input type="text" class="form-control pull-right"
                                                                       value="{{$agent->philhealth_no}}"
                                                                       id="txt_philhealth">
                                                            </div>

                                                        </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <hr style="margin-bottom:1%">
                                                    </div>

                                                    <div class="col-md-12">
                                                        <h4 class="box-title"><i class="fa fa-building"></i> Previous
                                                            Employer</h4>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="txt_employer_name"
                                                                   class="col-sm-2 control-label"> Name</label>

                                                            <div class="col-sm-4">
                                                                <input type="text" class="form-control pull-right"
                                                                       value="{{$agent->previous_employer}}"
                                                                       id="txt_employer_name">
                                                            </div>
                                                            <label for="txt_employer_address"
                                                                   class="col-sm-2 control-label"> Address</label>

                                                            <div class="col-sm-4">
                                                                <input type="text" class="form-control pull-right"
                                                                       value="{{$agent->p_employer_addr}}"
                                                                       id="txt_employer_address">
                                                            </div>

                                                        </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="txt_employer_contact"
                                                                   class="col-sm-2 control-label"> Contact No</label>

                                                            <div class="col-sm-4">
                                                                <input type="text" class="form-control pull-right"
                                                                       value="{{$agent->p_employer_telno}}"
                                                                       id="txt_employer_contact">
                                                            </div>
                                                            <label for="txt_employer_email"
                                                                   class="col-sm-2 control-label"> Email</label>

                                                            <div class="col-sm-4">
                                                                <input type="text" class="form-control pull-right"
                                                                       value="{{$agent->p_employer_email}}"
                                                                       id="txt_employer_email">
                                                            </div>

                                                        </div>
                                                    </div>

                                                </form>

                                                <!-- buttons  -->
                                                <div class="col-md-3 col-md-offset-9" id="div_update_user">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <button type="button" id="btn_save_agent"
                                                                    class="btn btn-info pull-right"> Update
                                                            </button>
                                                            <button class="btn btn-warning" id="btn_cancel_update">
                                                                Cancel
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <!-- /btn-group -->
                                                </div>
                                                <!-- /.buttons  -->

                                            </div><!-- col-md-12 -->
                                        </div><!-- /row -->
                                    </div>
                                    <!-- /.box-body -->
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>

                        <input type="hidden" id="prov_code">

                    </div>
                    <div class="tab-pane" id="sales">
                        <div class="">
                            <div class="box-header">
                                <h3 class="box-title"><i class="fa fa-shopping-cart"></i> Sales</h3>

                                <div class="pull-right">
                                    <div id="salesrange" class="pull-right"
                                         style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                                        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                                        <span></span> <b class="caret"></b>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="row">

                                    <div class="col-md-12">

                                        <table id="example1" class="table table-bordered table-striped">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>PO No</th>
                                                <th>Client Name</th>
                                                <th>Date Purchased</th>
                                                <th>Sales Amount</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($purchases as $purchase)
                                                <tr>
                                                    <td><input type="checkbox"></td>
                                                    <td>{{$purchase->purchase_order_no}}</td>
                                                    <td>{{$purchase->first_name}} {{$purchase->last_name}}</td>
                                                    <td>{{$purchase->purchase_date}}</td>
                                                    <td>{{number_format($purchase->total_price,2,".",",");}}</td>
                                                    <td>{{$purchase->status}}</td>
                                                    <td>
                                                        <a href="/purchases/viewPurchaseDetails/{{$purchase->id}}"
                                                           data-toggle="tooltip"
                                                           title="View {{$purchase->first_name}} {{$purchase->last_name}}'s purchase details"
                                                           class="btn btn-success btn-xs">
                                                            <i class="fa fa-info-circle"></i> View
                                                        </a>

                                                    </td>
                                                </tr>
                                            @endforeach

                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <th>#</th>
                                                <th>PO No</th>
                                                <th>Client</th>
                                                <th>Date Purchased</th>
                                                <th>Sales Amount</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                            </tfoot>
                                        </table>

                                    </div><!-- col-md-12 -->
                                </div><!-- /row -->
                            </div>
                            <!-- /.box-body -->
                        </div>

                    </div>
                    <div class="tab-pane" id="wsp">
                        <div class="">
                            <div class="box-header">
                                <h3 class="box-title"><i class="fa fa-bank"></i> WSP</h3>

                                <div class="pull-right">
                                    <div id="reportrange" class="pull-right"
                                         style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                                        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                                        <span></span> <b class="caret"></b>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-12">

                                    </div>

                                    <div class="col-md-12">

                                        <table id="wsp_table" class="table table-bordered table-striped">
                                            <thead>
                                            <tr>
                                                <th>Voucher Date</th>
                                                <th>Voucher No</th>
                                                <th>WSP Amount</th>
                                                <th>WSP Percentage</th>
                                                <th>Voucher Status</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($wsp_commissions as $purchase)
                                                <tr>
                                                    <td>{{$purchase->due_date}}</td>
                                                    <td>{{$purchase->voucher_no}}</td>
                                                    <td>{{number_format((float)$purchase->wsp_commission, 2, '.', ',');}}</td>
                                                    <td>{{$purchase->wsp_percentage}}%</td>
                                                    <td>{{$purchase->status}}</td>
                                                </tr>
                                            @endforeach

                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <th>Voucher Date</th>
                                                <th>Voucher No</th>
                                                <th>WSP Amount</th>
                                                <th>WSP Percentage</th>
                                                <th>Status</th>
                                            </tr>
                                            </tfoot>
                                        </table>

                                    </div><!-- col-md-12 -->
                                </div><!-- /row -->
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                    <div class="tab-pane" id="account_balance">
                        <div class="">
                            <div class="box-header">
                                <h3 class="box-title"><i class="fa fa-bank"></i> Account Balance Payment</h3>

                                <div class="pull-right">
                                    <div id="balancerange" class="pull-right"
                                         style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                                        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                                        <span></span> <b class="caret"></b>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="row">

                                    <div class="col-md-6">
                                        <label>Remaining Balance: P{{$total_balance->total_cash_advance}}</label>
                                    </div>
                                    <div class="col-md-6">
                                        <button class="pull-right btn-lg-success">Cash Advance</button>
                                        <button class="pull-right btn-lg-success">Payment</button>
                                    </div>
                                    <hr>

                                    <div class="col-md-12">

                                        <table id="account_balance_table" class="table table-bordered table-striped">
                                            <thead>
                                            <tr>
                                                <th>Voucher Date</th>
                                                <th>Voucher No</th>
                                                <th>Client Name</th>
                                                <th>Deduction for Account Balance</th>
                                                <th>Voucher Status</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($wsp_commissions as $purchase)
                                                <tr>
                                                    <td>{{$purchase->due_date}}</td>
                                                    <td>{{$purchase->voucher_no}}</td>
                                                    <td>{{$purchase->first_name}} {{$purchase->last_name}}</td>
                                                    <td>
                                                        {{
                                                        number_format((float)$purchase->balance_deductions, 2, '.', ',');
                                                        }}
                                                    </td>


                                                    <td>{{$purchase->status}}</td>
                                                </tr>
                                            @endforeach

                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <th>Voucher Date</th>
                                                <th>Voucher No</th>
                                                <th>Client Name</th>
                                                <th>Account Balance Deduction</th>
                                                <th>Status</th>
                                            </tr>
                                            </tfoot>
                                        </table>

                                    </div><!-- col-md-12 -->
                                </div><!-- /row -->
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                    <div class="tab-pane" id="group">
                        group
                    </div>

                </div>
                <!-- /.tab-content -->
            </div>
            <!-- /.nav-tabs-custom -->

        </div>
        <!-- /.col -->
    </div>
    <input type="hidden" id="id" value="{{$id}}">


    <div id="upload_image" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header bg-blue-active">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><i class="ion ion-add"></i> Upload Image</h4>
                </div>
                <div class="modal-body">
                    <center>
                        {{ Form::open(array('url' => '/agents/uploadImage', 'files' => true)) }}
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-preview thumbnail" data-trigger="fileinput"
                                 style="width: 200px; height: 150px;">
                            </div>
                            <div>
                                <span class="btn btn-default btn-file">
                                    <span class="fileinput-new">Select image</span>
                                    <span class="fileinput-exists">Change</span>
                                    <input type="file" name="avatar">
                                </span>
                                <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                <button class="btn btn-default fileinput-exists">Upload Image</button>
                            </div>
                            <input type="hidden" id="txt_agent_id" name="txt_agent_id" value="{{$agent->id}}">
                        </div>
                        {{ Form::close() }}
                    </center>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    {{--MODAL--}}
    <div id="save-agent-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-blue-gradient">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><i class="ion ion-check"></i> Save Agent OK</h4>
                </div>
                <div class="modal-body">
                    Record has been saved.
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" id="btn_close_modal" data-dismiss="modal">Close
                    </button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--MOdal End--}}
</section>

@endsection

@section('additional_footer')

    <script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/jquery.dataTables.min.js")}}'></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js")}}'></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.full.min.js")}}'></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker.js")}}'></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/js/jasny-bootstrap.min.js"></script>

    <script>
        $(document).ready(function () {
            var phase_1_start = "";
            var phase_1_end = "";
            var phase_2_start = "";
            var phase_2_end = "";

            $(".select2").select2();
            $(".birthdate").daterangepicker({
                singleDatePicker: true,
                showDropdowns: true
            });
            $("#txt_phase_one").daterangepicker({
                showDropdowns: true
            });
            $('#txt_phase_one').on('apply.daterangepicker', function (ev, picker) {
                phase_1_start = picker.startDate.format('YYYY-MM-DD');
                phase_1_end = picker.endDate.format('YYYY-MM-DD');
            });
            $("#txt_phase_two").daterangepicker({
                showDropdowns: true
            });
            $('#txt_phase_two').on('apply.daterangepicker', function (ev, picker) {
                phase_2_start = picker.startDate.format('YYYY-MM-DD');
                phase_2_end = picker.endDate.format('YYYY-MM-DD');
            });
            @if(!$edit_mode){
                cancelEdit();
            }
            @endif

            $("#btn_update").click(function () {
                console.log('hahahah');
                $("#txt_first_name").removeAttr('disabled');
                $("#txt_last_name").removeAttr('disabled');
                $("input[name=rdo_gender]:checked").removeAttr('disabled');
                $("#txt_agent_birthdate").removeAttr('disabled');
                $("#slct_civil_status").removeAttr('disabled');
                $("#slct_dependents").removeAttr('disabled');
                $("input[name=rdo_can_drive]").removeAttr('disabled');
                $("input[name=rdo_own_car]").removeAttr('disabled');
                $("#txt_spouse_last_name").removeAttr('disabled');
                $("#txt_spouse_first_name").removeAttr('disabled');
                $("#txt_street").removeAttr('disabled');
                $("#txt_brgy").removeAttr('disabled');
                $("#slct_town").removeAttr('disabled');
                $("#slct_province").removeAttr('disabled');
                $("#txt_agent_landline").removeAttr('disabled');
                $("#txt_agent_mobile").removeAttr('disabled');
                $("#txt_agent_email").removeAttr('disabled');
                $("#txt_agent_sponsor").removeAttr('disabled');
                $("#txt_agent_sponsor_date").removeAttr('disabled');
                $("#txt_sss").removeAttr('disabled');
                $("#txt_tin").removeAttr('disabled');
                $("#txt_pagibig").removeAttr('disabled');
                $("#txt_philhealth").removeAttr('disabled');
                $("#txt_employer_name").removeAttr('disabled');
                $("#txt_employer_address").removeAttr('disabled');
                $("#txt_employer_contact").removeAttr('disabled');
                $("#txt_employer_email").removeAttr('disabled');
                //$("#agent_level").removeAttr('disabled');
                $("#txt_wsp_percenage").removeAttr('disabled');
                $("#txt_account_balance").removeAttr('disabled');
                $("#txt_phase_one").removeAttr('disabled');
                $("#txt_phase_two").removeAttr('disabled');

                $("#div_update_user").show();

            });

            $("#btn_cancel_update").click(function () {
                cancelEdit();
            });

            function cancelEdit() {
                $("#txt_user_last_name").attr('disabled', 'true');
                $("#txt_first_name").attr('disabled', 'true');
                $("#txt_last_name").attr('disabled', 'true');
                $("input[name=rdo_gender]").attr('disabled', 'true');
                $("#txt_agent_birthdate").attr('disabled', 'true');
                $("#slct_civil_status").attr('disabled', 'true');
                $("#slct_dependents").attr('disabled', 'true');
                $("input[name=rdo_can_drive]").attr('disabled', 'true');
                $("input[name=rdo_own_car]").attr('disabled', 'true');
                $("#txt_spouse_last_name").attr('disabled', 'true');
                $("#txt_spouse_first_name").attr('disabled', 'true');
                $("#txt_street").attr('disabled', 'true');
                $("#txt_brgy").attr('disabled', 'true');
                $("#slct_town").attr('disabled', 'true');
                $("#slct_province").attr('disabled', 'true');
                $("#txt_agent_landline").attr('disabled', 'true');
                $("#txt_agent_mobile").attr('disabled', 'true');
                $("#txt_agent_email").attr('disabled', 'true');
                $("#txt_agent_sponsor").attr('disabled', 'true');
                $("#txt_agent_sponsor_date").attr('disabled', 'true');
                $("#txt_sss").attr('disabled', 'true');
                $("#txt_tin").attr('disabled', 'true');
                $("#txt_pagibig").attr('disabled', 'true');
                $("#txt_philhealth").attr('disabled', 'true');
                $("#txt_employer_name").attr('disabled', 'true');
                $("#txt_employer_address").attr('disabled', 'true');
                $("#txt_employer_contact").attr('disabled', 'true');
                $("#txt_employer_email").attr('disabled', 'true');
                $("#txt_wsp_percenage").attr('disabled', 'true');
                $("#txt_account_balance").attr('disabled', 'true');
                $("#agent_level").attr('disabled', 'true');
                $("#txt_phase_one").attr('disabled', 'true');
                $("#txt_phase_two").attr('disabled', 'true');
                $("#div_update_user").hide();

                $("#txt_user_last_name").css('background-color', 'white');
            }


            $("#btn_save_agent").click(function () {
                $.post("/agents/update", {
                    txt_first_name: $("#txt_first_name").val(),
                    txt_last_name: $("#txt_last_name").val(),
                    rdo_gender: $("input[name=rdo_gender]:checked").val(),
                    txt_agent_birthdate: $("#txt_agent_birthdate").val(),
                    slct_civil_status: $("#slct_civil_status").val(),
                    slct_dependents: $("#slct_dependents").val(),
                    rdo_can_drive: $("input[name=rdo_can_drive]:checked").val(),
                    rdo_own_car: $("input[name=rdo_own_car]:checked").val(),
                    txt_spouse_last_name: $("#txt_spouse_last_name").val(),
                    txt_spouse_first_name: $("#txt_spouse_first_name").val(),
                    txt_street: $("#txt_street").val(),
                    txt_brgy: $("#txt_brgy").val(),
                    slct_town: $("#slct_town").val(),
                    slct_province: $("#slct_province").val(),
                    txt_agent_landline: $("#txt_agent_landline").val(),
                    txt_agent_mobile: $("#txt_agent_mobile").val(),
                    txt_agent_email: $("#txt_agent_email").val(),
                    txt_agent_sponsor: $("#txt_agent_sponsor").val(),
                    txt_agent_sponsor_date: $("#txt_agent_sponsor_date").val(),
                    txt_sss: $("#txt_sss").val(),
                    txt_tin: $("#txt_tin").val(),
                    txt_pagibig: $("#txt_pagibig").val(),
                    txt_philhealth: $("#txt_philhealth").val(),
                    txt_employer_name: $("#txt_employer_name").val(),
                    txt_employer_address: $("#txt_employer_address").val(),
                    txt_employer_contact: $("#txt_employer_contact").val(),
                    txt_employer_email: $("#txt_employer_email").val(),
                    txt_agent_id: $("#txt_agent_id").val(),
                    agent_level: $("#agent_level").val(),
                    txt_wsp_percenage: $("#txt_wsp_percenage").val(),
                    txt_account_balance: $("#txt_account_balance").val(),
                    first_sale: $("#txt_first_sale").val(),
                    bom: $("#txt_bom_date").val(),
                    first_presentation: $("#txt_first_presentation").val(),
                    phase_one_start: phase_1_start,
                    phase_one_end: phase_1_end,
                    phase_two_start: phase_2_start,
                    phase_two_end: phase_2_end
                }).done(function (data) {

                    $("#save-agent-modal").modal('show');
                });
            });
            $("#btn_close_modal").click(function () {
                window.location = '/agents?save=success';
            });
            $("#slct_town").change(function () {
                console.log('a');
                if ($("#slct_town").val() == "") {
                    $("#prov_code").val('0');
                    $.post("/municipalities", {
                        provcode: ''
                    }).done(function (data) {
                        var list = "<option value=''>Display All Municipalities</option>";
                        $("#slct_town").empty();
                        for (var j = 0; j < data.length; j++) {
                            list += "<option value='" + $("#slct_town").val() + "'>" + data[j].cityname + "</option>";
                        }
                        $("#slct_town").html(list);
                    });
                } else {
                    $.post("/provinces", {
                                citycode: $("#slct_town").val()
                            })
                            .done(function (data) {
                                $("#prov_code").val(data);
                                $("#slct_province").select2('val', data);

                            });
                }

            });

            $("#slct_province").change(function () {
                console.log('b');
                if ($("#prov_code").val() != $("#slct_province").val()) {
                    $.post("/municipalities", {
                        provcode: $("#slct_province").val()
                    }).done(function (data) {
                        var list = "<option value=''>Display All Municipalities</option>";
                        $("#slct_town").empty();
                        for (var j = 0; j < data.length; j++) {
                            list += "<option value='" + $("#slct_province").val() + "'>" + data[j].cityname + "</option>";
                        }
                        $("#slct_town").html(list);
                        $("#slct_town").select2('val', $("#slct_province").val());
                    });
                }
            });

            $('#txt_agent_sponsor').select2({
                ajax: {
                    url: "/bookings/searchConsultantSponsor",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term // search term
                        };
                    },
                    processResults: function (data) {
                        // parse the results into the format expected by Select2.
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data
                        return {
                            results: data
                        };
                    },
                    cache: true
                },
                minimumInputLength: 2,
                maximumSelectionLength: 1
            });
            var sales_table = $('#example1').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true
            });


            var wsp_table = $('#wsp_table').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true
            });


            var account_balance_table = $('#account_balance_table').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true
            });


            $(function () {

                function cb(start, end) {
                    $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                }

                cb(moment().subtract(29, 'days'), moment());

                $('#reportrange').daterangepicker({
                    ranges: {
                        'Today': [moment(), moment()],
                        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                        'This Month': [moment().startOf('month'), moment().endOf('month')],
                        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    }
                }, cb);
            });

            $('#reportrange').on('apply.daterangepicker', function (ev, picker) {
                var date_from = picker.startDate.format('YYYY-MM-DD');
                var date_to = picker.endDate.format('YYYY-MM-DD');

                $.get("/agents/getWsp", {
                    id: $("#id").val(),
                    from: date_from,
                    to: date_to,
                }).done(function (data) {
                    wsp_table.clear().draw();

                    for (x = 0; x < data.length; x++) {
                        wsp_table.row.add(
                                [
                                    "P " + data[x].wsp_commission,
                                    data[x].due_date,
                                    data[x].id,
                                    data[x].wsp_percentage + "%",
                                    data[x].status,
                                ]
                        ).draw();

                    }


                });
            });

            $(function () {

                function cb(start, end) {
                    $('#salesrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                }

                cb(moment().subtract(29, 'days'), moment());

                $('#salesrange').daterangepicker({
                    ranges: {
                        'Today': [moment(), moment()],
                        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                        'This Month': [moment().startOf('month'), moment().endOf('month')],
                        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    }
                }, cb);
            });

            $('#salesrange').on('apply.daterangepicker', function (ev, picker) {
                var date_from = picker.startDate.format('YYYY-MM-DD');
                var date_to = picker.endDate.format('YYYY-MM-DD');

                $.get("/agents/getSales", {
                    id: $("#id").val(),
                    from: date_from,
                    to: date_to,
                }).done(function (data) {
                    sales_table.clear().draw();

                    for (x = 0; x < data.length; x++) {
                        sales_table.row.add(
                                [
                                    "<input type='checkbox'>",
                                    data[x].id,
                                    data[x].first_name + data[x].last_name,
                                    data[x].purchase_date,
                                    data[x].total_price,
                                    data[x].status,
                                    '<a href="purchases/viewPurchaseDetails/' + data[x].id +
                                    'data-toggle="tooltip"' +
                                    'title="View ' + data[x].first_name + data[x].last_name + ' purchase details"' +
                                    'class="btn btn-success btn-xs">' +
                                    '<i class="fa fa-info-circle"></i> View' +
                                    '</a>'
                                ]
                        ).draw();

                    }


                });
            });
            $(function () {

                function cb(start, end) {
                    $('#balancerange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                }

                cb(moment().subtract(29, 'days'), moment());

                $('#balancerange').daterangepicker({
                    ranges: {
                        'Today': [moment(), moment()],
                        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                        'This Month': [moment().startOf('month'), moment().endOf('month')],
                        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    }
                }, cb);
            });

            $('#balancerange').on('apply.daterangepicker', function (ev, picker) {
                var date_from = picker.startDate.format('YYYY-MM-DD');
                var date_to = picker.endDate.format('YYYY-MM-DD');

                $.get("/agents/getAccountBalancePayments", {
                    id: $("#id").val(),
                    from: date_from,
                    to: date_to,
                }).done(function (data) {
                    account_balance_table.clear().draw();

                    for (x = 0; x < data.length; x++) {
                        account_balance_table.row.add(
                                [
                                    "P " + data[x].balance_deductions,
                                    data[x].due_date,
                                    data[x].id,
                                    data[x].account_balance_percentage + "%",
                                    data[x].status,
                                ]
                        ).draw();

                    }


                });
            });

        })
    </script>
@endsection