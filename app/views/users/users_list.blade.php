@extends('admin_template')

@section('additional_header')

        <!-- DataTables -->
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.min.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker-bs3.css")}}'>

@endsection

@section('content')
        <!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <!-- general form elements -->
            <div class="com-md-12 box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Search Users</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <div class="box-body">
                    <div class="row">
                        <form role="form">
                            <div class="col-md-2 col-sm-2">
                                <button id="btn_search" type="button" class="btn btn-primary" data-toggle="modal"
                                        data-target="#search-purchase"><i class="ion ion-search"></i> Search User
                                    Records
                                </button>
                            </div>
                            <div class="col-md-2 col-sm-2 col-md-offset-5 col-sm-offset-5">
                                @if(isAccessModuleAllowed('users_add'))
                                    <a href="/users/addUser" class="btn btn-success pull-right"><i
                                                class="fa fa-user"></i>
                                        Add New User</a>
                                @endif
                            </div>
                            <div class="col-md-3">
                                <div class="input-group">
                                    <select class="form-control">
                                        <option>--Select Action--</option>
                                        <option>Active</option>
                                        <option>Deactivated</option>
                                    </select>

                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-info">Go</button>
                                    </div>
                                    <!-- /btn-group -->
                                </div>
                            </div>

                        </form>
                    </div>
                    <!-- /row -->

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
            <!--/.col (left) -->
        </div>
        <!-- search form -->
    </div>
    <!-- /.row (main row) -->

    <div class="row">
        <div class=" col-md-12 col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-street-view"></i> User Records</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>&nbsp;</th>
                            <th>User No</th>
                            <th>Last Name</th>
                            <th>First Name</th>
                            <th>User Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td><input type="checkbox"></td>
                                <td>{{$user['email']}}</td>
                                <td>{{$user['last_name']}}</td>
                                <td>{{$user['first_name']}}</td>
                                <td>{{$user['status']}}</td>
                                <td>
                                    @if(isAccessModuleAllowed('users_view'))
                                        <a href="users/viewProfile/{{$user['id']}}"
                                           data-toggle="tooltip"
                                           title="View {{$user['first_name']}} {{$user['last_name']}}'s details"
                                           class="btn btn-success btn-xs">
                                            <i class="fa fa-info-circle"></i> View
                                        </a>
                                    @endif
                                    @if(isAccessModuleAllowed('users_edit'))
                                        <a href="users/editProfile/{{$user['id']}}/edit"
                                           data-toggle="tooltip"
                                           title="Edit {{$user['first_name']}} {{$user['last_name']}}'s details"
                                           class="btn btn-primary btn-xs">
                                            <i class="fa fa-edit"></i> Edit
                                        </a>
                                    @endif
                                    @if(isAccessModuleAllowed('users_delete'))
                                        @if($user['status']=='ACTIVE')
                                            <button type="button" class="btn btn-danger btn-xs deactivate-user"
                                                    data-toggle="modal"
                                                    data-id="{{$user['id']}}"
                                                    data-target="#delete-user"><i class="fa fa-warning"></i> Deactivate
                                            </button>
                                        @else
                                            <button type="button" class="btn btn-warning btn-xs activate-user"
                                                    data-toggle="modal"
                                                    data-id="{{$user['id']}}"
                                                    data-target="#activate-user"><i class="fa fa-power-off"></i>
                                                Activate
                                            </button>
                                        @endif
                                    @endif

                                    {{--<a href="users/deleteProfile/{{$user['id']}}"--}}
                                    {{--data-toggle="tooltip"--}}
                                    {{--title="Deactivate {{$user['first_name']}} {{$user['last_name']}}'s details"--}}
                                    {{--class="btn btn-warning btn-xs"--}}
                                    {{--data-toggle="modal"--}}
                                    {{--data-target="#search-purchase">--}}
                                    {{--<i class="fa fa-warning"></i> Deactivate--}}
                                    {{--</a>--}}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>&nbsp;</th>
                            <th>Client No</th>
                            <th>Last Name</th>
                            <th>First Name</th>
                            <th>Client Status</th>
                            <th>Action</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
</section>

@endsection

@section('additional_footer')

    <script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/jquery.dataTables.min.js")}}'></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js")}}'></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.full.min.js")}}'></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker.js")}}'></script>


    <script>
        $(document).ready(function () {
            $('#example1').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true
            });

            $("#btn_search").click(function () {
                $(".select2").select2({
                    theme: "classic"
                });
                $('#txt_daterange').daterangepicker();

            });

            $(".hide-filter").each(function (index) {
                $(this).parent().parent().hide();
                $(this).on("click", function () {
                    $(this).parent().parent().hide();
                    $("." + $(this).parent().parent().attr('id')).show();
                });
            });

            $(".dropdown-menu li").each(function (index) {
                $(this).on("click", function () {
                    $("#" + $(this).children("input").val()).show();
                    $(this).hide();
                });
            });

            $(document).on("click", ".deactivate-user", function () {
                var myBookId = $(this).data('id');
                $(".modal-body #userId").val(myBookId);
            });

            $("#btn_deactivate").click(function () {
                $.post("/users/delete", {
                    userId: $(".modal-body #userId").val()
                }).done(function (data) {
                    window.location = '/users?deactivate=success';
                });
            });

            $(document).on("click", ".activate-user", function () {
                var myBookId = $(this).data('id');
                var button = $(this);
                $(".modal-body #userId").val(myBookId);
            });
            $("#btn_activate").click(function () {
                $.post("/users/activate", {
                    userId: $(".modal-body #userId").val()
                }).done(function (data) {
                    window.location = '/users?reactivate=success';

                });
            });


        });
    </script>
@endsection