@extends('admin_template')

@section('additional_header')

    <link rel="stylesheet"
          href='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker-bs3.css")}}'>
    <link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/iCheck/all.css")}}'>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.css")}}'>
    <link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.min.css")}}'>

    @endsection

    @section('content')
            <!-- Main content -->
    <section class="content">
        <!-- Purchase Order -->
        <div class="row">
            <div class="col-md-12 col-xs-12">

                <div class="com-md-12 box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-shopping-cart"></i> Purchase Order</h3>
                    </div>
                    <!-- /.box-header -->

                    <!-- form start -->
                    <form class="form-horizontal">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <!-- PO Number -->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="txt_po_no" class="col-sm-3 control-label">PO No</label>

                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-edit"></i>
                                                    </div>
                                                    <input type="text" class="form-control pull-right"
                                                           value="{{$purchase_order->purchase_order_no}}"
                                                           id="txt_purchase_order_no" readonly>
                                                    <input type="hidden" class="form-control pull-right"
                                                           value="{{date('Ymd').'-'.$purchase_id}}" readonly
                                                           id="txt_po_no" disabled style="background-color:white">
                                                    <input type="hidden" class="form-control pull-right"
                                                           value="{{$purchase_id}}" readonly id="hdn_po_no">
                                                </div>
                                                <!-- /.input group -->
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /PO Number -->
                                    <!-- Delivery Date -->

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="txt_delivery_date" class="col-sm-3 control-label">Delivery
                                                Date</label>

                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </div>

                                                    <input type="text" value="{{ $purchase_order->purchase_date }}"
                                                           class="form-control pull-right"
                                                           id="txt_delivery_date" disabled
                                                           style="background-color:white">
                                                </div>

                                                <!-- /.input group -->
                                            </div>
                                            <!-- /.col-sm-9 -->
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                    <!-- /Delivery Date -->

                                    <!-- Purchase Date -->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="txt_purchase_date" class="col-sm-3 control-label">Purchase
                                                Date</label>

                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </div>
                                                    <input type="text" value="{{ $purchase_order->delivery_date }}"
                                                           class="form-control pull-right"
                                                           id="txt_purchase_date" disabled
                                                           style="background-color:white">
                                                </div>

                                                <!-- /.input group -->
                                            </div>
                                            <!-- /.col-sm-8 -->
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                    <!-- /.col-md-6 -->
                                    <!-- Terms -->
                                    <div class="col-md-6">
                                        <div class="form-group" id="set">
                                            <label for="slct_terms" class="col-sm-3 control-label">Terms</label>

                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-rub"></i>
                                                    </div>
                                                    <select id="slct_terms" class="form-control select2" disabled
                                                            style="background-color:white">
                                                        <option @if($purchase_order->terms=='cod') selected
                                                                @endif value="cash">Cash on Delivery
                                                        </option>
                                                        <option @if($purchase_order->terms=='2') selected
                                                                @endif  value="2">2 Months Installment
                                                        </option>
                                                        <option @if($purchase_order->terms=='3') selected
                                                                @endif  value="3">3 Months Installment
                                                        </option>
                                                        <option @if($purchase_order->terms=='4') selected
                                                                @endif  value="4">4 Months Installment
                                                        </option>
                                                        <option @if($purchase_order->terms=='5') selected
                                                                @endif  value="5">5 Months Installment
                                                        </option>
                                                        <option @if($purchase_order->terms=='6') selected
                                                                @endif  value="6">6 Months Installment
                                                        </option>
                                                        <option @if($purchase_order->terms=='7') selected
                                                                @endif  value="7">7 Months Installment
                                                        </option>
                                                        <option @if($purchase_order->terms=='8') selected
                                                                @endif  value="8">8 Months Installment
                                                        </option>
                                                        <option @if($purchase_order->terms=='9') selected
                                                                @endif  value="9">9 Months Installment
                                                        </option>
                                                        <option @if($purchase_order->terms=='10') selected
                                                                @endif  value="10">10 Months Installment
                                                        </option>
                                                        <option @if($purchase_order->terms=='11') selected
                                                                @endif  value="11">11 Months Installment
                                                        </option>
                                                        <option @if($purchase_order->terms=='12') selected
                                                                @endif  value="12">12 Months Installment
                                                        </option>
                                                        <option @if($purchase_order->terms=='13') selected
                                                                @endif  value="13">13 Months Installment
                                                        </option>
                                                        <option @if($purchase_order->terms=='14') selected
                                                                @endif  value="14">14 Months Installment
                                                        </option>
                                                        <option @if($purchase_order->terms=='15') selected
                                                                @endif  value="15">15 Months Installment
                                                        </option>
                                                    </select>
                                                </div>
                                                <!-- /.input group -->
                                            </div>
                                            <!-- /.col-sm-9 -->
                                        </div>
                                        <!-- /. form-group -->
                                    </div>
                                    <!-- /.Terms -->

                                    <!-- Client -->
                                    <div class="col-md-6">
                                        <div class="form-group" id="client">
                                            <label for="slct_terms" class="col-sm-3 control-label">Client</label>

                                            <div class="col-sm-9">
                                                <select id="slct_clients" class="form-control" disabled
                                                        style="background-color:white">
                                                    <option value="{{$purchase_order->client_client_id}}">{{$client}}</option>
                                                </select>
                                                <!-- /.input group -->
                                            </div>
                                            <!-- /.col-sm-9 -->
                                        </div>
                                        <!-- /. form-group -->
                                    </div>
                                    <!-- /.Client -->

                                    <!-- Client -->
                                    <div class="col-md-6">
                                        <div class="form-group" id="client">
                                            <label for="slct_terms" class="col-sm-3 control-label">Total Purchase
                                                Price</label>

                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-rub"></i>
                                                    </div>
                                                    <input type="text" value="{{ $total_price }}"
                                                           class="form-control pull-right" disabled
                                                           style="background-color:white">
                                                </div>

                                                <!-- /.input group -->
                                            </div>
                                            <!-- /.col-sm-9 -->
                                        </div>
                                        <!-- /. form-group -->
                                    </div>
                                    <!-- /.Client -->


                                </div>
                                <!-- ./col-md-12 -->
                            </div>
                            <!-- /.row-->
                        </div>
                        <!-- /.box-body -->
                    </form>
                    <!-- /form-end -->
                </div>
                <!-- /.box -->
                <!--/.col (left) -->
            </div>
            <!-- search form -->
        </div>
        <!-- /Purchase Order -->

        <!-- Client Information -->
        <div class="row hidden">
            <div class=" col-md-12 col-xs-12">
                <div class="row">
                    <!-- client -->
                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="box-header">
                                <h3 class="box-title"><i class="fa fa-user"></i> Client Information</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="col-md-12">
                                    <div class="row">
                                        <!-- client information -->
                                        <div class="pull-right">
                                            <!-- Buttons -->
                                            <button id="btn_search" type="button" class="btn bg-olive"><i
                                                        class="ion ion-search"></i> Search Client
                                            </button>
                                            <a href="/clients/addNew?purchase=true" id="btn_search" type="button"
                                               class="btn btn-primary"><i class="ion ion-android-person-add"></i> New
                                                Client</a>
                                        </div>
                                        <!-- /.col-md-4 -->
                                    </div>
                                    <hr style="margin:2% 0 0 0">
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-2">
                                        <!-- Profile Image -->
                                        <img class="profile-user-img img-responsive img-circle"
                                             src='{{ asset ("/bower_components/AdminLTE/dist/img/user4-128x128.jpg") }}'
                                             style="margin-top:3%" alt="User profile picture">

                                        <h3 class="profile-username text-center">Nina Mcintire</h3>
                                        <a href="#" class="btn btn-primary btn-block"><b>View Profile</b></a>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="box-header with-border">
                                            <h3 class="box-title">Personal Information</h3>
                                        </div>
                                        <!-- /.box-header -->
                                        <div class="box-body">
                                            <strong><i class="fa fa-home margin-r-5"></i><i
                                                        class="fa fa-book marker-r-5"></i> Home Address</strong>

                                            <p class="text-muted">
                                                74 San Isidro St. Brgy. Sto. Niño, Quezon City Philippines 1113
                                            </p>

                                            <strong><i class="fa fa-home margin-r-5"></i><i
                                                        class="fa fa-map- fa-phone margin-r-5"></i> Contact
                                                Number</strong>

                                            <p class="text-muted">09258050517</p>

                                            <strong><i class="fa fa-home margin-r-5"></i><i
                                                        class="fa fa-envelope-o margin-r-5"></i> Email</strong>

                                            <p>emaymarino@gmail.com</p>
                                        </div>
                                        <!-- /.box-body -->
                                    </div>
                                    <div class="col-md-5">
                                        <div class="box-header with-border">
                                            <h3 class="box-title">Business Information</h3>
                                        </div>
                                        <!-- /.box-header -->
                                        <div class="box-body">
                                            <strong><i class="fa fa-briefcase margin-r-5"></i><i
                                                        class="fa fa-marker margin-r-5"></i> Business Address</strong>

                                            <p class="text-muted">
                                                74 San Isidro St. Brgy. Sto. Niño, Quezon City Philippines 1113
                                            </p>

                                            <strong><i class="fa fa-briefcase margin-r-5"></i><i
                                                        class="fa fa-map- fa-phone margin-r-5"></i> Contact
                                                Number</strong>

                                            <p class="text-muted">09258050517</p>

                                            <strong><i class="fa fa-briefcase margin-r-5"></i><i
                                                        class="fa fa-envelope-o margin-r-5"></i> Email</strong>

                                            <p>emaymarino@gmail.com</p>
                                        </div>
                                        <!-- /.box-body -->
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                    <!-- /client -->

                </div>
            </div>
        </div>
        <!-- / client information -->

        <!-- Purchased Items -->
        <div class="row">
            <div class="col-md-12 col-xs-12">

                <div class="com-md-12 box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-cutlery"></i> Purchase Items</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form class="form-horizontal">
                        <div class="box-body">
                            {{--add purchase item--}}
                            <div class="row hidden" id="add_purchase_item">
                                <div class="col-md-12">
                                    <!-- Purchase Type -->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="slct_purchase_type" class="col-sm-3 control-label">Purchase
                                                Type</label>

                                            <div class="col-sm-9">

                                                <select id="slct_purchase_type" class="form-control select2"
                                                        style="width: 100%;height:100%">
                                                    <option value="">Select Item Type</option>
                                                    <option value="item">Per Item</option>
                                                    <option value="set">Per Set</option>
                                                </select>
                                            </div>
                                            <!-- /.col-sm-9 -->
                                        </div>
                                    </div>
                                    <!-- /Purchase Type -->

                                    <!-- Item / Set -->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="slct_purchase_item" id="lbl_item_set"
                                                   class="col-sm-3 control-label">Item/Set</label>

                                            <div class="col-sm-9">
                                                <select id="slct_purchase_item" class="set form-control select2"
                                                        style="width: 100%;height:100%;">
                                                </select>

                                            </div>
                                            <!-- /.col-sm-9 -->
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                    <!-- /Item / Set -->

                                    <!-- Qty -->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="txt_qty" class="col-sm-3 control-label">Quantity</label>

                                            <div class="col-sm-9">
                                                <input type="text" value="1" class="form-class" id="txt_qty">
                                            </div>
                                            <!-- /.col-sm-8 -->
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                    <!-- /.Qty -->

                                    <!-- Gift -->
                                    <div class="col-md-6">
                                        <div class="form-group" id="set">
                                            <label for="rdo_gift" class="col-sm-3 control-label">Is this a gift?</label>

                                            <div class="col-sm-9">
                                                <label style="margin-top:2%;margin-left:2%">
                                                    <input type="radio" name="is_gift" value="Yes" class="minimal"
                                                           checked>
                                                    Yes
                                                </label>
                                                <label style="margin-top:2%;margin-left:2%">
                                                    <input type="radio" name="is_gift" value="No" class="minimal">
                                                    No
                                                </label>

                                            </div>
                                            <!-- /.col-sm-9 -->
                                        </div>
                                        <!-- /. form-group -->
                                    </div>
                                    <!-- //Gift -->

                                    <!-- Price -->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="lbl_item_set_price" class="col-sm-3 control-label">Price</label>

                                            <div class="col-sm-9">
                                                <label for="lbl_item_set" id="lbl_item_set_price" class="control-label"><i
                                                            class="fa fa-rub"></i> 0.00</label>
                                                <input type="hidden" id="total_item_price">
                                                <input type="hidden" id="unit_cost">
                                            </div>
                                            <!-- /.col-sm-9 -->
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                    <!-- /Price -->

                                    <!-- Add to List -->
                                    <div class="col-md-6">
                                        <button class="btn btn-danger pull-right" style="margin-left:3%"> Reset</button>
                                        <button type="button" id="btn_add_purchase_item"
                                                class="btn btn-primary pull-right"><i class="fa fa-plus"></i>Add to List
                                        </button>

                                    </div>
                                    <!-- /Add to List -->

                                </div>
                                <!-- ./col-md-12 -->
                            </div>
                            <!-- /.row-->

                            <hr class="hidden" style="margin-top:1%">

                            <!-- Total Purchase Price -->
                            <div class="row hidden">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-sm-3 col-sm-offset-6 control-label">Total Purchase
                                            Price</label>

                                        <div class="col-sm-2">
                                            <label class="control-label"><i class="fa fa-rub"></i>
                                                100,000,000.00</label>
                                        </div>
                                        <!-- /.col-sm-9 -->
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                            </div>

                            <!-- Purchase Item Table -->
                            <div class="row">
                                <div class="col-md-12">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>&nbsp;</th>
                                            <th>Item/Set</th>
                                            <th>Qty</th>
                                            <th>Unit Cost</th>
                                            <th>Is Gift</th>
                                            <th>Total Amount</th>
                                        </tr>
                                        </thead>
                                        <tbody id="purchase_item_list">
                                        @foreach($purchase_list as $purchase_item)
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td>{{Product::find($purchase_item->item_id)->description}}</td>
                                                <td>{{$purchase_item->qty}}</td>
                                                <td>{{$purchase_item->item_price }}</td>
                                                <td>{{$purchase_item->gift }}</td>
                                                <td>{{$purchase_item->item_price * $purchase_item->qty }}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /Purchase Item Table -->

                            <!-- Remove From List -->
                            <div class="row hidden">
                                <div class="col-md-12">
                                    <button class="btn bg-orange pull-right"> Remove From List</button>
                                </div>
                            </div>
                            <!-- /. Remove From List -->
                        </div>
                        <!-- /.box-body -->
                    </form>
                    <!-- /form-end -->
                </div>
                <!-- /.box -->
                <!--/.col (left) -->
            </div>
            <!-- search form -->
        </div>
        <!-- /Purchased Items -->

        <div class="row">
            <div class=" col-md-12">
                <!-- Representative -->
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title"><i class="fa fa-users"></i> Representative</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">

                        <!-- form start -->
                        <form class="form-horizontal">
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <!-- Consultant -->
                                        <div class="col-md-6">
                                            <div class="form-group" id="set">
                                                <label for="slct_consultant"
                                                       class="col-sm-3 control-label">Consultant</label>

                                                <div class="col-sm-9">
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-user"></i>
                                                        </div>
                                                        <select id="slct_consultant" class="form-control select2"
                                                                disabled
                                                                style="background-color:white">
                                                            <option value="{{$purchase_order->consultant_id}}">{{ $consultant_name }}</option>
                                                        </select>
                                                    </div>
                                                    <!-- /.input group -->
                                                </div>
                                                <!-- /.col-sm-9 -->
                                            </div>
                                            <!-- /. form-group -->
                                        </div>
                                        <!-- /.Consultant -->

                                        <!-- Sponsor -->
                                        <div class="col-md-6">
                                            <div class="form-group" id="set">
                                                <label for="slct_sponsor" class="col-sm-3 control-label">Sponsor</label>

                                                <div class="col-sm-9">
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-user"></i>
                                                        </div>
                                                        <select id="slct_sponsor" class="form-control select2"
                                                                disabled
                                                                style="background-color:white">
                                                            <option value="{{$purchase_order->sponsor_id}}">{{ $sponsor_name }}</option>
                                                        </select>
                                                    </div>
                                                    <!-- /.input group -->
                                                </div>
                                                <!-- /.col-sm-9 -->
                                            </div>
                                            <!-- /. form-group -->
                                        </div>
                                        <!-- /.Sponsor -->

                                        <!-- Associate -->
                                        <div class="col-md-6">
                                            <div class="form-group" id="set">
                                                <label for="slct_associate"
                                                       class="col-sm-3 control-label">Associate</label>

                                                <div class="col-sm-9">
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-user"></i>
                                                        </div>
                                                        <select id="slct_associate" class="form-control select2"
                                                                disabled
                                                                style="background-color:white">
                                                            <option value="{{$purchase_order->associate_id}}">{{ $associate_name }}</option>
                                                        </select>
                                                    </div>
                                                    <!-- /.input group -->
                                                </div>
                                                <!-- /.col-sm-9 -->
                                            </div>
                                            <!-- /. form-group -->
                                        </div>
                                        <!-- /.Associate -->

                                        <!-- Fast Track -->
                                        <div class="col-md-6">
                                            <div class="form-group" id="set">
                                                <label for="rdo_gift" class="col-sm-3 control-label">Fast Track?</label>

                                                <div class="col-sm-9">
                                                    <label style="margin-top:2%;margin-left:2%">
                                                        {{$purchase_order->fast_track}}
                                                    </label>


                                                </div>
                                                <!-- /.col-sm-9 -->
                                            </div>
                                            <!-- /. form-group -->
                                        </div>
                                        <!-- // Fast Track -->


                                    </div>
                                    <!-- ./col-md-12 -->
                                </div>
                                <!-- /.row-->
                            </div>
                            <!-- /.box-body -->
                        </form>
                        <!-- /form-end -->

                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box-primary -->
            </div>
        </div>

        <!-- Remarks -->
        <div class="row">
            <div class=" col-md-12">
                <!-- Representative -->
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title"><i class="fa  fa-pencil-square-o"></i> Details</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">

                        <!-- form start -->
                        <form class="form-horizontal">
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <!-- Remarks -->
                                        <div class="col-md-6">
                                            <div class="form-group" id="set">
                                                <label for="txtarea_remarks"
                                                       class="col-sm-3 control-label">Remarks</label>

                                                <div class="col-sm-9">
                                                    <div class="form-group">
                                                        <textarea id="txtarea_remarks" class="form-control" rows="3"
                                                                  placeholder="Enter ..." disabled
                                                                  style="background-color:white">{{$purchase_order->remarks}}</textarea>
                                                    </div>
                                                </div>
                                                <!-- /.col-sm-9 -->
                                            </div>
                                            <!-- /. form-group -->
                                        </div>
                                        <!-- /.Remarks -->

                                        <!-- Transaction Status -->
                                        <div class="col-md-6 hidden">
                                            <div class="form-group" id="set">
                                                <label for="slct_trans_status" class="col-sm-3 control-label">Transaction
                                                    Status</label>

                                                <div class="col-sm-9">
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-heartbeat"></i>
                                                        </div>
                                                        <select id="slct_trans_status" class="form-control select2"
                                                                disabled
                                                                style="background-color:white">
                                                            <option @if($purchase_order->status=='Paid') selected @endif>
                                                                Paid
                                                            </option>
                                                            <option @if($purchase_order->status=='Incomplete') selected @endif>
                                                                Incomplete
                                                            </option>
                                                        </select>
                                                    </div>
                                                    <!-- /.input group -->
                                                </div>
                                                <!-- /.col-sm-9 -->
                                            </div>
                                            <!-- /. form-group -->
                                        </div>
                                        <!-- /.Transaction Status -->

                                        <div class="row">
                                            <div class="col-md-12">
                                                <a href="/purchases/editPurchase/{{$purchase_order->id}}" type="button"
                                                   class="btn btn-success pull-right"
                                                   style="margin-left:1%"> Edit Purchase Order
                                                </a>
                                                @if($purchase_order->status!='Cancelled')
                                                    <button type="button" data-toggle="modal"
                                                            data-target="#cancel-po" class="btn bg-orange pull-right">
                                                        Cancel Purchase Order
                                                    </button>
                                                @else
                                                    <button type="button" data-toggle="modal"
                                                            data-target="#reactivate-po"
                                                            class="btn btn-info pull-right">
                                                        Reactivate Purchase Order
                                                    </button>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <!-- ./col-md-12 -->

                                    <!-- /. Remove From List -->
                                </div>
                                <!-- /.row-->
                            </div>
                            <!-- /.box-body -->
                        </form>
                        <!-- /form-end -->
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box-primary -->
            </div>
        </div>
        <!-- /.Remarks -->

        <div id="cancel-po" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-orange-active">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><i class="ion ion-search"></i>Cancel Purchase Order</h4>
                    </div>
                    <div class="modal-body">
                        Do you want to cancel this purchase order?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" id="btn_cancel" class="btn btn-warning"><i
                                    class="ion ion-close-round"></i>
                            Cancel Purchase Order
                        </button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

        <div id="reactivate-po" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-aqua-gradient">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><i class="ion ion-search"></i>Cancel Purchase Order</h4>
                    </div>
                    <div class="modal-body">
                        Do you want to reactivate this purchase order?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" id="btn_reactivate" class="btn btn-info"><i
                                    class="ion ion-close-round"></i>
                            Reactivate Purchase Order
                        </button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </section>
@endsection

@section('additional_footer')

    <script src='{{ asset("/bower_components/AdminLTE/plugins/jQueryUI/jquery-ui.min.js")}}'></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.full.min.js")}}'></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker.js")}}'></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/iCheck/icheck.min.js")}}'></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/jquery.dataTables.min.js")}}'></script>

    <script>
        $(document).ready(function () {
            var product_obj;
            $('#txt_delivery_date, #txt_purchase_date').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                locale: {
                    format: 'YYYY/mm/dd'
                }
            });

            $("#txt_qty").spinner({
                min: 1,
                change: function (event, ui) {
                    // alert();
                    updatePrice();
                }
            });


            //iCheck for checkbox and radio inputs
            $('input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass: 'iradio_minimal-blue'
            });

            var table_data = $('#example1').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "info": false,
                "autoWidth": true
            });

            $("#slct_purchase_type").change(function () {
                var product_type = $("#slct_purchase_type").val();
                if (product_type == "") {
                    $("#slct_purchase_item").empty();
                    return false;
                }

                $.post("/products/getProductsByType", {
                    product_type: product_type
                }).done(function (data) {
                    product_obj = data;
                    $('#slct_purchase_item').empty();
                    $('#slct_purchase_item').append($('<option>', {
                        value: '',
                        text: 'Select One'
                    }));
                    $.each(data, function (i, item) {
                        console.log(item);
                        $('#slct_purchase_item').append($('<option>', {
                            value: item.id,
                            text: item.item_code
                        }));

                    });
                });

            });

            $("#slct_purchase_item").change(function () {
                updatePrice();
            });

            $("#btn_add_purchase_item").click(function () {
                $.post("/purchases/createPOItem", {
                    hdn_po_no: $("#hdn_po_no").val(),
                    slct_client: $("#slct_clients").val(),
                    txt_po_no: $("#txt_po_no").val(),
                    slct_terms: $("#slct_terms").val(),
                    txt_delivery_date: $("#txt_delivery_date").val(),
                    txt_purchase_date: $("#txt_purchase_date").val(),
                    slct_purchase_item: $("#slct_purchase_item").val(),
                    txt_qty: $("#txt_qty").val(),
                    is_gift: $("input[name=is_gift]:checked").val(),
                    slct_consultant: $("#slct_consultant").val(),
                    slct_sponsor: $("#slct_sponsor").val(),
                    slct_associate: $("#slct_associate").val(),
                    is_fast_track: $("input[name=is_fast_track]:checked").val(),
                    txtarea_remarks: $("#txtarea_remarks").val(),
                    slct_trans_status: $("#slct_trans_status").val(),
                    total_item_price: $("#total_item_price").val(),
                    slct_purchase_type: $("#slct_purchase_type").val()
                }).done(function (data) {
                    console.log(data)
                    if (data[1]) {
                        $("#" + data[0].id + "_qty").val(data[0].qty);
                        $("#" + data[0].id + "_amt").val(data[0].item_price);
                        $("#" + data[0].id + "_qty").siblings("span").html(data[0].qty);
                        $("#" + data[0].id + "_amt").siblings("span").html('<i class="fa fa-rub"></i> ' + parseFloat(data[0].item_price).toFixed(2));
                    } else {
                        table_data.row.add(
                                [
                                    "<input type=checkbox>",
                                    $("#slct_purchase_type").val(),
                                    $("#slct_purchase_item :selected").text(),
                                    "<input type='hidden' id='" + data[0].id + "_qty'>" + "<span>" + $("#txt_qty").val() + "</span>",
                                    $("#unit_cost").val(),
                                    $("input[name=is_gift]:checked").val(),
                                    "<input type='hidden' id='" + data[0].id + "_amt'>" + "<span>" + '<i class="fa fa-rub"></i> ' + parseFloat($("#total_item_price").val() * $("#txt_qty").val()).toFixed(2) + "</span>"
                                ]
                        ).draw();
                    }


                    $('#slct_purchase_item').append($('<tr>'));
                    $('#slct_purchase_item').children('tr').last().append($('<td>', {
                        text: $("#slct_purchase_type").val()
                    }));
                });
            });

            $("input[type=radio][name=is_gift]").on('ifChanged', function () {
                updatePrice();
            });

            function updatePrice() {
                var qty = $("#txt_qty").val();
                if ($("#slct_purchase_item").children("option").length == 0) return;
                var result = $.grep(product_obj, function (e) {
                    return e.id == $("#slct_purchase_item").val();
                });
                if (result.length == 0) {
                    // not found
                } else if (result.length == 1) {
                    // access the foo property using result[0].foo
                    $("#lbl_item_set_price").html('<i class="fa fa-rub"></i> ' + parseFloat(result[0].retail_price * qty).toFixed(2));
                    $("#total_item_price").val(parseFloat(result[0].retail_price * qty));
                    $("#unit_cost").val(parseFloat(result[0].retail_price));
                } else {
                    // multiple items found
                }
                if ($("input[name=is_gift]:checked").val() == 'Yes') {
                    $("#lbl_item_set_price").html('<i class="fa fa-rub"></i> 0.00');
                    $("#total_item_price").val(0);
                    $("#unit_cost").val(result[0].retail_price);
                }
            }

            $("#btn_save_and_print").click(function () {
                $.post("/purchases/savePO", {
                    hdn_po_no: $("#hdn_po_no").val(),
                    slct_client: $("#slct_clients").val(),
                    txt_po_no: $("#txt_po_no").val(),
                    slct_terms: $("#slct_terms").val(),
                    txt_delivery_date: $("#txt_delivery_date").val(),
                    txt_purchase_date: $("#txt_purchase_date").val(),
                    slct_purchase_item: $("#slct_purchase_item").val(),
                    txt_qty: $("#txt_qty").val(),
                    is_gift: $("input[name=is_gift]:checked").val(),
                    slct_consultant: $("#slct_consultant").val(),
                    slct_sponsor: $("#slct_sponsor").val(),
                    slct_associate: $("#slct_associate").val(),
                    is_fast_track: $("input[name=is_fast_track]:checked").val(),
                    txtarea_remarks: $("#txtarea_remarks").val(),
                    slct_trans_status: $("#slct_trans_status").val(),
                    total_item_price: $("#total_item_price").val(),
                    slct_purchase_type: $("#slct_purchase_type").val()
                }).done(function (data) {
                    window.location = '/purchases?approved=true';
                });
            });

            $("#btn_cancel").click(function () {

            });

            $('#slct_clients').select2({
                ajax: {
                    url: "/purchases/searchClient",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term // search term
                        };
                    },
                    processResults: function (data) {
                        // parse the results into the format expected by Select2.
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data
                        return {
                            results: data
                        };
                    },
                    cache: true
                },
                minimumInputLength: 2
            });

            $("#btn_cancel").click(function () {
                $.post("/purchases/cancelPurchase/{{$purchase_order->id}}", {
                    hdn_po_no: $("#hdn_po_no").val()
                }).done(function (data) {
                    console.log(data);
                    window.location = '/purchases?cancelled=true';
                });
            });
            $("#btn_reactivate").click(function () {
                $.post("/purchases/reactivatePurchase/{{$purchase_order->id}}", {
                    hdn_po_no: $("#hdn_po_no").val()
                }).done(function (data) {
                    console.log(data);
                    window.location = '/purchases?cancelled=true';
                });
            });

        });
    </script>
@endsection