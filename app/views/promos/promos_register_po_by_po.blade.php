@extends('admin_template')

@section('additional_header')

    <link rel="stylesheet"
    
          href='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker-bs3.css")}}'>
    <link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/iCheck/all.css")}}'>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.css")}}'>
    <link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.min.css")}}'>

    @endsection

    @section('content')
            <!-- Main content -->
    <section class="content">
        <!-- Purchase Order -->
        <div class="row">
            <div class="col-md-12 col-xs-12">

                <div class="com-md-12 box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-shopping-cart"></i> Register Purchase Order</h3>
                    </div>
                    <!-- /.box-header -->

                    <!-- form start -->
                    <form class="form-horizontal">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <!-- /.Terms -->

                                    <div class="col-md-12">
                                        <div class="form-group" id="set">
                                            <label for="slct_terms" class="col-sm-3 control-label">Contest Title</label>

                                            <div class="col-sm-6">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-edit"></i>
                                                    </div>
                                                    <select id="slct_contest_list" class="form-control select2">

                                                    </select>
                                                </div>
                                                <!-- /.input group -->
                                            </div>
                                            <!-- /.col-sm-6 -->

                                            <div class="col-md-3">
                                                <button type="button" id="btn_load_po"
                                                        class="btn btn-success"
                                                        style="margin-left:1%"> Select
                                                </button>

                                            </div>
                                        </div>
                                        <!-- /. form-group -->
                                    </div>
                                    <!-- /.Terms -->

                                    <div class="col-md-12">
                                        <div class="box-body">
                                            <table id="tblcontestants" class="table table-bordered table-striped">
                                                <thead>
                                                <tr>
                                                    <th>PO NO</th>
                                                    <th>Date</th>
                                                    <th>Item</th>
                                                    <th>Status</th>
                                                    <th>Sponsor</th>
                                                    <th>Associate</th>
                                                    <th>Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($purchases as $purchase)
                                                    <tr>
                                                        <td><input type="checkbox"></td>
                                                        <td>{{$purchase->id}}</td>
                                                        <td>{{$purchase->first_name}} {{$purchase->last_name}}</td>
                                                        <td>{{$purchase->purchase_date}}</td>
                                                        <td>{{$purchase->status}}</td>
                                                        <td>
                                                            <a href="purchases/viewPurchaseDetails/{{$purchase->id}}"
                                                               data-toggle="tooltip"
                                                               title="View {{$purchase->first_name}} {{$purchase->last_name}}'s purchase details"
                                                               class="btn btn-success btn-xs">
                                                                <i class="fa fa-info-circle"></i> View
                                                            </a>
                                                            <a href="purchases/editPurchase/{{$purchase->id}}"
                                                               data-toggle="tooltip"
                                                               title="Edit {{$purchase->first_name}} {{$purchase->last_name}}'s purchase details"
                                                               class="btn btn-primary btn-xs">
                                                                <i class="fa fa-edit"></i> Edit
                                                            </a>

                                                            {{--<a href="users/deleteProfile/{{$purchase->id}}"--}}
                                                            {{--data-toggle="tooltip"--}}
                                                            {{--title="Deactivate {{$purchase->first_name}} {{$purchase->last_name}}'s details"--}}
                                                            {{--class="btn btn-warning btn-xs"--}}
                                                            {{--data-toggle="modal"--}}
                                                            {{--data-target="#search-purchase">--}}
                                                            {{--<i class="fa fa-warning"></i> Deactivate--}}
                                                            {{--</a>--}}
                                                        </td>
                                                    </tr>
                                                @endforeach

                                                </tbody>
                                                <tfoot>
                                                <tr>
                                                    <th>PO NO</th>
                                                    <th>Date</th>
                                                    <th>Item</th>
                                                    <th>Status</th>
                                                    <th>Sponsor</th>
                                                    <th>Associate</th>
                                                    <th>Action</th>
                                                </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>

                                </div>
                                <!-- ./col-md-12 -->
                            </div>
                            <!-- /.row-->
                        </div>
                        <!-- /.box-body -->
                    </form>
                    <!-- /form-end -->
                </div>
                <!-- /.box -->
                <!--/.col (left) -->
            </div>
            <!-- search form -->
        </div>
        <!-- /Purchase Order -->

        {{--MODAL--}}
        <div id="purchase_modal_details" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header bg-blue-gradient">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><i class="ion ion-check"></i> Register Purchase Order</h4>
                    </div>
                    <div class="modal-body" id="div_content">
                        <!-- Purchase Order -->
                        <div class="row">
                            <div class="col-md-12 col-xs-12">

                                <div class="com-md-12">
                                    
                                    <!-- /.box-header -->

                                    <!-- form start -->
                                    <form class="form-horizontal">
                                        <div class="box-body">
                                            <div class="row">
                                                    <!-- PO Number -->
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="txt_po_no" class="col-sm-3 control-label">PO No</label>

                                                            <div class="col-sm-9">
                                                                <div class="input-group">
                                                                    <div class="input-group-addon">
                                                                        <i class="fa fa-edit"></i>
                                                                    </div>
                                                                    <label class="form-control pull-right"
                                                                           value=""
                                                                           id="txt_purchase_order_no" ></label>
                                                                   <input type='hidden' id='hdn_po_id'>
                                                                </div>
                                                                <!-- /.input group -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- /PO Number -->
                                                   

                                                    <!-- Client -->
                                                    <div class="col-md-6">
                                                        <div class="form-group" id="client">
                                                            <label for="slct_clients" class="col-sm-3 control-label">Client</label>

                                                            <div class="col-sm-9">
                                                                <label class="form-control pull-right"
                                                                           value=""
                                                                           id="slct_clients" ></label>
                                                                <!-- /.input group -->
                                                            </div>
                                                            <!-- /.col-sm-9 -->
                                                        </div>
                                                        <!-- /. form-group -->
                                                    </div>
                                                    <!-- /.Client -->

                                            </div>
                                            <!-- /.row-->
                                        </div>
                                        <!-- /.box-body -->
                                    </form>
                                    <!-- /form-end -->
                                </div>
                                <!-- /.box -->
                                <!--/.col (left) -->
                            </div>
                            <!-- search form -->
                        </div>
                        <!-- /Purchase Order -->


                        <!-- Purchased Items -->
                        <div class="row">
                            <div class="col-md-12 col-xs-12">

                                <div class="com-md-12 ">
                                   
                                    <!-- /.box-header -->
                                    <!-- form start -->
                                    <form class="form-horizontal">
                                        <div class="box-body">

                                            <!-- Purchase Item Table -->
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <table id="purchase_item_list" class="table table-bordered table-striped">
                                                        <thead>
                                                        <tr>
                                                            <th>Item</th>
                                                            <th>Qty</th>
                                                            <th>Total Amount</th>
                                                            <th>Production Point/s</th>
                                                            <th>Production Point/s - Associate</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <!-- /Purchase Item Table -->
                                        </div>
                                        <!-- /.box-body -->
                                    </form>
                                    <!-- /form-end -->
                                </div>
                                <!-- /.box -->
                                <!--/.col (left) -->
                            </div>
                            <!-- search form -->
                        </div>
                        <!-- /Purchased Items -->

                        <div class="row">
                            <div class=" col-md-12">
                                <!-- Representative -->
                                <div class="">
                                    
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                        <div class="row">
                                                <div class="col-md-12">
                                                    <table id="agents" class="table table-bordered table-striped">
                                                        <thead>
                                                        <tr>
                                                            <th>Agent Code</th>
                                                            <th>Agent Name</th>
                                                            <th>Role</th>
                                                            <th>Level</th>
                                                            <th>Action</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody id="purchase_agents">
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        
                                    </div>
                                    <!-- /.box-body -->
                                </div>
                                <!-- /.box-primary -->
                            </div>
                        </div>

                     
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" id="btn_close_modal" data-dismiss="modal">Close
                        </button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        {{--MOdal End--}}


    </section>
@endsection

@section('additional_footer')

    <script src='{{ asset("/bower_components/AdminLTE/plugins/jQueryUI/jquery-ui.min.js")}}'></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.full.min.js")}}'></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker.js")}}'></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/iCheck/icheck.min.js")}}'></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/jquery.dataTables.min.js")}}'></script>

    <script>
        $(document).ready(function () {
            var product_obj;
            $('#txt_date_from, #txt_date_to').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                locale: {
                    format: 'YYYY/mm/dd'
                }
            });

            $('#txt_purchase_date').on('apply.daterangepicker', function (ev, picker) {
                $('#txt_delivery_date').daterangepicker({
                    singleDatePicker: true,
                    showDropdowns: true,
                    locale: {
                        format: 'YYYY/mm/dd'
                    },
                    minDate: $('#txt_purchase_date').val()
                });
                $('#txt_delivery_date').val($('#txt_purchase_date').val());
            });
            $('#txt_delivery_date').on('apply.daterangepicker', function (ev, picker) {
                $('#txt_purchase_date').daterangepicker({
                    singleDatePicker: true,
                    showDropdowns: true,
                    locale: {
                        format: 'YYYY/mm/dd'
                    },
                    minDate: $('#txt_delivery_date').val()
                });
                $('#txt_purchase_date').val($('#txt_delivery_date').val());
            });

            $('#txt_purchase_date').on('cancel.daterangepicker', function (ev, picker) {
                $('#txt_delivery_date').val('');
            });

            $("#txt_set_sold_1, #txt_set_sold_2, #txt_set_sold_3").spinner({
                min: 1,
                change: function (event, ui) {
                    // alert();
                    // updatePrice();
                }
            });


            var table_data = $('#tblcontestants').DataTable({
                "paging": false,
                "lengthChange": true,
                "searching": false,
                "info": false,
                "autoWidth": true
            });


            var agents = $('#agents').DataTable({
                "paging": false,
                "lengthChange": true,
                "searching": false,
                "info": false,
                "autoWidth": true
            });



            var tbl_purchase_list = $('#purchase_item_list').DataTable({
                "paging": false,
                "lengthChange": true,
                "searching": false,
                "info": false,
                "autoWidth": true
            });

            $('#slct_agent_list').select2({
                ajax: {
                    url: "/promos/getAgentsForPromo",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term,
                            contest_id:$("#slct_contest_list").select2('val')// search term
                        };
                    },
                    processResults: function (data) {
                        // parse the results into the format expected by Select2.
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data
                        return {
                            results: data
                        };
                    },
                    cache: true
                },
                minimumInputLength: 2,
                maximumSelectionLength: 3
            });

            $('#slct_agent_list').on("select2:select", function (e) {
                table_data.clear().draw();
            });


            $("#slct_contest_list").select2({
                ajax: {
                    url: "/promos/getContestTitle",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term // search term
                        };
                    },
                    processResults: function (data) {
                        // parse the results into the format expected by Select2.
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data
                        return {
                            results: data
                        };
                    },
                    cache: true
                },
                minimumInputLength: 2
            });

            $("#btn_load_po").click(function(){

                reloadPO();
            });

            function reloadPO(){
                var contest_id = $("#slct_contest_list").select2('val');

                $.get("/promos/getPOByContest", {
                    contestId:contest_id
                }).done(function (data) {
                    console.log(data);
                    table_data.clear().draw();

                    for (x = 0; x < data.length; x++) {
                        if(data[x].po_no != null){
                            table_data.row.add(
                                [
                                    data[x].id,
                                    data[x].purchase_date,
                                    data[x].description,
                                    data[x].status,
                                    data[x].sponsor,
                                    data[x].assoc,
                                    "<input type='button' class='registerPo btn btn-warning btn-xs' id='"+data[x].id+"' value='UnRegister PO'><input type='hidden' value='"+data[x].id+"'>",

                                ]
                            ).draw();
                        }else{
                            table_data.row.add(
                                [
                                    data[x].id,
                                    data[x].purchase_date,
                                    data[x].description,
                                    data[x].status,
                                    data[x].sponsor,
                                    data[x].assoc,
                                    "<input type='button' class='registerPo btn btn-success  btn-xs' id='"+data[x].id+"' value='Register PO'><input type='hidden' value='"+data[x].id+"'>",

                                ]
                            ).draw();    
                        }

                      


                    }

                    $(".registerPo").click(function(){

                        var po_no = $(this).next().get(0).value;
                        var contest = $('#slct_contest_list').select2('val');

                         //get purchase details
                        $.get("/promos/viewPurchaseDetails", {
                                po:po_no,
                                contestId:contest
                            }).done(function (data) {
                                console.log(data);
                                console.log('dito')
                                data.purchase_order = data.purchase_order[0];
                                $("#txt_purchase_order_no").html(data.purchase_order.purchase_order_no);
                                $("#slct_clients").html(data.purchase_order.client_name);
                                $("#hdn_po_id").val(data.purchase_order.id);
                                agents.clear().draw();
                                //populate sponsor and associate
                                if(data.purchase_order.assoc_id != null){
                                    if(data.purchase_order.po_no != null && data.purchase_order.assoc_id == data.purchase_order.representative){
                                        agents.row.add(
                                            [
                                                data.purchase_order.assoc_code,
                                                data.purchase_order.assoc,
                                                "ASSOCIATE",
                                                data.purchase_order.level_assoc,
                                                "<input type='button' class='assoc_button registerPoAction btn btn-danger btn-xs' id='"+data.purchase_order.assoc_id+"' value='Unregister PO'><input type='hidden' value='"+data.purchase_order.assoc_id+"'>",

                                            ]
                                        ).draw();
                                    }else{
                                        agents.row.add(
                                            [
                                            data.purchase_order.assoc_code,
                                            data.purchase_order.assoc,
                                            "ASSOCIATE",
                                            data.purchase_order.level_assoc,
                                            "<input type='button' class='assoc_button registerPoAction btn btn-success btn-xs' id='"+data.purchase_order.assoc_id+"' value='Register PO'><input type='hidden' value='"+data.purchase_order.assoc_id+"'>",

                                            ]
                                        ).draw();
                                    }
                                    
                                }

                                if(data.purchase_order.sponsor_id != null){

                                    if(data.purchase_order.po_no != null && data.purchase_order.sponsor_id == data.purchase_order.representative){
                                        agents.row.add(
                                        [
                                            data.purchase_order.sponsor_code,
                                            data.purchase_order.sponsor,
                                            "SPONSOR",
                                            data.purchase_order.level_sponsor,
                                            "<input type='button' class='sponsor_button registerPoAction btn btn-danger btn-xs' id='"+data.purchase_order.sponsor_id+"' value='Unregister PO'><input type='hidden' id='"+data.purchase_order.sponsor_id+"' value='"+data.purchase_order.sponsor_id+"'>",

                                        ]
                                    ).draw();
                                    }else{
                                        agents.row.add(
                                            [
                                                data.purchase_order.sponsor_code,
                                                data.purchase_order.sponsor,
                                                "SPONSOR",
                                                data.purchase_order.level_sponsor,
                                                "<input type='button' class='sponsor_button registerPoAction btn btn-success btn-xs' id='"+data.purchase_order.sponsor_id+"' value='Register PO'><input type='hidden' id='"+data.purchase_order.sponsor_id+"' value='"+data.purchase_order.sponsor_id+"'>",

                                            ]
                                        ).draw();
                                    }
                                    
                                }

                                if(data.purchase_order.assoc_id == data.purchase_order.representative && data.purchase_order.assoc_id  != null){
                                    $(".sponsor_button").attr('disabled','disabled');
                                    $(".assoc_button").removeAttr('disabled');
                                }else if(data.purchase_order.sponsor_id == data.purchase_order.representative  && data.purchase_order.sponsor_id  != null){
                                    $(".sponsor_button").attr('disabled','disabled');
                                    $(".assoc_button").removeAttr('disabled');
                                }else{
                                    $(".sponsor_button").removeAttr('disabled');
                                    $(".assoc_button").removeAttr('disabled');
                                }   
                                tbl_purchase_list.clear().draw();
                                // populate purchase items table
                                for(ctr=0; ctr<data.purchase_list.length; ctr++){
                                    tbl_purchase_list.row.add(
                                        [
                                            data.purchase_list[ctr].description,
                                            data.purchase_list[ctr].qty,
                                            data.purchase_list[ctr].total_price,
                                            data.purchase_list[ctr].production_points,
                                            data.purchase_list[ctr].production_points_associate
                                            

                                        ]
                                    ).draw();
                                }

                                $(".registerPoAction").click(function(){
                                    
                                    var po_no = $("#hdn_po_id").val();
                                    var button = $(this);

                                    var contest = $('#slct_contest_list').select2('val');
                                    var agent = button.get(0).id;
                                    
                                    if( button.val() != "Unregister PO" ){
                                        $.get("/promos/registerPoToPromo", {
                                            po:po_no,
                                            contestId:contest,
                                            agentId:agent
                                        }).done(function (data) {
                                            alert('Successfully registered!');
                                            button.val("Unregister PO");
                                            button.addClass('btn-danger');
                                            button.removeClass('btn-success');
                                            if(button.hasClass('sponsor_button')){
                                                $(".sponsor_button").removeAttr('disabled');
                                                $(".assoc_button").attr('disabled','disabled');
                                            }else{
                                                $(".assoc_button").removeAttr('disabled');
                                                $(".sponsor_button").attr('disabled','disabled');
                                            }
                                        });
                                    }else{
                                        $.get("/promos/unregisterPoToPromo", {
                                            po:po_no,
                                            contestId:contest,
                                            agentId:agent
                                        }).done(function (data) {
                                            alert('Successfully unregistered!');
                                            button.val("Register PO");
                                            button.addClass('btn-success');
                                            button.removeClass('btn-danger');
                                            $(".assoc_button").removeAttr('disabled');
                                            $(".sponsor_button").removeAttr('disabled');

                                        });
                                    }
                                });

                            });
                            
                        $("#purchase_modal_details").modal('show');

                        

                        
                    });



                });
            }

            $("#btn_close_modal").click(function(){
                reloadPO();
            });


        });

    </script>
@endsection