@extends('admin_template')

@section('additional_header')

        <!-- DataTables -->
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.min.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker-bs3.css")}}'>

@endsection

@section('content')
        <!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <!-- general form elements -->
            <div class="com-md-12 box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Search Promo</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <div class="box-body">
                    <div class="row">
                        <form role="form">
                            <div class="col-md-2 col-sm-2">
                                <button id="btn_search" type="button" class="btn btn-primary" data-toggle="modal"
                                        data-target="#search-purchase"><i class="ion ion-search"></i> Search Client
                                    Records
                                </button>
                            </div>
                            <div class="col-md-2 col-sm-2 col-md-offset-8 col-sm-offset-8">
                                @if(isAccessModuleAllowed('promos_add'))
                                    <a href="/promos/addNew" class="btn btn-success pull-right"><i
                                                class="fa fa-plus"></i> Add New Promo</a>
                                @endif
                            </div>

                        </form>
                    </div>
                    <!-- /row -->

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
            <!--/.col (left) -->
        </div>
        <!-- search form -->
    </div>
    <!-- /.row (main row) -->

    <div class="row">
        <div class=" col-md-12 col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-shopping-cart"></i> Promo Records</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>&nbsp;</th>
                            <th>Promo Title</th>
                            <th>Description</th>
                            <th>Date From</th>
                            <th>Date To</th>
                            <th>Minimum Qualification</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($promos as $promo)
                            <tr>
                                <td><input type="checkbox"></td>
                                <td>{{$promo->title}}</td>
                                <td>{{$promo->description}}</td>
                                <td>{{$promo->from}}</td>
                                <td>{{$promo->to}}</td>
                                @if($promo->minimum_qualification==1)
                                    <td>Associate Phase 1</td>
                                @elseif($promo->minimum_qualification==2)
                                    <td>Associate Phase 2</td>
                                @elseif($promo->minimum_qualification==3)
                                    <td>Consultant</td>
                                @elseif($promo->minimum_qualification==4)
                                    <td>Senior Consultant</td>
                                @elseif($promo->minimum_qualification==5)
                                    <td>Manager</td>
                                @elseif($promo->minimum_qualification==6)
                                    <td>Distributor</td>
                                @endif

                                <td>{{$promo->status}}</td>
                                <td>
                                    @if(isAccessModuleAllowed('promos_view'))
                                        <a href="promos/show/{{$promo->id}}"
                                           data-toggle="tooltip"
                                           title="View {{$promo->title}}'s details"
                                           class="btn btn-success btn-xs">
                                            <i class="fa fa-info-circle"></i> View
                                        </a>
                                    @endif
                                    @if(isAccessModuleAllowed('promos_edit'))
                                        <a href="promos/edit/{{$promo->id}}"
                                           data-toggle="tooltip"
                                           title="Edit {{$promo->title}}'s details"
                                           class="btn btn-primary btn-xs">
                                            <i class="fa fa-edit"></i> Edit
                                        </a>
                                    @endif
                                    {{--@if($promo->status=='ACTIVE')--}}
                                    {{--<button type="button" class="btn btn-danger btn-xs deactivate-product" data-toggle="modal"--}}
                                    {{--data-id="{{$promo->id}}"--}}
                                    {{--data-target="#delete-product"--}}
                                    {{--title="Deactivate {{$promo->title}}"--}}
                                    {{--><i class="fa fa-warning"></i> Deactivate--}}
                                    {{--</button>--}}
                                    {{--@else--}}
                                    {{--<button type="button" class="btn btn-warning btn-xs activate-product" data-toggle="modal"--}}
                                    {{--data-id="{{$promo->id}}"--}}
                                    {{--title="Activate {{$promo->title}}"--}}
                                    {{--data-target="#activate-product"><i class="fa fa-power-off"></i> Activate--}}
                                    {{--</button>--}}
                                    {{--@endif--}}

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>&nbsp;</th>
                            <th>Promo Title</th>
                            <th>Description</th>
                            <th>Date From</th>
                            <th>Date To</th>
                            <th>Minimum Qualification</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
</section>

@endsection

@section('additional_footer')

    <script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/jquery.dataTables.min.js")}}'></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js")}}'></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.full.min.js")}}'></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker.js")}}'></script>


    <script>
        $(document).ready(function () {
            $('#example1').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true
            });

        });
    </script>
@endsection