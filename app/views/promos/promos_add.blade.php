@extends('admin_template')

@section('additional_header')

    <link rel="stylesheet"
          href='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker-bs3.css")}}'>
    <link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/iCheck/all.css")}}'>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.css")}}'>
    <link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.min.css")}}'>

    @endsection

    @section('content')
            <!-- Main content -->
    <section class="content">
        <!-- Purchase Order -->
        <div class="row">
            <div class="col-md-12 col-xs-12">

                <div class="com-md-12 box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-shopping-cart"></i> New Contest</h3>
                    </div>
                    <!-- /.box-header -->

                    <!-- form start -->
                    <form class="form-horizontal">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <!-- PO Number -->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="txt_po_no" class="col-sm-3 control-label">Contest Title:</label>

                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-edit"></i>
                                                    </div>
                                                    <input type="text" class="form-control pull-right"
                                                           value=""
                                                           id="txt_contest_title">

                                                </div>
                                                <!-- /.input group -->
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /PO Number -->
                                    <!-- Purchase Date -->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="txt_date_from" class="col-sm-3 control-label">Description</label>

                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-edit"></i>
                                                    </div>
                                                    <textarea id="txt_contest_description" class="form-control"></textarea>
                                                </div>

                                                <!-- /.input group -->
                                            </div>
                                            <!-- /.col-sm-8 -->
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                    <!-- /.col-md-6 -->

                                    <!-- Delivery Date -->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="txt_date_to" class="col-sm-3 control-label">Date From</label>

                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </div>
                                                    <input type="text" class="form-control pull-right"
                                                           id="txt_date_from">
                                                </div>

                                                <!-- /.input group -->
                                            </div>
                                            <!-- /.col-sm-9 -->
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                    <!-- /Delivery Date -->

                                    <!-- Delivery Date -->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="txt_date_to" class="col-sm-3 control-label">Date To</label>

                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </div>
                                                    <input type="text" class="form-control pull-right"
                                                           id="txt_date_to">
                                                </div>

                                                <!-- /.input group -->
                                            </div>
                                            <!-- /.col-sm-9 -->
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                    <!-- /Delivery Date -->

                                    <!-- Terms -->
                                    <div class="col-md-6">
                                        <div class="form-group" id="set">
                                            <label for="slct_terms" class="col-sm-3 control-label">Minimum Requirements</label>

                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-rub"></i>
                                                    </div>
                                                    <select id="slct_minimum_requirements" class="form-control select2">
                                                        <option value="1">Associate Phase I</option>
                                                        <option value="2">Associate Phase II</option>
                                                        <option value="3">Consultant</option>
                                                        <option value="4">Senior Consultant</option>
                                                        <option value="5">Manager</option>
                                                        <option value="6">Distributor</option>
                                                    </select>
                                                </div>
                                                <!-- /.input group -->
                                            </div>
                                            <!-- /.col-sm-9 -->
                                        </div>
                                        <!-- /. form-group -->
                                    </div>
                                    <!-- /.Terms -->

                                </div>
                                <!-- ./col-md-12 -->
                            </div>
                            <!-- /.row-->
                        </div>
                        <!-- /.box-body -->
                    </form>
                    <!-- /form-end -->
                </div>
                <!-- /.box -->
                <!--/.col (left) -->
            </div>
            <!-- search form -->
        </div>
        <!-- /Purchase Order -->


        <!-- Purchased Items -->
        <div class="row">
            <div class="col-md-12 col-xs-12">

                <div class="com-md-12 box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-gear"></i> Mechanics</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form class="form-horizontal">
                        <div class="box-body">

                            <div class="row">
                                <div class="col-md-12">

                                    <!-- Qty -->
                                    <div class="col-md-5 col-md-offset-1">
                                        <div class="form-group">
                                            <label for="txt_qty" class="col-sm-3 control-label">Sets Sold</label>

                                            <div class="col-sm-9">
                                                <input type="text" value="1" class="form-class" id="txt_set_sold_1" style="width:100%">
                                            </div>
                                            <!-- /.col-sm-8 -->
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                    <!-- /.Qty -->
                                    <!-- Item / Set -->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="txt_po_no" class="col-sm-3 control-label">Prize:</label>

                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-trophy"></i>
                                                    </div>
                                                    <input type="text" class="form-control pull-right"
                                                           value=""
                                                           id="txt_prize_1">

                                                </div>
                                                <!-- /.input group -->
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /Item / Set -->
                                    <!-- Qty -->
                                    <div class="col-md-5 col-md-offset-1">
                                        <div class="form-group">
                                            <label for="txt_qty" class="col-sm-3 control-label">Sets Sold</label>

                                            <div class="col-sm-9">
                                                <input type="text" value="1" class="form-class" id="txt_set_sold_2" style="width:100%">
                                            </div>
                                            <!-- /.col-sm-8 -->
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                    <!-- /.Qty -->
                                    <!-- Item / Set -->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="txt_po_no" class="col-sm-3 control-label">Prize:</label>

                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-trophy"></i>
                                                    </div>
                                                    <input type="text" class="form-control pull-right"
                                                           value=""
                                                           id="txt_prize_2">

                                                </div>
                                                <!-- /.input group -->
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /Item / Set -->
                                    <!-- Qty -->
                                    <div class="col-md-5 col-md-offset-1">
                                        <div class="form-group">
                                            <label for="txt_qty" class="col-sm-3 control-label">Sets Sold</label>

                                            <div class="col-sm-9">
                                                <input type="text" value="1" class="form-class" id="txt_set_sold_3" style="width:100%">
                                            </div>
                                            <!-- /.col-sm-8 -->
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                    <!-- /.Qty -->
                                    <!-- Item / Set -->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="txt_po_no" class="col-sm-3 control-label">Prize:</label>

                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-trophy"></i>
                                                    </div>
                                                    <input type="text" class="form-control pull-right"
                                                           value=""
                                                           id="txt_prize_3">

                                                </div>
                                                <!-- /.input group -->
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /Item / Set -->

                                    <div class="row">
                                        <div class="col-md-12">
                                            <button type="button" id="btn_save_contest"
                                                    class="btn btn-success pull-right"
                                                    style="margin-left:1%"> Save Contest Details
                                            </button>
                                            <button type="button" id="btn_cancel" class="btn bg-orange pull-right">
                                                Cancel
                                            </button>
                                        </div>
                                    </div>

                                </div>
                                <!-- ./col-md-12 -->
                            </div>
                            <!-- /.row-->


                        </div>
                        <!-- /.box-body -->
                    </form>
                    <!-- /form-end -->
                </div>
                <!-- /.box -->
                <!--/.col (left) -->
            </div>
            <!-- search form -->
        </div>
        <!-- /Purchased Items -->

  {{--MODAL--}}
    <div id="save-promo-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-blue-gradient">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><i class="ion ion-check"></i> Save Promo</h4>
                </div>
                <div class="modal-body">
                    Record has been saved.
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" id="btn_close_modal" data-dismiss="modal">Close
                    </button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--MOdal End--}}

    </section>
@endsection

@section('additional_footer')

    <script src='{{ asset("/bower_components/AdminLTE/plugins/jQueryUI/jquery-ui.min.js")}}'></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.full.min.js")}}'></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker.js")}}'></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/iCheck/icheck.min.js")}}'></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/jquery.dataTables.min.js")}}'></script>

    <script>
        $(document).ready(function () {
            var product_obj;
            var date_from = '';
            var date_to = '';
            $('#txt_date_to, #txt_date_from').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                locale: {
                    format: 'YYYY/mm/dd'
                }
            });

            $('#txt_date_from').on('apply.daterangepicker', function (ev, picker) {
                date_from = picker.startDate.format('YYYY-MM-DD');

                $('#txt_date_to').daterangepicker({
                    singleDatePicker: true,
                    showDropdowns: true,
                    locale: {
                        format: 'YYYY/mm/dd'
                    },
                    minDate: $('#txt_date_from').val()
                });
                $('#txt_date_to').on('apply.daterangepicker', function (ev, picker) {
                    date_to = picker.startDate.format('YYYY-MM-DD');

                    $('#txt_date_from').daterangepicker({
                        singleDatePicker: true,
                        showDropdowns: true,
                        locale: {
                            format: 'YYYY/mm/dd'
                        },
                        minDate: $('#txt_date_to').val()
                    });

                });

                $('#txt_date_to').val($('#txt_date_from').val());
            });
            $('#txt_date_to').on('apply.daterangepicker', function (ev, picker) {
                date_to = picker.startDate.format('YYYY-MM-DD');

                $('#txt_date_from').daterangepicker({
                    singleDatePicker: true,
                    showDropdowns: true,
                    locale: {
                        format: 'YYYY/mm/dd'
                    },
                    minDate: $('#txt_date_to').val()
                });
                $('#txt_date_from').on('apply.daterangepicker', function (ev, picker) {
                    date_from = picker.startDate.format('YYYY-MM-DD');

                    $('#txt_date_to').daterangepicker({
                        singleDatePicker: true,
                        showDropdowns: true,
                        locale: {
                            format: 'YYYY/mm/dd'
                        },
                        minDate: $('#txt_date_from').val()
                    });
                    $('#txt_date_to').val($('#txt_date_from').val());
                });

            });

            $('#txt_date_from').on('cancel.daterangepicker', function (ev, picker) {
                $('#txt_date_to').val('');
            });


            $("#txt_set_sold_1, #txt_set_sold_2, #txt_set_sold_3").spinner({
                min: 0,
                step:5,
                change: function (event, ui) {
                    // alert();
                    // updatePrice();
                }
            });

            $("#btn_save_contest").click(function(){


                if($("#txt_contest_title").val() == ""){
                    alert('Contest Title is required');
                    $("#txt_contest_title").focus();
                    return false;
                }

                $.post("/promos/store", {
                    txt_contest_title:$("#txt_contest_title").val(),
                    txt_contest_description:$("#txt_contest_description").val(),
                    txt_date_from:date_from,
                    txt_date_to:date_to,
                    slct_minimum_requirements:$("#slct_minimum_requirements").val(),
                    txt_set_sold_1:$("#txt_set_sold_1").val(),
                    txt_prize_1:$("#txt_prize_1").val(),
                    txt_set_sold_2:$("#txt_set_sold_2").val(),
                    txt_prize_2:$("#txt_prize_2").val(),
                    txt_set_sold_3:$("#txt_set_sold_3").val(),
                    txt_prize_3:$("#txt_prize_3").val()
                }).done(function (data) {
                    
                    $("#save-promo-modal").modal('show');
                });
            });
            $("#btn_close_modal").click(function(){
                window.location = '/promos';
            });



        });
    </script>
@endsection