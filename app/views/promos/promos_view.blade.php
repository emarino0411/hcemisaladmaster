@extends('admin_template')

@section('additional_header')

    <link rel="stylesheet"
          href='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker-bs3.css")}}'>
    <link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/iCheck/all.css")}}'>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.css")}}'>
    <link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.min.css")}}'>

    @endsection

    @section('content')
            <!-- Main content -->
    <section class="content">
        <!-- Purchase Order -->
        <div class="row">
            <div class="col-md-12 col-xs-12">

                <div class="com-md-12 box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-shopping-cart"></i> New Contest</h3>
                    </div>
                    <!-- /.box-header -->

                    <!-- form start -->
                    <form class="form-horizontal">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <!-- PO Number -->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="txt_po_no" class="col-sm-3 control-label">Contest Title:</label>

                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-edit"></i>
                                                    </div>
                                                    <input type="text" class="form-control pull-right"
                                                           value="{{$promo->title}}"
                                                           id="txt_contest_title" readonly>

                                                </div>
                                                <!-- /.input group -->
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /PO Number -->
                                    <!-- Purchase Date -->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="txt_purchase_date" class="col-sm-3 control-label">Description</label>

                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-edit"></i>
                                                    </div>
                                                    <textarea readonly id="txt_contest_description" class="form-control">{{$promo->description}}</textarea>
                                                </div>

                                                <!-- /.input group -->
                                            </div>
                                            <!-- /.col-sm-8 -->
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                    <!-- /.col-md-6 -->

                                    <!-- Delivery Date -->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="txt_delivery_date" class="col-sm-3 control-label">Date From</label>

                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </div>
                                                    <input readonly type="text" class="form-control pull-right"
                                                           id="txt_date_from" value="{{$promo->from}}">
                                                </div>

                                                <!-- /.input group -->
                                            </div>
                                            <!-- /.col-sm-9 -->
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                    <!-- /Delivery Date -->

                                    <!-- Delivery Date -->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="txt_delivery_date" class="col-sm-3 control-label">Date To</label>

                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </div>
                                                    <input readonly type="text" class="form-control pull-right"
                                                           id="txt_date_to" value="{{$promo->to}}">
                                                </div>

                                                <!-- /.input group -->
                                            </div>
                                            <!-- /.col-sm-9 -->
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                    <!-- /Delivery Date -->

                                    <!-- Terms -->
                                    <div class="col-md-6">
                                        <div class="form-group" id="set">
                                            <label for="slct_terms" class="col-sm-3 control-label">Minimum Requirements</label>

                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-rub"></i>
                                                    </div>
                                                    <select disabled id="slct_minimum_requirements" class="form-control">
                                                        <option value="1" @if($promo->minimum_qualification==1)selected @endif>Associate Phase I</option>
                                                        <option value="2" @if($promo->minimum_qualification==2)selected @endif>Associate Phase II</option>
                                                        <option value="3" @if($promo->minimum_qualification==3)selected @endif>Consultant</option>
                                                        <option value="4" @if($promo->minimum_qualification==4)selected @endif>Senior Consultant</option>
                                                        <option value="5" @if($promo->minimum_qualification==5)selected @endif>Manager</option>
                                                        <option value="6" @if($promo->minimum_qualification==6)selected @endif>Distributor</option>
                                                    </select>
                                                </div>
                                                <!-- /.input group -->
                                            </div>
                                            <!-- /.col-sm-9 -->
                                        </div>
                                        <!-- /. form-group -->
                                    </div>
                                    <!-- /.Terms -->

                                </div>
                                <!-- ./col-md-12 -->
                            </div>
                            <!-- /.row-->
                        </div>
                        <!-- /.box-body -->
                    </form>
                    <!-- /form-end -->
                </div>
                <!-- /.box -->
                <!--/.col (left) -->
            </div>
            <!-- search form -->
        </div>
        <!-- /Purchase Order -->


        <!-- Purchased Items -->
        <div class="row">
            <div class="col-md-12 col-xs-12">

                <div class="com-md-12 box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-gear"></i> Mechanics</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form class="form-horizontal">
                        <div class="box-body">

                            <div class="row">
                                <div class="col-md-12">

                                    <!-- Qty -->
                                    <div class="col-md-5 col-md-offset-1">
                                        <div class="form-group">
                                            <label for="txt_qty" class="col-sm-3 control-label">Sets Sold</label>

                                            <div class="col-sm-9">
                                                <input readonly type="text" value="{{$promo->mechanics_1_points}}" class="form-class" id="txt_set_sold_1" style="width:100%">
                                            </div>
                                            <!-- /.col-sm-8 -->
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                    <!-- /.Qty -->
                                    <!-- Item / Set -->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="txt_po_no" class="col-sm-3 control-label">Prize:</label>

                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-trophy"></i>
                                                    </div>
                                                    <input  readonly type="text" class="form-control pull-right"
                                                           value="{{$promo->mechanics_1_prize}}"
                                                           id="txt_prize_1">

                                                </div>
                                                <!-- /.input group -->
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /Item / Set -->
                                    <!-- Qty -->
                                    <div class="col-md-5 col-md-offset-1">
                                        <div class="form-group">
                                            <label for="txt_qty" class="col-sm-3 control-label">Sets Sold</label>

                                            <div class="col-sm-9">
                                                <input type="text" readonly value="{{$promo->mechanics_2_points}}" class="form-class" id="txt_set_sold_2" style="width:100%">
                                            </div>
                                            <!-- /.col-sm-8 -->
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                    <!-- /.Qty -->
                                    <!-- Item / Set -->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="txt_po_no" readonly class="col-sm-3 control-label">Prize:</label>

                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-trophy"></i>
                                                    </div>
                                                    <input type="text" class="form-control pull-right"
                                                           value="{{$promo->mechanics_2_prize}}"
                                                           id="txt_prize_2" readonly>

                                                </div>
                                                <!-- /.input group -->
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /Item / Set -->
                                    <!-- Qty -->
                                    <div class="col-md-5 col-md-offset-1">
                                        <div class="form-group">
                                            <label for="txt_qty" class="col-sm-3 control-label">Sets Sold</label>

                                            <div class="col-sm-9">
                                                <input type="text" readonly value="{{$promo->mechanics_3_points}}" class="form-class" id="txt_set_sold_3" style="width:100%">
                                            </div>
                                            <!-- /.col-sm-8 -->
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                    <!-- /.Qty -->
                                    <!-- Item / Set -->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="txt_po_no" class="col-sm-3 control-label">Prize:</label>

                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-trophy"></i>
                                                    </div>
                                                    <input type="text" class="form-control pull-right"
                                                           value="{{$promo->mechanics_3_prize}}"
                                                           id="txt_prize_3" readonly>

                                                </div>
                                                <!-- /.input group -->
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /Item / Set -->

                                    <div class="row">
                                        <div class="col-md-12">
                                            <a href="/promos/edit/{{$promo->id}}" type="button" id="btn_save_contest"
                                                    class="btn btn-success pull-right"
                                                    style="margin-left:1%"> Edit Contest Details
                                            </a>
                                            <button type="button" id="btn_cancel" class="btn bg-orange pull-right">
                                                Cancel
                                            </button>
                                        </div>
                                    </div>

                                </div>
                                <!-- ./col-md-12 -->
                            </div>
                            <!-- /.row-->


                        </div>
                        <!-- /.box-body -->
                    </form>
                    <!-- /form-end -->
                </div>
                <!-- /.box -->
                <!--/.col (left) -->
            </div>
            <!-- search form -->
        </div>
        <!-- /Purchased Items -->


    </section>
@endsection

@section('additional_footer')

    <script src='{{ asset("/bower_components/AdminLTE/plugins/jQueryUI/jquery-ui.min.js")}}'></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.full.min.js")}}'></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker.js")}}'></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/iCheck/icheck.min.js")}}'></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/jquery.dataTables.min.js")}}'></script>

@endsection