@extends('admin_template')

@section('additional_header')

        <!-- DataTables -->
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.min.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker-bs3.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.min.css")}}'>

@endsection

@section('content')
<style>
table{
    width:100%;
}
</style>
        <!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <!-- general form elements -->
            <div class="com-md-12 box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Display Top Sellers / Top Sponsor</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <div class="box-body">
                    <div class="row">
                        <form role="form" class="form-horizontal">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label for="txt_payment_date" class="col-sm-4 control-label">Sales Date</label>

                                    <div class="col-sm-8">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text"
                                                   class="form-control pull-right"
                                                   id="txt_date_received">

                                        </div>

                                        <!-- /.input group -->
                                    </div>
                                    <!-- /.col-sm-8 -->
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <div class="col-md-2 col-sm-2">
                                <button id="btn_search" type="button" class="btn btn-primary"><i class="ion ion-search"></i> Search Sales
                                </button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
            <!-- /.box -->
            <!--/.col (left) -->
        </div>
        <!-- search form -->
    </div>
    <!-- /.row (main row) -->

    <div class="row">
        <div class=" col-md-12 col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-shopping-cart"></i> Top Sellers / Top Sponsors</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">

                     <div class="col-md-12">
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#distributors" data-toggle="tab">Top Distributors</a></li>
                                <li><a href="#managers" data-toggle="tab">Top Managers</a></li>
                                <li><a href="#senior" data-toggle="tab">Top Senior Consultants</a></li>
                                <li><a href="#consultants" data-toggle="tab">Top Consultants</a></li>
                                <li><a href="#associate" data-toggle="tab">Top Associate</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="active tab-pane" id="distributors">
                                    <table id="tbl_distributors" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Agent Name</th>
                                                <th>Current Level</th>
                                                <th>Sold POs</th>
                                                <th>Total Production Points</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <th>Agent Name</th>
                                                <th>Current Level</th>
                                                <th>Sold POs</th>
                                                <th>Total Production Points</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                                <div class="tab-pane" id="managers">
                                    <table id="tbl_managers" class="table table-bordered table-striped">
                                            <thead>
                                            <tr>
                                                <th>Agent Name</th>
                                                <th>Current Level</th>
                                                <th>Sold POs</th>
                                                <th>Total Production Points</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <th>Agent Name</th>
                                                <th>Current Level</th>
                                                <th>Sold POs</th>
                                                <th>Total Production Points</th>
                                            </tr>
                                        </tfoot>
                                        </table>
                                </div>
                                <div class="tab-pane" id="senior">
                                    <table id="tbl_senior" class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Agent Name</th>
                                                    <th>Current Level</th>
                                                    <th>Sold POs</th>
                                                    <th>Total Production Points</th>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                </tbody>
                                                <tfoot>
                                                <tr>
                                                    <th>Agent Name</th>
                                                    <th>Current Level</th>
                                                    <th>Sold POs</th>
                                                    <th>Total Production Points</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                </div>
                                <div class="tab-pane" id="consultants">
                                    <table id="tbl_consultants" class="table table-bordered table-striped">
                                           <thead>
                                                <tr>
                                                    <th>Agent Name</th>
                                                    <th>Current Level</th>
                                                    <th>Sold POs</th>
                                                    <th>Total Production Points</th>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                </tbody>
                                                <tfoot>
                                                <tr>
                                                    <th>Agent Name</th>
                                                    <th>Current Level</th>
                                                    <th>Sold POs</th>
                                                    <th>Total Production Points</th>
                                                </tr>
                                            </tfoot>
                                        </table>                                
                                </div>
                                <div class="tab-pane" id="associate">
                                    <table id="tbl_associate" class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Agent Name</th>
                                                    <th>Current Level</th>
                                                    <th>Sold POs</th>
                                                    <th>Total Production Points</th>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                </tbody>
                                                <tfoot>
                                                <tr>
                                                    <th>Agent Name</th>
                                                    <th>Current Level</th>
                                                    <th>Sold POs</th>
                                                    <th>Total Production Points</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                </div>

                            </div>
                            <!-- /.tab-content -->
                        </div>
                        <!-- /.nav-tabs-custom -->

                    </div>
                    
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>

    <div id="no-record-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-blue-gradient">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><i class="fa fa-close"></i> No Record To Display</h4>
                    </div>
                    <div class="modal-body">
                        There are no records to display.
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" id="btn_close_modal" data-dismiss="modal">Close
                        </button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
</section>

@endsection

@section('additional_footer')

    <script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/jquery.dataTables.min.js")}}'></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js")}}'></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.full.min.js")}}'></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker.js")}}'></script>


    <script>
        $(document).ready(function () {
            var date_from;
            var date_to;

            var table_data = $('#tbl_distributors').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true
            });
            
            var table_data = $('#tbl_distributors').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true
            });
            var tbl_managers = $('#tbl_managers').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true
            });
            var tbl_senior = $('#tbl_senior').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true
            });
            var tbl_consultants = $('#tbl_consultants').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true
            });
            var tbl_associate = $('#tbl_associate').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true
            });
            
            $('#txt_date_received').daterangepicker({
                showDropdowns: true,
                startDate: new Date(),
                locale: {
                    format: 'YYYY-MM-DD'
                }
            });

            $('#txt_date_received').on('apply.daterangepicker', function (ev, picker) {
                date_from = picker.startDate.format('YYYY-MM-DD');
                date_to = picker.endDate.format('YYYY-MM-DD');
            });
            
           
            $("#btn_search").click(function(){
                $.get("/reports/getTopSeller", {
                   _date_from:date_from,
                   _date_to:date_to

                }).done(function (data) {
                    console.log(data);
                    table_data.clear().draw();

                    if(data.length==0){
                        $("#no-record-modal").modal('show');
                        return false;    
                    }else{
                        var points = data;
                        
                        for(ctr=0;ctr<points.length;ctr++){

                            table_data.row.add(
                                [
                                    points[ctr].representative_code,
                                    points[ctr].first_name+ " "+points[ctr].last_name,
                                    points[ctr].current_level,
                                    points[ctr].purchase_count,
                                    points[ctr].total_points                                    
                                ]
                            ).draw();
                        }
                        alert();
                    }
                    
                    
                });
            });
            

            
        });
    </script>
@endsection