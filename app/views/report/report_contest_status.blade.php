@extends('admin_template')

@section('additional_header')

        <!-- DataTables -->
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.min.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker-bs3.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.min.css")}}'>

@endsection

@section('content')
        <!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <!-- general form elements -->
            <div class="com-md-12 box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Display Contest Status</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <div class="box-body">
                    <div class="row">
                        <form role="form" class="form-horizontal">
                            <div class="col-md-12">
                                <div class="form-group" id="set">
                                    <label for="slct_terms" class="col-sm-3 control-label">Contest Title</label>

                                    <div class="col-sm-6">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-edit"></i>
                                            </div>
                                            <select id="slct_contest_list" class="form-control select2">

                                            </select>
                                        </div>
                                        <!-- /.input group -->
                                    </div>
                                    <!-- /.col-sm-6 -->

                                    <div class="col-md-2 col-sm-2">
                                        <button id="btn_search" type="button" class="btn btn-primary"><i class="ion ion-search"></i> Search Contest Records
                                        </button>
                                    </div>

                                </div>
                                <!-- /. form-group -->
                               
                             </div>

                        </form>
                    </div>
                </div>
            </div>
            <!-- /.box -->
            <!--/.col (left) -->
        </div>
        <!-- search form -->
    </div>
    <!-- /.row (main row) -->

    <div class="row">
        <div class=" col-md-12 col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-shopping-cart"></i> Sales By Agent</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Agent</th>
                                <th>Total Points</th>
                                <th>Target Mechanic</th>
                                <th>Target Points</th>
                                <th>Status</th>
                                <th>Points to Accumulate</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                            <tfoot>
                            <tr>
                                <th>Agent</th>
                                <th>Total Points</th>
                                <th>Target Mechanic</th>
                                <th>Target Points</th>
                                <th>Status</th>
                                <th>Points to Accumulate</th>
                            </tr>
                            </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>

    <div class="row">
        <div class=" col-md-12 col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-shopping-cart"></i> Summary</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="tbl_summary" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Not Qualified</th>
                                <th>Mechanic 1 Qualifier</th>
                                <th>Mechanic 2 Qualifier</th>
                                <th>Mechanic 3 Qualifier</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                            <tfoot>
                            <tr>
                                <th>Not Qualified</th>
                                <th>Mechanic 1 Qualifier</th>
                                <th>Mechanic 2 Qualifier</th>
                                <th>Mechanic 3 Qualifier</th>
                            </tr>
                            </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
    <div id="no-record-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-blue-gradient">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><i class="fa fa-close"></i> No Record To Display</h4>
                    </div>
                    <div class="modal-body">
                        There are no records to display.
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" id="btn_close_modal" data-dismiss="modal">Close
                        </button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
</section>

@endsection

@section('additional_footer')

    <script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/jquery.dataTables.min.js")}}'></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js")}}'></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.full.min.js")}}'></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker.js")}}'></script>


    <script>
        $(document).ready(function () {
            var date_from;
            var date_to;
            var not_qualified = 0;
            var qualified_1 = 0;
            var qualified_2 = 0;
            var qualified_3 = 0;
            
            var table_data = $('#example1').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true
            });

            var summary = $('#tbl_summary').DataTable({
                "paging": false,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": false,
                "autoWidth": true
            });
            
            
            $('#txt_date_received').daterangepicker({
                showDropdowns: true,
                startDate: new Date(),
                locale: {
                    format: 'YYYY-MM-DD'
                }
            });

            $('#txt_date_received').on('apply.daterangepicker', function (ev, picker) {
                date_from = picker.startDate.format('YYYY-MM-DD');
                date_to = picker.endDate.format('YYYY-MM-DD');
            });


            $("#btn_search").click(function(){
                $.get("/reports/getContestStatus", {
                    contest_id:$('#slct_contest_list').val()
                }).done(function (data) {
                    console.log(data);
                    table_data.clear().draw();
                    var remaining = 0;
                    if(data.length==0){
                        $("#no-record-modal").modal('show');
                        return false;    
                    }else{
                        for(ctr=0;ctr<data.length;ctr++){
                            if(data[ctr].total_points < data[ctr].mechanics_1_points ){
                                remaining = parseFloat(data[ctr].mechanics_1_points - data[ctr].total_points);
                                table_data.row.add(
                                    [
                                        data[ctr].first_name+ " "+data[ctr].last_name,
                                        data[ctr].total_points,
                                        "Mechanic 1",
                                        data[ctr].mechanics_1_points,
                                        "Not Qualified",
                                        remaining,
                                    ]
                                ).draw();
                                not_qualified++;
                            }else if(data[ctr].total_points < data[ctr].mechanics_2_points ){
                                remaining = parseFloat(data[ctr].mechanics_2_points - data[ctr].total_points);
                                table_data.row.add(
                                    [
                                        data[ctr].first_name+ " "+data[ctr].last_name,
                                        data[ctr].total_points,
                                        "Mechanic 2",
                                        data[ctr].mechanics_2_points,
                                        "[Qualified]"+data[ctr].mechanics_1_prize,
                                        remaining
                                    ]
                                ).draw();
                                qualified_1++;
                            }else if(data[ctr].total_points < data[ctr].mechanics_3_points ){
                                remaining = parseFloat(data[ctr].mechanics_3_points - data[ctr].total_points);
                                table_data.row.add(
                                    [
                                        data[ctr].first_name+ " "+data[ctr].last_name,
                                        data[ctr].total_points,
                                        "Mechanic 3",
                                        data[ctr].mechanics_3_points,
                                        "[Qualified]"+data[ctr].mechanics_2_prize,
                                        remaining
                                    ]
                                ).draw();
                                qualified_2++;
                            }else{
                                remaining = parseFloat(data[ctr].total_points - data[ctr].mechanics_3_points);
                                table_data.row.add(
                                    [
                                        data[ctr].first_name+ " "+data[ctr].last_name,
                                        data[ctr].total_points,
                                        "Mechanic 3",
                                        data[ctr].mechanics_3_points,
                                        "[Qualified]"+data[ctr].mechanics_3_prize,
                                        "N/A"
                                    ]
                                ).draw();
                                qualified_3++;
                            }
                            
                        }
                        summary.clear().draw();
                        summary.row.add([
                                not_qualified,
                                qualified_1,
                                qualified_2,
                                qualified_3
                            ]).draw();

                        not_qualified = 0;
                        qualified_1 = 0;
                        qualified_2 = 0;
                        qualified_3 = 0;
                    }
                    
                    
                });
            });
            $('#slct_contest_list').select2({
                ajax: {
                    url: "/promos/getContestTitle",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term // search term
                        };
                    },
                    processResults: function (data) {
                        // parse the results into the format expected by Select2.
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data
                        return {
                            results: data
                        };
                    },
                    cache: true
                },
                minimumInputLength: 2
            });



            
        });
    </script>
@endsection