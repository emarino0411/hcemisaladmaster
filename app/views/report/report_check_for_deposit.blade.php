@extends('admin_template')

@section('additional_header')

        <!-- DataTables -->
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.min.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker-bs3.css")}}'>

@endsection

@section('content')
        <!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <!-- general form elements -->
            <div class="com-md-12 box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Display Checks for Deposit</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <div class="box-body">
                    <div class="row">
                        <form role="form" class="form-horizontal">
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <label for="txt_payment_date" class="col-sm-6 control-label">Check Date / Deposit Date</label>

                                    <div class="col-sm-6">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text"
                                                   class="form-control pull-right"
                                                   id="txt_date_received">

                                        </div>

                                        <!-- /.input group -->
                                    </div>
                                    <!-- /.col-sm-8 -->
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <div class="col-md-2 col-sm-2">
                                <button id="btn_search" type="button" class="btn btn-primary"><i class="ion ion-search"></i> Search Checks
                                </button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
            <!-- /.box -->
            <!--/.col (left) -->
        </div>
        <!-- search form -->
    </div>
    <!-- /.row (main row) -->

    <div class="row">
        <div class=" col-md-12 col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-shopping-cart"></i> Checks for Deposit</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Deposit Date</th>
                                <th>PO No</th>
                                <th>Client</th>
                                <th>Bank</th>
                                <th>Branch</th>
                                <th>Check Date</th>
                                <th>Check No</th>                                
                                <th>Amount</th>
                                <th>Remarks</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                            <tfoot>
                            <tr>
                                <th>Deposit Date</th>
                                <th>PO No</th>
                                <th>Client</th>
                                <th>Bank</th>
                                <th>Branch</th>
                                <th>Check Date</th>
                                <th>Check No</th>                                
                                <th>Amount</th>
                                <th>Remarks</th>
                            </tr>
                            </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
    <div id="no-record-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-blue-gradient">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><i class="fa fa-close"></i> No Record To Display</h4>
                    </div>
                    <div class="modal-body">
                        There are no checks for deposit today.
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" id="btn_close_modal" data-dismiss="modal">Close
                        </button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
</section>

@endsection

@section('additional_footer')

    <script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/jquery.dataTables.min.js")}}'></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js")}}'></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.full.min.js")}}'></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker.js")}}'></script>


    <script>
        $(document).ready(function () {
            var date_from;
            var date_to;
            var table_data = $('#example1').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true
            });
            
            $('#txt_date_received').daterangepicker({
                showDropdowns: true,
                startDate: new Date(),
                locale: {
                    format: 'YYYY-MM-DD'
                }
            });

            $('#txt_date_received').on('apply.daterangepicker', function (ev, picker) {
                date_from = picker.startDate.format('YYYY-MM-DD');
                date_to = picker.endDate.format('YYYY-MM-DD');
                
            });


            $("#btn_search").click(function(){
                $.get("/reports/getCheckForDeposit", {
                    _date_from: date_from,
                    _date_to: date_to
                }).done(function (data) {
                    table_data.clear().draw();

                    if(data.length==0){
                        $("#no-record-modal").modal('show');
                        return false;    
                    }else{
                        for(ctr=0;ctr<data.length;ctr++){
                            if(data[ctr].status == 'Daif'){
                                table_data.row.add(
                                    [
                                        data[ctr].deposit_date1,
                                        data[ctr].purchase_order_no,
                                        data[ctr].first_name+ " "+data[ctr].last_name,
                                        data[ctr].bank,
                                        data[ctr].branch,
                                        data[ctr].check_date,
                                        data[ctr].check_number,
                                        formatToNumberJS(data[ctr].amount),
                                        data[ctr].status,
                                    ]
                                ).draw();
                            }else if(data[ctr].status == 'Daif2'){
                                table_data.row.add(
                                    [
                                        data[ctr].deposit_date2,
                                        data[ctr].purchase_order_no,
                                        data[ctr].first_name+ " "+data[ctr].last_name,
                                        data[ctr].bank,
                                        data[ctr].branch,
                                        data[ctr].check_date,
                                        data[ctr].check_number,
                                        formatToNumberJS(data[ctr].amount),
                                        data[ctr].status,
                                    ]
                                ).draw();
                            }else{
                                table_data.row.add(
                                    [
                                        data[ctr].deposit_date1,
                                        data[ctr].purchase_order_no,
                                        data[ctr].first_name+ " "+data[ctr].last_name,
                                        data[ctr].bank,
                                        data[ctr].branch,
                                        data[ctr].check_date,
                                        data[ctr].check_number,
                                        formatToNumberJS(data[ctr].amount),
                                        data[ctr].status,
                                    ]
                                ).draw();
                            }
                            
                        }
                    }
                    
                    
                });
            });


            
        });
    </script>
@endsection