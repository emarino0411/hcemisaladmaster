@extends('admin_template')

@section('additional_header')

        <!-- DataTables -->
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.min.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker-bs3.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.min.css")}}'>

@endsection

@section('content')
        <!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <!-- general form elements -->
            <div class="com-md-12 box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Display Client Purchases</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <div class="box-body">
                    <div class="row">
                        <form role="form" class="form-horizontal">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label for="slct_client" class="col-sm-4 control-label">Select Client</label>
                                    <div class="col-sm-8">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-user"></i>
                                            </div>
                                            <select class="form-control" id="slct_client">
                                            </select>

                                        </div>
                                        <!-- /.input group -->
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label for="txt_payment_date" class="col-sm-4 control-label">Sales Date</label>

                                    <div class="col-sm-8">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text"
                                                   class="form-control pull-right"
                                                   id="txt_date_received">

                                        </div>

                                        <!-- /.input group -->
                                    </div>
                                    <!-- /.col-sm-8 -->
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <div class="col-md-2 col-sm-2">
                                <button id="btn_search" type="button" class="btn btn-primary"><i class="ion ion-search"></i> Search Purchases
                                </button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
            <!-- /.box -->
            <!--/.col (left) -->
        </div>
        <!-- search form -->
    </div>
    <!-- /.row (main row) -->

    <div class="row">
        <div class=" col-md-12 col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-shopping-cart"></i> List Client Purchases</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>PO NO</th>
                                <th>Client</th>
                                <th>Amount Purchased</th>
                                <th>Sponsor</th>
                                <th>Presenter</th>
                                <th>Action</th>
                                
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                            <tfoot>
                            <tr>
                                <th>PO NO</th>
                                <th>Client</th>
                                <th>Amount Purchased</th>
                                <th>Sponsor</th>
                                <th>Presenter</th>
                                <th>Action</th>
                            </tr>
                            </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>

    <div class="row hidden">
        <div class=" col-md-12 col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-shopping-cart"></i> Summary</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="tbl_summary" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No of PO</th>
                                <th>No of Closed Account</th>
                                <th>No of SPO</th>
                                <th>No of Daif</th>
                                <th>No of Daif 2x</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                            <tfoot>
                            <tr>
                                 <th>No of PO</th>
                                <th>No of Closed Account</th>
                                <th>No of SPO</th>
                                <th>No of Daif</th>
                                <th>No of Daif 2x</th>
                            </tr>
                            </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
    <div id="no-record-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-blue-gradient">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><i class="fa fa-close"></i> No Record To Display</h4>
                    </div>
                    <div class="modal-body">
                        There are no records to display.
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" id="btn_close_modal" data-dismiss="modal">Close
                        </button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
</section>

@endsection

@section('additional_footer')

    <script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/jquery.dataTables.min.js")}}'></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js")}}'></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.full.min.js")}}'></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker.js")}}'></script>


    <script>
        $(document).ready(function () {

             $('#slct_client').select2({
                ajax: {
                    url: "/purchases/searchClient",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term // search term
                        };
                    },
                    processResults: function (data) {
                        // parse the results into the format expected by Select2.
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data
                        return {
                            results: data
                        };
                    },
                    cache: true
                },
                minimumInputLength: 2
            });

            var date_from;
            var date_to;
            var total_payment = 0;
            var remaining_total = 0;
            var total_po = 0;
            var total_payments = 0;
            
            var table_data = $('#example1').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true
            });

            var summary = $('#tbl_summary').DataTable({
                "paging": false,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": false,
                "autoWidth": true
            });
            
            
            $('#txt_date_received').daterangepicker({
                showDropdowns: true,
                startDate: new Date(),
                locale: {
                    format: 'YYYY-MM-DD'
                }
            });

            $('#txt_date_received').on('apply.daterangepicker', function (ev, picker) {
                date_from = picker.startDate.format('YYYY-MM-DD');
                date_to = picker.endDate.format('YYYY-MM-DD');
            });
            
           
            $("#btn_search").click(function(){
               getPurchaseForFollowUp();                
            });

            getPurchaseForFollowUp(); 

            $('#slct_agent').select2({
                ajax: {
                    url: "/bookings/searchRepresentative",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term // search term
                        };
                    },
                    processResults: function (data) {
                        // parse the results into the format expected by Select2.
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data
                        return {
                            results: data
                        };
                    },
                    cache: true
                },
                minimumInputLength: 2
            });

            function getPurchaseForFollowUp(){
                $.get("/reports/getClientPurchases", {
                   _date_from:date_from,
                   _date_to:date_to,
                   client: $("#slct_client").select2('val')

                }).done(function (data) {
                    console.log(data);
                    var purchase = data[0];
                    var payments = data[1];
                    var payment_idx = 0;

                    table_data.clear().draw();
                    var purchase = data[0];    
                    var purchase_price = data[1];  

                    if(data.length==0){
                        $("#no-record-modal").modal('show');
                        return false;    
                    }else{
                        var price_index = 0;
                        for(ctr=0;ctr<purchase.length;ctr++){
                            if( !purchase[ctr].presenter_first_name || !purchase[ctr].presenter_last_name){
                                presenter_name = "N/A";
                            }else{
                                presenter_name = purchase[ctr].presenter_first_name + " " + purchase[ctr].presenter_last_name;
                            }
                            
                            for(ctr1=0;ctr1<purchase_price.length;ctr1++){
                                if(purchase[ctr].id==purchase_price[ctr1].id){
                                    price_index=ctr1;
                                    break;
                                }
                            }
                           
                            table_data.row.add(
                                [
                                    purchase[ctr].purchase_order_no,
                                    purchase[ctr].client_first_name + " " + purchase[ctr].client_last_name,
                                    formatToNumberJS(purchase_price[price_index].amount),
                                    
                                    purchase[ctr].first_name + " " + purchase[ctr].last_name,
                                    presenter_name,
                                    '<a href="/purchases/viewPurchaseDetails/'+purchase[ctr].id+'" data-toggle="tooltip" title="View purchase details" class="btn btn-success btn-xs"> <i class="fa fa-info-circle"></i> View </a>'
                                ]
                            ).draw();
                            
                            total_po++;
                           
                        }
                     
                    }
                    
                    
                });
            }


           




            
        });
    </script>
@endsection