@extends('admin_template')

@section('additional_header')

        <!-- DataTables -->
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.min.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker-bs3.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.min.css")}}'>

@endsection

@section('content')
        <!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <!-- general form elements -->
            <div class="com-md-12 box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Display Checks By Status</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <div class="box-body">
                    <div class="row">
                        <form role="form" class="form-horizontal">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label for="slct_status" class="col-sm-4 control-label">Check Status</label>
                                    <div class="col-sm-8">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-heartbeat"></i>
                                            </div>
                                            <select id="slct_status" class="form-control">
                                            <option value="all">Display All Checks</option>
                                            <option value="For Deposit">For Deposit</option>
                                            <option value="Good">Good / Deposited</option>
                                            <option value="On Hold">On Hold</option>
                                            <option value="Closed">Accounts Closed</option>
                                            <option value="Daif">Daif</option>
                                            <option value="Daif2">Daif 2x</option>
                                            <option value="SPO">Stop Payment Order</option>
                                        </select>

                                        </div>
                                        <!-- /.input group -->
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label for="txt_payment_date" class="col-sm-4 control-label">Check Date</label>

                                    <div class="col-sm-8">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text"
                                                   class="form-control pull-right"
                                                   id="txt_date_received">

                                        </div>

                                        <!-- /.input group -->
                                    </div>
                                    <!-- /.col-sm-8 -->
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <div class="col-md-2 col-sm-2">
                                <button id="btn_search" type="button" class="btn btn-primary"><i class="ion ion-search"></i> Search Sales
                                </button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
            <!-- /.box -->
            <!--/.col (left) -->
        </div>
        <!-- search form -->
    </div>
    <!-- /.row (main row) -->

    <div class="row">
        <div class=" col-md-12 col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-shopping-cart"></i> List of Checks By Status</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>PO No</th>
                                <th>Client</th>
                                <th>Check No</th>
                                <th>Check Date</th>
                                <th>Deposit Date</th>
                                <th>Bank</th>
                                <th>Branch</th>
                                <th>Amount</th>
                                <th>Remarks</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                            <tfoot>
                            <tr>
                                <th>PO No</th>
                                <th>Client</th>
                                <th>Check No</th>
                                <th>Check Date</th>
                                <th>Deposit Date</th>
                                <th>Bank</th>
                                <th>Branch</th>
                                <th>Amount</th>
                                <th>Remarks</th>
                            </tr>
                            </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>

    <div class="row">
        <div class=" col-md-12 col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-shopping-cart"></i> Summary</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="tbl_summary" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No of POs</th>
                                <th>No Of Good Checks</th>
                                <th>No Of For Deposit Checks</th>
                                <th>No Of On Hold Checks</th>
                                <th>No Of Accounts Closed Checks</th>
                                <th>No Of Daif Checks</th>
                                <th>No Of Daif 2x Checks</th>
                                <th>No Of SPO Checks</th>
                                <th>No Of Under Garnishment</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                            <tfoot>
                            <tr>
                                <th>No of POs</th>
                                <th>No Of Good Checks</th>
                                <th>No Of For Deposit Checks</th>
                                <th>No Of On Hold Checks</th>
                                <th>No Of Accounts Closed Checks</th>
                                <th>No Of Daif Checks</th>
                                <th>No Of Daif 2x Checks</th>
                                <th>No Of SPO Checks</th>
                                <th>No Of Under Garnishment</th>
                            </tr>
                            </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
    <div id="no-record-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-blue-gradient">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><i class="fa fa-close"></i> No Record To Display</h4>
                    </div>
                    <div class="modal-body">
                        There are no checks for deposit today.
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" id="btn_close_modal" data-dismiss="modal">Close
                        </button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
</section>

@endsection

@section('additional_footer')

    <script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/jquery.dataTables.min.js")}}'></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js")}}'></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.full.min.js")}}'></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker.js")}}'></script>


    <script>
        $(document).ready(function () {
            var date_from;
            var date_to;
            var total_po_count = 0;
            var total_points = 0;
            var total_purchase_price = 0;
            
            var table_data = $('#example1').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true
            });

            var summary = $('#tbl_summary').DataTable({
                "paging": false,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": false,
                "autoWidth": true
            });
            
            
            $('#txt_date_received').daterangepicker({
                showDropdowns: true,
                startDate: new Date(),
                locale: {
                    format: 'YYYY-MM-DD'
                }
            });

            $('#txt_date_received').on('apply.daterangepicker', function (ev, picker) {
                date_from = picker.startDate.format('YYYY-MM-DD');
                date_to = picker.endDate.format('YYYY-MM-DD');
            });


            $("#btn_search").click(function(){
            	var for_verif = 0;
                var good = 0;
                var on_hold = 0;
                var closed = 0;
                var daif = 0;
                var daif2 = 0;
                var spo = 0;
                var garnish = 0;
                var po = 0;

                $.get("/reports/getListOfCheckByStatus", {
                    _date_from: date_from,
                    _date_to: date_to,
                    check_status:$('#slct_status').val()
                }).done(function (data) {
	                    console.log(data);
	                    table_data.clear().draw();

	                    if(data.length==0){
	                        $("#no-record-modal").modal('show');
	                        return false;    
	                    }else{
	                        for(ctr=0;ctr<data.length;ctr++){
                                po++;
	                        	if(data[ctr].status == 'For Deposit'){
	                                for_verif++;
	                            }else if(data[ctr].status == 'Good'){
	                                good++;
	                            }else if(data[ctr].status == 'On Hold'){
	                                on_hold++;
	                            }else if(data[ctr].status == 'Closed'){
	                                closed++;
	                            }else if(data[ctr].status == 'Daif'){
	                                daif++;
	                            }else if(data[ctr].status == 'Daif2'){
	                                daif2++;
	                            }else if(data[ctr].status == 'SPO'){
	                                spo++;
	                            }
                                else if(data[ctr].status == 'Under Garnishment'){
                                    garnish++;
                                }else{

	                            }


	                        	if(data[ctr].status == 'Daif'){
	                                table_data.row.add(
	                                    [
	                                        data[ctr].purchase_order_no,
	                                        data[ctr].first_name+ " "+data[ctr].last_name,
	                                        data[ctr].check_number,
	                                        data[ctr].check_date,
	                                        data[ctr].deposit_date1,
	                                        data[ctr].bank,
	                                        data[ctr].branch,
	                                        formatToNumberJS(data[ctr].amount),
	                                        data[ctr].status,
	                                    ]
	                                ).draw();
	                            }else if(data[ctr].status == 'Daif2'){
	                                table_data.row.add(
	                                    [
	                                        data[ctr].purchase_order_no,
	                                        data[ctr].first_name+ " "+data[ctr].last_name,
	                                        data[ctr].check_number,
	                                        data[ctr].check_date,
	                                        data[ctr].deposit_date2,
	                                        data[ctr].bank,
	                                        data[ctr].branch,
	                                        formatToNumberJS(data[ctr].amount),
	                                        data[ctr].status,
	                                    ]
	                                ).draw();
	                            }else{
	                                table_data.row.add(
	                                    [
	                                        data[ctr].purchase_order_no,
	                                        data[ctr].first_name+ " "+data[ctr].last_name,
	                                        data[ctr].check_number,
	                                        data[ctr].check_date,
	                                        data[ctr].deposit_date1,
	                                        data[ctr].bank,
	                                        data[ctr].branch,
	                                        formatToNumberJS(data[ctr].amount),
	                                        data[ctr].status,
	                                    ]
	                                ).draw();
	                            }
	                            
	                        
	                   
	                    }
                        summary.clear().draw();
                    	summary.row.add([
                          	    po,
                                for_verif,
                                good,
                                on_hold,
                                closed,
                                daif,
                                daif2,
                                spo,
                                garnish
                        ]).draw();
                        for_verif = 0;
                        good = 0;
                        on_hold = 0;
                        closed = 0;
                        daif = 0;
                        daif2 = 0;
                        spo = 0;
                        garnish = 0;
                        po = 0;

	                }
	          	
	          	});      
                    
                
            });

          



            
        });
    </script>
@endsection