@extends('admin_template')

@section('additional_header')

        <!-- DataTables -->
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.min.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker-bs3.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.min.css")}}'>

@endsection

@section('content')
        <!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <!-- general form elements -->
            <div class="com-md-12 box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Display Sales By Agent</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <div class="box-body">
                    <div class="row">
                        <form role="form" class="form-horizontal">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label for="slct_agent" class="col-sm-4 control-label">Select Agent</label>
                                    <div class="col-sm-8">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-user"></i>
                                            </div>
                                            <select class="form-control" id="slct_agent">
                                            </select>

                                        </div>
                                        <!-- /.input group -->
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label for="txt_payment_date" class="col-sm-4 control-label">Sales Date</label>

                                    <div class="col-sm-8">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text"
                                                   class="form-control pull-right"
                                                   id="txt_date_received">

                                        </div>

                                        <!-- /.input group -->
                                    </div>
                                    <!-- /.col-sm-8 -->
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <div class="col-md-2 col-sm-2">
                                <button id="btn_search" type="button" class="btn btn-primary"><i class="ion ion-search"></i> Search Sales
                                </button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
            <!-- /.box -->
            <!--/.col (left) -->
        </div>
        <!-- search form -->
    </div>
    <!-- /.row (main row) -->

    <div class="row">
        <div class=" col-md-12 col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-shopping-cart"></i> Sales By Agent</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Purchase Date</th>
                                <th>PO No</th>
                                <th>Client</th>
                                <th>Client Address</th>
                                <th>Items Purchased</th>
                                <th>Amount</th>
                                <th>Remarks</th>
                                <th>Total Production Points</th>
                                <th>PO Status</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                            <tfoot>
                            <tr>
                                <th>Purchase Date</th>
                                <th>PO No</th>
                                <th>Client</th>
                                <th>Client Address</th>
                                <th>Items Purchased</th>
                                <th>Amount</th>
                                <th>Remarks</th>
                                <th>Total Production Points</th>
                                <th>PO Status</th>
                            </tr>
                            </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>

    <div class="row">
        <div class=" col-md-12 col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-shopping-cart"></i> Summary</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="tbl_summary" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No of PO</th>
                                <th>Total Purchase Price</th>
                                <th>Total Purchase Points</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                            <tfoot>
                            <tr>
                                <th>No of PO</th>
                                <th>Total Purchase Price</th>
                                <th>Total Purchase Points</th>
                            </tr>
                            </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
    <div id="no-record-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-blue-gradient">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><i class="fa fa-close"></i> No Record To Display</h4>
                    </div>
                    <div class="modal-body">
                        There are no records to display.
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" id="btn_close_modal" data-dismiss="modal">Close
                        </button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
</section>

@endsection

@section('additional_footer')

    <script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/jquery.dataTables.min.js")}}'></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js")}}'></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.full.min.js")}}'></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker.js")}}'></script>


    <script>
        $(document).ready(function () {
            var date_from;
            var date_to;
            var total_po_count = 0;
            var total_points = 0;
            var total_purchase_price = 0;
            
            var table_data = $('#example1').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true
            });

            var summary = $('#tbl_summary').DataTable({
                "paging": false,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": false,
                "autoWidth": true
            });
            
            
            $('#txt_date_received').daterangepicker({
                showDropdowns: true,
                startDate: new Date(),
                locale: {
                    format: 'YYYY-MM-DD'
                }
            });

            $('#txt_date_received').on('apply.daterangepicker', function (ev, picker) {
                date_from = picker.startDate.format('YYYY-MM-DD');
                date_to = picker.endDate.format('YYYY-MM-DD');
            });


            $("#btn_search").click(function(){
                $.get("/reports/getSalesByAgent", {
                    _date_from: date_from,
                    _date_to: date_to,
                    _agent:$('#slct_agent').val()
                }).done(function (data) {
                    console.log(data);
                    var po = data[0];
                    var items = data[1];
                    table_data.clear().draw();

                    if(data.length==0){
                        $("#no-record-modal").modal('show');
                        return false;    
                    }else{
                        for(ctr=0;ctr<po.length;ctr++){
                            if(po[ctr].total_points == null){
                                po[ctr].total_points = 0;
                            }
                            var idx=0;
                            var item = '';
                            for(idx=0;idx<items.length;idx++){
                                if(items[idx].id==po[ctr].id){
                                    if( item == ''){
                                        item += items[idx].description;
                                    }else{
                                        item += " | " + items[idx].description;
                                    }

                                }
                            }
                            table_data.row.add(
                                [
                                    po[ctr].purchase_date,
                                    po[ctr].purchase_order_number,
                                    po[ctr].first_name+ " "+po[ctr].last_name,
                                    po[ctr].client_street+ ", "+po[ctr].cityname+ ", "+po[ctr].provname,
                                    item,
                                    formatToNumberJS(po[ctr].total_price),
                                    po[ctr].remarks,
                                    parseFloat(po[ctr].total_points).toFixed(4),
                                    po[ctr].status
                                ]
                            ).draw();
                            total_po_count++;

                            total_points += parseFloat(po[ctr].total_points);
                            total_purchase_price += parseFloat(po[ctr].total_price);
                            
                        }
                        summary.clear().draw();
                        summary.row.add([
                                total_po_count,
                                formatToNumberJS(total_purchase_price),
                                total_points.toFixed(4)
                            ]).draw();
                        total_po_count = 0;
                        total_purchase_price = 0;
                        total_points = 0;
                    }
                    
                    
                });
            });
            $('#slct_agent').select2({
                ajax: {
                    url: "/bookings/searchRepresentative",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term // search term
                        };
                    },
                    processResults: function (data) {
                        // parse the results into the format expected by Select2.
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data
                        return {
                            results: data
                        };
                    },
                    cache: true
                },
                minimumInputLength: 2
            });



            
        });
    </script>
@endsection