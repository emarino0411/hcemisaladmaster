@extends('admin_template')

@section('additional_header')

    <link rel="stylesheet"
          href='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker-bs3.css")}}'>
    <link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/iCheck/all.css")}}'>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.css")}}'>
    <link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.min.css")}}'>

    @endsection

    @section('content')
            <!-- Main content -->
    <section class="content">

    <!-- Client Information -->
        <div class="row">
            <div class=" col-md-12 col-xs-12">
                <div class="row">
                    <!-- client -->
                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="box-header">
                                <h3 class="box-title"><i class="fa fa-user"></i> Search Client</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <form class="form-horizontal">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="txt_po_no" class="col-sm-4 control-label">Add Collection Update
                                                        For</label>

                                                    <div class="col-sm-8">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-user"></i>
                                                            </div>
                                                            <select id="slct_client" class="form-control">
                                                                <option value="{{$client->id}}">{{$client->last_name}}, {{$client->first_name}}</option>
                                                            </select>
                                                        </div>
                                                        <!-- /.input group -->
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="txt_po_no" class="col-sm-4 control-label">Select PO
                                                        No</label>

                                                    <div class="col-sm-8">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-edit"></i>
                                                            </div>
                                                            <select class="form-control" id="po_list">
                                                                {{--<option>Please select Client</option>--}}
                                                                <option value="{{$purchase->id}}">{{$purchase->purchase_order_no}}</option>
                                                            </select>
                                                        </div>
                                                        <!-- /.input group -->
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                    <!-- /client -->

                </div>
            </div>
        </div>
   <!-- / client information -->

   <!-- Client Details -->
        <div class="row">
            <div class="col-md-12 col-xs-12">
             <div class="com-md-12 box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-user"></i> Client Details</h3>
                    </div>
             <div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="tbl_client_detail" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Address</th>
                            <th>Contact Details</th>
                            <th>Email Address</th>
                            <th>Sponsor</th>
                            <th>Presenter</th>
                        </tr>
                        </thead>
                        <tbody>
                       @foreach($collectiondetails as $details)
                           <tr>
                                <td>{{$details->first_name}} &nbsp;{{$details->last_name}} </td>
                                <td>{{$details->client_street}}<br>{{$details->cityname}}<br>{{$details->provname}}</td>
                                <td>{{$details->phone2}}<br>{{$details->phone1}}</td>
                                <td>{{$details->email_address}}</td>
                                <td>@foreach($sponsordetails as $splist){{$splist->first_name}} {{$splist->last_name}}@endforeach</td>
                                <td>@foreach($presenterdetails as $prlist){{$prlist->first_name}} {{$prlist->last_name}}@endforeach</td>
                           </tr>
                       @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
             </div>

             </div>
          </div>
        </div>
  <!-- /Client Details -->


  <!-- Purchase Details -->
        <div class="row">
            <div class="col-md-12 col-xs-12">
             <div class="com-md-12 box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-shopping-cart"></i> Purchase Details</h3>
                    </div>

             <div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="tbl_purchase_detail" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Items Purchased</th>
                            <th>Date Purchased</th>
                            <th>Terms</th>
                            <th>Remarks</th>
                            <th>Payment History</th>
                        </tr>
                        </thead>
                        <tbody>
                           <tr>
                                <td>@foreach($purchaselistdetails as $purchaselist){{$purchaselist->description}}<br> @endforeach</td>
                                <td>{{$purchaselist->purchase_date}}</td>
                                <td>{{$purchaselist->terms}} months</td>  
                                <td>{{$purchaselist->remarks}}</td>
                                <td><a type="button" target="_blank" href="/payments/viewPayments/{{$purchase->id}}" class="btn btn-success pull-left"><i
                                    class="fa fa-plus"></i>View Payment History
                        </a></td>
                           </tr>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
             </div>



             </div>
          </div>
        </div>
  <!-- /Purchase Details -->


        <!-- Payment Details -->
        <div class="row">
                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="box-header">

                            </div>
                    <!-- /.box-header -->




                            <!-- Purchase Item Table -->
                            <div class="row">
                                <div class="col-md-12">
                                  <h6 class="page-header"><i class="fa fa-list-alt"></i> List of Updates</h6>
                                    <!-- /row -->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table id="tbl_payments_list" class="table table-bordered table-striped">
                                                <thead align="center">
                                                <tr>
                                                    <th width="15%">Date</th>
                                                    <th width="15%">Person-in-Charge</th>
                                                    <th width="70%">Remarks</th
                                                </tr>
                                                </thead>
                                                <tbody>

                                                </tbody>
                                                <tfoot>
                                                <tr>
                                                    <th>Date</th>
                                                    <th>Person-in-Charge</th>
                                                    <th>Remarks</th
                                                </tr>
                                                </tfoot>
                                            </table>
                                        </div>

                                    </div>

                                </div>

                            </div>
                            <!-- /Purchase Item Table -->
                            <hr>
                        
         

                    <!-- form start -->
                    <form class="form-horizontal">
                        <div class="box-body">
                            <div class="row @if(!isAccessModuleAllowed('payments_add')) hidden @endif"
                                 id="div_add_payment">
                    <div class="box-header with-border">
                        <h3 class="page-header"><i class="fa fa-book"></i>Enter Collection Updates</h3>
                        
                    </div>


                                <div class="col-md-12">

                                    <!-- Payment Date -->
                                    <div class="col-md-12">
                                        <div class="form-group">
                                           
                                            <div class="col-md-12">
                                               
                                                <textarea id="txtarea_update" rows="5" class="form-control"></textarea>
                                            </div>
                                            <!-- /.col-sm-8 -->
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                    <!-- /Payment Date -->

                                    <!-- Add to List -->
                                    <div class="col-md-12 pull-right">

                                        <button type="button" id="btn_add_to_list" class="btn btn-primary pull-right"><i
                                                    class="fa fa-plus"></i>Add to List
                                        </button>

                                    </div>
                                    <!-- /Add to List -->

                                </div>
                                <!-- ./col-md-12 -->
                            </div>
                            <!-- /.row-->

                
                        </div>
                        <!-- /.box-body -->
                    </form>
                    <!-- /form-end -->
                </div>
                <!-- /.box -->
                <!--/.col (left) -->
            </div>
            <!-- search form -->
        </div>
   

        {{--MODAL--}}
        <div id="add-payment-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-blue-gradient">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><i class="ion ion-check"></i> Add Payment</h4>
                    </div>
                    <div class="modal-body">
                        Record has been added.
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" id="btn_close_modal" data-dismiss="modal">Close
                        </button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        {{--MOdal End--}}



    </section>
@endsection

@section('additional_footer')

    <script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/jquery.dataTables.min.js")}}'></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js")}}'></script>

    <script src='{{ asset("/bower_components/AdminLTE/plugins/jQueryUI/jquery-ui.min.js")}}'></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.full.min.js")}}'></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker.js")}}'></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/iCheck/icheck.min.js")}}'></script>

    <script>
        $(document).ready(function () {
            var new_deposit_date_value = '';
            $('#verify_complete_payment').modal({show: false});

            $('#txt_date_received').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                startDate: new Date(),
                locale: {
                    format: 'YYYY-MM-DD'
                }
            });

            $("#slct_bank").select2();

            $("#slct_payment_type").change(function () {
                if ($(this).val() == 'cash') {
                    $(".div_cash").show();
                    $("#div_check").hide();
                } else {
                    $(".div_cash").hide();
                    $("#div_check").show();
                }
            });

            //iCheck for checkbox and radio inputs
            $('input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass: 'iradio_minimal-blue'
            });

            var table_data = $('#tbl_payments_list').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true,
                 "order": [[ 0, "desc" ]],
                "scrollX": true
            });
            $('#slct_client').select2({
                ajax: {
                    url: "/purchases/searchClient",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term // search term
                        };
                    },
                    processResults: function (data) {
                        // parse the results into the format expected by Select2.
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data
                        return {
                            results: data
                        };
                    },
                    cache: true
                },
                minimumInputLength: 2
            });

            $('#slct_client').on("select2:select", function (e) {
                $.get("/payments/getClientPO", {
                    client_id: $('#slct_client').val()
                }).done(function (data) {
                    $("#po_list").empty();
                    $("#po_list").append($('<option>', {value: ''})
                            .text('Select PO Number'));
                    $("#po_list").select2({
                        data: data
                    });
                });
            });

            $('#po_list').on("select2:select", function (e) {
                updatePaymentTable();
            });

            $("#btn_add_to_list").click(function () {

                if ($("#slct_client").val() == null) {
                    alert('Client is required');
                    $("#slct_client").focus();
                    $("#slct_client").select2("open");
                    return false;
                }

                if ($("#po_list").val() == '') {
                    alert('PO Number is required');
                    $("#po_list").focus();
                    $("#po_list").select2("open");
                    return false;
                }

                $.get("/collections/saveCollectionsUpdate", {
                    po_id: $('#po_list').val(),
                    update: $("#txtarea_update").val()
                }).done(function (data) {
                    updatePaymentTable();
                });
            });
 
            function updatePaymentTable(){

                $.get("/collections/getCollectionsList", {
                    po_id: $('#po_list').val()
                }).done(function (data) {
                    table_data.clear().draw();
                    console.log(data.length);
                    for(ctr=0;ctr<data.length;ctr++){
                         table_data.row.add(
                            [
                              data[ctr].update_date,
                              data[ctr].last_name+", "+data[ctr].first_name,
                              data[ctr].update
                            ]).draw();
                    }
                });
            }
            updatePaymentTable();
                
        });
    </script>
@endsection



