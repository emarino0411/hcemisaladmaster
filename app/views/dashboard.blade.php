@extends('admin_template')

@section('additional_header')

        <!-- fullCalendar 2.2.5-->
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/fullcalendar/fullcalendar.min.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/fullcalendar/fullcalendar.print.css")}}'
      media="print">

@endsection

@section('content')
        <!-- Main content -->
<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
        @if(isAccessModuleAllowed('dashboard_new_agent'))
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3>{{$agents_count}}</h3>

                        <p>Agents</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-person-add"></i>
                    </div>
                    <a href="/agents" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
        @endif

            @if(isAccessModuleAllowed('dashboard_new_po'))
                    <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3>{{$po_count}}</h3>

                        <p>Purchases</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-shopping-cart"></i>
                    </div>
                    <a href="/purchases" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            @endif

            @if(isAccessModuleAllowed('dashboard_bookings'))
                    <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3>{{$bookings_count}}</h3>

                        <p>Bookings Today</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-calendar"></i>
                    </div>
                    <a href="/bookings" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
        @endif
        @if(isAccessModuleAllowed('dashboard_due_payments'))
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3>{{$due_payments}}</h3>

                        <p>Due Payments Today</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-alert-circled"></i>
                    </div>
                    <a href="/payments/checksForDeposit" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            @endif
                    <!-- ./col -->

            @if(isAccessModuleAllowed('dashboard_approved_commissions'))
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-purple">
                        <div class="inner">
                            <h3>{{$approved_commissions}}</h3>
                            <h4>Approved Commissions</h4>

                        </div>
                        <div class="icon">
                            <i class="ion ion-thumbsup"></i>
                        </div>
                        <a href="/commissions/list" class="small-box-footer">More info <i
                                    class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
            @endif

            @if(isAccessModuleAllowed('dashboard_promo_qualifiers'))
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-light-blue">
                        <div class="inner">
                            <h3>{{$promo_qualifiers}}</h3>
                            <h4>Promo Qualifiers</h4>

                        </div>
                        <div class="icon">
                            <i class="ion ion-happy"></i>
                        </div>
                        <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
            @endif

            @if(isAccessModuleAllowed('dashboard_checks_follow_up'))
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-teal">
                        <div class="inner">
                            <h3>{{$checks_for_follow_up}}</h3>
                            <h4>Checks for Follow Up</h4>

                        </div>
                        <div class="icon">
                            <i class="ion ion-social-usd"></i>
                        </div>
                        <a href="/collections" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
            @endif

            @if(isAccessModuleAllowed('dashboard_for_pull_out'))
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-gray">
                        <div class="inner">
                            <h3>{{$for_pull_out}}</h3>
                            <h4>For Pull-Out</h4>

                        </div>
                        <div class="icon">
                            <i class="ion ion-sad"></i>
                        </div>
                        <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->

            @endif

    </div>
    <!-- /.row -->
    <!-- Main row -->
    <div class="row">
        @if(isAccessModuleAllowed('dashboard_calendar'))
            <div class="col-md-6">
                <div class="box box-primary">
                    <div class="box-body no-padding">
                        <div class="box-header with-border">
                            <i class="ion ion-calendar"></i>

                            <h3 class="box-title">Bookings</h3>
                        </div>
                        <!-- THE CALENDAR -->
                        <div id="calendar"></div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /. box -->
            </div>
            <!-- /.col -->
        @endif

        <div class="col-md-6">
            @if(isAccessModuleAllowed('dashboard_orders'))
                    <!-- TABLE: LATEST ORDERS -->
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Latest Purchase Orders</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i
                                    class="fa fa-times"></i>
                        </button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>PO No</th>
                                <th>Client Name</th>
                                <th>Date Purchased</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($purchases) == 0)
                                <tr>
                                    <td colspan="5">No data to display</td>

                                </tr>
                            @endif

                            @foreach($purchases as $purchase)
                                <tr>
                                    <td><input type="checkbox"></td>
                                    <td>{{$purchase->purchase_order_no}}</td>
                                    <td>{{$purchase->first_name}} {{$purchase->last_name}}</td>
                                    <td>{{$purchase->purchase_date}}</td>
                                    <td>{{$purchase->transaction_status}}</td>

                                </tr>
                            @endforeach

                            </tbody>
                            <tfoot>
                            <tr>
                                <th>#</th>
                                <th>PO No</th>
                                <th>Client</th>
                                <th>Date Purchased</th>
                                <th>Status</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer clearfix">
                    <a href="/purchases/addNew" class="btn btn-sm btn-info btn-flat pull-left">Place New Order</a>
                    <a href="/purchases/" class="btn btn-sm btn-default btn-flat pull-right">View All Orders</a>
                </div>
                <!-- /.box-footer -->
            </div>
            @endif
                    <!-- /.box -->
            @if(isAccessModuleAllowed('dashboard_top_agents'))
                <div class="box box-success">
                    <div class="box-header with-border bg-green">
                        <i class="ion ion-trophy"></i>

                        <h3 class="box-title">Top 10 Agents</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body no-padding">
                        <table class="table table-striped">
                            <tr>
                                <th style="width: 10px">Ranking</th>
                                <th>Agent Name</th>
                                <th>Sales</th>
                                <th style="width: 40px">No Of Sales</th>
                            </tr>
                            <tr>
                                <td>1.</td>
                                <td>Juan Dela Cruz</td>
                                <td>
                                    <div class="progress progress-xs">
                                        <div class="progress-bar progress-bar-danger" style="width: 55%"></div>
                                    </div>
                                </td>
                                <td><span class="badge bg-red">55%</span></td>
                            </tr>
                            <tr>
                                <td>2.</td>
                                <td>Pedro Cruz</td>
                                <td>
                                    <div class="progress progress-xs">
                                        <div class="progress-bar progress-bar-yellow" style="width: 70%"></div>
                                    </div>
                                </td>
                                <td><span class="badge bg-yellow">70%</span></td>
                            </tr>
                            <tr>
                                <td>3.</td>
                                <td>Liwanag Dalamhati</td>
                                <td>
                                    <div class="progress progress-xs progress-striped active">
                                        <div class="progress-bar progress-bar-primary" style="width: 30%"></div>
                                    </div>
                                </td>
                                <td><span class="badge bg-light-blue">30%</span></td>
                            </tr>
                            <tr>
                                <td>4.</td>
                                <td>Makisig Dimagiba</td>
                                <td>
                                    <div class="progress progress-xs progress-striped active">
                                        <div class="progress-bar progress-bar-success" style="width: 90%"></div>
                                    </div>
                                </td>
                                <td><span class="badge bg-green">90%</span></td>
                            </tr>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            @endif
        </div>
        <!-- /.col -->

    </div>
    <!-- /.row (main row) -->

</section>

@endsection

@section('additional_footer')
        <!-- fullCalendar 2.2.5 -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src='{{ asset("/bower_components/AdminLTE/plugins/fullcalendar/fullcalendar.min.js")}}'></script>

<script>
    $(document).ready(function () {


        /* initialize the external events
         -----------------------------------------------------------------*/
        function ini_events(ele) {
            ele.each(function () {

                // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
                // it doesn't need to have a start or end
                var eventObject = {
                    title: $.trim($(this).text()) // use the element's text as the event title
                };

                // store the Event Object in the DOM element so we can get to it later
                $(this).data('eventObject', eventObject);

                // make the event draggable using jQuery UI
                $(this).draggable({
                    zIndex: 1070,
                    revert: true, // will cause the event to go back to its
                    revertDuration: 0  //  original position after the drag
                });

            });
        }

        ini_events($('#external-events div.external-event'));

        /* initialize the calendar
         -----------------------------------------------------------------*/
        //Date for the calendar events (dummy data)
        var date = new Date();
        var d = date.getDate(),
                m = date.getMonth(),
                y = date.getFullYear();

        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            buttonText: {
                today: 'today',
                month: 'month',
                week: 'week',
                day: 'day'
            },
            //Random default events
            events: [
                    @foreach($bookings as $booking)
                    {
                    title: "{{$booking->title}}",
                    @if($booking->start == null)
                      allDay: true,
                    start: new Date("{{$booking->booking_date}}"),
                    @else
                      start: new Date("{{$booking->start}}"),
                    end: new Date("{{$booking->end}}"),
                    @endif

                    backgroundColor: "{{$booking->backgroundColor}}", //red
                    borderColor: "{{$booking->borderColor}}" //red
                }
                @if ($booking != end($bookings)) , @endif
        @endforeach
      ],
            //[
//        {
//          title: 'All Day Event',
//          start: new Date(y, m, 1),
//          backgroundColor: "#f56954", //red
//          borderColor: "#f56954" //red
//        },
//        {
//          title: 'Long Event',
//          start: new Date(y, m, d - 5),
//          end: new Date(y, m, d - 2),
//          backgroundColor: "#f39c12", //yellow
//          borderColor: "#f39c12" //yellow
//        },
//        {
//          title: 'Meeting',
//          start: new Date(y, m, d, 10, 30),
//          allDay: false,
//          backgroundColor: "#0073b7", //Blue
//          borderColor: "#0073b7" //Blue
//        },
//        {
//          title: 'Lunch',
//          start: new Date(y, m, d, 12, 0),
//          end: new Date(y, m, d, 14, 0),
//          allDay: false,
//          backgroundColor: "#00c0ef", //Info (aqua)
//          borderColor: "#00c0ef" //Info (aqua)
//        },
//        {
//          title: 'Birthday Party',
//          start: new Date(y, m, d + 1, 19, 0),
//          end: new Date(y, m, d + 1, 22, 30),
//          allDay: false,
//          backgroundColor: "#00a65a", //Success (green)
//          borderColor: "#00a65a" //Success (green)
//        },
//        {
//          title: 'Click for Google',
//          start: new Date(y, m, 28),
//          end: new Date(y, m, 29),
//          url: 'http://google.com/',
//          backgroundColor: "#3c8dbc", //Primary (light-blue)
//          borderColor: "#3c8dbc" //Primary (light-blue)
//        }
            //],
            editable: true,
            eventResize: function (event) {
                var booking_date = $.fullCalendar.moment(event.start).format();

                var _date = booking_date.substring(0, booking_date.indexOf("T"));
                var time_start = booking_date.substring(booking_date.indexOf("T") + 1, booking_date.length);

                var title = event.title;
                var id = title.substring(1, title.indexOf(":"));

                var defaultDuration = moment.duration($('#calendar').fullCalendar('option', 'defaultTimedEventDuration')); // get the default and convert it to proper type
                var end = event.end || event.start.clone().add(defaultDuration); // If there is no end, compute it
                var time_end = end.format().substring(end.format().indexOf("T") + 1, booking_date.length);

                $.post("/bookings/updateDate", {
                    id: id,
                    booking_date: _date,
                    booking_time: time_start,
                    booking_time_end: time_end
                }).done(function (data) {
                    console.log("Update date ok!");
                });
            },
            eventDrop: function (event) {
                var booking_date = $.fullCalendar.moment(event.start).format();
                console.log(booking_date.indexOf("T"));
                var _date;
                var time_start;
                if (booking_date.indexOf("T") > -1) {
                    _date = booking_date.substring(0, booking_date.indexOf("T"));
                } else {
                    _date = booking_date;
                }
                if (booking_date.indexOf("T") != -1) {
                    time_start = booking_date.substring(booking_date.indexOf("T") + 1, booking_date.length);
                    var defaultDuration = moment.duration($('#calendar').fullCalendar('option', 'defaultTimedEventDuration')); // get the default and convert it to proper type
                    var end = event.end || event.start.clone().add(defaultDuration); // If there is no end, compute it
                    var time_end = end.format().substring(end.format().indexOf("T") + 1, booking_date.length);
                } else {
                    time_start = '00:00:00';
                    time_end = '02:00:00';
                }
                console.log(time_start);
                console.log(time_end);
                var title = event.title;
                var id = title.substring(1, title.indexOf(":"));

                $.post("/bookings/updateDate", {
                    id: id,
                    booking_date: _date,
                    booking_time: time_start,
                    booking_time_end: time_end
                }).done(function (data) {
                    console.log("Update date ok!");
                });
            },
            droppable: true, // this allows things to be dropped onto the calendar !!!
            drop: function (date, allDay) { // this function is called when something is dropped

                // retrieve the dropped element's stored Event Object
                var originalEventObject = $(this).data('eventObject');

                // we need to copy it, so that multiple events don't have a reference to the same object
                var copiedEventObject = $.extend({}, originalEventObject);

                // assign it the date that was reported
                copiedEventObject.start = date;
                copiedEventObject.allDay = allDay;
                copiedEventObject.backgroundColor = $(this).css("background-color");
                copiedEventObject.borderColor = $(this).css("border-color");

                var title = copiedEventObject.title;
                var id = title.substring(1, title.indexOf(":"));

                $.post("/bookings/updateDate", {
                    id: id,
                    booking_date: copiedEventObject.start.format()
                }).done(function (data) {
                    console.log("Update date ok!");
                });
                copiedEventObject.id = id;
                // render the event on the calendar
                // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
                $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);

                // is the "remove after drop" checkbox checked?
                if ($('#drop-remove').is(':checked')) {
                    // if so, remove the element from the "Draggable Events" list
                    $(this).remove();
                }
                $("#new-event").val('');
                $("#txt_street").val('');
                $("#txt_city").val('');
                $("#txt_province").val('');
                $("#txt_client_name").val('');
                $("#txt_representative").select2('val', '');
                $("#txt_contact_no").val('');
                $("#txt_remarks").val('');

            }, eventClick: function (calEvent, jsEvent, view) {

                var id = calEvent.title.substring(1, calEvent.title.indexOf(":"));
                $("#eventId").val(calEvent._id);
                $.post("/bookings/getBookingDetails", {
                    bookingId: id
                }).done(function (result) {
                    console.log(result);
                    var data = result[0];
                    $("#bookingId").val(id);
                    $("#booking_event_name").html(data.event_name);
                    $("#booking_client_name").html(data.client);
                    var representative_names = '';
                    for (ctr = 0; ctr < result.length; ctr++) {
                        if (representative_names == "") {
                            representative_names += result[ctr].first_name + " " + result[ctr].first_name;
                        } else {
                            representative_names += "| " + result[ctr].first_name + " " + result[ctr].first_name;
                        }

                    }
                    $("#booking_representative_name").html(representative_names == "" ? 'N/A' : representative_names);
                    $("#booking_address").html(data.street + ' ' + data.city + ' ' + data.province);
                    $('#view-booking').modal({
                        show: 'true'
                    });
                });

            }
        });

        /* ADDING EVENTS */
        var currColor = "#3c8dbc"; //Red by default
        //Color chooser button
        var colorChooser = $("#color-chooser-btn");
        $("#color-chooser > li > a").click(function (e) {
            e.preventDefault();
            //Save color
            currColor = $(this).css("color");
            //Add color effect to button
            $('#add-new-event').css({"background-color": currColor, "border-color": currColor});
        });
        $("#add-new-event").click(function (e) {
            e.preventDefault();
            //Get value and make sure it is not null
            var val = $("#new-event").val();
            if (val.length == 0) {
                return;
            }

            $.get("/bookings/create", {
                event_name: $("#new-event").val(),
                street: $("#txt_street").val(),
                city: $("#txt_city").val(),
                province: $("#txt_province").val(),
                client: $("#txt_client_name").val(),
                representative: $("#txt_representative").val(),
                color: currColor,
                contact: $("#txt_contact_no").val(),
                remarks: $("#txt_remarks").val()
            }).done(function (data) {
                //console.log(data);
                val = data;
                //Create events
                var event = $("<div />");
                event.css({
                    "background-color": currColor,
                    "border-color": currColor,
                    "color": "#fff"
                }).addClass("external-event");
                event.html(val);
                $('#external-events').prepend(event);
                //Add draggable funtionality
                ini_events(event);

                //Remove event from text input
                $("#new-event").val("");
                $("#new-event").val('');
                $("#txt_street").val('');
                $("#txt_city").val('');
                $("#txt_province").val('');
                $("#txt_client_name").val('');
                $("#txt_representative").select2('val', '');
                $("#txt_contact_no").val('');
                $("#txt_remarks").val('');
            });

        });

        $("#btn_cancel_update").click(function () {
            var bookingId = $("#bookingId").val();
            var eventId = $("#eventId").val();
            $('#calendar').fullCalendar('removeEvents', eventId);
            $.post("/bookings/cancelBooking", {
                bookingId: bookingId
            }).done(function (result) {
                $('#view-booking').modal('hide');
            });
        });
        $('#txt_representative').select2({
            ajax: {
                url: "/bookings/searchRepresentative",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term // search term
                    };
                },
                processResults: function (data) {
                    // parse the results into the format expected by Select2.
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data
                    return {
                        results: data
                    };
                },
                cache: true
            },
            minimumInputLength: 2,
            multiple: true,
            maximumSelectionLength: 3
        });

    });
</script>


@endsection