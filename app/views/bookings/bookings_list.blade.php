@extends('admin_template')

@section('additional_header')

        <!-- DataTables -->
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.min.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker-bs3.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/fullcalendar/fullcalendar.min.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/fullcalendar/fullcalendar.print.css")}}'
      media="print">

@endsection

@section('content')
        <!-- Main content -->
<section class="content">

    <div class="row">
        <div class="col-md-12 col-xs-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-search"></i> Search Bookings</h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <form role="form">
                            <div class="col-md-2 col-sm-2">
                                <button id="btn_search" type="button" class="btn btn-primary" data-toggle="modal"
                                        data-target="#search-purchase"><i class="ion ion-search"></i> Search Booking
                                    Records
                                </button>
                            </div>
                            <div class="col-md-2 col-sm-2 col-md-offset-5 col-sm-offset-5">
                                @if(isAccessModuleAllowed('bookings_add'))
                                    <button type="button" class="btn btn-success pull-right" data-toggle="modal"
                                            data-target="#new_event">Add New Booking
                                    </button>
                                @endif
                            </div>
                            <div class="col-md-3">
                                <div class="input-group">
                                    @if(isAccessModuleAllowed('bookings_edit'))
                                        <select class="form-control" id='slct_booking_status'>
                                            <option>--Select Action--</option>
                                            <option>Cooked</option>
                                            <option>Cancelled</option>
                                        </select>

                                        <div class="input-group-btn">
                                            <button type="button" class="btn btn-info" id="btn_update_booking_status">
                                                Go
                                            </button>
                                        </div>
                                        @endif
                                                <!-- /btn-group -->
                                </div>
                            </div>

                        </form>
                    </div>
                    <!-- /row -->
                </div>
            </div>
            <!-- /.box -->
            <!--/.col (left) -->
        </div>
        <!-- search form -->
    </div>

    <div class="row">
        <div class=" col-md-12 col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-calendar"></i> Booking Schedule</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>&nbsp;</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Sponsor</th>
                            <th>Associate</th>
                            <th>Presenter</th>
                            <th>Fast Track</th>
                            <th>Remarks</th>
                            <th>Status</th>
                            <th>PO No</th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach($bookings as $booking)

                            <tr>
                                <td><input type="checkbox" name="booking_no" value="{{$booking->id}}"></td>
                                <td>{{$booking->booking_date}}</td>
                                <td>{{$booking->booking_time}}</td>
                                <?php
                                $sponsor_found = false;
                                $associate_found = false;
                                $presenter_found = false;
                                ?>
                                @foreach($agents as $agent)
                                    @if($agent->id == $booking->id && $agent->type == 'consultant')
                                        <td>{{$agent->first_name}} {{$agent->last_name}}</td>
                                        <?php $sponsor_found = true; ?>
                                    @endif
                                @endforeach
                                @foreach($agents as $agent)
                                    @if($agent->id == $booking->id && $agent->type == 'associate')
                                        <td>{{$agent->first_name}} {{$agent->last_name}}</td>
                                        <?php $associate_found = true;?>
                                    @endif
                                @endforeach
                                @foreach($agents as $agent)
                                    @if($agent->id == $booking->id && $agent->type == 'presenter')
                                        <td>{{$agent->first_name}} {{$agent->last_name}}</td>
                                        <?php $presenter_found = true;?>
                                    @endif
                                @endforeach

                                @if($sponsor_found == false)
                                    <td>N/A</td>
                                @endif
                                @if($associate_found == false)
                                    <td>N/A</td>
                                @endif
                                @if($presenter_found == false)
                                    <td>N/A</td>
                                @endif

                                <td>{{ucwords($booking->fast_track)}}</td>
                                <td>{{$booking->remarks}}</td>
                                <td><span id="status_{{$booking->id}}">{{$booking->status}}</span></td>
                                <td>{{$booking->po_no}}</td>

                            </tr>
                        @endforeach

                        </tbody>
                        <tfoot>
                        <tr>
                            <th>&nbsp;</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Agent</th>
                            <th>Location</th>

                            <th>Fast Track</th>
                            <th>Telethon</th>
                            <th>Remarks</th>
                            <th>Status</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->

            </div>
        </div>
    </div>
    <!-- /.tab-pane -->

    <div id="new_event" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="box box-solid " id="">
                <div class="box-header with-border">
                    <h3 class="box-title">Create Booking Event</h3>
                </div>
                <div class="box-body">
                    <div class="col-md-12">
                        <div class="btn-group" style="width: 100%; margin-bottom: 10px;">
                            <!--<button type="button" id="color-chooser-btn" class="btn btn-info btn-block dropdown-toggle" data-toggle="dropdown">Color <span class="caret"></span></button>-->
                            <ul class="fc-color-picker" id="color-chooser">
                                <li><a class="text-aqua" href="#"><i class="fa fa-square"></i></a></li>
                                <li><a class="text-blue" href="#"><i class="fa fa-square"></i></a></li>
                                <li><a class="text-light-blue" href="#"><i class="fa fa-square"></i></a>
                                </li>
                                <li><a class="text-teal" href="#"><i class="fa fa-square"></i></a></li>
                                <li><a class="text-yellow" href="#"><i class="fa fa-square"></i></a>
                                </li>
                                <li><a class="text-orange" href="#"><i class="fa fa-square"></i></a>
                                </li>
                                <li><a class="text-green" href="#"><i class="fa fa-square"></i></a></li>
                                <li><a class="text-lime" href="#"><i class="fa fa-square"></i></a></li>
                                <li><a class="text-red" href="#"><i class="fa fa-square"></i></a></li>
                                <li><a class="text-purple" href="#"><i class="fa fa-square"></i></a>
                                </li>
                                <li><a class="text-fuchsia" href="#"><i class="fa fa-square"></i></a>
                                </li>
                                <li><a class="text-muted" href="#"><i class="fa fa-square"></i></a></li>
                                <li><a class="text-navy" href="#"><i class="fa fa-square"></i></a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="txt_event_name">Event Name</label>
                            {{--<input type="text" class="form-control" id="new-event" placeholder="Enter Event Name">--}}
                            <select id="new-event" class="form-control">
                                <option value="Basic">Basic</option>
                                <option value="Tutorial">Tutorial</option>
                                <option value="Servicing">Servicing</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="txt_event_name">Time</label>
                            {{--<input type="text" class="form-control" id="new-event" placeholder="Enter Event Name">--}}
                            <select id="slct_time" class="form-control">
                                <option value="Breakfast">Breakfast</option>
                                <option value="Early Lunch">Early Lunch</option>
                                <option value="Late Lunch">Late Lunch</option>
                                <option value="Early Dinner">Early Dinner</option>
                                <option value="Late Dinner">Late Dinner</option>
                            </select>
                        </div>
                    </div>


                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="txt_client_name">Date</label>
                            <input type="text" class="form-control" id="txt_date"
                                   placeholder="Select Date">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="txt_client_name">Time</label>
                            <input type="time" class="form-control" id="txt_time"
                                   placeholder="Select Date">
                        </div>
                    </div>


                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="txt_client_name">Host/Prospect</label>
                            <input type="text" class="form-control" id="txt_client_name"
                                   placeholder="Enter Client Name">
                        </div>
                    </div>

                    <div class="col-md-6">

                        <div class="form-group">
                            <label for="txt_client_name">Contact Number</label>
                            <input type="text" class="form-control" id="txt_contact_no"
                                   placeholder="Enter Contact Number">
                        </div>
                    </div>


                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="txt_representative">Associate</label>
                            {{--<input type="text" class="form-control" id="txt_representative" placeholder="Enter Representative Name">--}}
                            <select id="txt_booker" class="form-control" style="width:100%">
                            </select>
                        </div>

                    </div>


                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="txt_representative">Consultant</label>
                            {{--<input type="text" class="form-control" id="txt_representative" placeholder="Enter Representative Name">--}}
                            <select id="txt_consultant" class="form-control" style="width:100%">
                            </select>
                        </div>
                    </div>


                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="txt_representative">Presenter</label>
                            {{--<input type="text" class="form-control" id="txt_representative" placeholder="Enter Representative Name">--}}
                            <select id="txt_representative" class="form-control" style="width:100%">
                            </select>
                        </div>
                    </div>


                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="txt_representative">Assistant</label>
                            {{--<input type="text" class="form-control" id="txt_representative" placeholder="Enter Representative Name">--}}
                            <select id="txt_assistant" class="form-control" style="width:100%">
                            </select>
                        </div>
                    </div>


                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="txt_street">Street</label>
                            <input type="text" class="form-control" id="txt_street"
                                   placeholder="Enter Street">
                        </div>
                    </div>


                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="txt_city">City</label>
                            <input type="text" class="form-control" id="txt_city"
                                   placeholder="Enter City">
                        </div>
                    </div>


                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="txt_province">Province</label>
                            <input type="text" class="form-control" id="txt_province"
                                   placeholder="Enter Province">
                        </div>
                    </div>


                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="txt_province">Status</label>
                            <select id="slct_status" class="form-control">
                                <option value="Booked">Booked</option>
                                <option value="Cancelled">Cancelled</option>
                                <option value="Cooked">Cooked</option>
                                <option value="Rescheduled">Rescheduled</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="txt_client_name">Remarks</label>
                            <input type="text" class="form-control" id="txt_remarks"
                                   placeholder="Enter Remarks">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="checkbox">
                                <label><input type="checkbox" value="" id="is_fast_track">Fast Track</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="checkbox">
                                <label><input type="checkbox" value="" id="is_telethon">Telethon</label>
                            </div>
                        </div>

                    </div>


                </div>
                <div class="box-footer">
                    <button id="add-new-event" type="button" class="btn btn-primary btn-flat"
                            data-dismiss="modal">Add
                        Event
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div id="cancel-booking-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-blue-gradient">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><i class="ion ion-check"></i> Cancel Booking</h4>
                </div>
                <div class="modal-body">
                    Do you want to mark these Booking records as Cancelled?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn_agree_mark" id="btn_cancel_bookings"
                            data-dismiss="modal">Yes
                    </button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close
                    </button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div id="cooked-booking-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-blue-gradient">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><i class="ion ion-check"></i> Cooked Booking</h4>
                </div>
                <div class="modal-body">
                    Do you want to mark these Booking records as Cooked?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn_agree_mark" id="btn_cooked_booking"
                            data-dismiss="modal">Yes
                    </button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close
                    </button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div id="updated-ok-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-blue-gradient">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><i class="ion ion-check"></i> Booking Status Update</h4>
                </div>
                <div class="modal-body">
                    Record/s has been updated.
                </div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-default btn_close_modal" data-dismiss="modal">Close
                    </button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

</section>

@endsection

@section('additional_footer')

    <script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/jquery.dataTables.min.js")}}'></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js")}}'></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.full.min.js")}}'></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker.js")}}'></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/fullcalendar/fullcalendar.min.js")}}'></script>

    <script>
        $(document).ready(function () {
            $('#example1').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true
            });

            $("#btn_search").click(function () {
                $(".select2").select2({
                    theme: "classic"
                });
                $('#txt_daterange').daterangepicker();

            });

            $(".hide-filter").each(function (index) {
                $(this).parent().parent().hide();
                $(this).on("click", function () {
                    $(this).parent().parent().hide();
                    $("." + $(this).parent().parent().attr('id')).show();
                });
            });

            $(".dropdown-menu li").each(function (index) {
                $(this).on("click", function () {
                    $("#" + $(this).children("input").val()).show();
                    $(this).hide();
                });
            });

            /* ADDING EVENTS */
            var currColor = "#3c8dbc"; //Red by default
            //Color chooser button
            var colorChooser = $("#color-chooser-btn");
            $("#color-chooser > li > a").click(function (e) {
                e.preventDefault();
                //Save color
                currColor = $(this).css("color");
                //Add color effect to button
                $('#add-new-event').css({"background-color": currColor, "border-color": currColor});
            });

            $("#add-new-event").click(function (e) {
                e.preventDefault();
                //Get value and make sure it is not null
                var val = $("#new-event").val();
                if (val.length == 0) {
                    return;
                }

                $.get("/bookings/create", {
                    event_name: $("#new-event").val(),
                    street: $("#txt_street").val(),
                    city: $("#txt_city").val(),
                    province: $("#txt_province").val(),
                    client: $("#txt_client_name").val(),
                    representative: $("#txt_representative").val(),
                    assistant: $("#txt_assistant").val(),
                    booker: $("#txt_booker").val(),
                    consultant: $("#txt_consultant").val(),
                    color: currColor,
                    contact: $("#txt_contact_no").val(),
                    remarks: $("#txt_remarks").val(),
                    time: $("#slct_time").val(),
                    status: $("#slct_status").val(),
                    fast_track: $("#is_fast_track").is(":checked"),// $('#' + id).is(":checked")
                    telethon: $("#is_telethon").is(":checked"),
                    booking_date: $("#txt_date").val(),
                    booking_time: $("#txt_time").val(),
                }).done(function (data) {
                    console.log(data);
                    location.reload();
                });

            });

            $('#txt_representative').select2({
                ajax: {
                    url: "/bookings/searchRepresentative",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term // search term
                        };
                    },
                    processResults: function (data) {
                        // parse the results into the format expected by Select2.
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data
                        return {
                            results: data
                        };
                    },
                    cache: true
                },
                minimumInputLength: 2
            });
            $('#txt_consultant').select2({
                ajax: {
                    url: "/bookings/searchRepresentative",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term // search term
                        };
                    },
                    processResults: function (data) {
                        // parse the results into the format expected by Select2.
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data
                        return {
                            results: data
                        };
                    },
                    cache: true
                },
                minimumInputLength: 2
            });
            $('#txt_assistant').select2({
                ajax: {
                    url: "/bookings/searchRepresentative",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term // search term
                        };
                    },
                    processResults: function (data) {
                        // parse the results into the format expected by Select2.
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data
                        return {
                            results: data
                        };
                    },
                    cache: true
                },
                minimumInputLength: 2
            });
            $('#txt_booker').select2({
                ajax: {
                    url: "/bookings/searchRepresentative",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term // search term
                        };
                    },
                    processResults: function (data) {
                        // parse the results into the format expected by Select2.
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data
                        return {
                            results: data
                        };
                    },
                    cache: true
                },
                minimumInputLength: 2
            });

            $('#txt_date').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                locale: {
                    format: 'YYYY-MM-DD'
                }

            });

            $('#txt_date').on('apply.daterangepicker', function (ev, picker) {
                picker.startDate.format('YYYY-MM-DD');
                $("#txt_date").val(picker.startDate.format('YYYY-MM-DD'));
                console.log(picker.startDate.format('YYYY-MM-DD'));
                console.log(picker.endDate.format('YYYY-MM-DD'));
            });

            $("#btn_cancel_bookings").click(function () {
                var items = $("input[type=checkbox][name=booking_no]:checked");
                var for_removal = [];
                $("input[type=checkbox][name=booking_no]:checked").each(function () {
                    for_removal.push(this.value);
                });
                console.log(for_removal);
                $.post("/bookings/cancelBulkBooking", {
                    update_items: for_removal,
                    status: $("#slct_booking_status").val()
                }).done(function (data) {
                    console.log(data);
                    $("#updated-ok-modal").modal('show');
                    $("input[type=checkbox][name=booking_no]:checked").each(function () {
                        $(this).parent().parent().addClass('hidden');
                    });
                });
            });

            $("#btn_cooked_booking").click(function () {
                var items = $("input[type=checkbox][name=booking_no]:checked");
                var for_removal = [];
                $("input[type=checkbox][name=booking_no]:checked").each(function () {
                    for_removal.push(this.value);
                });
                console.log(for_removal);
                $.post("/bookings/cookedBulkBooking", {
                    update_items: for_removal,
                    status: $("#slct_booking_status").val()
                }).done(function (data) {
                    console.log(data);
                    $("#updated-ok-modal").modal('show');

                    $("input[type=checkbox][name=booking_no]:checked").each(function () {

                        $("#status_"+this.value).html('Cooked');
                    });
                });
            });

            $("#btn_update_booking_status").click(function () {

                if ($("#slct_booking_status").val() == 'Cancelled') {
                    $("#cancel-booking-modal").modal('show');
                } else if ($("#slct_booking_status").val() == 'Cooked') {
                    $("#cooked-booking-modal").modal('show');
                } else {
                    $("#active-po-modal").modal('show');
                }

            });

        });


    </script>
@endsection