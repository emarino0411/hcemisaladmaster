@extends('admin_template')

@section('additional_header')

        <!-- DataTables -->
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.min.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker-bs3.css")}}'>

@endsection

@section('content')
        <!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-3">

            <!-- Profile Image -->
            <div class="box box-primary">
                <div class="box-body box-profile">
                    <img class="profile-user-img img-responsive img-circle"
                         src="{{ asset("/bower_components/AdminLTE/dist/img/user2-160x160.jpg") }}"
                         alt="User profile picture">

                    <h3 class="profile-username text-center">{{ucwords($agent->first_name)}} {{ucwords($agent->last_name)}}</h3>

                    <p class="text-muted text-center">Agent</p>
                    <hr>

                    <p class="text-muted text-center"><b>Agent Code</b><br>{{$agent->representative_code}}</p>
                    <hr>
                    @if($agent->city != null)
                        <p class="text-muted text-center"><b>Address</b><br>{{$agent->street}} {{$agent->brgy}}
                            , {{$agent->city[0]->cityname}}
                            , {{$agent->province[0]->provname}}</p>
                        <hr>
                    @else
                        <p class="text-muted text-center"><b>Address</b><br>N/A
                        <hr>
                    @endif
                    @if($agent->phone1 == "" && $agent->phone2=="")
                        <p class="text-muted text-center"><b>Contact Information</b><br>{{$agent->phone1}}
                            /{{$agent->phone2}}</p>
                        <hr>
                    @else
                        <p class="text-muted text-center"><b>Contact Information</b><br>N/A</p>
                        <hr>
                    @endif
                    <p class="text-muted text-center"><b>User Status</b><br>{{$agent->status}}</p>

                    @if(isAccessModuleAllowed('multilevel_update'))
                    <hr>
                    <center>
                        <button type="button" class="btn btn-success btn-sm"
                                data-toggle="modal" data-target="#add-promotion">Add Promotion
                        </button>
                       {{-- @if($agent->current_level=='associate')

                            <button type="button" class="btn btn-warning btn-sm"
                                    data-toggle="modal" data-target="#add-phases">Associate Phase
                            </button>

                        @endif
--}}
                    </center>
                    @endif


                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

        </div>
        <!-- /.col -->
        <div class="col-md-9">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#promotion_history" data-toggle="tab">Promotion History</a></li>
                    <li><a href="#associate_phase" data-toggle="tab">Associate Phase</a></li>
                </ul>
                <div class="tab-content">
                    <div class="active tab-pane" id="promotion_history">
                        <div class="">
                            <div class="box-header">
                                <h3 class="box-title"><i class="fa fa-arrow-up"></i> Promotion History</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="row">

                                    <div class="col-md-6">
                                        <label>Current Level: {{ucwords($agent->current_level)}}</label>
                                    </div>

                                    <div class="col-md-12">

                                        <table id="multilevels" class="table table-bordered table-striped">
                                            <thead>
                                            <tr>
                                                <th>Type</th>
                                                <th>Level From</th>
                                                <th>Level To</th>
                                                <th>Effectivity Date</th>
                                                <th>Approved By</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($multilevels as $multilevel)
                                                <tr>
                                                    <td>
                                                        @if($multilevel->type!="" && $multilevel->type!=null)
                                                            {{ucwords($multilevel->level_from)}} (Initial)
                                                        @else 
                                                            Promotion 
                                                        @endif
                                                    </td>
                                                    <td>
                                                    @if($multilevel->type=="" || $multilevel->type==null)
                                                        {{ucwords($multilevel->level_from)}} 
                                                    @else
                                                    -- 
                                                    @endif
                                                    </td>
                                                    <td>
                                                        {{ucwords($multilevel->level_to)}} 
                                                    </td>
                                                    <td>
                                                        {{ucwords($multilevel->effective_date)}}
                                                    </td>
                                                    <td>
                                                    {{$multilevel->approved_by}}
                                                    </td>
                                                </tr>
                                            @endforeach

                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <th>Type</th>
                                                <th>Level From</th>
                                                <th>Level To</th>
                                                <th>Effectivity Date</th>
                                                <th>Approved By</th>
                                            </tr>
                                            </tfoot>
                                        </table>

                                    </div><!-- col-md-12 -->

                                </div><!-- /row -->

                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                    <div class="tab-pane" id="associate_phase">
                        <div class="">
                            <div class="box-header">
                                <h3 class="box-title"><i class="fa fa-arrow-up"></i> Associate Phase</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="row">

                                    <div class="col-md-6">
                                        <label>Current Level: {{ucwords($agent->current_level)}}</label>
                                    </div>

                                    <div class="col-md-12">

                                        <table class="table table-bordered table-striped">
                                            <tr>
                                                <th colspan="4" align="center">Associate Information</th>
                                            </tr>
                                            <tr>
                                                <th>Name of Associate</th>
                                                <th>Date of First Presentation</th>
                                                <th>Sponsor</th>
                                                <th>Group</th>
                                            </tr>
                                            <tr>
                                                <td>{{ucwords($agent->first_name)}} {{ucwords($agent->last_name)}}</td>
                                                <td>{{ucwords($agent->first_presentation)}}</td>
                                                <td>{{ucwords($agent->sponsor["sponsor_first"])}} {{ucwords($agent->sponsor["sponsor_last"])}}</td>
                                                <td>Sunga, Maria Elfleda</td>
                                            </tr>
                                        </table>

                                        <table class="table table-bordered table-striped">
                                            <tr>
                                                <th colspan="6" align="center">Associate Activities</th>
                                            </tr>
                                            <tr>
                                                <th>Booking No</th>
                                                <th>Presentation Date</th>
                                                <th>PO NO</th>
                                                <th>Item Sold</th>
                                                <th>Remarks</th>
                                                <th>Prod Points</th>
                                            </tr>
                                            @foreach($associate_bookings as $associate_booking)

                                                <tr>
                                                    <td>{{date('Y') . '-'.ucwords($associate_booking->booking_id)}}</td>
                                                    <td>{{ucwords($associate_booking->booking_date)}}</td>
                                                    <td>{{ucwords($associate_booking->purchase_order_no)}}</td>
                                                    <td>
                                                    @foreach($sold_items as $sold_item)
                                                        @if($sold_item->purchase_order_no == $associate_booking->purchase_order_no)
                                                            {{$sold_item->description}}
                                                        @endif
                                                    @endforeach
                                                    </td>
                                                    <td> @foreach($sold_items as $sold_item)
                                                            @if($sold_item->purchase_order_no == $associate_booking->purchase_order_no)
                                                                {{$sold_item->remarks}}
                                                            @endif
                                                        @endforeach</td>
                                                    <td>{{ucwords($associate_booking->purchase_order_no)}}</td>

                                                </tr>
                                            @endforeach

                                        </table>

                                        <table class="table table-bordered table-striped">
                                            <tr>
                                                <th colspan="2" align="center">Summary</th>
                                            </tr>
                                            <tr>
                                                <td colspan="2" align="left">Phase I</td>
                                            </tr>
                                            <tr>
                                                <th width="30%">Start Date</th><td width="70%"> {{$agent->phase1_start}} </td>
                                            </tr>
                                            <tr>
                                                <th>No of Presentations</th><td> XX </td>
                                            </tr>
                                            <tr>
                                                <th>Total Production Points</th><td> XX </td>
                                            </tr>
                                            <tr>
                                                <th>Phase I Fast Track?</th><td> XX </td>
                                            </tr>
                                            <tr>
                                                <th>Completed Phase I?</th><td> XX </td>
                                            </tr>


                                            <tr>
                                                <td colspan="2" align="left">Phase II</td>
                                            </tr>
                                            <tr>
                                                <th>Start Date</th><td> {{$agent->phase2_start}} </td>
                                            </tr>
                                            <tr>
                                                <th>No of Presentations</th><td> XX </td>
                                            </tr>
                                            <tr>
                                                <th>Total Production Points</th><td> XX </td>
                                            </tr>
                                            <tr>
                                                <th>Phase I Fast Track?</th><td> XX </td>
                                            </tr>
                                            <tr>
                                                <th>Completed Phase I?</th><td> XX </td>
                                            </tr>

                                        </table>
                                    </div><!-- col-md-12 -->
                                </div><!-- /row -->

                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                </div>
                <!-- /.tab-content -->
            </div>
            <!-- /.nav-tabs-custom -->

        </div>
        <!-- /.col -->
    </div>
    <input type="hidden" id="id" value="{{$id}}">


    <div id="add-promotion" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg">
            <div class="box box-solid " id="">
                <div class="box-header with-border">
                    <h3 class="box-title">Add New Promotion</h3>
                </div>
                <div class="box-body">

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="txt_event_name">Level From</label>
                            {{--<input type="text" class="form-control" id="new-event" placeholder="Enter Event Name">--}}
                            <select id="slct_level_from" class="form-control">
                                <option value="Associate" @if($agent->current_level=='associate') selected @endif>
                                    Associate
                                </option>
                                <option value="Consultant" @if($agent->current_level=='consultant') selected @endif>
                                    Consultant
                                </option>
                                <option value="Senior Consultant"
                                        @if($agent->current_level=='senior consultant') selected @endif>Senior
                                    Consultant
                                </option>
                                <option value="Manager" @if($agent->current_level=='manager') selected @endif>Manager
                                </option>
                                <option value="Distributor" @if($agent->current_level=='distributor') selected @endif>
                                    Distributor
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="txt_event_name">Level To</label>
                            {{--<input type="text" class="form-control" id="new-event" placeholder="Enter Event Name">--}}
                            <select id="slct_level_to" class="form-control">
                                <option value="Associate" @if($agent->current_level=='') selected @endif>Associate
                                </option>
                                <option value="Consultant" @if($agent->current_level=='associate') selected @endif>
                                    Consultant
                                </option>
                                <option value="Senior Consultant"
                                        @if($agent->current_level=='consultant') selected @endif>Senior Consultant
                                </option>
                                <option value="Manager" @if($agent->current_level=='senior consultant') selected @endif>
                                    Manager
                                </option>
                                <option value="Distributor" @if($agent->current_level=='manager') selected @endif>
                                    Distributor
                                </option>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="txt_effectivity_date">Effectivity Date</label>
                            <input type="text" class="form-control" id="txt_effectivity_date" placeholder="Select Date">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="txt_client_name">Approved By</label>
                            <input type="text" class="form-control" id="txt_approved_by"
                                   value="{{Auth::user()->email}}" placeholder="Approved By">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="txt_client_name">Sales Duration</label>
                            <input type="text" class="form-control" id="txt_sales_duration"
                                   placeholder="Sales Duration">
                            <input type="hidden" class="form-control" id="txt_sales_duration_month_count"
                                   placeholder="Sales Duration">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="txt_client_name">Sales Date</label>
                            <input type="text" class="form-control" id="txt_sales_dates"
                                   placeholder="Sales Dates">
                        </div>
                    </div>
                    <hr>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="txt_client_name">Required Points</label>
                            <input type="text" class="form-control" id="txt_required_points"
                                   placeholder="Required Points">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="txt_client_name">Required Set</label>
                            <input type="text" class="form-control" id="txt_required_set"
                                   placeholder="Required Set">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="txt_client_name">Required Personal Sales</label>
                            <input type="text" class="form-control" id="txt_required_personal_sales"
                                   placeholder="Required Personal Sales">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="txt_client_name">Group Level</label>
                            <input type="text" class="form-control" id="txt_group_level"
                                   placeholder="Group Level">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="txt_client_name">Personal Sales Count</label>
                            <input type="text" class="form-control" id="txt_personal_sales"
                                   placeholder="Personal Sales Count">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="txt_client_name">Group Sales Count</label>
                            <input type="text" class="form-control" id="txt_group_sales"
                                   placeholder="Personal Sales Count">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="txt_client_name">Total Sales Count</label>
                            <input type="text" class="form-control" id="txt_total_sales"
                                   placeholder="Total Sales">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="txt_client_name">Promotion Status</label>
                            <input type="text" class="form-control" id="txt_promotion_status"
                                   placeholder="Approved / Rejected ">
                        </div>
                    </div>


                </div>
                <div class="box-footer">
                    <div class="pull-right">
                        <button id="add-new-promotion" type="button" class="btn btn-primary btn-flat"
                                data-dismiss="modal">Save Promotion
                        </button>
                        <button class="btn btn-warning" data-dismiss="modal">Close
                        </button>
                    </div>

                </div>
            </div>
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div id="update_phases" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg">
            <div class="box box-solid " id="">
                <div class="box-header with-border">
                    <h3 class="box-title">Update Associate Phase</h3>
                </div>
                <div class="box-body">

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="txt_event_name">Associate Phase</label>
                            <input type="text" class="form-control" id="txt_associate_phase">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="txt_client_name">Phase Duration</label>
                            <input type="text" class="form-control" id="txt_phase_duration"
                                   placeholder="Sales Duration">
                        </div>
                    </div>
                    <hr>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="txt_client_name">Required Points</label>
                            <input type="text" class="form-control" id="txt_phase_required_points"
                                   placeholder="Required Points">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="txt_client_name">Total Points</label>
                            <input type="text" class="form-control" id="txt_total_phase_points"
                                   placeholder="Total Sales">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="txt_client_name">Promotion Status</label>
                            <input type="text" class="form-control" id="txt_phase_status"
                                   placeholder="Approved / Disapproved ">
                        </div>
                    </div>

                </div>
                <div class="box-footer">
                    <div class="pull-right">
                        <button id="approve-new-promotion" type="button" class="btn btn-primary btn-flat"
                                data-dismiss="modal">Approve Phase
                        </button>
                        <button id="disapprove-new-promotion" type="button" class="btn btn-danger btn-flat"
                                data-dismiss="modal">Disapprove Phase
                        </button>
                        <button class="btn btn-warning" data-dismiss="modal">Close
                        </button>
                    </div>

                </div>
            </div>
        </div>
        <!-- /.modal-dialog -->
    </div>


    <div id="add-phases" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg">
            <div class="box box-solid " id="">
                <div class="box-header with-border">
                    <h3 class="box-title">Associate Phase I/II</h3>
                </div>
                <div class="box-body">

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="txt_event_name">Select Phase</label>
                            {{--<input type="text" class="form-control" id="new-event" placeholder="Enter Event Name">--}}
                            <select id="slct_phase" class="form-control">
                                <option value="Phase I">Phase I</option>
                                <option value="Phase II">Phase II</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="txt_client_name">Added By</label>
                            <input type="text" class="form-control" id="txt_added_by"
                                   placeholder="Added By">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="txt_client_name">Phase Start</label>
                            <input type="text" class="form-control" id="txt_phase_start"
                                   placeholder="Select Date">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="txt_client_name">Phase End</label>
                            <input type="text" class="form-control" id="txt_phase_end"
                                   placeholder="Select Date">
                        </div>
                    </div>


                </div>
                <div class="box-footer">
                    <button id="add-new-phase" type="button" class="btn btn-primary btn-flat"
                            data-dismiss="modal">Add Associate Phase
                    </button>
                </div>
            </div>
        </div>
        <!-- /.modal-dialog -->
    </div>


    {{--MODAL--}}
    <div id="promotion_existing_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-red-gradient">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><i class="fa fa-error"></i> Promotion Existing</h4>
                </div>
                <div class="modal-body">
                    [ERROR] Promotion already existing.
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" id="btn_close_modal" data-dismiss="modal">Close
                    </button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--MOdal End--}}


    {{--MODAL--}}
    <div id="promotion_invalid_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-red-gradient">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><i class="fa fa-error"></i> Promotion Existing</h4>
                </div>
                <div class="modal-body">
                    [ERROR] Invalid promotion. Higher rank promotion record existing. Please promote according to level.
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" id="btn_close_modal" data-dismiss="modal">Close
                    </button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--MOdal End--}}

<input type="hidden" id="txt_agent_id" value="{{$agent->id}}">


</section>

@endsection

@section('additional_footer')

    <script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/jquery.dataTables.min.js")}}'></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js")}}'></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.full.min.js")}}'></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker.js")}}'></script>


    <script>
        $(document).ready(function () {

            var date_from;
            var date_to;
            var effectivity_date = '';
            var sales_date_start;
            var sales_date_end;

            var multilevels = $('#multilevels').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": false,
                "info": true,
                "autoWidth": true
            });
            var multilevels_associate = $('#multilevels_associate').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": false,
                "info": true,
                "autoWidth": true
            });
            $("#add-new-promotion").click(function () {
                if(effectivity_date == ''){
                    effectivity_date = $("#txt_effectivity_date").val();
                }

                $.get("/multilevel/savePromotion", {
                    level_from: $("#slct_level_from").val(),
                    level_to: $("#slct_level_to").val(),
                    effectivity_date: effectivity_date,
                    approved_by: $("#txt_approved_by").val(),
                    agent_id: $("#txt_agent_id").val()
                }).done(function (data) {
                    if(data=='PROMOTION_EXISTING'){
                        $("#promotion_existing_modal").modal('show');
                    }else if(data=='INVALID_PROMOTION'){
                        $("#promotion_invalid_modal").modal('show');
                    }else{
                        window.location.reload();    
                    }
                    
                });
            });
            $("#add-new-phase").click(function () {
                $.get("/multilevel/savePhase", {
                    phase: $("#slct_phase").val(),
                    phase_start: date_from,
                    phase_end: date_to,
                    approved_by: $("#txt_added_by").val(),
                    agent_id: $("#txt_agent_id").val()
                }).done(function (data) {
                    window.location.reload();
                });
            });

            $("#slct_level_from").change(function () {
                changeLevel();
            });

            function changeLevel() {
                var from_level = $("#slct_level_from").val();
                var to_level = $("#slct_level_to");
                var sales_duration = $("#txt_sales_duration");
                var sales_duration_month_count = $("#txt_sales_duration_month_count");
                var required_points = $("#txt_required_points");
                var required_personal_sales = $("#txt_required_personal_sales");
                var group_level = $("#txt_group_level");
                var required_set = $("#txt_required_set");

                var personal_points = 0;
                var group_points = 0;
                var total_points = 0;
                var req_personal = 0;
                var req_group = 0;
                var req_level = 0;

                if (from_level == 'Associate') {
                    to_level.val('Consultant');
                    sales_duration.val("ALL");
                    sales_duration_month_count.val("ALL");
                    required_personal_sales.val("5");
                    group_level.val("0");
                    required_set.val("0");
                    required_points.val("5");

                    req_personal = 5;
                    req_group = 0;
                    req_level = 0;
                } else if (from_level == 'Consultant') {
                    to_level.val('Senior Consultant');
                    sales_duration.val("5 Months");
                    sales_duration_month_count.val("5");
                    required_points.val("30");
                    required_personal_sales.val("10");
                    group_level.val("3");
                    required_set.val("30");

                    req_personal = 10;
                    req_group = 20;
                    req_level = 3;
                } else if (from_level == 'Senior Consultant') {
                    to_level.val('Manager');
                    sales_duration.val("5 Months");
                    sales_duration_month_count.val("5");
                    required_points.val("40");
                    required_personal_sales.val("12");
                    group_level.val("3");
                    required_set.val("40");

                    req_personal = 12;
                    req_group = 28;
                    req_level = 3;
                } else if (from_level == 'Manager') {
                    to_level.val('Distributor');
                    sales_duration.val("6 Months");
                    sales_duration_month_count.val("6");
                    required_points.val("75");
                    required_personal_sales.val("20");
                    group_level.val("ALL");
                    required_set.val("75");

                    req_personal = 20;
                    req_group = 55;
                    req_level = "ALL";
                } else if (from_level == 'Distributor') {
                    to_level.val('Distributor');
                    sales_duration.val("N/A");
                    sales_duration_month_count.val("N/A");
                    required_points.val("N/A");
                    required_personal_sales.val("N/A");
                    group_level.val("ALL");
                    required_set.val("N/A");

                    req_personal = 20;
                    req_group = 55;
                    req_level = "ALL";
                } else {
                    to_level.val('Distributor');
                    sales_duration_month_count.val("N/A");
                    required_points.val("N/A");
                    required_personal_sales.val("N/A");
                    group_level.val("ALL");
                    required_set.val("N/A");
                    req_personal = 20;
                    req_group = 55;
                    req_level = "ALL";
                }

                $.get("/multilevel/getSales", {
                    level: req_level,
                    date_from: sales_date_start,
                    date_to: sales_date_end,
                    agent_id: $("#txt_agent_id").val()
                }).done(function (data) {
                    console.log(data);
                    $("#txt_personal_sales").val(data.personal);
                    $("#txt_group_sales").val(data.group);
                    $("#txt_total_sales").val(data.total);

                    if (parseFloat(data.total) < parseFloat(required_points.val())) {
                        //$("#add-new-promotion").attr('disabled', 'disabled');
                        $("#txt_promotion_status").val('Not Yet Qualified');
                    } else {
                        //$("#add-new-promotion").removeAttr('disabled');
                        $("#txt_promotion_status").val('Qualified For Promotion');
                    }
                });

            }

            function getAssociateSales() {


                var assoc_phase = $("#txt_associate_phase").val();
                var phase_duration = $("#txt_phase_duration");
                var required_points = $("#txt_phase_required_points");
                var phase_points = $("#txt_total_phase_points");
                var phase_status = $("#txt_phase_status");

                var personal_points = 0;
                var total_points = 0;

                if (assoc_phase == 'Phase I') {
                    personal_points = 3;
                    phase_points.val(3);
                } else {
                    personal_points = 2;
                    phase_points.val(2);
                }

                $.get("/multilevel/getAssociateSales", {
                    date_from: sales_date_start,
                    date_to: sales_date_end,
                    agent_id: $("#txt_agent_id").val()
                }).done(function (data) {
                    console.log(data);
                    $("#txt_personal_sales").val(data.personal);
                    $("#txt_group_sales").val(data.group);
                    $("#txt_total_sales").val(data.total);

                    if (parseFloat(data.total) < parseFloat(required_points.val())) {
                        // $("#add-new-promotion").attr('disabled','disabled');
                        $("#txt_promotion_status").val('Not Yet Qualified');
                    } else {
                        // $("#add-new-promotion").removeAttr('disabled');
                        $("#txt_promotion_status").val('Qualified For Promotion');
                    }
                });
            }


            $('#txt_phase_start, #txt_phase_end').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                locale: {
                    format: 'MM/DD/YYYY'
                }
            });

            $('#txt_effectivity_date').daterangepicker({
                "singleDatePicker": true,
                "maxDate":new Date()
                
            });

            $('#txt_effectivity_date').on('apply.daterangepicker', function (ev, picker) {
                effectivity_date = picker.startDate.format('YYYY-MM-DD');
                
            });


            var start = moment().subtract(5, 'months');
            var end = moment();

            $('#txt_sales_dates').daterangepicker({
                "startDate": moment().subtract(5, 'months'),
                "endDate": moment()
            }, cb);

            $('#txt_phase_duration').daterangepicker(cb);

            function cb(start, end) {
                $('#txt_sales_dates').val(start.format('MMMM DD, YYYY') + ' - ' + end.format('MMMM DD, YYYY'));
                sales_date_start = start.format('YYYY-MM-DD');
                sales_date_end = end.format('YYYY-MM-DD');

            }

            cb(start, end);

           

            $('#txt_phase_start').on('apply.daterangepicker', function (ev, picker) {
                date_from = picker.startDate.format('YYYY-MM-DD');
            });

            $('#txt_phase_end').on('apply.daterangepicker', function (ev, picker) {
                date_to = picker.startDate.format('YYYY-MM-DD');
            });
            $('#txt_sales_dates').on('apply.daterangepicker', function (ev, picker) {
                sales_date_start = picker.startDate.format('YYYY-MM-DD');
                sales_date_end = picker.endDate.format('YYYY-MM-DD');
                changeLevel();
            });

            changeLevel();

            $(document).on("click", "#btn_update_phase", function () {
                var promotion_id = $(this).data('id');

                //get phase details
                $.get("/multilevel/getPhaseDetails", {
                    id: promotion_id
                }).done(function (data) {
                    console.log(data);
                    var details = data[0];
                    $("#txt_associate_phase").val(details.type);
                    $("#txt_phase_duration").val(details.start_date + "-" + details.end_date);
                    if (details.type == 'Phase I') {
                        $("#txt_phase_required_points").val(3);
                    } else {
                        $("#txt_phase_required_points").val(2);
                    }
                });
                var total_sales = 0;
                var details = [];
                var required_points = 0;

                $.get("/multilevel/getPhaseDetails", {
                    id: promotion_id
                }).done(function (data) {
                    console.log(data);
                    details = data[0];
                    $("#txt_associate_phase").val(details.type);
                    $("#txt_phase_duration").val(details.start_date + " to " + details.end_date);
                    if (details.type == 'Phase I') {
                        $("#txt_phase_required_points").val(3);
                        required_points = 3;
                    } else {
                        $("#txt_phase_required_points").val(2);
                        required_points = 2;
                    }

                    $.get("/multilevel/getAssociateSales", {
                        date_from: details.start_date,
                        date_to: details.end_date,
                        agent_id: $("#txt_agent_id").val()
                    }).done(function (data) {

                        $("#txt_total_phase_points").val(data.sales);
                        total_sales = data.sales;

                        if (parseFloat(total_sales) >= parseFloat(required_points)) {
                            $("#approve-new-promotion").removeAttr('disabled');
                            $("#disapprove-new-promotion").attr('disabled', 'disabled');
                            $("#txt_phase_status").val('Qualified For Promotion');
                        } else {
                            $("#approve-new-promotion").attr('disabled', 'disabled');
                            $("#disapprove-new-promotion").removeAttr('disabled');
                            $("#txt_phase_status").val('Not Yet Qualified');
                        }
                    });
                });


                // As pointed out in comments,
                // it is superfluous to have to manually call the modal.
                // $('#addBookDialog').modal('show');
            });

        })
    </script>
@endsection