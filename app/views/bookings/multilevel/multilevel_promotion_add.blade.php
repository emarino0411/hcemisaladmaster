@extends('admin_template')

@section('additional_header')

    <link rel="stylesheet"
          href='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker-bs3.css")}}'>
    <link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/iCheck/all.css")}}'>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.css")}}'>
    <link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.min.css")}}'>

    @endsection

    @section('content')
            <!-- Main content -->
    <section class="content">
        <!-- Purchase Order -->
        <div class="row">
            <div class="col-md-12 col-xs-12">

                <div class="com-md-12 box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-shopping-cart"></i> Contest Update</h3>
                    </div>
                    <!-- /.box-header -->

                    <!-- form start -->
                    <form class="form-horizontal">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <!-- Terms -->
                                    <div class="col-md-12">
                                        <div class="form-group" id="set">
                                            <label for="slct_terms" class="col-sm-3 control-label">Contest Title</label>

                                            <div class="col-sm-6">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-rub"></i>
                                                    </div>
                                                    <select id="slct_contest_list" class="form-control select2">

                                                    </select>
                                                </div>
                                                <!-- /.input group -->
                                            </div>
                                            <!-- /.col-sm-6 -->

                                            <div class="col-md-3">
                                                <button type="button" id="btn_update_contest"
                                                        class="btn btn-success"
                                                        style="margin-left:1%"> Select
                                                </button>

                                            </div>
                                        </div>
                                        <!-- /. form-group -->
                                    </div>
                                    <!-- /.Terms -->

                                    <div class="col-md-12">
                                        <div class="box-body">
                                            <table id="tblcontestants" class="table table-bordered table-striped">
                                                <thead>
                                                <tr>
                                                    <th>Contestant</th>
                                                    <th>Sets Sold</th>
                                                    <th>Status</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($purchases as $purchase)
                                                    <tr>
                                                        <td><input type="checkbox"></td>
                                                        <td>{{$purchase->id}}</td>
                                                        <td>{{$purchase->first_name}} {{$purchase->last_name}}</td>
                                                        <td>{{$purchase->purchase_date}}</td>
                                                        <td>{{$purchase->status}}</td>
                                                        <td>
                                                            <a href="purchases/viewPurchaseDetails/{{$purchase->id}}"
                                                               data-toggle="tooltip"
                                                               title="View {{$purchase->first_name}} {{$purchase->last_name}}'s purchase details"
                                                               class="btn btn-success btn-xs">
                                                                <i class="fa fa-info-circle"></i> View
                                                            </a>
                                                            <a href="purchases/editPurchase/{{$purchase->id}}"
                                                               data-toggle="tooltip"
                                                               title="Edit {{$purchase->first_name}} {{$purchase->last_name}}'s purchase details"
                                                               class="btn btn-primary btn-xs">
                                                                <i class="fa fa-edit"></i> Edit
                                                            </a>

                                                            {{--<a href="users/deleteProfile/{{$purchase->id}}"--}}
                                                            {{--data-toggle="tooltip"--}}
                                                            {{--title="Deactivate {{$purchase->first_name}} {{$purchase->last_name}}'s details"--}}
                                                            {{--class="btn btn-warning btn-xs"--}}
                                                            {{--data-toggle="modal"--}}
                                                            {{--data-target="#search-purchase">--}}
                                                            {{--<i class="fa fa-warning"></i> Deactivate--}}
                                                            {{--</a>--}}
                                                        </td>
                                                    </tr>
                                                @endforeach

                                                </tbody>
                                                <tfoot>
                                                <tr>
                                                    <th>Contestant</th>
                                                    <th>Sets Sold</th>
                                                    <th>Status</th>
                                                </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>

                                </div>
                                <!-- ./col-md-12 -->
                            </div>
                            <!-- /.row-->
                        </div>
                        <!-- /.box-body -->
                    </form>
                    <!-- /form-end -->
                </div>
                <!-- /.box -->
                <!--/.col (left) -->
            </div>
            <!-- search form -->
        </div>
        <!-- /Purchase Order -->




    </section>
@endsection

@section('additional_footer')

    <script src='{{ asset("/bower_components/AdminLTE/plugins/jQueryUI/jquery-ui.min.js")}}'></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.full.min.js")}}'></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker.js")}}'></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/iCheck/icheck.min.js")}}'></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/jquery.dataTables.min.js")}}'></script>

    <script>
        $(document).ready(function () {
            var product_obj;
            $('#txt_date_from, #txt_date_to').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                locale: {
                    format: 'YYYY-mm-dd'
                }
            });

            $('#txt_purchase_date').on('apply.daterangepicker', function (ev, picker) {
                $('#txt_delivery_date').daterangepicker({
                    singleDatePicker: true,
                    showDropdowns: true,
                    locale: {
                        format: 'YYYY-mm-dd'
                    },
                    minDate: $('#txt_purchase_date').val()
                });
                $('#txt_delivery_date').val($('#txt_purchase_date').val());
            });
            $('#txt_delivery_date').on('apply.daterangepicker', function (ev, picker) {
                $('#txt_purchase_date').daterangepicker({
                    singleDatePicker: true,
                    showDropdowns: true,
                    locale: {
                        format: 'YYYY-mm-dd'
                    },
                    minDate: $('#txt_delivery_date').val()
                });
                $('#txt_purchase_date').val($('#txt_delivery_date').val());
            });

            $('#txt_purchase_date').on('cancel.daterangepicker', function (ev, picker) {
                $('#txt_delivery_date').val('');
            });

            $("#txt_set_sold_1, #txt_set_sold_2, #txt_set_sold_3").spinner({
                min: 1,
                change: function (event, ui) {
                    // alert();
                    // updatePrice();
                }
            });


            var table_data = $('#tblcontestants').DataTable({
                "paging": false,
                "lengthChange": true,
                "searching": false,
                "info": false,
                "autoWidth": true
            });

            $("#slct_contest_list").select2({
                ajax: {
                    url: "/promos/getContestTitle",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term // search term
                        };
                    },
                    processResults: function (data) {
                        // parse the results into the format expected by Select2.
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data
                        return {
                            results: data
                        };
                    },
                    cache: true
                },
                minimumInputLength: 2
            });

            $("#btn_update_contest").click(function(){
                var contest_id =  $("#slct_contest_list").select2('val');
                $.get("/promos/getContestants", {
                    contestId:contest_id
                }).done(function (data) {
                    table_data.clear().draw();
                    var qualified_for = "Not qualified";
                    for (x = 0; x < data.length; x++) {

                        if(data[x].point >= data[x].mechanics_3_points){
                            qualified_for = "Qualified: "+data[x].mechanics_3_prize;
                        }else if(data[x].point >= data[x].mechanics_2_points){
                            qualified_for = "Qualified: "+data[x].mechanics_2_prize;
                        }else if(data[x].point >= data[x].mechanics_1_points){
                            qualified_for = "Qualified: "+data[x].mechanics_1_prize;
                        }else{
                            // do nothing
                        }

                        table_data.row.add(
                                [
                                    data[x].first_name +" "+ data[x].last_name,
                                    data[x].point,
                                    data[x].first_name +" "+ data[x].last_name

                                ]
                        ).draw();

                    }

                });
            });

        });
    </script>
@endsection