@extends('admin_template')

@section('additional_header')

        <!-- DataTables -->
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.min.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker-bs3.css")}}'>

@endsection

@section('content')
        <!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <!-- general form elements -->
            <div class="com-md-12 box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Display Commissions For Release</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <div class="box-body">
                    <div class="row">
                        <form role="form" class="form-horizontal">
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <label for="txt_payment_date" class="col-sm-6 control-label">Commission Release Date</label>

                                    <div class="col-sm-6">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text"
                                                   class="form-control pull-right"
                                                   id="txt_date_received">

                                        </div>

                                        <!-- /.input group -->
                                    </div>
                                    <!-- /.col-sm-8 -->
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <div class="col-md-2 col-sm-2">
                                <button id="btn_search" type="button" class="btn btn-primary"><i class="ion ion-search"></i> Search Commissions For Release
                                </button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
            <!-- /.box -->
            <!--/.col (left) -->
        </div>
        <!-- search form -->
    </div>
    <!-- /.row (main row) -->

    <div class="row">
        <div class=" col-md-12 col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-shopping-cart"></i> Commission For Release</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>&nbsp;</th>
                                <th>Release Date</th>
                                <th>Voucher No</th>
                                <th>Payee Name(Agent)</th>
                                <th>PO NO</th>
                                <th>Client Name</th>
                                <th>Check No</th>
                                <th>Amount</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                            <tfoot>
                            <tr>
                                <th>&nbsp;</th>
                                <th>Release Date</th>
                                <th>Voucher No</th>
                                <th>Payee Name(Agent)</th>
                                <th>PO NO</th>
                                <th>Client Name</th>
                                <th>Check No</th>
                                <th>Amount</th>
                                <th>Status</th>
                            </tr>
                            </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>

    <div class="row">
        <div class=" col-md-12 col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-edit"></i> Update Commission Status</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <!-- Remove From List -->
                    <div class="row">

                        <div class="col-md-4">
                            <div class="input-group">
                                <select id="slct_status" class="form-control">
                                    <option>--Select Action--</option>
                                    <option value="FOR RELEASE">For release</option>
                                    <option value="RELEASED">Released</option>
                                    <option value="HOLD">Hold</option>
                                    <option value="FOR REVERSE">For Reverse</option>
                                    <option value="REVERSED">Reversed</option>
                                    <option value="CHARGED TO ACCOUNT">Charged To Account</option>
                                    <option value="PAYMENT">Payment</option>
                                </select>

                                <div class="input-group-btn">
                                    <button type="button" id="btn_update_payments" class="btn btn-info">Go</button>
                                </div>
                                <!-- /btn-group -->
                            </div>
                        </div>


                    </div>
                    <!-- /. Remove From List -->
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
    <div id="no-record-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-blue-gradient">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><i class="fa fa-close"></i> No Record To Display</h4>
                </div>
                <div class="modal-body">
                    No commission/s to display.
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" id="btn_close_modal" data-dismiss="modal">Close
                    </button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div id="update_payment_status" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-blue-active">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><i class="fa fa-edit"></i> Update Commission Status</h4>
                </div>
                <div class="modal-body">
                    Mark commission/s record as <label id="lbl_status_update"></label>?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                    <button type="button" id="btn_update_status_payment" class="btn btn-success" data-dismiss="modal">Yes
                        
                    </button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

</section>

@endsection

@section('additional_footer')

    <script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/jquery.dataTables.min.js")}}'></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js")}}'></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.full.min.js")}}'></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker.js")}}'></script>


    <script>
        $(document).ready(function () {
            var date_from;
            var date_to;
            var table_data = $('#example1').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true
            });
            
            $('#txt_date_received').daterangepicker({
                showDropdowns: true,
                startDate: new Date(),
                locale: {
                    format: 'YYYY-MM-DD'
                }
            });

            $('#txt_date_received').on('apply.daterangepicker', function (ev, picker) {
                date_from = picker.startDate.format('YYYY-MM-DD');
                date_to = picker.endDate.format('YYYY-MM-DD');
            });


            $("#btn_search").click(function(){
                updatePaymentTable();
            });

            $("#btn_update_payments").click(function () {
                if($("#slct_status").val()=='--Select Action--'){
                    return false;
                }
                $("#update_payment_status").modal('show');
                $("#lbl_status_update").html($("#slct_status").val());

            });

            $("#btn_update_status_payment").click(function(){
                var items = $("input[type=checkbox][name=commission]:checked");
                var for_update = [];
                $("input[type=checkbox][name=commission]:checked").each(function () {
                    for_update.push(this.value);
                });

                $.get("/commissions/updateCommissionsStatus/", {
                    update_items: for_update,
                    status: $("#slct_status").val()
                }).done(function (data) {
                    console.log(data);
                    updatePaymentTable();
                });
            });
            
            function updatePaymentTable(){
                $.get("/commissions/getCommissionForRelease", {
                    _date_from: date_from,
                    _date_to: date_to
                }).done(function (data) {
                    table_data.clear().draw();

                    if(data.length==0){
                        $("#no-record-modal").modal('show');
                        return false;    
                    }else{
                        for(ctr=0;ctr<data.length;ctr++){
                            table_data.row.add(
                                [
                                    "<input type=checkbox name='commission' value='" + data[ctr].id + "'><input type='hidden' id='" + data[ctr].id + "' value='" + data[ctr].amount + "'>",
                                    data[ctr].due_date,
                                    data[ctr].voucher_no,
                                    data[ctr].first_name + " " + data[ctr].last_name,
                                    data[ctr].purchase_order_no,
                                    data[ctr].client_first + " " + data[ctr].client_last,
                                    data[ctr].check_no,
                                    data[ctr].received_commission,
                                    data[ctr].status

                                ]
                            ).draw();
                            
                        }
                    }
                    
                    
                });
            }
        });
    </script>
@endsection