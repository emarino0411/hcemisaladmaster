@extends('admin_template')

@section('additional_header')

    <link rel="stylesheet"
          href='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker-bs3.css")}}'>
    <link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/iCheck/all.css")}}'>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.css")}}'>
    <link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.min.css")}}'>
    <style>
        /*
      Force table width to 100%
      */
        table.table-fixedheader {

        }

        /*
        Set table elements to block mode.  (Normally they are inline).
        This allows a responsive table, such as one where columns can be stacked
        if the display is narrow.
        */
        table.table-fixedheader, table.table-fixedheader > thead, table.table-fixedheader > tbody, table.table-fixedheader > thead > tr, table.table-fixedheader > tbody > tr, table.table-fixedheader > thead > tr > th, table.table-fixedheader > tbody > td {
            display: block;
        }

        table.table-fixedheader > thead > tr:after, table.table-fixedheader > tbody > tr:after {
            content: ' ';
            display: block;
            visibility: hidden;
            clear: both;
        }

        /*
        When scrolling the table, actually it is only the tbody portion of the
        table that scrolls (not the entire table: we want the thead to remain
        fixed).  We must specify an explicit height for the tbody.  We include
        100px as a default, but it can be overridden elsewhere.

        Also, we force the scrollbar to always be displayed so that the usable
        width for the table contents doesn't change (such as becoming narrower
        when a scrollbar is visible and wider when it is not).
        */
        table.table-fixedheader > tbody {
            overflow-y: scroll;
            height: 100%

        }

        /*
        We really don't want to scroll the thead contents, but we want to force
        a scrollbar to be displayed anyway so that the usable width of the thead
        will exactly match the tbody.
        */
        table.table-fixedheader > thead {
            overflow-y: scroll;
        }

        /*
        For browsers that support it (webkit), we set the background color of
        the unneeded scrollbar in the thead to make it invisible.  (Setting
        visiblity: hidden defeats the purpose, as this alters the usable width
        of the thead.)
        */
        table.table-fixedheader > thead::-webkit-scrollbar {
            background-color: inherit;
        }

        table.table-fixedheader > thead > tr > th:after, table.table-fixedheader > tbody > tr > td:after {
            content: ' ';
            display: table-cell;
            visibility: hidden;
            clear: both;
        }

        /*
        We want to set <th> and <td> elements to float left.
        We also must explicitly set the width for each column (both for the <th>
        and the <td>).  We set to 20% here a default placeholder, but it can be
        overridden elsewhere.
        */

        table.table-fixedheader > thead tr th, table.table-fixedheader > tbody tr td {
            float: left;
            word-wrap: break-word;
        }


    </style>
    @endsection

    @section('content')
            <!-- Main content -->
    <section class="content">

        <!-- Client Information -->
        <div class="row">
            <div class=" col-md-12 col-xs-12">
                <div class="row">
                    <!-- client -->
                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="box-header">
                                <h3 class="box-title"><i class="fa fa-user"></i> Search Client</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <form class="form-horizontal">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="txt_po_no" class="col-sm-4 control-label">Search
                                                        Client</label>

                                                    <div class="col-sm-8">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-user"></i>
                                                            </div>
                                                            <select id="slct_client" class="form-control">
                                                            </select>
                                                        </div>
                                                        <!-- /.input group -->
                                                    </div>
                                                </div>
                                            </div>
                                            {{--<div class="col-md-1 col-sm-1">--}}
                                            {{--<button id="btn_search" type="button" class="btn btn-primary"--}}
                                            {{--data-toggle="modal" data-target="#search-purchase"><i--}}
                                            {{--class="ion ion-search"></i> Search Client--}}
                                            {{--</button>--}}
                                            {{--</div>--}}
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="txt_po_no" class="col-sm-4 control-label">Select PO
                                                        No</label>

                                                    <div class="col-sm-8">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-edit"></i>
                                                            </div>
                                                            <select class="form-control" id="po_list">
                                                                <option>Please Select Client</option>
                                                            </select>
                                                        </div>
                                                        <!-- /.input group -->
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                    <!-- /client -->

                </div>
            </div>
        </div>
        <!-- / client information -->

        <!-- Purchase Information -->
        <div class="row">
            <div class=" col-md-12 col-xs-12">
                <div class="row">
                    <!-- client -->
                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="box-header">
                                <h3 class="box-title"><i class="fa fa-user"></i> Client Information</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">

                                <div class="col-md-12">

                                    <div class="col-md-3 col-md-offset-1">
                                        <div class="box-header with-border">
                                            <h3 class="box-title">Purchase Order</h3>
                                        </div>
                                        <!-- /.box-header -->
                                        <div class="box-body">
                                            <strong><i class="fa fa-edit"></i> PO Number</strong>

                                            <p class="text-muted" id="detail_po_no">
                                                N/A
                                            </p>

                                            <strong><i class="fa fa-forward"></i> Is Fast Track</strong>

                                            <p class="text-muted" id="detail_fast_track">
                                                N/A
                                            </p>


                                            <strong><i class="fa fa-calendar"></i> Sales Date</strong>

                                            <p class="text-muted" id="detail_sales_date">
                                                N/A
                                            </p>
                                        </div>
                                        <!-- /.box-body -->
                                    </div>
                                    <div class="col-md-3">
                                        <div class="box-header with-border">
                                            <h3 class="box-title">Payment Information</h3>
                                        </div>
                                        <!-- /.box-header -->
                                        <div class="box-body">
                                            <strong><i class="fa fa-list-alt"></i> Terms</strong>

                                            <p class="text-muted" id="detail_terms">
                                                N/A
                                            </p>

                                            <strong><i class="fa fa-calendar"></i> Total Transaction Price</strong>

                                            <p class="text-muted" id="detail_total_price">
                                                N/A
                                            </p>

                                            <strong><i class="fa fa-calendar"></i> Payment Status</strong>

                                            <p class="text-muted" id="detail_payment_status">
                                                N/A
                                            </p>
                                        </div>
                                        <!-- /.box-body -->
                                    </div>
                                    <div class="col-md-5">
                                        <!-- /.box-header -->
                                        <div class="box-header">
                                            <table class="table table-fixedheader table-bordered"
                                                   style="margin-top:-10px;">
                                                <thead>
                                                <tr>
                                                    <th width="60%">Purchased Items</th>
                                                    <th width="20%">Qty</th>
                                                    <th width="20%">Is Gift</th>
                                                </tr>
                                                </thead>
                                                <tbody id="tbl_purchased_items">
                                                <tr>
                                                    <td width="60%">Professional Set</td>
                                                    <td width="20%">1</td>
                                                    <td width="20%"><span class="label label-success">Yes</span></td>
                                                </tr>

                                                </tbody>

                                            </table>
                                        </div>
                                        <!-- /.box-body -->
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                    <!-- /client -->

                </div>
            </div>
        </div>
        <!-- / client information -->

        <!-- Representative List -->
        <div class="row">
            <div class="col-md-12 col-xs-12">

                <div class="com-md-12 box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-users"></i> Representatives</h3>
                    </div>
                    <!-- /.box-header -->


                    <form class="form-horizontal">
                        <div class="box-body">

                            <!-- Representatives Table -->
                            <div class="row">
                                <div class="col-md-8 col-md-offset-2">
                                    <!-- form start -->
                                    <form class="form-horizontal">
                                        <div class="box-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <!-- associate -->
                                                    <div class="col-md-8">
                                                        <div class="form-group">
                                                            <label for="txt_po_no" class="col-sm-3 control-label">Associate</label>

                                                            <div class="col-sm-9">
                                                                <div class="input-group">
                                                                    <div class="input-group-addon">
                                                                        <i class="fa fa-user"></i>
                                                                    </div>
                                                                    <label class="form-control"
                                                                           id="lbl_associate"></label>

                                                                </div>
                                                                <!-- /.input group -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- /associate -->
                                                    <!--  asociate percentage -->
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <div class="col-sm-12">
                                                                <div class="input-group">
                                                                    <input type="text" class="form-control pull-right"
                                                                           id="txt_associate_percentage" readonly
                                                                    >
                                                                    <input type="hidden"
                                                                           id="is_associate_voucher_printed">

                                                                    <div class="input-group-addon">
                                                                        <i class="fa fa-percent"></i>
                                                                    </div>
                                                                </div>

                                                                <!-- /.input group -->
                                                            </div>


                                                            <!-- /.col-sm-8 -->
                                                        </div>
                                                        <!-- /.form-group -->
                                                    </div>
                                                    <!-- /asociate percentage  -->
                                                    <div class="col-md-2">
                                                        <label class="lbl-warning" id="associate_status"></label>
                                                    </div>


                                                    <!-- sponsor -->
                                                    <div class="col-md-8">
                                                        <div class="form-group">
                                                            <label for="txt_po_no" class="col-sm-3 control-label">Sponsor</label>

                                                            <div class="col-sm-9">
                                                                <div class="input-group">
                                                                    <div class="input-group-addon">
                                                                        <i class="fa fa-user"></i>
                                                                    </div>
                                                                    <label class="form-control"
                                                                           id="lbl_sponsor"></label>

                                                                </div>
                                                                <!-- /.input group -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- /sponsor -->
                                                    <!--  sponsor percentage -->
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <div class="col-sm-12">
                                                                <div class="input-group">

                                                                    <input type="text" class="form-control pull-right"
                                                                           value="" id="txt_sponsor_percentage" readonly
                                                                    >
                                                                    <input type="hidden"
                                                                           id="is_sponsor_voucher_printed">

                                                                    <div class="input-group-addon">
                                                                        <i class="fa fa-percent"></i>
                                                                    </div>
                                                                </div>

                                                                <!-- /.input group -->
                                                            </div>


                                                            <!-- /.col-sm-8 -->
                                                        </div>
                                                        <!-- /.form-group -->
                                                    </div>
                                                    <!-- /sponsor percentage  -->
                                                    <div class="col-md-2">
                                                        <label class="lbl-warning" id="sponsor_status"></label>
                                                    </div>

                                                    <!-- presenter -->
                                                    <div class="col-md-8">
                                                        <div class="form-group">
                                                            <label for="txt_po_no" class="col-sm-3 control-label">Presenter</label>

                                                            <div class="col-sm-9">
                                                                <div class="input-group">
                                                                    <div class="input-group-addon">
                                                                        <i class="fa fa-user"></i>
                                                                    </div>
                                                                    <label class="form-control"
                                                                           id="lbl_consultant"></label>

                                                                </div>
                                                                <!-- /.input group -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- /presenter -->
                                                    <!--  presenter percentage -->
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <div class="col-sm-12">
                                                                <div class="input-group">

                                                                    <input type="text" class="form-control pull-right"
                                                                           id="txt_presenter_percentage" readonly
                                                                    >
                                                                    <input type="hidden"
                                                                           id="is_presenter_voucher_printed">

                                                                    <div class="input-group-addon">
                                                                        <i class="fa fa-percent"></i>
                                                                    </div>

                                                                </div>


                                                                <!-- /.input group -->
                                                            </div>


                                                            <!-- /.col-sm-8 -->
                                                        </div>
                                                        <!-- /.form-group -->
                                                    </div>
                                                    <!-- /presenter  percentage  -->
                                                    <div class="col-md-2">
                                                        <label class="lbl-warning" id="presenter_status"></label>
                                                    </div>

                                                    <!-- Cook -->
                                                    <div class="col-md-8">
                                                        <div class="form-group">
                                                            <label for="txt_po_no"
                                                                   class="col-sm-3 control-label">Cook</label>

                                                            <div class="col-sm-9">
                                                                {{--<div class="input-group">--}}
                                                                {{--<div class="input-group-addon">--}}
                                                                {{--<i class="fa fa-user"></i>--}}
                                                                {{--</div>--}}
                                                                {{--<input type="text" class="form-control"--}}
                                                                {{--value="" id="txt_driver" multiple>--}}
                                                                {{----}}

                                                                {{--</div>--}}
                                                                <select id="txt_cook" class="form-control"
                                                                        style="width: 100%;height:100%">
                                                                </select>
                                                                <!-- /.input group -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- /Cook -->
                                                    <!--  Cook percentage -->
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <div class="col-sm-12">
                                                                <div class="input-group">

                                                                    <input type="text" class="form-control pull-right"
                                                                           id="txt_cook_percentage" readonly
                                                                    >
                                                                    <input type="hidden" id="is_cook_voucher_printed">

                                                                    <div class="input-group-addon">
                                                                        <i class="fa fa-percent"></i>
                                                                    </div>
                                                                </div>

                                                                <!-- /.input group -->
                                                            </div>

                                                            <!-- /.col-sm-8 -->
                                                        </div>
                                                        <!-- /.form-group -->
                                                    </div>
                                                    <!-- /Cook  percentage  -->
                                                    <div class="col-md-2">
                                                        <label class="lbl-warning" id="cook_status"></label>
                                                    </div>


                                                    <!-- Cook -->
                                                    <div class="col-md-8">
                                                        <div class="form-group">
                                                            <label for="txt_po_no"
                                                                   class="col-sm-3 control-label">Driver</label>

                                                            <div class="col-sm-9">
                                                                {{--<div class="input-group">--}}
                                                                {{--<div class="input-group-addon">--}}
                                                                {{--<i class="fa fa-user"></i>--}}
                                                                {{--</div>--}}
                                                                {{--<input type="text" class="form-control"--}}
                                                                {{--value="" id="txt_driver" multiple>--}}
                                                                {{----}}

                                                                {{--</div>--}}
                                                                <select id="txt_driver" class="form-control"
                                                                        style="width: 100%;height:100%">
                                                                </select>
                                                                <!-- /.input group -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- /Cook -->
                                                    <!--  Cook percentage -->
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <div class="col-sm-12">
                                                                <div class="input-group">

                                                                    <input type="text" class="form-control pull-right"
                                                                           id="txt_driver_percentage" readonly
                                                                    >
                                                                    <input type="hidden" id="is_driver_voucher_printed">

                                                                    <div class="input-group-addon">
                                                                        <i class="fa fa-percent"></i>
                                                                    </div>
                                                                </div>


                                                                <!-- /.input group -->
                                                            </div>


                                                            <!-- /.col-sm-8 -->
                                                        </div>
                                                        <!-- /.form-group -->
                                                    </div>
                                                    <!-- /Cook  percentage  -->
                                                    <div class="col-md-2">
                                                        <label id="driver_status"></label>
                                                    </div>


                                                </div>
                                                <!-- ./col-md-12 -->
                                            </div>
                                            <!-- /.row-->
                                        </div>
                                        <!-- /.box-body -->
                                    </form>
                                    <!-- /form-end -->

                                </div>
                            </div>
                            <!-- /Representatives Table -->
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-12">

                                        <button class="btn btn-success pull-right" type="button" id="btn_modify">
                                            Modify
                                        </button>
                                        <button class="btn btn-success pull-right text hidden" type="button"
                                                id="btn_cancel_modify">
                                            Cancel Modify
                                        </button>
                                        <button type="button" class="btn btn-warning pull-right"
                                                style="margin-right:1%" id="btn_save_commissions"> Save Distribution
                                        </button>

                                    </div>
                                </div>
                                <!-- /btn-group -->
                            </div>

                            <!-- buttons representative -->
                            {{--<div class="12">--}}
                            {{--<div class="row">--}}
                            {{--<div class="col-md-12">--}}
                            {{--<button class="btn btn-success pull-right"> Save Distribution</button>--}}
                            {{--<button class="btn bg-orange pull-right" style="margin-right:1%"> Modify--}}
                            {{--</button>--}}
                            {{--<a href="/agents/addNew" target="_blank" class="btn bg-blue pull-right" style="margin-right:1%"> Add New Agent--}}
                            {{--</a>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--<!-- /btn-group -->--}}
                            {{--</div>--}}


                        </div>
                        <!-- /buttons representative -->
                    </form>

                </div>
                <!-- /.box -->
                <!--/.col (left) -->
            </div>
            <!-- search form -->
        </div>

        {{--MODAL--}}
        <div id="save-commission-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-blue-gradient">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-text="true">&times;</span></button>
                        <h4 class="modal-title"><i class="fa fa-check"></i> Save Distribute Commission</h4>
                    </div>
                    <div class="modal-body">
                        Save Distribute Commission OK
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" id="btn_close_modal" data-dismiss="modal">Close
                        </button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        {{--MOdal End--}}

        <!-- /Representative List -->
        <input type="hidden" id="txt_sponsor_id">
        <input type="hidden" id="txt_consultant_id">
        <input type="hidden" id="txt_associate_id">
        <input type="hidden" id="txt_cook_id">
        <input type="hidden" id="txt_driver_id">
        <input type="hidden" id="txt_po">
        <input type="hidden" id="txt_terms">


    </section>
@endsection

@section('additional_footer')

    <script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/jquery.dataTables.min.js")}}'></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js")}}'></script>

    <script src='{{ asset("/bower_components/AdminLTE/plugins/jQueryUI/jquery-ui.min.js")}}'></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.full.min.js")}}'></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker.js")}}'></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/iCheck/icheck.min.js")}}'></script>

    <script>
        $(document).ready(function () {

            $('#txt_check_date, #txt_purchase_date').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true
            });

            $("#slct_payment_type").change(function () {
                if ($(this).val() == 'cash') {
                    $("#div_cash").show();
                    $("#div_check").hide();
                } else {
                    $("#div_cash").hide();
                    $("#div_check").show();
                }
            });

            //iCheck for checkbox and radio inputs
            $('input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass: 'iradio_minimal-blue'
            });

            $('#tbl_payments_list').DataTable({
                "paging": false,
                "lengthChange": true,
                "searching": false,
                "ordering": true,
                "info": false,
                "autoWidth": true
            });

            $('#slct_client').select2({
                ajax: {
                    url: "/purchases/searchClient",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term // search term
                        };
                    },
                    processResults: function (data) {
                        // parse the results into the format expected by Select2.
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data
                        return {
                            results: data
                        };
                    },
                    cache: true
                },
                minimumInputLength: 2
            });

            $('#slct_client').on("select2:select", function (e) {
                $.get("/payments/getClientPO", {
                    client_id: $('#slct_client').val()
                }).done(function (data) {
                    $("#po_list").empty();
                    $("#po_list").append($('<option>', {value: ''})
                            .text('Select PO Number'));
                    $("#po_list").select2({
                        data: data
                    });
                });
            });


            $('#po_list').on("select2:select", function (e) {
                $("#txt_po").val($('#po_list').val());

                $.get("/commissions/getClientPurchaseDetails", {
                    po_id: $('#po_list').val()
                }).done(function (data) {
                    console.log(data);
                    var detail = data[0];
                    $("#detail_po_no").html(detail.purchase_order_no);
                    $("#detail_sales_date").html(detail.purchase_date);
                    $("#detail_terms").html(detail.terms);
                    $("#detail_fast_track").html(detail.fast_track);
                    $("#detail_delivery_date").html(detail.delivery_date);
                    $("#txt_terms").val(detail.terms);
                   // alert(detail.terms);


                    $.get("/commissions/getPurchasedItems", {
                        po_id: $('#po_list').val()
                    }).done(function (data) {
                        console.log(data);
                        console.log('panget')
                        var total_price = 0;
                        $("#tbl_purchased_items").empty();
                        for (ctr = 0; ctr < data.length; ctr++) {
                            $("#tbl_purchased_items").append(
                                    '<tr>' +
                                    '<td width="60%">' + data[ctr].description + '</td>' +
                                    '<td width="20%">' + data[ctr].qty + '</td>' +
                                    '<td width="20%">' + data[ctr].gift + '</td>' +
                                    '</tr>'
                            );
                            total_price += (parseInt(data[ctr].qty) * parseInt(data[ctr].item_price));
                        }
                        $("#detail_total_price").html(total_price);
                        $("#txt_detail_total_price").val(total_price);


                    });

                    var url = '';

                    if ($("#txt_terms").val() != 'cod') {
                        url = "/bookings/searchRepresentativeNotInPurchase/" + $("#txt_po").val();
                    } else {
                        url = "/bookings/searchCookDriver/";
                    }

                    $.get("/commissions/getAgentCommissions", {
                        po_id: $('#po_list').val()
                    }).done(function (data) {


                        $('#txt_driver').select2({
                            ajax: {
                                url: url,
                                //  url: "searchRepresentative",
                                dataType: 'json',
                                delay: 250,
                                data: function (params) {
                                    return {
                                        q: params.term,
                                        not: $("#txt_consultant_id").val()// search term
                                    };
                                },
                                processResults: function (data) {
                                    // parse the results into the format expected by Select2.
                                    // since we are using custom formatting functions we do not need to
                                    // alter the remote JSON data
                                    return {
                                        results: data
                                    };
                                },
                                cache: true
                            },
                            minimumInputLength: 2
                        });

                        console.log(data);
                        console.log(typeof(data.associate));
                        console.log(typeof(data.consultant));
                        console.log(typeof(data.sponsor));

                        if (typeof(data.associate) == 'object') {
                            $("#lbl_associate").html(data.associate.last_name + "," + data.associate.first_name);
                            $("#txt_associate_id").val(data.associate.id);
                        } else {
                            $("#lbl_associate").html(data.associate);
                            $("#txt_associate_id").val(0);
                        }
                        if (typeof(data.consultant) == 'object') {
                            $("#lbl_consultant").html(data.consultant.last_name + "," + data.consultant.first_name);
                            $("#txt_consultant_id").val(data.consultant.id);
                        } else {
                            $("#lbl_consultant").html(data.consultant);
                            $("#txt_consultant_id").val(0);
                        }
                        if (typeof(data.sponsor) == 'object') {
                            $("#lbl_sponsor").html(data.sponsor.last_name + "," + data.sponsor.first_name);
                            $("#txt_sponsor_id").val(data.sponsor.id);
                        } else {
                            $("#lbl_sponsor").html(data.sponsor);
                            $("#txt_sponsor_id").val(0);
                        }

                        if (typeof(data.cook) == 'object') {
                            $('#txt_cook').select2('destroy');
                            $('#txt_cook').append($('<option>', {
                                value: data.cook.id,
                                text: data.cook.last_name + "," + data.cook.first_name
                            }));
                            $("#txt_cook_id").val(data.cook.id);
                            $("#txt_cook_percentage").val(data.cook_percentage);
                        }

                        if (typeof(data.driver) == 'object') {
                            $('#txt_driver').select2('destroy');
                            $('#txt_driver').append($('<option>', {
                                value: data.driver.id,
                                text: data.driver.last_name + "," + data.driver.first_name
                            }));
                            $("#txt_driver_id").val(data.driver.id);
                            $("#txt_driver_percentage").val(data.driver_percentage);
                        }

                        $("#txt_presenter_percentage").val(data.consultant_percentage);
                        $("#txt_associate_percentage").val(data.associate_percentage);
                        $("#txt_sponsor_percentage").val(data.sponsor_percentage);

                        $("#is_presenter_voucher_printed").val(data.presenter_voucher);
                        $("#is_associate_voucher_printed").val(data.associate_voucher);
                        $("#is_sponsor_voucher_printed").val(data.sponsor_voucher);
                        $("#is_cook_voucher_printed").val(data.cook_voucher);
                        $("#is_driver_voucher_printed").val(data.driver_voucher);

                        if (data.presenter_voucher == 'VOUCHER PRINTED') {
                            $("#presenter_status").html('**Voucher Released');
                        }
                        if (data.associate_voucher == 'VOUCHER PRINTED') {
                            $("#associate_status").html('**Voucher Released');
                        }
                        if (data.sponsor_voucher == 'VOUCHER PRINTED') {
                            $("#sponsor_status").html('**Voucher Released');
                        }
                        if (data.cook_voucher == 'VOUCHER PRINTED') {
                            $("#cook_status").html('**Voucher Released');
                        }
                        if (data.driver_voucher == 'VOUCHER PRINTED') {
                            $("#driver_status").html('**Voucher Released');
                        }


                    });


                    $('#txt_cook').select2({
                        ajax: {
                            url: url,
                            //  url: "searchRepresentative",
                            dataType: 'json',
                            delay: 250,
                            data: function (params) {
                                return {
                                    q: params.term,
                                    not: $("#txt_consultant_id").val()// search term
                                };
                            },
                            processResults: function (data) {
                                // parse the results into the format expected by Select2.
                                // since we are using custom formatting functions we do not need to
                                // alter the remote JSON data
                                return {
                                    results: data
                                };
                            },
                            cache: true
                        },
                        minimumInputLength: 2
                    });
//                    alert(url);
                });





            });


            $('#txt_driver').on("select2:select", function (e) {
                var sponsor_percentage = parseInt($("#txt_sponsor_percentage").val());
                var new_sponsor_percentage = sponsor_percentage - 1;
                $("#txt_sponsor_percentage").val(new_sponsor_percentage);
                $("#txt_driver_percentage").val(1);

            });

            $('#txt_driver').on("select2:unselect", function (e) {
                var sponsor_percentage = parseInt($("#txt_sponsor_percentage").val()) + 1;
                $("#txt_sponsor_percentage").val(sponsor_percentage);
                $("#txt_driver_percentage").val(0);

            });

            $('#txt_cook').on("select2:select", function (e) {
                var sponsor_percentage = parseInt($("#txt_sponsor_percentage").val());
                var new_sponsor_percentage = sponsor_percentage - 1;
                $("#txt_sponsor_percentage").val(new_sponsor_percentage);
                $("#txt_cook_percentage").val(1);

            });

            $('#txt_cook').on("select2:unselect", function (e) {
                var sponsor_percentage = parseInt($("#txt_sponsor_percentage").val()) + 1;
                $("#txt_sponsor_percentage").val(sponsor_percentage);
                $("#txt_cook_percentage").val(0);

            });

            $("#btn_save_commissions").click(function () {
                $.get("/commissions/saveCommissions", {
                    txt_sponsor_id: $("#txt_sponsor_id").val(),
                    txt_consultant_id: $("#txt_consultant_id").val(),
                    txt_associate_id: $("#txt_associate_id").val(),
                    txt_cook_id: $("#txt_cook").val(),
                    txt_driver_id: $("#txt_driver").val(),
                    txt_sponsor_percentage: $("#txt_sponsor_percentage").val(),
                    txt_associate_percentage: $("#txt_associate_percentage").val(),
                    txt_consultant_percentage: $("#txt_presenter_percentage").val(),
                    txt_driver_percentage: $("#txt_driver_percentage").val(),
                    txt_cook_percentage: $("#txt_cook_percentage").val(),
                    txt_po: $("#txt_po").val(),
                }).done(function (data) {
                    $("#txt_sponsor_percentage").attr('readonly');
                    $("#txt_associate_percentage").attr('readonly');
                    $("#txt_presenter_percentage").attr('readonly');
                    $("#txt_driver_percentage").attr('readonly');
                    $("#txt_cook_percentage").attr('readonly');
                    $("#btn_cancel_modify").addClass('hidden');
                    $("#btn_modify").removeClass('hidden');
                    $("#save-commission-modal").modal('show');
                });
            });


            $("#btn_modify").click(function () {

                if ($("#is_associate_voucher_printed").val() != 'VOUCHER PRINTED') {
                    $("#txt_associate_percentage").removeAttr('readonly');
                }
                if ($("#is_sponsor_voucher_printed").val() != 'VOUCHER PRINTED') {
                    $("#txt_sponsor_percentage").removeAttr('readonly');
                }
                if ($("#is_cook_voucher_printed").val() != 'VOUCHER PRINTED') {
                    $("#txt_cook_percentage").removeAttr('readonly');
                }
                if ($("#is_presenter_voucher_printed").val() != 'VOUCHER PRINTED') {
                    $("#txt_presenter_percentage").removeAttr('readonly');
                }
                if ($("#is_driver_voucher_printed").val() != 'VOUCHER PRINTED') {
                    $("#txt_driver_percentage").removeAttr('readonly');
                }

                $("#btn_cancel_modify").removeClass('hidden');
                $("#btn_modify").addClass('hidden');

                if($("#is_sponsor_voucher_printed").val() != 'VOUCHER PRINTED'){
                    $("#txt_cook").removeAttr('disabled');
                    $("#txt_driver").removeAttr('disabled');
                }

            });

            $("#btn_cancel_modify").click(function () {
                if ($("#is_associate_voucher_printed").val() != 'VOUCHER PRINTED') {
                    $("#txt_associate_percentage").attr('readonly', 'readonly');
                }
                if ($("#is_sponsor_voucher_printed").val() != 'VOUCHER PRINTED') {
                    $("#txt_sponsor_percentage").attr('readonly', 'readonly');
                }
                if ($("#is_cook_voucher_printed").val() != 'VOUCHER PRINTED') {
                    $("#txt_cook_percentage").attr('readonly', 'readonly');
                }
                if ($("#is_presenter_voucher_printed").val() != 'VOUCHER PRINTED') {
                    $("#txt_presenter_percentage").attr('readonly', 'readonly');
                }
                if ($("#is_driver_voucher_printed").val() != 'VOUCHER PRINTED') {
                    $("#txt_driver_percentage").attr('readonly', 'readonly');
                }

                $("#btn_cancel_modify").addClass('hidden');
                $("#btn_modify").removeClass('hidden');

                $("#txt_cook").select2('val','');
                $("#txt_driver").select2('val','');

                $("#txt_cook").attr('disabled', 'disabled');
                $("#txt_driver").attr('disabled', 'disabled');

            });
            $("#txt_cook").attr('disabled', 'disabled');

            $("#txt_driver").attr('disabled', 'disabled');


        });
    </script>
@endsection