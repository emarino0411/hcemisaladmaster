@extends('admin_template')

@section('additional_header')

    <link rel="stylesheet"
          href='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker-bs3.css")}}'>
    <link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/iCheck/all.css")}}'>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.css")}}'>
    <link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.min.css")}}'>
    <style type="text/css" media="print">
        @page {
            size: landscape;
        }
    </style>
    <style>
        /*
      Force table width to 100%
      */
        table.table-fixedheader {

        }

        /*
        Set table elements to block mode.  (Normally they are inline).
        This allows a responsive table, such as one where columns can be stacked
        if the display is narrow.
        */
        table.table-fixedheader, table.table-fixedheader > thead, table.table-fixedheader > tbody, table.table-fixedheader > thead > tr, table.table-fixedheader > tbody > tr, table.table-fixedheader > thead > tr > th, table.table-fixedheader > tbody > td {
            display: block;
        }

        table.table-fixedheader > thead > tr:after, table.table-fixedheader > tbody > tr:after {
            content: ' ';
            display: block;
            visibility: hidden;
            clear: both;
        }

        /*
        When scrolling the table, actually it is only the tbody portion of the
        table that scrolls (not the entire table: we want the thead to remain
        fixed).  We must specify an explicit height for the tbody.  We include
        100px as a default, but it can be overridden elsewhere.

        Also, we force the scrollbar to always be displayed so that the usable
        width for the table contents doesn't change (such as becoming narrower
        when a scrollbar is visible and wider when it is not).
        */
        table.table-fixedheader > tbody {
            overflow-y: scroll;
            height: 100%

        }

        /*
        We really don't want to scroll the thead contents, but we want to force
        a scrollbar to be displayed anyway so that the usable width of the thead
        will exactly match the tbody.
        */
        table.table-fixedheader > thead {
            overflow-y: scroll;
        }

        /*
        For browsers that support it (webkit), we set the background color of
        the unneeded scrollbar in the thead to make it invisible.  (Setting
        visiblity: hidden defeats the purpose, as this alters the usable width
        of the thead.)
        */
        table.table-fixedheader > thead::-webkit-scrollbar {
            background-color: inherit;
        }

        table.table-fixedheader > thead > tr > th:after, table.table-fixedheader > tbody > tr > td:after {
            content: ' ';
            display: table-cell;
            visibility: hidden;
            clear: both;
        }

        /*
        We want to set <th> and <td> elements to float left.
        We also must explicitly set the width for each column (both for the <th>
        and the <td>).  We set to 20% here a default placeholder, but it can be
        overridden elsewhere.
        */

        table.table-fixedheader > thead tr th, table.table-fixedheader > tbody tr td {
            float: left;
            word-wrap: break-word;
        }


    </style>
    @endsection

    @section('content')
            <!-- Main content -->
    <section class="content">

        <!-- Client Information -->
        <div class="row">
            <div class=" col-md-12 col-xs-12">
                <div class="row">
                    <!-- client -->
                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="box-header">
                                <h3 class="box-title"><i class="fa fa-user"></i> Search Client</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <form class="form-horizontal">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="txt_po_no" class="col-sm-4 control-label">Search
                                                        Client</label>

                                                    <div class="col-sm-8">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-user"></i>
                                                            </div>
                                                            <select id="slct_client" class="form-control">
                                                                @if($purchase!=null)
                                                                    <option value="{{$client->id}}">{{$client->last_name}}
                                                                        , {{$client->first_name}}</option>
                                                                @endif
                                                            </select>
                                                        </div>
                                                        <!-- /.input group -->
                                                    </div>
                                                </div>
                                            </div>
                                            {{--<div class="col-md-1 col-sm-1">--}}
                                            {{--<button id="btn_search" type="button" class="btn btn-primary"--}}
                                            {{--data-toggle="modal" data-target="#search-purchase"><i--}}
                                            {{--class="ion ion-search"></i> Search Client--}}
                                            {{--</button>--}}
                                            {{--</div>--}}
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="txt_po_no" class="col-sm-4 control-label">Select PO
                                                        No</label>

                                                    <div class="col-sm-8">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-edit"></i>
                                                            </div>
                                                            <select class="form-control" id="po_list">
                                                                @if($purchase!=null)
                                                                    <option value="{{$purchase->id}}">{{$purchase->purchase_order_no}}</option>
                                                                @else
                                                                    <option>Please select Client</option>
                                                                @endif
                                                            </select>
                                                        </div>
                                                        <!-- /.input group -->
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                    <!-- /client -->

                </div>
            </div>
        </div>
        <!-- / client information -->

        <!-- Purchase Information -->
        <div class="row">
            <div class=" col-md-12 col-xs-12">
                <div class="row">
                    <!-- client -->
                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="box-header">
                                <h3 class="box-title"><i class="fa fa-user"></i> Client Information</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">

                                <div class="col-md-12">

                                    <div class="col-md-3 col-md-offset-1">
                                        <div class="box-header with-border">
                                            <h3 class="box-title">Purchase Order</h3>
                                        </div>
                                        <!-- /.box-header -->
                                        <div class="box-body">
                                            <strong><i class="fa fa-edit"></i> PO Number</strong>

                                            <p class="text-muted" id="detail_po_no">
                                                N/A
                                            </p>

                                            <strong><i class="fa fa-forward"></i> Is Fast Track</strong>

                                            <p class="text-muted" id="detail_fast_track">
                                                N/A
                                            </p>


                                            <strong><i class="fa fa-calendar"></i> Sales Date</strong>

                                            <p class="text-muted" id="detail_sales_date">
                                                N/A
                                            </p>
                                        </div>
                                        <!-- /.box-body -->
                                    </div>
                                    <div class="col-md-3">
                                        <div class="box-header with-border">
                                            <h3 class="box-title">Payment Information</h3>
                                        </div>
                                        <!-- /.box-header -->
                                        <div class="box-body">
                                            <strong><i class="fa fa-list-alt"></i> Terms</strong>

                                            <p class="text-muted" id="detail_terms">
                                                N/A
                                            </p>

                                            <strong><i class="fa fa-calendar"></i> Total Transaction Price</strong>

                                            <p class="text-muted" id="detail_total_price">
                                                N/A
                                            </p>
                                            <input type="hidden" id="txt_detail_total_price">

                                            <strong><i class="fa fa-calendar"></i> Payment Status</strong>

                                            <p class="text-muted" id="detail_payment_status">
                                                N/A
                                            </p>
                                        </div>
                                        <!-- /.box-body -->
                                    </div>
                                    <div class="col-md-5">
                                        <!-- /.box-header -->
                                        <div class="box-header">
                                            <table class="table table-fixedheader table-bordered"
                                                   style="margin-top:-10px;">
                                                <thead>
                                                <tr>
                                                    <th width="60%">Purchased Items</th>
                                                    <th width="20%">Qty</th>
                                                    <th width="20%">Is Gift</th>
                                                </tr>
                                                </thead>
                                                <tbody id="tbl_purchased_items">
                                                <tr>
                                                    <td width="60%">Professional Set</td>
                                                    <td width="20%">1</td>
                                                    <td width="20%"><span class="label label-success">Yes</span></td>
                                                </tr>

                                                </tbody>

                                            </table>
                                        </div>
                                        <!-- /.box-body -->
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                    <!-- /client -->

                </div>
            </div>
        </div>
        <!-- / client information -->

        <!-- Commission Information -->
        <div class="row">
            <div class=" col-md-12 col-xs-12">
                <div class="row">
                    <!-- client -->
                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="box-header">
                                <h3 class="box-title"><i class="fa fa-user"></i> Commission Information</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <form class="form-horizontal">
                                            <!-- Commission Agent -->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="slct_agent" class="col-sm-4 control-label">Select
                                                        Agent</label>

                                                    <div class="col-sm-8">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-user"></i>
                                                            </div>
                                                            <select class="form-control" id="slct_agent">
                                                                @if($purchase!=null)
                                                                    <option value="{{$commission->id}}">{{$agent->last_name}}
                                                                        , {{$agent->first_name}}</option>
                                                                @else
                                                                    <option>Please select Agent</option>
                                                                @endif
                                                            </select>

                                                        </div>
                                                        <!-- /.input group -->
                                                    </div>

                                                </div>
                                            </div>
                                            <!-- Commission Agent -->

                                            <!-- Release Date -->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="txt_release_date" class="col-sm-4 control-label">Release
                                                        Date</label>

                                                    <div class="col-sm-8">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-calendar"></i>
                                                            </div>
                                                            <input type="text" class="form-control pull-right"
                                                                   id="txt_release_date">
                                                        </div>
                                                        <!-- /.input group -->
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Release Date -->

                                            <div class="col-md-12">
                                                <hr>
                                            </div>

                                            <!-- Commissions Percentage -->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="txt_commission_percentage"
                                                           class="col-sm-4 control-label"> Commission Percentage</label>

                                                    <div class="col-sm-8">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-percent"></i>
                                                            </div>
                                                            <input type="text" class="form-control pull-right" value=""
                                                                   readonly style="background-color:white"
                                                                   id="txt_commission_percentage">
                                                        </div>
                                                        <!-- /.input group -->
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Commissions Percentage -->

                                            <!-- Commissions Amount -->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="txt_commission_amount" class="col-sm-4 control-label">
                                                        Commission Amount</label>

                                                    <div class="col-sm-8">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-rub"></i>
                                                            </div>
                                                            <input type="text" class="form-control pull-right" value=""
                                                                   readonly
                                                                   style="background-color:white;color:red;font-size:30px;"
                                                                   id="txt_commission_amount">
                                                        </div>
                                                        <!-- /.input group -->
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Commissions Amount -->
                                        </form>
                                    </div>
                                    <div class="col-md-12">
                                        <form class="form-horizontal">


                                            <!-- Release Date -->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="slct_bank" class="col-sm-4 control-label">Check
                                                        Number</label>

                                                    <div class="col-sm-8">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-edit"></i>
                                                            </div>
                                                            <select id="slct_bank"
                                                                    style="width: 100%;height:100%">
                                                                <option value="BDO">BDO</option>
                                                                <option value="Metrobank">Metrobank</option>
                                                                <option value="Landbank">Landbank</option>
                                                                <option value="BPI">BPI</option>
                                                                <option value="PNB">PNB</option>
                                                                <option value="DBP">DBP</option>
                                                                <option value="Security Bank">Security Bank</option>
                                                                <option value="Chinabank">Chinabank</option>
                                                                <option value="RCBC">RCBC</option>
                                                                <option value="Unionbank">Unionbank</option>
                                                                <option value="Citibank">Citibank</option>
                                                                <option value="UCPB">UCPB</option>
                                                                <option value="EastWest Bank">EastWest Bank</option>
                                                                <option value="HSBC">HSBC</option>
                                                                <option value="Philtrust Bank">Philtrust Bank</option>
                                                                <option value="AUB">AUB</option>
                                                                <option value="Bank of Commerce">Bank of Commerce
                                                                </option>
                                                                <option value="Maybank">Maybank</option>
                                                                <option value="PBCom">PBCom</option>
                                                                <option value="Standard Chartered">Standard Chartered
                                                                </option>
                                                                <option value="The Bank of Tokyo-Mitsubishi UFJ, Ltd.">
                                                                    The Bank of Tokyo-Mitsubishi UFJ, Ltd.
                                                                </option>
                                                                <option value="Veterans Bank">Veterans Bank</option>
                                                                <option value="Robinsons Bank">Robinsons Bank</option>
                                                                <option value="Deutsche Bank">Deutsche Bank</option>
                                                                <option value="ANZ">ANZ</option>
                                                                <option value="JPMorgan Chase">JPMorgan Chase</option>
                                                                <option value="Mizuho Bank, Ltd. Manila Branch">Mizuho
                                                                    Bank, Ltd. Manila Branch
                                                                </option>
                                                                <option value="ING Group N.V.">ING Group N.V.</option>
                                                                <option value="Chinatrust">Chinatrust</option>
                                                                <option value="Bank of China">Bank of China</option>
                                                                <option value="Bank of America">Bank of America</option>
                                                                <option value="Mega International Commercial Bank">Mega
                                                                    International Commercial Bank
                                                                </option>
                                                                <option value="Korea Exchange Bank">Korea Exchange
                                                                    Bank
                                                                </option>
                                                                <option value="Bangkok Bank">Bangkok Bank</option>
                                                                <option value="Al-Amanah Islamic Investment Bank of the Philippines">
                                                                    Al-Amanah Islamic Investment Bank of the Philippines
                                                                </option>
                                                                <option value="CitySavings Bank ">CitySavings Bank
                                                                </option>
                                                                <option value="Citystate Savings Bank">Citystate Savings
                                                                    Bank
                                                                </option>
                                                                <option value="Equicom">Equicom</option>
                                                                <option value="Farmers Savings and Loan Bank, Inc.">
                                                                    Farmers Savings and Loan Bank, Inc.
                                                                </option>
                                                                <option value="Malayan">Malayan</option>
                                                                <option value="Philippine Business Bank">Philippine
                                                                    Business Bank
                                                                </option>
                                                                <option value="Postalbank">Postalbank</option>
                                                                <option value="PS Bank">PS Bank</option>
                                                                <option value="Plantersbank">Plantersbank</option>
                                                                <option value="Producers Bank">Producers Bank</option>
                                                                <option value="Philippine Resources Savings Banking">
                                                                    Philippine Resources Savings Banking
                                                                </option>
                                                                <option value="Queen City Development Bank">Queen City
                                                                    Development Bank
                                                                </option>
                                                                <option value="Rizal Microbank">Rizal Microbank</option>
                                                                <option value="Sterling Bank of Asia">Sterling Bank of
                                                                    Asia
                                                                </option>
                                                                <option value="Enterprise Bank">Enterprise Bank</option>
                                                                <option value="Cooperative Bank of Benguet">Cooperative
                                                                    Bank of Benguet
                                                                </option>
                                                                <option value="Consolidated Cooperative Bank">
                                                                    Consolidated Cooperative Bank
                                                                </option>
                                                                <option value="Cooperative Bank of Misamis Oriental">
                                                                    Cooperative Bank of Misamis Oriental
                                                                </option>
                                                                <option value="Cooperative Bank of Tarlac">Cooperative
                                                                    Bank of Tarlac
                                                                </option>
                                                                <option value="Network Consolidated Cooperative Bank">
                                                                    Network Consolidated Cooperative Bank
                                                                </option>
                                                                <option value="FICO Bank">FICO Bank</option>
                                                                <option value="Zamboanga del Norte Cooperative Bank ">
                                                                    Zamboanga del Norte Cooperative Bank
                                                                </option>
                                                                <option value="One Network Bank">One Network Bank
                                                                </option>
                                                                <option value="Bank of Makati">Bank of Makati</option>
                                                                <option value="CARD Bank">CARD Bank</option>
                                                                <option value="Bank of Florida">Bank of Florida</option>
                                                                <option value="Insular Bank">Insular Bank</option>
                                                                <option value="Rang-ay Rural Bank">Rang-ay Rural Bank
                                                                </option>
                                                                <option value="Cantilan Bank">Cantilan Bank</option>
                                                                <option value="Bangko Kabayan">Bangko Kabayan</option>
                                                                <option value="Katipunan Bank">Katipunan Bank</option>
                                                                <option value="Tanay Rural Bank">Tanay Rural Bank
                                                                </option>
                                                                <option value="Rural Bank of Porak">Rural Bank of
                                                                    Porak
                                                                </option>
                                                                <option value="Guagua Rural Bank">Guagua Rural Bank
                                                                </option>
                                                                <option value="Rural Bank of Tanza (Mabuhay Bank)">Rural
                                                                    Bank of Tanza (Mabuhay Bank)
                                                                </option>
                                                                <option value="Camalig Bank">Camalig Bank</option>
                                                                <option value="Rural Bank">Rural Bank</option>
                                                            </select>
                                                        </div>
                                                        <!-- /.input group -->
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Release Date -->

                                            <!-- Release Date -->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="txt_check_date" class="col-sm-4 control-label">Check
                                                        Number</label>

                                                    <div class="col-sm-8">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-edit"></i>
                                                            </div>
                                                            <input type="text" class="form-control pull-right"
                                                                   id="txt_check_no">
                                                        </div>
                                                        <!-- /.input group -->
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Release Date -->

                                            <!-- Release Date -->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="txt_check_date" class="col-sm-4 control-label">Voucher
                                                        Number</label>

                                                    <div class="col-sm-8">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-edit"></i>
                                                            </div>
                                                            <input type="text" class="form-control pull-right"
                                                                   id="txt_voucher_no">
                                                        </div>
                                                        <!-- /.input group -->
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Release Date -->


                                            <!-- Release Date -->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="txt_check_date" class="col-sm-4 control-label">Check
                                                        Date</label>

                                                    <div class="col-sm-8">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-calendar"></i>
                                                            </div>
                                                            <input type="text" class="form-control pull-right"
                                                                   id="txt_check_date">
                                                        </div>
                                                        <!-- /.input group -->
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Release Date -->


                                            <!-- Commissions Amount -->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="lbl_commission_status" class="col-sm-4 control-label">
                                                        Commission Status</label>

                                                    <div class="col-sm-8">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-rub"></i>
                                                            </div>
                                                            <label class="form-control"
                                                                   id="lbl_commission_status"></label>
                                                        </div>
                                                        <!-- /.input group -->
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Commissions Amount -->

                                            <!-- Commissions Remarks -->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="lbl_commission_status" class="col-sm-4 control-label">
                                                        Commission Remarks</label>

                                                    <div class="col-sm-8">
                                                        <textarea id="txt_remarks" class="form-control"></textarea>
                                                        <!-- /.input group -->
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Commissions Remarks -->

                                            <div class="col-md-12">
                                                <hr style="margin-bottom:1%">
                                            </div>

                                            <div class="col-md-12">
                                                <h4 class="box-title"><i class="fa fa-minus-square"></i> Deductions</h4>
                                            </div>

                                            <!-- Account Balance -->
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="txt_account_balance" class="col-sm-3 control-label">
                                                        Account Balance</label>

                                                    <div class="col-sm-3">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-percent"></i>
                                                            </div>
                                                            <input type="number" class="form-control pull-right"
                                                                   value="0" id="txt_account_balance">
                                                        </div>
                                                        <!-- /.input group -->
                                                    </div>
                                                    <div class="col-sm-5">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-rub"></i>
                                                            </div>
                                                            <input type="text" class="form-control pull-right" value=""
                                                                   readonly style="background-color:white;"
                                                                   id="txt_account_balance_amount">
                                                        </div>
                                                        <!-- /.input group -->
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Account Balance -->

                                            <!-- WSP -->
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="txt_wsp" class="col-sm-3 control-label"> WSP</label>

                                                    <div class="col-sm-3">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-percent"></i>
                                                            </div>
                                                            <input type="number" class="form-control pull-right"
                                                                   value="" id="txt_wsp">
                                                        </div>
                                                        <!-- /.input group -->
                                                    </div>
                                                    <div class="col-sm-5">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-rub"></i>
                                                            </div>
                                                            <input type="text" class="form-control pull-right" value=""
                                                                   readonly style="background-color:white"
                                                                   id="txt_wsp_amount">
                                                        </div>
                                                        <!-- /.input group -->
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- WSP -->


                                            <!-- Others -->
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="txt_wsp" class="col-sm-3 control-label">
                                                        Description</label>

                                                    <div class="col-sm-3">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-edit"></i>
                                                            </div>
                                                            <input type="text" class="form-control pull-right" value=""
                                                                   id="txt_others_desc">
                                                        </div>
                                                        <!-- /.input group -->
                                                    </div>
                                                    <div class="col-sm-5">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-rub"></i>
                                                            </div>
                                                            <input type="number" class="form-control pull-right"
                                                                   value="" id="txt_others_amount">
                                                        </div>
                                                        <!-- /.input group -->
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Others -->

                                            <div class="col-md-12">
                                                <hr style="margin-bottom:1%">
                                            </div>


                                            <!-- Prepared By -->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="txt_commission_amount" class="col-sm-5 control-label">
                                                        Voucher Prepared By</label>

                                                    <div class="col-sm-7">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-check"></i>
                                                            </div>
                                                            <input type="text" class="form-control pull-right"
                                                                   value="{{$name->first_name}} {{$name->last_name}}"
                                                                   readonly style="background-color:white"
                                                                   id="txt_prepared_by">
                                                            <input type="hidden" class="form-control pull-right"
                                                                   value="{{$name->id}}" readonly
                                                                   style="background-color:white" id="txt_user_id">
                                                        </div>
                                                        <!-- /.input group -->
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Prepared By  -->

                                            <!-- Commissions Amount -->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="txt_commission_amount" class="col-sm-5 control-label">
                                                        Commission After Deductions</label>

                                                    <div class="col-sm-7">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-rub"></i>
                                                            </div>
                                                            <input type="text" class="form-control pull-right" value=""
                                                                   readonly
                                                                   style="background-color:white;color:red;font-size:30px;"
                                                                   id="txt_receivable_commission">
                                                        </div>
                                                        <!-- /.input group -->
                                                    </div>
                                                </div>
                                            </div>
                                        </form>

                                        <br>
                                        <!-- buttons  -->
                                        <div class="col-md-4 col-md-offset-8">
                                            <div class="row" id="div_modify_buttons">
                                                <div class="col-md-12">


                                                    <button class="btn btn-info pull-right" id="btn_save_print"> Print
                                                        Voucher
                                                    </button>
                                                    <button class="btn btn-success pull-right" style="margin-right:1%"
                                                            id="btn_save_exit"> Save
                                                    </button>

                                                </div>
                                            </div>
                                            <!-- /btn-group -->
                                        </div>
                                        <!-- /.buttons  -->

                                    </div><!-- col-md-12 -->
                                </div><!-- /row -->
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                    <!-- /client -->

                </div>
            </div>
        </div>
        <!-- / Commission information -->

        {{--MODAL--}}
        <div id="print-voucher-option" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-blue-gradient">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><i class="ion ion-check"></i> Please Choose Print Option</h4>
                    </div>
                    <div class="modal-body">
                        <button type="button" class="btn btn-default" id="btn_print_voucher" data-dismiss="modal">Print
                            Voucher Form
                        </button>
                        <button type="button" class="btn btn-default" id="btn_print_values" data-dismiss="modal">Print
                            on Voucher
                        </button>
                        <button class="btn btn-danger pull-right" data-dismiss="modal" id="btn_print"> Print Check
                        </button>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" id="btn_close_modal" data-dismiss="modal">Close
                        </button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        {{--MOdal End--}}

        {{--MODAL--}}
        <div id="save-commission-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-blue-gradient">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><i class="fa fa-check"></i> Save Commission</h4>
                    </div>
                    <div class="modal-body">
                        Save Commission OK
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" id="btn_close_modal" data-dismiss="modal">Close
                        </button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        {{--MOdal End--}}


        <input type="hidden" id="txt_sponsor_id">
        <input type="hidden" id="txt_consultant_id">
        <input type="hidden" id="txt_associate_id">
        <input type="hidden" id="txt_cook_id">
        <input type="hidden" id="txt_driver_id">
        <input type="hidden" id="txt_po">


        @if($agent != null)
            <input type="hidden" id="rep_id" value={{$agent->id}}>
            <input type="hidden" id="commission_id" value={{$commission->id}}>
        @else
            <input type="hidden" id="rep_id">
            <input type="hidden" id="commission_id">
        @endif


    </section>
@endsection

@section('additional_footer')

    <script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/jquery.dataTables.min.js")}}'></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js")}}'></script>

    <script src='{{ asset("/bower_components/AdminLTE/plugins/jQueryUI/jquery-ui.min.js")}}'></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.full.min.js")}}'></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker.js")}}'></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/iCheck/icheck.min.js")}}'></script>

    <script>
        $(document).ready(function () {
            var representative_details;
            var check_release_date = '';
            var check_date = '';

            $('#txt_release_date').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true
            });
            $('#txt_release_date').on('apply.daterangepicker', function (ev, picker) {
                check_release_date = picker.startDate.format('YYYY-MM-DD');

            });

            $('#txt_check_date').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true
            });
            $('#txt_check_date').on('apply.daterangepicker', function (ev, picker) {
                check_date = picker.startDate.format('YYYY-MM-DD');

            });


            $('#slct_client').select2({
                ajax: {
                    url: "/purchases/searchClient",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term // search term
                        };
                    },
                    processResults: function (data) {
                        // parse the results into the format expected by Select2.
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data
                        return {
                            results: data
                        };
                    },
                    cache: true
                },
                minimumInputLength: 2
            });

            $('#slct_client').on("select2:select", function (e) {
                $.get("/payments/getClientPO", {
                    client_id: $('#slct_client').val()
                }).done(function (data) {
                    $("#po_list").empty();
                    $("#po_list").append($('<option>', {value: ''})
                            .text('Select PO Number'));
                    $("#po_list").select2({
                        data: data
                    });
                });
            });


            $('#po_list').on("select2:select", function (e) {
                updateAgentList();
            });

            function updateAgentList() {
                $("#txt_po").val($('#po_list').val());

                $.get("/commissions/getClientPurchaseDetails", {
                    po_id: $('#po_list').val()
                }).done(function (data) {
                    console.log(data);
                    var detail = data[0];
                    $("#detail_po_no").html(detail.purchase_order_no);
                    $("#detail_sales_date").html(detail.purchase_date);
                    $("#detail_terms").html(detail.terms);
                    $("#detail_payment_status").html(detail.transaction_status);
                    $("#detail_fast_track").html(detail.fast_track);
                    $("#detail_delivery_date").html(detail.delivery_date);
                });

                $.get("/commissions/getPurchasedItems", {
                    po_id: $('#po_list').val()
                }).done(function (data) {
                    console.log(data);
                    console.log('panget')
                    var total_price = 0;
                    $("#tbl_purchased_items").empty();
                    for (ctr = 0; ctr < data.length; ctr++) {
                        $("#tbl_purchased_items").append(
                                '<tr>' +
                                '<td width="60%">' + data[ctr].description + '</td>' +
                                '<td width="20%">' + data[ctr].qty + '</td>' +
                                '<td width="20%">' + data[ctr].gift + '</td>' +
                                '</tr>'
                        );
                        total_price += (parseInt(data[ctr].qty) * parseInt(data[ctr].item_price));
                    }
                    $("#detail_total_price").html(total_price);
                    $("#txt_detail_total_price").val(total_price);


                });

                $("#txt_commission_percentage").val('');
                $("#txt_commission_amount").val('');
                $("#txt_wsp").val('');
                $("#txt_wsp_amount").val('');
                $("#txt_account_balance").val(0);
                $("#txt_account_balance_amount").val('');
                $("#txt_others_desc").val('');
                $("#txt_others_amount").val('');

                //alert($('#po_list').val());

                $.get("/commissions/getAgentsList", {
                    po_id: $('#po_list').val()
                }).done(function (data) {
                    console.log("agents list.>>>>")
                    console.log(data);
                    if (data.length == 0) {
                        $("#slct_agent").empty();
                        $("#slct_agent").append('<option>PO Commission Has Not Been Distributed</option>');
                        return false;
                    }
                    representative_details = data;
                    console.log(data);
                    console.log('agent');
                    $("#slct_agent").empty();
                    $("#slct_agent").append('<option>Please Select Agent</option>');
                    for (ctr = 0; ctr < data.length; ctr++) {
                        $("#slct_agent").append(
                                '<option value="' + data[ctr].id + '">[' + data[ctr].type.toUpperCase() + '] ' + data[ctr].first_name.toUpperCase() + ' ' + data[ctr].last_name.toLocaleUpperCase() + '</option>'
                        );
                    }

                });

            }

            $("#slct_agent").change(function () {
                updateAgent();

            });

            function updateAgent() {

                var agent;
                var commission = 0;
                var wsp_amount = 0;
                var account_balance_amount_deduction = 0;
                var receivable_commission = 0;
                if ($("#slct_agent").val() == "") {
                    $("#txt_commission_percentage").val("");
                    $("#txt_commission_amount").val("");
                    $("#txt_wsp").val("");
                    $("#txt_wsp_amount").val("");
                    $("#txt_account_balance_amount").val("");
                    $("#txt_receivable_commission").val("");
                    $("#lbl_commission_status").html("");
                    $("#txt_release_date").val("");
                    $("#txt_remarks").val("");
                    $("#btn_save_exit").removeAttr('disabled');
                    $("#btn_save_print").removeAttr('disabled');
                } else {
                    //alert($("#slct_agent option:selected").text());
                    for (ctr = 0; ctr < representative_details.length; ctr++) {
                        agent = representative_details[ctr];
                        console.log(representative_details[ctr].id + "==" +$("#slct_agent").val());
                        if (representative_details[ctr].id == $("#slct_agent").val()) {
                            console.log(agent);
                            $("#txt_commission_percentage").val(agent.percentage);

                            commission = parseFloat(parseInt($("#txt_detail_total_price").val()) * .83 * parseFloat(agent.percentage / 100)).toFixed(2);
                            $("#txt_commission_amount").val(commission);

                            if (agent.wsp_percentage != "") {
                                $("#txt_wsp").val(agent.wsp_percentage);
                            } else {
                                $("#txt_wsp").val(agent.agent_wsp);
                                agent.wsp_percentage = agent.agent_wsp;
                            }

                            if (agent.account_balance_percentage != "") {
                                $("#txt_account_balance").val(agent.account_balance_percentage);
                            } else {
                                $("#txt_account_balance").val(agent.agent_account_balance);
                                agent.account_balance_percentage = agent.agent_account_balance;
                            }


                            wsp_amount = parseFloat(commission * parseFloat(agent.wsp_percentage / 100)).toFixed(2);
                            $("#txt_wsp_amount").val(wsp_amount);
                            account_balance_amount_deduction = parseFloat(commission * parseFloat(agent.account_balance_percentage / 100)).toFixed(2);
                            $("#txt_account_balance_amount").val(account_balance_amount_deduction);

                            receivable_commission = commission - wsp_amount - account_balance_amount_deduction - agent.other_deduction_amount;
                            $("#txt_receivable_commission").val(receivable_commission.toFixed(2));
                            $("#lbl_commission_status").html(agent.status);
                            $("#txt_release_date").val(agent.due_date);
                            $("#txt_check_date").val(agent.check_date);

                            check_release_date = agent.due_date;
                            check_date = agent.check_date;

                            $("#txt_voucher_no").val(agent.voucher_no);
                            $("#txt_check_no").val(agent.check_no);
                            $("#txt_others_desc").val(agent.other_deduction);
                            $("#txt_others_amount").val(agent.other_deduction_amount);
                            $("#txt_remarks").val(agent.remarks);


                            // if(agent.status=='RELEASED'){
                            //     $("#btn_save_exit").attr('disabled','disabled');
                            //     $("#btn_save_print").attr('disabled','disabled');
                            // }else{
                            //     $("#btn_save_exit").removeAttr('disabled');
                            //     $("#btn_save_print").removeAttr('disabled');
                            // }


                        }
                    }
                }
            }

            $("#txt_wsp").change(function () {
                var agent_commission = $("#txt_commission_amount").val();
                var account_balance = $("#txt_account_balance_amount").val();
                var others_amount = $("#txt_others_amount").val();
                var wsp = $("#txt_wsp").val();
                var wsp_amount = 0;
                var receivable_commission = 0;

                wsp_amount = parseFloat(agent_commission * parseFloat(wsp / 100)).toFixed(2);
                $("#txt_wsp_amount").val(wsp_amount);


                receivable_commission = agent_commission - wsp_amount - account_balance - others_amount;
                $("#txt_receivable_commission").val(receivable_commission.toFixed(2));
            });

            $("#txt_account_balance").change(function () {
                var agent_commission = $("#txt_commission_amount").val();
                var wsp_amount = $("#txt_wsp_amount").val();
                var account_balance = $("#txt_account_balance").val();
                var others_amount = $("#txt_others_amount").val();

                var account_balance_amount = 0;

                account_balance_amount = parseFloat(agent_commission * parseFloat(account_balance / 100)).toFixed(2);
                $("#txt_account_balance_amount").val(account_balance_amount);

                receivable_commission = agent_commission - wsp_amount - account_balance_amount - others_amount;
                $("#txt_receivable_commission").val(receivable_commission.toFixed(2));
            });

            $("#txt_others_amount").change(function () {
                var agent_commission = $("#txt_commission_amount").val();
                var wsp_amount = $("#txt_wsp_amount").val();
                var account_balance = $("#txt_account_balance_amount").val();
                var others_amount = $("#txt_others_amount").val();

                receivable_commission = agent_commission - wsp_amount - account_balance - others_amount;
                $("#txt_receivable_commission").val(receivable_commission.toFixed(2));
            });

            $("#slct_agent").change(function () {
                //alert($("#slct_agent").val());
            });


            $("#btn_save_exit").click(function () {
                $.get("/commissions/saveVoucherCommissions", {
                    agent_id: $("#slct_agent").val(),
                    po_no: $("#po_list").val(),
                    release_date: check_release_date,
                    account_balance_percentage: $("#txt_account_balance").val(),
                    wsp_percentage: $("#txt_wsp").val(),
                    other_deduction: $("#txt_others_desc").val(),
                    other_deduction_amount: $("#txt_others_amount").val(),
                    received_commission: $("#txt_receivable_commission").val(),
                    user_user_id: $("#txt_user_id").val(),
                    status: 'FOR RELEASE',
                    _check_date: check_date,
                    check_no: $("#txt_check_no").val(),
                    voucher_no: $("#txt_voucher_no").val(),
                    remarks: $("#txt_remarks").val(),
                    bank: $("#slct_bank").val()
                }).done(function (data) {
                    $("#save-commission-modal").modal('show');
                });
            });
            var id = 0;
            $("#btn_save_print").click(function () {
                $.get("/commissions/saveVoucherCommissions", {
                    agent_id: $("#slct_agent").val(),
                    po_no: $("#po_list").val(),
                    release_date: check_release_date,
                    account_balance_percentage: $("#txt_account_balance").val(),
                    wsp_percentage: $("#txt_wsp").val(),
                    other_deduction: $("#txt_others_desc").val(),
                    other_deduction_amount: $("#txt_others_amount").val(),
                    received_commission: $("#txt_receivable_commission").val(),
                    user_user_id: $("#txt_user_id").val(),
                    status: 'RELEASED',
                    _check_date: check_date,
                    check_no: $("#txt_check_no").val(),
                    voucher_no: $("#txt_voucher_no").val(),
                    bank: $("#slct_bank").val()
                }).done(function (data) {
                    console.log(data);

                    // window.print();
                    $("#print-voucher-option").modal('show');
                    id = data[0].id;
                });
            });
            $("#slct_bank").select2();
            $("#btn_print_voucher").click(function () {
                $.get("/commissions/saveVoucherCommissions", {
                    agent_id: $("#slct_agent").val(),
                    po_no: $("#po_list").val(),
                    release_date: check_release_date,
                    account_balance_percentage: $("#txt_account_balance").val(),
                    wsp_percentage: $("#txt_wsp").val(),
                    other_deduction: $("#txt_others_desc").val(),
                    other_deduction_amount: $("#txt_others_amount").val(),
                    received_commission: $("#txt_receivable_commission").val(),
                    user_user_id: $("#txt_user_id").val(),
                    status: 'VOUCHER PRINTED',
                    _check_date: check_date,
                    check_no: $("#txt_check_no").val(),
                    voucher_no: $("#txt_voucher_no").val(),
                    bank: $("#slct_bank").val()
                }).done(function (data) {
                    console.log(data);

                    // window.print();
                    $("#print-voucher-option").modal('show');
                    id = data[0].id;
                });
                window.open("/commissions/printVoucher/" + id);
            });
            $("#btn_print_values").click(function () {
                $.get("/commissions/saveVoucherCommissions", {
                    agent_id: $("#slct_agent").val(),
                    po_no: $("#po_list").val(),
                    release_date: check_release_date,
                    account_balance_percentage: $("#txt_account_balance").val(),
                    wsp_percentage: $("#txt_wsp").val(),
                    other_deduction: $("#txt_others_desc").val(),
                    other_deduction_amount: $("#txt_others_amount").val(),
                    received_commission: $("#txt_receivable_commission").val(),
                    user_user_id: $("#txt_user_id").val(),
                    status: 'VOUCHER PRINTED',
                    _check_date: check_date,
                    check_no: $("#txt_check_no").val(),
                    voucher_no: $("#txt_voucher_no").val(),
                    bank: $("#slct_bank").val()
                }).done(function (data) {
                    console.log(data);
                    id = data[0].representative_id;
                });
                window.open("/commissions/printVoucherValue/" + id);
            })
            $("#btn_print").click(function () {
                $.get("/commissions/saveVoucherCommissions", {
                    agent_id: $("#slct_agent").val(),
                    po_no: $("#po_list").val(),
                    release_date: check_release_date,
                    account_balance_percentage: $("#txt_account_balance").val(),
                    wsp_percentage: $("#txt_wsp").val(),
                    other_deduction: $("#txt_others_desc").val(),
                    other_deduction_amount: $("#txt_others_amount").val(),
                    received_commission: $("#txt_receivable_commission").val(),
                    user_user_id: $("#txt_user_id").val(),
                    status: 'CHECK PRINTED',
                    _check_date: check_date,
                    check_no: $("#txt_check_no").val(),
                    voucher_no: $("#txt_voucher_no").val(),
                    bank: $("#slct_bank").val()
                }).done(function (data) {
                    console.log(data);
                    id = data[0].id;
                });
                window.open("/commissions/printCheck/" + id);
            });

            if (parseInt($('#po_list').val()) != 0) {

                $("#txt_po").val($('#po_list').val());

                $.get("/commissions/getClientPurchaseDetails", {
                    po_id: $('#po_list').val()
                }).done(function (data) {
                    console.log(data);
                    var detail = data[0];
                    $("#detail_po_no").html(detail.purchase_order_no);
                    $("#detail_sales_date").html(detail.purchase_date);
                    $("#detail_terms").html(detail.terms);
                    $("#detail_payment_status").html(detail.transaction_status);
                    $("#detail_fast_track").html(detail.fast_track);
                    $("#detail_delivery_date").html(detail.delivery_date);
                });

                $.get("/commissions/getPurchasedItems", {
                    po_id: $('#po_list').val()
                }).done(function (data) {
                    console.log(data);
                    console.log('panget')
                    var total_price = 0;
                    $("#tbl_purchased_items").empty();
                    for (ctr = 0; ctr < data.length; ctr++) {
                        $("#tbl_purchased_items").append(
                                '<tr>' +
                                '<td width="60%">' + data[ctr].description + '</td>' +
                                '<td width="20%">' + data[ctr].qty + '</td>' +
                                '<td width="20%">' + data[ctr].gift + '</td>' +
                                '</tr>'
                        );
                        total_price += (parseInt(data[ctr].qty) * parseInt(data[ctr].item_price));
                    }
                    $("#detail_total_price").html(total_price);
                    $("#txt_detail_total_price").val(total_price);


                });

                $("#txt_commission_percentage").val('');
                $("#txt_commission_amount").val('');
                $("#txt_wsp").val('');
                $("#txt_wsp_amount").val('');
                $("#txt_account_balance").val(0);
                $("#txt_account_balance_amount").val('');
                $("#txt_others_desc").val('');
                $("#txt_others_amount").val('');


                $.get("/commissions/getAgentsList", {
                    po_id: $('#po_list').val()
                }).done(function (data) {
                    console.log("agents list.>>>>")
                    console.log(data);
                    if (data.length == 0) {
                        $("#slct_agent").empty();
                        $("#slct_agent").append('<option>PO Commission Has Not Been Distributed</option>');
                        return false;
                    }
                    representative_details = data;
                    var agent = $("#commission_id").val();
//                    alert(agent);
                    $("#slct_agent select").val(agent);
                    console.log($("#slct_agent").val());
                    updateAgent();

                });


            }

        });
    </script>
@endsection
