<?php
$total_commission_amount = $total_commission * ($commissions->percentage / 100);

$account_bal = $total_commission * ($commissions->percentage / 100) * ($account_balance_percentage / 100);
$wsp_amount = $total_commission * ($commissions->percentage / 100) * ($wsp / 100);
$other_deduction = $commissions->other_deduction_amount;

$total_commission_amount_receivable = $total_commission_amount - $account_bal - $wsp_amount - $commissions->other_deduction_amount;

function convert_number_to_words($number)
{

    $hyphen = '-';
    $conjunction = ' and ';
    $separator = ', ';
    $negative = 'negative ';
    $decimal = ' and ';
    $dictionary = array(
            0 => 'Zero',
            1 => 'One',
            2 => 'Two',
            3 => 'Three',
            4 => 'Four',
            5 => 'Five',
            6 => 'Six',
            7 => 'Seven',
            8 => 'Eight',
            9 => 'Nine',
            10 => 'Ten',
            11 => 'Eleven',
            12 => 'Twelve',
            13 => 'Thirteen',
            14 => 'Fourteen',
            15 => 'Fifteen',
            16 => 'Sixteen',
            17 => 'Seventeen',
            18 => 'Eighteen',
            19 => 'Nineteen',
            20 => 'Twenty',
            30 => 'Thirty',
            40 => 'Forty',
            50 => 'Fifty',
            60 => 'Sixty',
            70 => 'Seventy',
            80 => 'Eighty',
            90 => 'Ninety',
            100 => 'Hundred',
            1000 => 'Thousand',
            1000000 => 'Million',
            1000000000 => 'Billion',
            1000000000000 => 'Trillion',
            1000000000000000 => 'Quadrillion',
            1000000000000000000 => 'Quintillion'
    );

    if (!is_numeric($number)) {
        return false;
    }

    if (($number >= 0 && (int)$number < 0) || (int)$number < 0 - PHP_INT_MAX) {
        // overflow
        trigger_error(
                'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
                E_USER_WARNING
        );
        return false;
    }

    if ($number < 0) {
        return $negative . convert_number_to_words(abs($number));
    }

    $string = $fraction = null;

    if (strpos($number, '.') !== false) {
        list($number, $fraction) = explode('.', $number);
    }
    $number = abs($number);
    switch (true) {
        case $number < 21:
            $string = $dictionary[$number];
            break;
        case $number < 100:
            $tens = ((int)($number / 10)) * 10;
            $units = $number % 10;
            $string = $dictionary[$tens];
            if ($units != 0) {
                $string .= $hyphen . $dictionary[$units];
            }
            break;
        case $number < 1000:
            $hundreds = $number / 100;
            $remainder = $number % 100;
            $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
            if ($remainder) {
                $string .= $conjunction . convert_number_to_words($remainder);
            }
            break;
        default:
            $baseUnit = pow(1000, floor(log($number, 1000)));
            $numBaseUnits = (int)($number / $baseUnit);
            $remainder = $number % $baseUnit;
            $string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];


            if ($remainder) {
                $string .= $remainder < 100 ? $conjunction : $separator;
                $string .= convert_number_to_words($remainder);
            }
            break;
    }

    if (null !== $fraction && is_numeric($fraction)) {
        // $string .= ' Pesos '.$decimal;
        // $words = array();
        // foreach (str_split((string) $fraction) as $number) {
        //     $words[] = $dictionary[$number];
        // }
        // $string .= implode(' ', $words);

        $tens = ((int)($fraction / 10)) * 10;
        $units = $fraction % 10;
        if ($tens == 0 && $units == 0) {
            $string .= ' Pesos Only';
        } else {
            $string .= ' Pesos ' . $decimal;

            if ($fraction < 21) {
                $string .= convert_number_to_words($fraction);
            } else {
                $string .= $dictionary[$tens];
                if ($units != 0) {
                    $string .= $hyphen . $dictionary[$units];
                }
            }

            $string .= " Centavos only";
        }


    }

    return $string;
}

?>
<link href="{{ asset("/bower_components/AdminLTE/bootstrap/css/bootstrap.min.css") }}" rel="stylesheet"
      type="text/css"/>
<style media="print">
    div {
        font-family: 'Times New Roman';
    }

    h3 {
        margin: 0;
    }

    .payee_cell {
        border: 1px solid black;
        margin: 3%;
    }

    .hide_text {
        font-color: white;
        color: white;
    }

    @page {
        size: auto;   /* auto is the initial value */
        margin-top: 5mm;  /* this affects the margin in the printer settings */
        margin-bottom: 5mm;

    }

</style>

<?php
function format_to_money($value)
{
    return number_format(floatval($value), 2, ".", ",");
}

function format_with_correct_decimal($value)
{
    return number_format(floatval($value), 2, ".", "");
}
?>
<script>
    function formatToNumberJS(n) {
        return n.toFixed(2).replace(/./g, function (c, i, a) {
            return i && c !== "." && ((a.length - i) % 3 === 0) ? ',' + c : c;
        });
    }


</script>
<body>
<div style="text-align:center; margin:0 auto;">

    <table width="100%" align="center" border="0" style="z-index:9999;top:0;left:0;">
        <tr>
            <td colspan="4" align="center" style="color:white"><b>&nbsp;</b></td>
        </tr>
        <tr class="header-text">
            <td colspan="4" align="center" style="color:white">&nbsp;</td>
        </tr>
        <tr class="header-text">
            <td colspan="4" align="center" style="color:white">&nbsp;</td>
        </tr>

        <tr class="header-text">
            <td colspan="4" align="center" style="color:white">&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td style="font-size:10px; font-style:italic; text-align:center; color:white">&nbsp;</td>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td width="2%" rowspan="4" style="color:white"><b>&nbsp; </b></td>
            <td width="30%" align="center" rowspan="4" style="padding-left:5%">
                <h3>{{$commissions->first_name}} {{$commissions->last_name}}</h3>
            </td>
            <td width="3%">&nbsp;</td>
            <td width="30%" colspan="2" style="color:white">
                <h3><b>&nbsp;</b>

                    <h3>
            </td>
        </tr>
        <tr>
            <td width="3%">&nbsp;</td>
            <td colspan="2"><b>&nbsp;</b></td>
        </tr>
        <tr>
            <td width="3%">&nbsp;</td>
            <td colspan="2" style="padding-left:20%"><b>{{$commissions->due_date}}</b></td>

        </tr>
    </table>
    <br>
    <table width="100%" align="center" cellpadding="10">
        <tr style="color:white">
            <td width="70%" align="center"><h4><b>&nbsp;</b></h4></td>
            <td width="30%" align="center"><h4><b>&nbsp;</b></h4></td>
        </tr>


        <tr>
            <td width="70%" height="100px" align="left">
                <table width="100%">
                    <tr>

                        <td style="padding-left:15%">COMMISSION</td>

                    </tr>
                    <tr>
                        <td style="padding-left:15%">{{ strtoupper($client->first_name.' '.$client->last_name) }}</td>
                    </tr>
                    <tr>
                        <td style="padding-left:15%">PO # {{ strtoupper($purchase->purchase_order_no) }}
                            - {{ strtoupper($commissions->percentage) }} %

                            @foreach($purchase_item_list as $item)
                                | {{$item->description}}
                            @endforeach
                            <br> Total Purchase Amount P {{ format_to_money($total_price) }}
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-left:15%; margin-top:0;"
                            valign="top">{{ strtoupper($commissions->check_date) }}</td>
                    </tr>
                </table>
            </td>
            <td width="30%" height="100px" align="center"
                style="text-align:center; vertical-align:top">{{ format_to_money($total_commission_amount_receivable) }}</td>
        </tr>
        <tr>
            <td width="70%" align="right" style="padding-right:5px; color:white">TOTAL</td>
            <td width="30%" align="center">{{ format_to_money($total_commission_amount_receivable) }}</td>
        </tr>
    </table>

    <table width="100%" align="center" cellpadding="10">
        <tr>
            <td colspan="4"><h6>&nbsp;</h6></td>
        </tr>
        <tr>
            <td colspan="4"><h6>&nbsp;</h6></td>
        </tr>

        <tr>
            <td colspan="4"><h6>&nbsp;</h6></td>
        </tr>

        <tr>
            <td colspan="4"><h6>&nbsp;</h6></td>
        </tr>
    </table>

    <table width="100%" align="center" cellpadding="10">
        <tr>
            <td width="60%" style="text-align:center; vertical-align:top">
                <table width="80%" align="center" cellpadding="10" style="margin-left:0px;">
                    <tr>
                        <td align="center">COMMISSION</td>
                        <td align="center">{{ format_to_money($total_commission_amount) }}</td>
                        <td align="center">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="center" width="60%">WSP Account</td>
                        <td align="center" width="20%">&nbsp;</td>
                        <td align="center" width="20%">{{format_to_money($wsp_amount)}}</td>
                    </tr>
                    <tr>
                        <td align="center">Account Balance Deduction</td>
                        <td align="center">&nbsp;</td>
                        <td align="center">{{format_to_money($account_bal)}}</td>
                    </tr>
                    <tr>
                        <td align="center">Other Deduction</td>
                        <td align="center">&nbsp;</td>
                        <td align="center">{{format_to_money($other_deduction)}}</td>
                    </tr>
                    <tr>
                        <td align="center">&nbsp;</td>
                        <td align="center">&nbsp;</td>
                        <td align="center">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="center">&nbsp;</td>
                        <td align="center">&nbsp;</td>
                        <td align="center">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="center">&nbsp;</td>
                        <td align="center">&nbsp;</td>
                        <td align="center">&nbsp;</td>
                    </tr>
                    </tr>
                    <tr>
                        <td align="center">&nbsp;</td>
                        <td align="center">&nbsp;</td>
                        <td align="center">&nbsp;</td>
                    </tr>
                </table>
            </td>
            <td width="2%">&nbsp;</td>
            <td width="38%">
                <table width="100%" align="center" style="margin-top:-100px;">
                    <tr>
                        <td align="center" width="10%" style="color:white">&nbsp;</td>
                        <td align="right" width="90%" colspan="3" width="90%"
                            style="padding-right:20%">{{convert_number_to_words(format_with_correct_decimal($total_commission_amount_receivable))}}</td>
                    </tr>

                </table>
                <br>
                <table width="70%" align="left">
                    <tr>
                        <td width="20%" style="color:white"><h6>&nbsp;</h6></td>
                        <td width="30%">{{$commissions->bank_name}}</td>
                        <td width="20%" style="color:white"><h6>&nbsp;</h6></td>
                        <td width="30%">{{$commissions->check_no}}</td>
                    </tr>
                </table>
                <br>
                <table width="100%" align="center">
                    <tr>
                        <td colspan="4" style="color:white">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            {{$commissions->first_name}} {{$commissions->last_name}}
                        </td>
                    </tr>

                </table>
            </td>

        </tr>
    </table>

    <table width="100%" align="center" cellpadding="10">
        <tr>
            <td width="30%" align="center" style="padding-top:5%">
                {{$user->first_name . ' '. $user->last_name}}
            </td>
            <td>&nbsp;</td>
            <td style="color:white" width="30%">&nbsp;</td>
            <td>&nbsp;</td>
            <td style="color:white" width="30%">&nbsp;</td>
        </tr>
    </table>

</div>
</body>


<script>
    function printPage() {

        window.print();
    }
    printPage();
</script>
