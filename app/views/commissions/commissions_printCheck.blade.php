<?php
	$total_commission_amount =  $total_commission * ($commissions->percentage/100);

	$account_bal =  $total_commission * ($commissions->percentage/100) *  ($account_balance_percentage/100);
	$wsp_amount = $total_commission  * ($commissions->percentage/100) * ($wsp/100);
	$other_deduction = $commissions->other_deduction_amount;

	$total_commission_amount_receivable = $total_commission_amount- $account_bal - $wsp_amount - $commissions->other_deduction_amount;

	function convert_number_to_words($number) {

    $hyphen      = '-';
    $conjunction = ' and ';
    $separator   = ', ';
    $negative    = 'negative ';
    $decimal     = ' and ';
    $dictionary  = array(
        0                   => 'Zero',
        1                   => 'One',
        2                   => 'Two',
        3                   => 'Three',
        4                   => 'Four',
        5                   => 'Five',
        6                   => 'Six',
        7                   => 'Seven',
        8                   => 'Eight',
        9                   => 'Nine',
        10                  => 'Ten',
        11                  => 'Eleven',
        12                  => 'Twelve',
        13                  => 'Thirteen',
        14                  => 'Fourteen',
        15                  => 'Fifteen',
        16                  => 'Sixteen',
        17                  => 'Seventeen',
        18                  => 'Eighteen',
        19                  => 'Nineteen',
        20                  => 'Twenty',
        30                  => 'Thirty',
        40                  => 'Forty',
        50                  => 'Fifty',
        60                  => 'Sixty',
        70                  => 'Seventy',
        80                  => 'Eighty',
        90                  => 'Ninety',
        100                 => 'Hundred',
        1000                => 'Thousand',
        1000000             => 'Million',
        1000000000          => 'Billion',
        1000000000000       => 'Trillion',
        1000000000000000    => 'Quadrillion',
        1000000000000000000 => 'Quintillion'
    );

    if (!is_numeric($number)) {
        return false;
    }

    if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
        // overflow
        trigger_error(
            'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
            E_USER_WARNING
        );
        return false;
    }

    if ($number < 0) {
        return $negative . convert_number_to_words(abs($number));
    }

    $string = $fraction = null;

    if (strpos($number, '.') !== false) {
        list($number, $fraction) = explode('.', $number);
    }
    $number = abs($number);
    switch (true) {
        case $number < 21:
            $string = $dictionary[$number];
            break;
        case $number < 100:
            $tens   = ((int) ($number / 10)) * 10;
            $units  = $number % 10;
            $string = $dictionary[$tens];
            if ($units != 0) {
                $string .= $hyphen . $dictionary[$units];
            }
            break;
        case $number < 1000:
            $hundreds  = $number / 100;
            $remainder = $number % 100;
            $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
            if ($remainder) {
                $string .= $conjunction . convert_number_to_words($remainder);
            }
            break;
        default:
            $baseUnit = pow(1000, floor(log($number, 1000)));
            $numBaseUnits = (int) ($number / $baseUnit);
            $remainder = $number % $baseUnit;
            $string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];

            
            if ($remainder) {
                $string .= $remainder < 100 ? $conjunction : $separator;
                $string .= convert_number_to_words($remainder);
            }
            break;
    }

    if (null !== $fraction && is_numeric($fraction)) {
        // $string .= ' Pesos '.$decimal;
        // $words = array();
        // foreach (str_split((string) $fraction) as $number) {
        //     $words[] = $dictionary[$number];
        // }
        // $string .= implode(' ', $words);

        $tens   = ((int) ($fraction / 10)) * 10;
        $units  = $fraction % 10;
         if( $tens == 0 && $units == 0 ){
            $string .= ' Pesos Only';
        }else{
            $string .= ' Pesos '.$decimal;

             if( $fraction < 21 ){
                 $string .= convert_number_to_words($fraction);
             }else{
                 $string .= $dictionary[$tens];
                 if ($units != 0) {
                     $string .= $hyphen . $dictionary[$units];
                 }
             }

             $string .= " Centavos only";
        }

        
    }

    return $string;
}

?><link href="{{ asset("/bower_components/AdminLTE/bootstrap/css/bootstrap.min.css") }}" rel="stylesheet" type="text/css" />
<style  media="print">
div{
	font-family: 'Times New Roman';
}
h3{
	margin:0;
}
.payee_cell{
	border: 1px solid black;
	margin:3%;
}
@page 
    {
        size:  auto;   /* auto is the initial value */
        margin: 0mm;  /* this affects the margin in the printer settings */
    }

td{
    word-wrap:break-word
}

</style>

<?php
    function format_to_money($value){
        return number_format(floatval ($value),2,".",",");
    }
    function format_with_correct_decimal($value){
        return number_format(floatval ($value),2,".","");
    }
?>

<br>    
<br>
<div style="width:100%">
    <div style="width:50%;float:left;text-align:center;padding-top:20px; position:absolute;">** {{$commissions->first_name}} {{$commissions->last_name}} **</div>
    <div style="width:100%;float:right;text-align:right;padding-top:5px; padding-right:15%; position:absolute;">{{date_format(date_create($commissions->check_date),"m/d/Y")}}</div>
    <div style="width:100%;float:left;text-align:left;padding-top:50px; padding-left:113px; position:absolute;">{{convert_number_to_words(format_with_correct_decimal($total_commission_amount_receivable))}} </div>
    <div style="width:100%;float:right;text-align:right;padding-top:35px; padding-right:20%; position:absolute;"> {{number_format($total_commission_amount_receivable,2,".",",");}}***</div>
</div>
       

 



<script>
	function printPage(){
		window.print();
	}
    printPage();
</script>
