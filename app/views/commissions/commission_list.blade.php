@extends('admin_template')

@section('additional_header')

        <!-- DataTables -->
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.min.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker-bs3.css")}}'>

@endsection

@section('content')
        <!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <!-- general form elements -->
            <div class="com-md-12 box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Search Commission</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <div class="box-body">
                    <div class="row">
                        <form role="form">
                            <div class="col-md-2 col-sm-2">
                                <button id="btn_search" type="button" class="btn btn-primary" data-toggle="modal"
                                        data-target="#search-purchase"><i class="ion ion-search"></i> Search Commission
                                </button>
                            </div>
                            <div class="col-md-2 col-sm-2 col-md-offset-5 col-sm-offset-5">
                                @if(isAccessModuleAllowed('distribute_commission'))
                                    <a href="/commissions/" class="btn btn-success"><i
                                                class="fa fa-money"></i>
                                        Create New Commissions</a>
                                @endif

                            </div>
                            <div class="col-md-3">
                                @if(isAccessModuleAllowed('purchase_edit'))
                                    <div class="input-group">
                                        <select id="slct_purchase_status" class="form-control">
                                            <option >--Select Action--</option>
                                            <option value="Active">Active</option>
                                            <option value="Archive">Archive</option>
                                            <option value="Payment Complete">Payment Completed</option>
                                            <option value="For Pull Out">For Pull Out</option>
                                            @if(isAccessModuleAllowed('purchase_delete'))
                                            <option value="Cancelled">Cancel PO</option>
                                            @endif
                                            <option value="Pulled Out">Pulled Out</option>
                                        </select>

                                        <div class="input-group-btn">
                                            <button type="button" id="btn_update_purchase" class="btn btn-info">Go</button>
                                        </div>
                                    <!-- /btn-group -->
                                    </div>
                                @endif
                                
                            </div>

                        </form>
                    </div>
                </div>
            </div>
            <!-- /.box -->
            <!--/.col (left) -->
        </div>
        <!-- search form -->
    </div>
    <!-- /.row (main row) -->

    <div class="row">
        <div class=" col-md-12 col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-shopping-cart"></i> Commissions Records</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Voucher No.</th>
                            <th>Agent/ Payee</th>
                            <th>Client</th>
                            <th>PO No.</th>
                            <th>Status</th>
                            <th>Check No.</th>
                            <th>Check Date</th>
                            <th>For Release Date</th>
                            <th>Check Amount</th>
                            <th>Remarks/ Date Received</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($commissions as $commission)
                            <tr>
                                <td>&nbsp;</td>
                               <td>{{$commission->voucher_no}}</td>
                                <td>{{$commission->agent}}</td>
                                <td>{{$commission->client}}</td>
                                <td>{{$commission->purchase_order_no}}</td>
                                <td>{{$commission->status}}</td>
                                <td>{{$commission->check_no}}</td>
                                <td>{{$commission->check_date}}</td>
                                <td>{{$commission->due_date}}</td>
                                <td>{{number_format((double)$commission->received_commission, 2)}}</td>
                                <td>{{$commission->remarks}}</td>
                                <td>
                                    <a href="/commissions/voucher/{{$commission->id}}"
                                       data-toggle="tooltip"
                                       target="_blank"
                                       title="View {{$commission->agent}}'s voucher details"
                                       class="btn btn-success btn-xs">
                                        <i class="fa fa-info-circle"></i> View
                                    </a>
                                    @if(isAccessModuleAllowed('commissions_edit'))
                                        <a href="/voucher/{{$commission->id}}"
                                           data-toggle="tooltip"
                                           title="Edit {{$commission->agent}}'s voucher details"
                                           class="btn btn-primary btn-xs">
                                            <i class="fa fa-edit"></i> Edit
                                        </a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                        <tfoot>
                        <tr>
                            <th>#</th>
                            <th>Voucher No.</th>
                            <th>Agent/ Payee</th>
                            <th>Client</th>
                            <th>PO No.</th>
                            <th>Status</th>
                            <th>Check No.</th>
                            <th>Check Date</th>
                            <th>For Release Date</th>
                            <th>Check Amount</th>
                            <th>Remarks/ Date Received</th>
                            <th>Action</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
    {{--MODAL--}}
        <div id="archive-po-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-blue-gradient">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><i class="ion ion-check"></i> Archive Purchase Order</h4>
                    </div>
                    <div class="modal-body">
                        Do you want to mark these Purchase Order records as Archived?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn_agree_mark" data-dismiss="modal">Yes
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close
                        </button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <div id="complete-po-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-blue-gradient">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><i class="ion ion-check"></i> Payment Completed Purchase Order</h4>
                    </div>
                    <div class="modal-body">
                        Do you want to mark these Purchase Order records as Payment Completed?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn_agree_mark"  data-dismiss="modal">Yes
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close
                        </button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <div id="pullout-po-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-blue-gradient">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><i class="ion ion-check"></i> For Pull Out Purchase Order</h4>
                    </div>
                    <div class="modal-body">
                        Do you want to mark these Purchase Order records as For Pull Out?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn_agree_mark"  data-dismiss="modal">Yes
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close
                        </button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <div id="cancel-po-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-blue-gradient">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><i class="ion ion-check"></i> Cancel Purchase Order</h4>
                    </div>
                    <div class="modal-body">
                        Do you want to mark these Purchase Order records as Cancelled?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn_agree_mark"  data-dismiss="modal">Yes
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close
                        </button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

        <div id="active-po-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-blue-gradient">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><i class="ion ion-check"></i> Activate Purchase Order</h4>
                    </div>
                    <div class="modal-body">
                        Do you want to mark these Purchase Order records as Active?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn_agree_mark"  data-dismiss="modal">Yes
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close
                        </button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

        <div id="updated-ok-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-blue-gradient">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><i class="ion ion-check"></i> Purchase Order Status Update</h4>
                    </div>
                    <div class="modal-body">
                        Record/s has been updated.
                    </div>
                    <div class="modal-footer">
                        
                        <button type="button" class="btn btn-default btn_close_modal" data-dismiss="modal">Close
                        </button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

        <div id="move-paid-commission-to-balance" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header bg-blue-gradient">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><i class="ion ion-check"></i> Pull Out Purchase Order / Update Commission Status</h4>
                    </div>
                    <div class="modal-body">
                        <table id="tbl_commission" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>PO No</th>
                                <th>Agent Name</th>
                                <th>Commission Amount</th>
                                <th>Date Released</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                           

                            </tbody>
                            <tfoot>
                            <tr>
                                <th>PO No</th>
                                <th>Agent Name</th>
                                <th>Commission Amount</th>
                                <th>Date Released</th>
                                <th>Action</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" id="btn_update_commission_record" >Update Commission Record
                        </button>
                        <button type="button" class="btn btn-default btn_close_modal" data-dismiss="modal">Close
                        </button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        {{--MOdal End--}}
</section>

@endsection

@section('additional_footer')

    <script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/jquery.dataTables.min.js")}}'></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js")}}'></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.full.min.js")}}'></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker.js")}}'></script>


    <script>
        $(document).ready(function () {
            var table = $('#example1').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true
            });
            
            var tbl_commission = $('#tbl_commission').DataTable({
                "paging": false,
                "lengthChange": true,
                "searching": false,
                "ordering": true,
                "info": false,
                "autoWidth": true
            });

            $("#btn_search").click(function () {
                $(".select2").select2({
                    theme: "classic"
                });
                $('#txt_daterange').daterangepicker();

            });

            $(".hide-filter").each(function (index) {
                $(this).parent().parent().hide();
                $(this).on("click", function () {
                    $(this).parent().parent().hide();
                    $("." + $(this).parent().parent().attr('id')).show();
                });
            });

            $(".dropdown-menu li").each(function (index) {
                $(this).on("click", function () {
                    $("#" + $(this).children("input").val()).show();
                    $(this).hide();
                });
            });

            $("#btn_search_purchases").click(function(){
                table.clear().draw();
            });

            $("#btn_update_purchase").click(function(){
                
                if($("#slct_purchase_status").val()=='Archive'){
                    $("#archive-po-modal").modal('show');
                }else if($("#slct_purchase_status").val()=='Payment Completed'){
                    $("#complete-po-modal").modal('show');
                }else if($("#slct_purchase_status").val()=='For Pull Out'){
                    $("#pullout-po-modal").modal('show');
                }else if($("#slct_purchase_status").val()=='Cancel PO'){
                    $("#cancel-po-modal").modal('show');
                }else{
                    $("#active-po-modal").modal('show');
                }
                
            });
            $( ".btn_agree_mark" ).each(function( i ) {
                
                $(this).click(function(){
                    
                    var items = $("input[type=checkbox][name=purchase_no]:checked");
                    var for_removal = [];
                    $("input[type=checkbox][name=purchase_no]:checked").each(function () {
                        for_removal.push(this.value);
                    });

                    $.get("/purchases/updatePurchaseStatus/", {
                        update_items: for_removal,
                        status: $("#slct_purchase_status").val()
                    }).done(function (data) {
                        console.log(data);
                        if(data.length == 0){
                            $("#updated-ok-modal").modal('show');
                        }else{
                            for(ctr=-0;ctr<data.length;ctr++){
                                tbl_commission.row.add(
                                [
                                    '<input type="hidden" name="agent_no" value="'+data[ctr].agent_id+'">'+
                                    '<input type="hidden" name="commission_id" value="'+data[ctr].commission_id+'">'+
                                    '<input type="hidden" name="po_no" value="'+data[ctr].po_no+'">'+
                                    '<input type="hidden" name="amount" value="'+data[ctr].received_commission+'">'+
                                    data[ctr].po_no,
                                    data[ctr].last_name + data[ctr].first_name,
                                    data[ctr].received_commission,
                                    data[ctr].due_date,
                                    "<select name='user_action'>"+
                                    "<option value='Cash Advance'>Cash Advance</option>"+
                                    "<option value='Replace with Cash Advance'>Replace with Commission</option>"+
                                    "<option value='Enlist for Payment'>Enlist for Payment</option>"+
                                    "</select>"

                                ]
                        ).draw();
                            }
                            $("#move-paid-commission-to-balance").modal('show');
                        }
                        
                    });
                });
                
            });
            
            $(".btn_close_modal").click(function(){
                
                window.location.reload();
                
            });

            $("#btn_update_commission_record").click(function(){
                var agents = [];
                $("input[type=hidden][name=agent_no]").each(function () {
                    agents.push(this.value);
                });

                var po_no = [];
                $("input[type=hidden][name=po_no]").each(function () {
                    po_no.push(this.value);
                });

                var commission_id = [];
                $("input[type=hidden][name=commission_id]").each(function () {
                    commission_id.push(this.value);
                });
                
                var amount = [];
                $("input[type=hidden][name=amount]").each(function () {
                    amount.push(this.value);
                });

                var user_action = [];
                $("select[name=user_action]").each(function () {
                    user_action.push(this.value);
                });

                console.log(agents)
                $.get("/purchases/addPullOutCommissionToCashAdvance", {
                    _agents : agents,
                    _po_no : po_no,
                    _commission_id : commission_id,
                    _amount: amount,
                    _user_action: user_action
                }).done(function (data) {
                    console.log(data);
                    return false;
                    $("#updated-ok-modal").modal('show');
                });
            });
            
            
        });
    </script>
@endsection