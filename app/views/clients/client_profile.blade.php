@extends('admin_template')

@section('additional_header')

    <!-- DataTables -->
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.min.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker-bs3.css")}}'>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/css/jasny-bootstrap.min.css">
@endsection

@section('content')
    <!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-3">

      <!-- Profile Image -->
      <div class="box box-primary">
        <div class="box-body box-profile">
          <a href="#" data-toggle="modal" data-target="#upload_image">
              @if($client->profile_picture!="" && $client->profile_picture!= null )
                  <img class="profile-user-img img-responsive img-circle"
                       src="{{$client->profile_picture}}"
                       alt="User profile picture"
                       id="user_image">
              @else
                  <img class="profile-user-img img-responsive img-circle"
                       src="{{ asset("/bower_components/AdminLTE/dist/img/user2-160x160.jpg") }}"
                       alt="User profile picture"
                       id="user_image">
              @endif

          </a>

          <h3 class="profile-username text-center">{{ucwords($client->first_name)}} {{ucwords($client->last_name)}}</h3>

          <p class="text-muted text-center">{{ucwords($client->occupation)}}</p>
          <hr>

          @if($client->_city != null && $client->_province != null)
              <p class="text-muted text-center"><b>Address</b><br>{{$client->street}} {{$client->brgy}}
                  , {{$client->_city[0]->cityname}}
                  , {{$client->_province[0]->provname}}</p>
              <hr>
          @else
              <p class="text-muted text-center"><b>Address</b><br>N/A
              <hr>
          @endif
          <p class="text-muted text-center"><b>Contact Information</b><br>{{$client->phone1}}/{{$client->phone2}}</p>
          <hr>
          <p class="text-muted text-center"><b>Client Status</b><br>{{$client->status}}</p>
          <hr>
          <center>
            <button class="btn btn-primary" id="btn_update">Update Information</button>
          </center>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->

    </div>
    <!-- /.col -->
    <div class="col-md-9">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title"><i class="fa fa-user"></i> Personal Information</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">

                <div class="col-md-12">
                  <form class="form-horizontal">
                    <!-- Account Balance -->
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="txt_last_name" class="col-sm-2 control-label"> Last Name</label>

                        <div class="col-sm-4">
                          <input type="text" class="form-control pull-right" value="{{ucwords($client->last_name)}}"
                                 id="txt_last_name">
                        </div>
                        <label for="txt_first_name" class="col-sm-2 control-label"> First Name</label>

                        <div class="col-sm-4">
                          <input type="text" value="{{ucwords($client->first_name)}}" class="form-control pull-right"
                                 id="txt_first_name">
                        </div>

                      </div>
                    </div>


                    <div class="col-md-6 hidden">
                      <div class="form-group">
                        <label for="rdo_gender" class="col-sm-4 control-label"> Gender</label>

                        <div class="col-sm-8">
                          <label style="margin-top:2%;margin-left:2%">
                            <input type="radio" @if($client->gender=='Male') {{'checked'}} @endif name="rdo_gender"
                                   value="Male" class="minimal" checked>
                            Male
                          </label>
                          <label style="margin-top:2%;margin-left:2%">
                            <input type="radio" @if($client->gender=='Female') {{'checked'}} @endif name="rdo_gender"
                                   value="Female" class="minimal">
                            Female
                          </label>
                        </div>
                      </div>
                    </div>


                    <div class="col-md-6 hidden">
                      <div class="form-group">
                        <label for="txt_birthdate" class="col-sm-4 control-label birthdate"> BirthDate</label>

                        <div class="col-sm-8">

                          <input type="text" class="form-control pull-right" value="{{$client->birthdate}}"
                                 id="txt_birthdate">

                        </div>
                      </div>
                    </div>

                    <div class="col-md-6 hidden">
                      <div class="form-group">
                        <label for="slct_civil_status" class="col-sm-4 control-label"> Civil Status</label>

                        <div class="col-sm-8">
                          <select id="slct_civil_status" class="form-control select2"
                                  style="width: 100%;height:100%;background-color:white">
                            <option @if($client->civil_status=='Single') selected @endif>Single</option>
                            <option @if($client->civil_status=='Married') selected @endif>Married</option>
                            <option @if($client->civil_status=='Widowed') selected @endif>Widowed</option>
                            <option @if($client->civil_status=='Separated') selected @endif>Separated</option>
                          </select>
                        </div>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="txt_occupation" class="col-sm-4 control-label"> Occupation</label>

                        <div class="col-sm-8">

                          <input type="text" class="form-control pull-right" value="{{$client->occupation}}"
                                 id="txt_occupation">

                        </div>
                      </div>
                    </div>

                    <div class="col-md-12">
                      <hr style="margin-bottom:1%">
                    </div>

                    <div class="col-md-12">
                      <h4 class="box-title"><i class="fa fa-phone"></i> Contact Information</h4>
                    </div>

                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="txt_street" class="col-sm-2 control-label"> Street</label>

                        <div class="col-sm-10">

                          <input type="text" class="form-control pull-right" id="txt_street"
                                 value="{{$client->street}}">

                        </div>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="txt_brgy" class="col-sm-4 control-label"> Barangay</label>

                        <div class="col-sm-8">
                          <input type="text" class="form-control pull-right" id="txt_brgy"
                                 value="{{$client->brgy}}">
                        </div>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="slct_town" class="col-sm-4 control-label"> Municipality</label>

                        <div class="col-sm-8">
                          <select id="slct_town" class="form-control select2"
                                  style="width: 100%;height:100%;background-color:white">
                            <option value="">Select Municipality</option>
                            @foreach($municipalities as $municipality)
                              <option
                                  value="{{$municipality->citycode}}" @if($client->city == $municipality->citycode){{'selected'}} @endif>{{$municipality->cityname}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="slct_province" class="col-sm-4 control-label"> Province</label>

                        <div class="col-sm-8">
                          <select id="slct_province" class="form-control select2"
                                  style="width: 100%;height:100%;background-color:white">
                            <option value="">Select Province</option>
                            @foreach($provinces as $province)
                              <option
                                  value="{{$province->provcode}}" @if($client->province == $province->provcode){{'selected'}} @endif>
                                {{$province->provname}}
                              </option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="txt_landline" class="col-sm-4 control-label"> Landline</label>

                        <div class="col-sm-8">
                          <input type="text" class="form-control pull-right" value="{{$client->phone1}}"
                                 id="txt_landline">
                        </div>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="txt_mobile" class="col-sm-4 control-label"> Mobile</label>

                        <div class="col-sm-8">
                          <input type="text" class="form-control pull-right" value="{{$client->phone2}}"
                                 id="txt_mobile">
                        </div>
                      </div>
                    </div>


                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="txt_email" class="col-sm-4 control-label"> Email</label>

                        <div class="col-sm-8">
                          <input type="text" class="form-control pull-right" value="{{$client->email_address}}"
                                 id="txt_email">
                        </div>
                      </div>
                    </div>

                    <div class="col-md-12">
                      <hr style="margin-bottom:1%">
                    </div>

                    <div class="col-md-12">
                      <h4 class="box-title"><i class="fa fa-briefcase"></i> Business Information</h4>
                    </div>

                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="txt_business_name" class="col-sm-2 control-label"> Business Name</label>

                        <div class="col-sm-10">

                          <input type="text" class="form-control pull-right" value="{{$client->business_name}}"
                                 id="txt_business_name">

                        </div>
                      </div>
                    </div>

                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="txt_business_street" class="col-sm-2 control-label"> Street</label>

                        <div class="col-sm-10">

                          <input type="text" class="form-control pull-right" value="{{$client->business_street}}"
                                 id="txt_business_street">

                        </div>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="slct_business_brgy" class="col-sm-4 control-label"> Barangay</label>

                        <div class="col-sm-8">
                          <input type="text" class="form-control pull-right" value="{{$client->business_brgy}}"
                                 id="slct_business_brgy">
                        </div>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="slct_business_town" class="col-sm-4 control-label"> Municipality</label>

                        <div class="col-sm-8">
                          <select id="slct_business_town" class="form-control select2"
                                  style="width: 100%;height:100%;background-color:white">
                            <option value="">Select Municipality</option>
                            @foreach($municipalities as $municipality)
                              <option
                                  value="{{$municipality->citycode}}" @if($client->business_city == $municipality->citycode){{'selected'}} @endif>{{$municipality->cityname}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="slct_business_province" class="col-sm-4 control-label"> Province</label>

                        <div class="col-sm-8">
                          <select id="slct_business_province" class="form-control select2"
                                  style="width: 100%;height:100%;background-color:white">
                            <option value="">Select Province</option>
                            @foreach($provinces as $province)
                              <option
                                  value="{{$province->provcode}}" @if($client->business_province == $province->provcode){{'selected'}} @endif>
                                {{$province->provname}}
                              </option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="txt_business_landline" class="col-sm-4 control-label"> Landline</label>

                        <div class="col-sm-8">
                          <input type="text" class="form-control pull-right" value="{{$client->business_landline}}"
                                 id="txt_business_landline">
                        </div>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="txt_business_mobile" class="col-sm-4 control-label"> Mobile</label>

                        <div class="col-sm-8">
                          <input type="text" class="form-control pull-right" value="{{$client->business_mobile}}"
                                 id="txt_business_mobile">
                        </div>
                      </div>
                    </div>


                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="txt_business_email" class="col-sm-4 control-label"> Email</label>

                        <div class="col-sm-8">
                          <input type="text" class="form-control pull-right" value="{{$client->business_email}}"
                                 id="txt_business_email">
                        </div>
                      </div>
                    </div>

                    <div class="col-md-12">
                      <hr style="margin-bottom:1%">
                    </div>

                    <div class="col-md-12">
                      <h4 class="box-title"><i class="fa fa-check"></i> Valid ID</h4>
                    </div>

                    {{--<div class="col-md-6">--}}
                      {{--<div class="form-group">--}}
                        {{--<div class="col-sm-12">--}}
                          {{--<img src="{{ asset("/bower_components/AdminLTE/dist/img/user2-160x160.jpg") }}"--}}
                               {{--alt="User profile picture" style="repeat-x:repeat">--}}
                        {{--</div>--}}
                      {{--</div>--}}
                    {{--</div>--}}

                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="txt_id_type" class="col-sm-3 control-label"> ID Type</label>

                        <div class="col-sm-9">
                          <input type="text" class="form-control pull-right" value="{{$client->valid_id_type}}"
                                 style="background-color:white" id="txt_id_type">
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="txt_id_no" class="col-sm-3 control-label"> ID No</label>

                        <div class="col-sm-9">
                          <input type="text" class="form-control pull-right" value="{{$client->valid_id_no}}"
                                 id="txt_id_no">
                        </div>
                      </div>

                    </div>

                    <div class="col-md-12">
                      <hr style="margin-bottom:1%">
                    </div>

                    <div class="col-md-12 hidden">
                      <h4 class="box-title"><i class="fa fa-pencil"></i> Signature Specimen</h4>
                    </div>

                    {{--<div class="col-md-4">--}}
                      {{--<div class="form-group">--}}
                        {{--<div class="col-sm-12">--}}
                          {{--<img src="{{ asset("/bower_components/AdminLTE/dist/img/user2-160x160.jpg") }}"--}}
                               {{--alt="User profile picture" style="repeat-x:repeat">--}}
                        {{--</div>--}}
                      {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="col-md-4">--}}
                      {{--<div class="form-group">--}}
                        {{--<div class="col-sm-12">--}}
                          {{--<img src="{{ asset("/bower_components/AdminLTE/dist/img/user2-160x160.jpg") }}"--}}
                               {{--alt="User profile picture" style="repeat-x:repeat">--}}
                        {{--</div>--}}
                      {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="col-md-4">--}}
                      {{--<div class="form-group">--}}
                        {{--<div class="col-sm-12">--}}
                          {{--<img src="{{ asset("/bower_components/AdminLTE/dist/img/user2-160x160.jpg") }}"--}}
                               {{--alt="User profile picture" style="repeat-x:repeat">--}}
                        {{--</div>--}}
                      {{--</div>--}}
                    {{--</div>--}}

                    {{--<div class="col-md-12">--}}
                      {{--<hr style="margin-bottom:1%">--}}
                    {{--</div>--}}

                  </form>

                  <!-- buttons  -->
                  <div class="col-md-3 col-md-offset-9" id="div_update_user">
                    <div class="row">
                      <div class="col-md-12">
                        <button type="button" id="btn_save_client" class="btn btn-info pull-right"> Update</button>
                        <button class="btn btn-warning" id="btn_cancel_update"> Cancel</button>
                      </div>
                    </div>
                    <!-- /btn-group -->
                  </div>
                  <!-- /.buttons  -->

                </div><!-- col-md-12 -->
              </div><!-- /row -->
            </div>
            <!-- /.box-body -->
          </div>
        </div>
        <!-- /.col -->
      </div>

      <div id="upload_image" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header bg-blue-active">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><i class="ion ion-add"></i> Upload Image</h4>
                </div>
                <div class="modal-body">
                    <center>
                        {{ Form::open(array('url' => '/clients/uploadImage', 'files' => true)) }}
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-preview thumbnail" data-trigger="fileinput"
                                 style="width: 200px; height: 150px;">
                            </div>
                            <div>
                                <span class="btn btn-default btn-file">
                                    <span class="fileinput-new">Select image</span>
                                    <span class="fileinput-exists">Change</span>
                                    <input type="file" name="avatar">
                                </span>
                                <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                <button class="btn btn-default fileinput-exists">Upload Image</button>
                            </div>
                            <input type="hidden" id="txt_agent_id" name="txt_agent_id" value="{{$client->id}}">
                        </div>
                        {{ Form::close() }}
                    </center>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

      <input type="hidden" id="prov_code">
      <input type="hidden" id="prov_code1">
      <input type="hidden" id="client_id" value="{{$client->id}}">
    </div>
    <!-- /.col -->
  </div>

   {{--MODAL--}}
    <div id="save-client-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-blue-gradient">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><i class="ion ion-check"></i> Save Client OK</h4>
                </div>
                <div class="modal-body">
                    Record has been saved.
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" id="btn_close_modal" data-dismiss="modal">Close
                    </button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--MOdal End--}}

</section>

@endsection

@section('additional_footer')

  <script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/jquery.dataTables.min.js")}}'></script>
  <script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js")}}'></script>
  <script src='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.full.min.js")}}'></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
  <script src='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker.js")}}'></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/js/jasny-bootstrap.min.js"></script>


  <script>
    $(document).ready(function () {

      $(".select2").select2();
      $(".birthdate").daterangepicker({
        singleDatePicker: true,
        showDropdowns: true
      });

      $("#btn_save_client").click(function () {
        $.post("/clients/update", {
          txt_last_name: $("#txt_last_name").val(),
          txt_first_name: $("#txt_first_name").val(),
          rdo_gender: $("input[name=rdo_gender]:checked").val(),
          txt_birthdate: $("#txt_birthdate").val(),
          slct_civil_status: $("#slct_civil_status").val(),
          txt_occupation: $("#txt_occupation").val(),
          txt_street: $("#txt_street").val(),
          txt_brgy: $("#txt_brgy").val(),
          slct_town: $("#slct_town").val(),
          slct_province: $("#slct_province").val(),
          txt_landline: $("#txt_landline").val(),
          txt_mobile: $("#txt_mobile").val(),
          txt_email: $("#txt_email").val(),
          txt_business_street: $("#txt_business_street").val(),
          txt_business_name: $("#txt_business_name").val(),
          slct_business_brgy: $("#slct_business_brgy").val(),
          slct_business_town: $("#slct_business_town").val(),
          slct_business_province: $("#slct_business_province").val(),
          txt_business_landline: $("#txt_business_landline").val(),
          txt_business_mobile: $("#txt_business_mobile").val(),
          txt_business_email: $("#txt_business_email").val(),
          txt_id_type: $("#txt_id_type").val(),
          txt_id_no: $("#txt_id_no").val(),
          client_id: $("#client_id").val(),
          txt_occupation: $("#txt_occupation").val()
        }).done(function (data) {
          // document.write(data);
         $("#save-client-modal").modal('show');
        });
      });

      $("#btn_close_modal").click(function(){
        window.location = '/clients?save=success';
      });
      
      $("#slct_town").change(function () {
        $("#prov_code").val('0');
        if ($("#slct_town").val() == "") {
          $("#slct_province").select2('val', '');
          $.post("/municipalities", {
            provcode: ''
          }).done(function (data) {
            var list = "<option value=''>Display All Municipalities</option>";
            $("#slct_town").empty();
            for (var j = 0; j < data.length; j++) {
              list += "<option value='" + $("#slct_town").val() + "'>" + data[j].cityname + "</option>";
            }
            $("#slct_town").html(list);
          });
        }
        else {
          var province = $("#slct_town").val().substr(0, 4);
          $("#slct_province").select2('val', province);

        }
      });

      $("#slct_province").change(function () {
        console.log('b');
        if ($("#prov_code").val() != '0') {
          $.post("/municipalities", {
            provcode: $("#slct_province").val()
          }).done(function (data) {
            var list = "<option value=''>Display All Municipalities</option>";
            $("#slct_town").empty();
            for (var j = 0; j < data.length; j++) {
              list += "<option value='" + $("#slct_province").val() + "'>" + data[j].cityname + "</option>";
            }
            $("#slct_town").html(list);
            $("#slct_town").select2('val', $("#slct_province").val());
          });
        } else {
          $("#prov_code1").val('1');
        }
      });

      $("#slct_business_town").change(function () {
        $("#prov_code1").val('0');
        if ($("#slct_business_town").val() == "") {
          $("#slct_business_province").select2('val', '');
          $.post("/municipalities", {
            provcode: ''
          }).done(function (data) {
            var list = "<option value=''>Display All Municipalities</option>";
            $("#slct_business_town").empty();
            for (var j = 0; j < data.length; j++) {
              list += "<option value='" + $("#slct_town").val() + "'>" + data[j].cityname + "</option>";
            }
            $("#slct_business_town").html(list);
          });
        }
        else {
          var province = $("#slct_business_town").val().substr(0, 4);
          $("#slct_business_province").select2('val', province);

        }
      });

      $("#slct_business_province").change(function () {
        console.log('b');
        if ($("#prov_code1").val() != '0') {
          $.post("/municipalities", {
            provcode: $("#slct_business_province").val()
          }).done(function (data) {
            var list = "<option value=''>Display All Municipalities</option>";
            $("#slct_business_town").empty();
            for (var j = 0; j < data.length; j++) {
              list += "<option value='" + $("#slct_business_province").val() + "'>" + data[j].cityname + "</option>";
            }
            $("#slct_business_town").html(list);
            $("#slct_business_town").select2('val', $("#slct_business_province").val());
          });
        } else {
          $("#prov_code1").val('1');
        }
      });


        @if(!$edit_mode){
        cancelEdit();
      }
      @endif

      $("#btn_update").click(function () {
        console.log('hahahah');
        $("#txt_last_name").removeAttr('disabled');
        $("#txt_first_name").removeAttr('disabled');
        $("input[name=rdo_gender]").removeAttr('disabled');
        $("#txt_birthdate").removeAttr('disabled');
        $("#slct_civil_status").removeAttr('disabled');
        $("#txt_occupation").removeAttr('disabled');
        $("#txt_street").removeAttr('disabled');
        $("#txt_brgy").removeAttr('disabled');
        $("#slct_town").removeAttr('disabled');
        $("#slct_province").removeAttr('disabled');
        $("#txt_landline").removeAttr('disabled');
        $("#txt_mobile").removeAttr('disabled');
        $("#txt_email").removeAttr('disabled');
        $("#txt_business_name").removeAttr('disabled');
        $("#txt_business_street").removeAttr('disabled');
        $("#slct_business_brgy").removeAttr('disabled');
        $("#slct_business_town").removeAttr('disabled');
        $("#slct_business_province").removeAttr('disabled');
        $("#txt_business_landline").removeAttr('disabled');
        $("#txt_business_mobile").removeAttr('disabled');
        $("#txt_business_email").removeAttr('disabled');
        $("#txt_id_type").removeAttr('disabled');
        $("#txt_id_no").removeAttr('disabled');
        $("#txt_id_expiry").removeAttr('disabled');
        $("#div_update_user").show();

      });

      $("#btn_cancel_update").click(function () {
        cancelEdit();
      });

      function cancelEdit() {

        $("#txt_last_name").attr('disabled', 'true');
        $("#txt_first_name").attr('disabled', 'true');
        $("input[name=rdo_gender]").attr('disabled', 'true');
        $("#txt_birthdate").attr('disabled', 'true');
        $("#slct_civil_status").attr('disabled', 'true');
        $("#txt_occupation").attr('disabled', 'true');
        $("#txt_street").attr('disabled', 'true');
        $("#txt_brgy").attr('disabled', 'true');
        $("#slct_town").attr('disabled', 'true');
        $("#slct_province").attr('disabled', 'true');
        $("#txt_landline").attr('disabled', 'true');
        $("#txt_mobile").attr('disabled', 'true');
        $("#txt_email").attr('disabled', 'true');
        $("#txt_business_name").attr('disabled', 'true');
        $("#txt_business_street").attr('disabled', 'true');
        $("#slct_business_brgy").attr('disabled', 'true');
        $("#slct_business_town").attr('disabled', 'true');
        $("#slct_business_province").attr('disabled', 'true');
        $("#txt_business_landline").attr('disabled', 'true');
        $("#txt_business_mobile").attr('disabled', 'true');
        $("#txt_business_email").attr('disabled', 'true');
        $("#txt_id_type").attr('disabled', 'true');
        $("#txt_id_no").attr('disabled', 'true');
        $("#txt_id_expiry").attr('disabled', 'true');
        $("#div_update_user").hide();

      }

    });</script>
@endsection