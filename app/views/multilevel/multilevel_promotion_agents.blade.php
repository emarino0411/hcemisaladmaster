@extends('admin_template')

@section('additional_header')

        <!-- DataTables -->
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.min.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker-bs3.css")}}'>

@endsection

@section('content')
        <!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12 col-xs-12 hidden">
            <!-- general form elements -->
            <div class="com-md-12 box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Search Agent</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <div class="box-body">
                    <div class="row">
                        <form role="form">
                            <div class="col-md-2 col-sm-2">
                                <button id="btn_search" type="button" class="btn btn-primary" data-toggle="modal"
                                        data-target="#search-purchase"><i class="ion ion-search"></i> Search Client
                                    Records
                                </button>
                            </div>
                          
                        </form>
                    </div>
                    <!-- /row -->

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
            <!--/.col (left) -->
        </div>
        <!-- search form -->
    </div>
    <!-- /.row (main row) -->

    <div class="row">
        <div class=" col-md-12 col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-shopping-cart"></i> Agent Records</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>&nbsp;</th>
                            <th>Agent No</th>
                            <th>Last Name</th>
                            <th>First Name</th>
                            <th>Level</th>
                            <th>Agent Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($agents as $agent)
                            <tr>
                                <td><input type="checkbox"></td>
                                <td>{{$agent->representative_code}}</td>
                                <td>{{$agent->last_name}}</td>
                                <td>{{$agent->first_name}}</td>
                                <td>{{ucwords($agent->current_level)}}</td>
                                <td>{{$agent->status}}</td>
                                <td>
                                    @if(isAccessModuleAllowed('multilevel_view'))
                                        <a href="multilevel/history/{{$agent->id}}"
                                           data-toggle="tooltip"
                                           title="View {{$agent->first_name}} {{$agent->last_name}}'s details"
                                           class="btn btn-success btn-xs">
                                            <i class="fa fa-info-circle"></i> View Promotion History
                                        </a>
                                    @endif

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>&nbsp;</th>
                            <th>Agent No</th>
                            <th>Last Name</th>
                            <th>First Name</th>
                            <th>Level</th>
                            <th>Agent Status</th>
                            <th>Action</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
</section>

@endsection

@section('additional_footer')

    <script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/jquery.dataTables.min.js")}}'></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js")}}'></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.full.min.js")}}'></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker.js")}}'></script>


    <script>
        $(document).ready(function () {
            $('#example1').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true
            });

            $("#btn_search").click(function () {
                $(".select2").select2({
                    theme: "classic"
                });
                $('#txt_daterange').daterangepicker();

            });

            $(".hide-filter").each(function (index) {
                $(this).parent().parent().hide();
                $(this).on("click", function () {
                    $(this).parent().parent().hide();
                    $("." + $(this).parent().parent().attr('id')).show();
                });
            });

            $(".dropdown-menu li").each(function (index) {
                $(this).on("click", function () {
                    $("#" + $(this).children("input").val()).show();
                    $(this).hide();
                });
            });

            $(document).on("click", ".deactivate-agent", function () {
                var myBookId = $(this).data('id');

                $(".modal-body #agentId").val(myBookId);
            });

            $("#btn_deactivate_agent").click(function () {
                $.post("/agents/delete", {
                    userId: $(".modal-body #agentId").val()
                }).done(function (data) {
                    document.write(data);
                    window.location = '/users?deactivate=success';
                });
            });

            $(document).on("click", ".activate-agent", function () {
                var myBookId = $(this).data('id');
                var button = $(this);
                $(".modal-body #agentId").val(myBookId);
            });

            $("#btn_activate_agent").click(function () {
                $.post("/agents/activate", {
                    userId: $(".modal-body #agentId").val()
                }).done(function (data) {
                    window.location = '/users?reactivate=success';

                });
            });
        });
    </script>
@endsection