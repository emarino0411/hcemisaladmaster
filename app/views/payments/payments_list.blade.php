@extends('admin_template')

@section('additional_header')

        <!-- DataTables -->
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.min.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker-bs3.css")}}'>

@endsection

@section('content')
        <!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <!-- general form elements -->
            <div class="com-md-12 box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Search Payment Records</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <div class="box-body">
                    <div class="row">
                        <form role="form">
                            <div class="col-md-2 col-sm-2">
                                <button id="btn_search" type="button" class="btn btn-primary" data-toggle="modal"
                                        data-target="#search-purchase"><i class="ion ion-search"></i> Search Payment
                                    Records
                                </button>
                            </div>

                          
                            @if(isAccessModuleAllowed('payments_edit'))
                            <div class="col-md-3  pull-right">
                                
                                <div class="input-group">
                                    <select class="form-control">
                                        <option>--Select Action--</option>
                                        <option>Verified</option>
                                        <option>On Hold Check</option>
                                        <option>Error on Check</option>
                                    </select>

                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-info">Go</button>
                                    </div>
                                    <!-- /btn-group -->
                                </div>
                                
                            </div>
                            @endif

                            <div class="col-md-7">
                                @if(isAccessModuleAllowed('payments_add'))
                                <a href="payments/addNew" class="btn btn-success pull-right"><i
                                            class="fa fa-money"></i>
                                    Add New Payment</a>
                                @endif
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                @if(isAccessModuleAllowed('payments_view'))
                                    <a href="payments/viewPayments" class="btn btn-info"><i
                                                class="fa fa-eye"></i>
                                        View Payments</a>
                                @endif
                                <a href="payments/checksForDeposit" class="btn btn-warning"><i
                                                class="fa fa-bank"></i>
                                    Checks for Deposit</a>
                            </div>

                        </form>
                    </div>
                    <!-- /row -->

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
            <!--/.col (left) -->
        </div>
        <!-- search form -->
    </div>
    <!-- /.row (main row) -->

    <div class="row">
        <div class=" col-md-12 col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-shopping-cart"></i> Payment Records</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>&nbsp;</th>
                            <th>Date</th>
                            <th>PO No</th>
                            <th>Client</th>
                            <th>Type</th>
                            <th>Check No</th>
                            <th>Bank</th>
                            <th>Branch</th>
                            <th>Amount</th>
                            <th>Remarks</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($payments as $payment)
                            <tr>
                                <td><input type="checkbox" name="payment" value="{{$payment->po_no}}"></td>
                                <td>{{$payment->remittance_date}}</td>
                                <td>{{$payment->po_no}}</td>
                                <td>{{$payment->client}}</td>
                                <td>{{ucwords($payment->type)}}</td>
                                @if($payment->type=='cash')
                                    <td>--</td>
                                    <td>--</td>
                                    <td>--</td>
                                @else
                                    <td>{{$payment->check_number}}</td>
                                    <td>{{ucwords($payment->bank)}}</td>
                                    <td>{{ucwords($payment->branch)}}</td>
                                @endif

                                <td>P{{$payment->amount}}</td>

                                @if($payment->type=='cash')
                                    <td>--</td>
                                @else
                                    <td>{{ucwords($payment->remarks)}}</td>
                                @endif

                                <td>{{$payment->status}}</td>
                            </tr>
                        @endforeach

                        </tbody>
                        <tfoot>
                        <tr>
                            <th>&nbsp;</th>
                            <th>Date</th>
                            <th>PO No</th>
                            <th>Client</th>
                            <th>Check No</th>
                            <th>Bank</th>
                            <th>Branch</th>
                            <th>Amount</th>
                            <th>Remarks</th>
                            <th>Status</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
</section>

@endsection

@section('additional_footer')

    <script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/jquery.dataTables.min.js")}}'></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js")}}'></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.full.min.js")}}'></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker.js")}}'></script>


    <script>
        $(document).ready(function () {
            $('#example1').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true
            });

            $("#btn_search").click(function () {
                $(".select2").select2({
                    theme: "classic"
                });
                $('#txt_daterange').daterangepicker();

            });

            $(".hide-filter").each(function (index) {
                $(this).parent().parent().hide();
                $(this).on("click", function () {
                    $(this).parent().parent().hide();
                    $("." + $(this).parent().parent().attr('id')).show();
                });
            });

            $(".dropdown-menu li").each(function (index) {
                $(this).on("click", function () {
                    $("#" + $(this).children("input").val()).show();
                    $(this).hide();
                });
            })
        });
    </script>
@endsection