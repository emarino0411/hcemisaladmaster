@extends('admin_template')

@section('additional_header')

    <link rel="stylesheet"
          href='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker-bs3.css")}}'>
    <link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/iCheck/all.css")}}'>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.css")}}'>
    <link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.min.css")}}'>

    @endsection

    @section('content')
            <!-- Main content -->
    <section class="content">

        <!-- Client Information -->
        <div class="row">
            <div class=" col-md-12 col-xs-12">
                <div class="row">
                    <!-- client -->
                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="box-header">
                                <h3 class="box-title"><i class="fa fa-user"></i> Search Client</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <form class="form-horizontal">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="txt_po_no" class="col-sm-4 control-label">View Payment
                                                        For</label>

                                                    <div class="col-sm-8">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-user"></i>
                                                            </div>
                                                            <select id="slct_client" class="form-control">
                                                                @if($purchase!=null)
                                                                    <option value="{{$client->id}}">{{$client->last_name}}
                                                                        , {{$client->first_name}}</option>
                                                                @endif
                                                            </select>
                                                        </div>
                                                        <!-- /.input group -->
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="txt_po_no" class="col-sm-4 control-label">Select PO
                                                        No</label>

                                                    <div class="col-sm-8">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-edit"></i>
                                                            </div>
                                                            <select class="form-control" id="po_list">

                                                                @if($purchase!=null)
                                                                    <option value="{{$purchase->id}}">{{$purchase->purchase_order_no}}</option>
                                                                @else
                                                                    <option>Please select Client</option>
                                                                @endif
                                                            </select>
                                                        </div>
                                                        <!-- /.input group -->
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                    <!-- /client -->

                </div>
            </div>
        </div>
        <!-- / client information -->

        <!-- Payment Details -->
        <div class="row">
            <div class="col-md-12 col-xs-12">

                <div class="com-md-12 box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-money"></i> Payment Details</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form class="form-horizontal">
                        <div class="box-body">
                            <div class="row 
                                @if(!isAccessModuleAllowed('payments_view'))
                                    hidden 
                                @endif"
                                 id="div_add_payment" hidden>
                                <div class="col-md-12">

                                    <!-- Payment Date -->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="txt_payment_date" class="col-sm-3 control-label">Date
                                                Received</label>

                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </div>
                                                    <input type="text"
                                                           class="form-control pull-right"
                                                           id="txt_date_received">

                                                </div>

                                                <!-- /.input group -->
                                            </div>
                                            <!-- /.col-sm-8 -->
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                    <!-- /Payment Date -->


                                    <!-- Payment Type -->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="slct_payment_type" id="lbl_item_set"
                                                   class="col-sm-3 control-label">Payment Type</label>

                                            <div class="col-sm-9">
                                                <select id="slct_payment_type" class="set form-control select2"
                                                        style="width: 100%;height:100%;">
                                                    <option value="cash">Cash</option>
                                                    <option value="check">Check</option>
                                                </select>
                                            </div>
                                            <!-- /.col-sm-9 -->
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                    <!-- Payment Type -->

                                    <div class="col-md-12">
                                        <hr style="margin:1%">
                                    </div>


                                    <!-- Cash -->

                                    <div class="col-md-6 col-md-offset-6" id="div_cash">

                                        <div class="form-group">
                                            <label for="txt_amount_cash" class="col-sm-3 control-label">Cash
                                                Amount</label>

                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-rub"></i>
                                                    </div>
                                                    <input type="text" class="form-control pull-right"
                                                           id="txt_amount_cash">
                                                </div>

                                                <!-- /.input group -->
                                            </div>
                                            <!-- /.col-sm-9 -->
                                        </div>
                                        <!-- /. form-group -->
                                    </div>

                                    <!-- //Cash -->


                                    <!-- Check -->
                                    <div class="row" id="div_check" style="display:none;">
                                        <!-- form start -->
                                        <form class="form-horizontal">
                                            <div class="box-body">
                                                <div class="row">
                                                    <div class="col-md-12">

                                                        <div class="col-md-10 col-md-offset-1">
                                                            <h6 class="page-header"><i class="fa fa-list-alt"></i> Check
                                                                Details</h6>
                                                        </div>
                                                        <!-- Bank-->
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="slct_bank" class="col-sm-3 control-label">Bank</label>

                                                                <div class="col-sm-9">
                                                                    <div class="input-group">
                                                                        <div class="input-group-addon">
                                                                            <i class="fa fa-bank"></i>
                                                                        </div>
                                                                        <select id="slct_bank"
                                                                                style="width: 100%;height:100%">
                                                                            <option>BDO Unibank</option>
                                                                            <option>MetroBank</option>
                                                                            <option>Land Bank of the Philippines
                                                                            </option>
                                                                            <option>Bank of the Philippine Islands
                                                                            </option>
                                                                            <option>Philippine National Bank (PNB)
                                                                            </option>
                                                                            <option>Development Bank of the
                                                                                Philippines
                                                                            </option>
                                                                            <option>Security Bank</option>
                                                                            <option>Chinabank</option>
                                                                            <option>RCBC</option>
                                                                            <option>Unionbank</option>
                                                                            <option>Citibank</option>
                                                                            <option>UCPB</option>
                                                                            <option>EastWest Bank</option>
                                                                            <option>HSBC</option>
                                                                            <option>Philtrust Bank</option>
                                                                            <option>Asia United Bank (AUB)</option>
                                                                            <option>Bank of Commerce</option>
                                                                            <option>Maybank</option>
                                                                            <option>PBCom</option>
                                                                            <option>Standard Chartered Bank
                                                                                Philippines
                                                                            </option>
                                                                            <option>The Bank of Tokyo-Mitsubishi UFJ,
                                                                                Ltd.
                                                                            </option>
                                                                            <option>Philippine Veterans Bank</option>
                                                                            <option>Robinsons Bank</option>
                                                                            <option>Deutsche Bank</option>
                                                                            <option>Australia and New Zealand Banking
                                                                                Group (ANZ)
                                                                            </option>
                                                                            <option>JPMorgan Chase</option>
                                                                            <option>Mizuho Bank, Ltd. Manila Branch
                                                                            </option>
                                                                            <option>ING Group N.V.</option>
                                                                            <option>Chinatrust</option>
                                                                            <option>Bank of China Manila Branch</option>
                                                                            <option>Bank of America, N.A.</option>
                                                                            <option>Mega International Commercial Bank
                                                                            </option>
                                                                            <option>Korea Exchange Bank</option>
                                                                            <option>Bangkok Bank</option>
                                                                            <option>Al-Amanah Islamic Investment Bank of
                                                                                the Philippines
                                                                            </option>

                                                                            <option>Citystate Savings Bank</option>
                                                                            <option>Equicom Savings Bank (Equicom)
                                                                            </option>
                                                                            <option>Farmers Savings</option>
                                                                            <option>Malayan Savings Bank</option>
                                                                            <option>Philippine Business Bank</option>
                                                                            <option>Producers Bank</option>
                                                                            <option>Queen City Development Bank</option>
                                                                            <option>Sterling Bank of Asia</option>
                                                                            <option>Enterprise Bank</option>
                                                                        </select>
                                                                    </div>
                                                                    <!-- /.input group -->
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- /Bank -->

                                                        <!-- Branch-->
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="txt_branch" class="col-sm-3 control-label">Branch</label>

                                                                <div class="col-sm-9">
                                                                    <div class="input-group">
                                                                        <div class="input-group-addon">
                                                                            <i class="fa fa-map-marker"></i>
                                                                        </div>
                                                                        <input type="text"
                                                                               class="form-control pull-right"
                                                                               id="txt_branch">
                                                                    </div>
                                                                    <!-- /.input group -->
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- /Branch -->

                                                        <!-- Check Date -->
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for=""
                                                                       class="col-sm-3 control-label">Check Date</label>

                                                                <div class="col-sm-9">
                                                                    <div class="input-group">

                                                                        <select id="txt_month" class="form-control"
                                                                                style="width:40%">
                                                                            <option value="01">January</option>
                                                                            <option value="02">February</option>
                                                                            <option value="03">March</option>
                                                                            <option value="04">April</option>
                                                                            <option value="05">May</option>
                                                                            <option value="06">June</option>
                                                                            <option value="07">July</option>
                                                                            <option value="08">August</option>
                                                                            <option value="09">September</option>
                                                                            <option value="10">October</option>
                                                                            <option value="11">November</option>
                                                                            <option value="12">December</option>
                                                                        </select>

                                                                        <select id="txt_day" class="form-control"
                                                                                style="width:30%">
                                                                            <option value="01">01</option>
                                                                            <option value="02">02</option>
                                                                            <option value="03">03</option>
                                                                            <option value="04">04</option>
                                                                            <option value="05">05</option>
                                                                            <option value="06">06</option>
                                                                            <option value="07">07</option>
                                                                            <option value="08">08</option>
                                                                            <option value="09">09</option>
                                                                            <option value="10">10</option>
                                                                            <option value="11">11</option>
                                                                            <option value="12">12</option>
                                                                            <option value="13">13</option>
                                                                            <option value="14">14</option>
                                                                            <option value="15">15</option>
                                                                            <option value="16">16</option>
                                                                            <option value="17">17</option>
                                                                            <option value="18">18</option>
                                                                            <option value="19">19</option>
                                                                            <option value="20">20</option>
                                                                            <option value="21">21</option>
                                                                            <option value="22">22</option>
                                                                            <option value="23">23</option>
                                                                            <option value="24">24</option>
                                                                            <option value="25">25</option>
                                                                            <option value="26">26</option>
                                                                            <option value="27">27</option>
                                                                            <option value="28">28</option>
                                                                            <option value="29">29</option>
                                                                            <option value="30">30</option>
                                                                            <option value="31">31</option>
                                                                        </select>
                                                                        <select id="txt_year" class="form-control"
                                                                                style="width:30%">
                                                                            <option value="1990"> 1990</option>
                                                                            <option value="1991"> 1991</option>
                                                                            <option value="1992"> 1992</option>
                                                                            <option value="1993"> 1993</option>
                                                                            <option value="1994"> 1994</option>
                                                                            <option value="1995"> 1995</option>
                                                                            <option value="1996"> 1996</option>
                                                                            <option value="1997"> 1997</option>
                                                                            <option value="1998"> 1998</option>
                                                                            <option value="1999"> 1999</option>
                                                                            <option value="2000"> 2000</option>
                                                                            <option value="2001"> 2001</option>
                                                                            <option value="2002"> 2002</option>
                                                                            <option value="2003"> 2003</option>
                                                                            <option value="2004"> 2004</option>
                                                                            <option value="2005"> 2005</option>
                                                                            <option value="2006"> 2006</option>
                                                                            <option value="2007"> 2007</option>
                                                                            <option value="2008"> 2008</option>
                                                                            <option value="2009"> 2009</option>
                                                                            <option value="2010"> 2010</option>
                                                                            <option value="2011"> 2011</option>
                                                                            <option value="2012"> 2012</option>
                                                                            <option value="2013"> 2013</option>
                                                                            <option value="2014"> 2014</option>
                                                                            <option value="2015"> 2015</option>
                                                                            <option value="2016" @if(date('Y')=='2016'))
                                                                                    selected @endif> 2016
                                                                            </option>
                                                                            <option value="2017" @if(date('Y')=='2017'))
                                                                                    selected @endif> 2017
                                                                            </option>
                                                                            <option value="2018" @if(date('Y')=='2018'))
                                                                                    selected @endif> 2018
                                                                            </option>
                                                                            <option value="2019" @if(date('Y')=='2019'))
                                                                                    selected @endif> 2019
                                                                            </option>
                                                                            <option value="2020" @if(date('Y')=='2020'))
                                                                                    selected @endif> 2020
                                                                            </option>
                                                                            <option value="2021" @if(date('Y')=='2021'))
                                                                                    selected @endif> 2021
                                                                            </option>
                                                                            <option value="2022" @if(date('Y')=='2022'))
                                                                                    selected @endif> 2022
                                                                            </option>
                                                                            <option value="2023" @if(date('Y')=='2023'))
                                                                                    selected @endif> 2023
                                                                            </option>
                                                                            <option value="2024" @if(date('Y')=='2024'))
                                                                                    selected @endif> 2024
                                                                            </option>
                                                                            <option value="2025" @if(date('Y')=='2025'))
                                                                                    selected @endif> 2025
                                                                            </option>
                                                                        </select>

                                                                    </div>

                                                                    <!-- /.input group -->
                                                                </div>
                                                                <!-- /.col-sm-9 -->
                                                            </div>
                                                            <!-- /.form-group -->
                                                        </div>
                                                        <!-- /Check Date -->

                                                        <!-- Check No-->
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="txt_check_no"
                                                                       class="col-sm-3 control-label">Check No</label>

                                                                <div class="col-sm-9">
                                                                    <div class="input-group">
                                                                        <div class="input-group-addon">
                                                                            <i class="fa fa-edit"></i>
                                                                        </div>
                                                                        <input type="text"
                                                                               class="form-control pull-right"
                                                                               id="txt_check_no">
                                                                    </div>
                                                                    <!-- /.input group -->
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- /Check No -->

                                                        <!-- Check No-->
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="txt_check_amount"
                                                                       class="col-sm-3 control-label">Check
                                                                    Amount</label>

                                                                <div class="col-sm-9">
                                                                    <div class="input-group">
                                                                        <div class="input-group-addon">
                                                                            <i class="fa fa-rub"></i>
                                                                        </div>
                                                                        <input type="text"
                                                                               class="form-control pull-right"
                                                                               id="txt_check_amount">
                                                                    </div>
                                                                    <!-- /.input group -->
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- /Check No -->

                                                        <!-- Check No-->
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="txt_check_amount"
                                                                       class="col-sm-3 control-label">Remarks</label>

                                                                <div class="col-sm-9">
                                                                    <div class="input-group">
                                                                        <div class="input-group-addon">
                                                                            <i class="fa fa-rub"></i>
                                                                        </div>
                                                                        <input type="text"
                                                                               class="form-control pull-right"
                                                                               id="txt_check_remarks">
                                                                    </div>
                                                                    <!-- /.input group -->
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- /Check No -->


                                                    </div>
                                                    <!-- ./col-md-12 -->
                                                </div>
                                                <!-- /.row-->
                                            </div>
                                            <!-- /.box-body -->
                                        </form>
                                        <!-- /form-end -->
                                    </div>
                                    <!-- //Check -->

                                    <!-- Add to List -->
                                    <div class="col-md-6 col-md-offset-6">
                                        <button type="button" id="btn_add_to_list" class="btn btn-primary pull-right"><i
                                                    class="fa fa-plus"></i>Add to List
                                        </button>

                                    </div>
                                    <!-- /Add to List -->

                                </div>
                                <!-- ./col-md-12 -->
                            </div>
                            <!-- /.row-->

                            <!-- Purchase Item Table -->
                            <div class="row">
                                <div class="col-md-12">
                                    <h6 class="page-header"><i class="fa fa-list-alt"></i> Payments List</h6>
                                    <!-- /row -->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table id="tbl_payments_list" class="table table-bordered table-striped">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Date</th>
                                                    <th>PO No</th>
                                                    <th>Type</th>
                                                    <th>Check No</th>
                                                    <th>Check Date</th>
                                                    <th>Deposit Date</th>
                                                    <th>Bank</th>
                                                    <th>Branch</th>
                                                    <th>Amount</th>
                                                    <th>Remarks</th>
                                                    <th>Status</th>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                </tbody>
                                                <tfoot>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Date</th>
                                                    <th>PO No</th>
                                                    <th>Type</th>
                                                    <th>Check No</th>
                                                    <th>Check Date</th>
                                                    <th>Deposit Date</th>
                                                    <th>Bank</th>
                                                    <th>Branch</th>
                                                    <th>Amount</th>
                                                    <th>Remarks</th>
                                                    <th>Status</th>
                                                </tr>
                                                </tfoot>
                                            </table>
                                        </div>

                                    </div>

                                </div>

                            </div>
                            <!-- /Purchase Item Table -->
                            <hr>
                            <!-- Remove From List -->
                            <div class="row hidden">

                                <div class="col-md-4">
                                    <div class="input-group">
                                        <select id="slct_status" class="form-control">
                                            <option>--Select Action--</option>
                                            <option value="For Deposit">For Deposit</option>
                                            <option value="Good">Good / Deposited</option>
                                            <option value="On Hold">On Hold</option>
                                            <option value="Closed">Accounts Closed</option>
                                            <option value="Daif">Daif</option>
                                            <option value="Daif2">Daif 2x</option>
                                            <option value="SPO">Stop Payment Order</option>
                                            <option value="Under Garnishment">Under Garnishment</option>
                                        </select>

                                        <div class="input-group-btn">
                                            <button type="button" id="btn_update_payments" class="btn btn-info">Go
                                            </button>
                                        </div>
                                        <!-- /btn-group -->
                                    </div>
                                </div>

                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-12">

                                            {{--<button class="btn btn-success pull-right"> Print Invoice</button>--}}
                                            <button type="button"
                                                    class="btn btn-warning pull-right @if(!isAccessModuleAllowed('payments_edit')) hidden @endif"
                                                    style="margin-right:1%" id="btn_remove_from_list"> Remove
                                                from List
                                            </button>

                                        </div>
                                    </div>
                                    <!-- /btn-group -->
                                </div>

                            </div>
                            <!-- /. Remove From List -->
                            <hr>
                            <div class="row">
                                <div class="col-md-4">
                                    <table class="table table-bordered">
                                        <tr>
                                            <th colspan="2">PAYMENT SUMMARY</th>
                                        </tr>
                                       <tr>
                                            <th>Total Checks + Cash</th>
                                            <td>P <label id="lbl_total_payment" style="text-align:left;">0.00</label>
                                            </td>
                                            <input type="hidden" id="total_amount_paid" value="0">
                                        </tr>
                                        <tr>
                                            <th>   Total Cash</th>
                                            <td>P <label id="lbl_total_cash" style="text-align:left;">0.00</label></td>
                                            <input type="hidden" id="total_cash_amount_paid" value="0">
                                        </tr>
                                        <tr>
                                            <th>   Total Checks Received</th>
                                            <td>P <label id="lbl_check_received" style="text-align:left;">0.00</label>
                                            </td>
                                            <input type="hidden" id="total_check_received" value="0">
                                        </tr>
                                        <tr>
                                            <th>      Total Cleared Checks</th>
                                            <td>P <label id="lbl_total_check" style="text-align:left;">0.00</label></td>
                                            <input type="hidden" id="total_check_amount_paid" value="0">
                                        </tr>
                                        <tr>
                                            <th>      Total Bad Checks</th>
                                            <td>P <label id="lbl_bad_checks" style="text-align:left;">0.00</label>
                                            </td>
                                            <input type="hidden" id="total_bad_checks" value="0">
                                        </tr>
                                        <tr>
                                            <th>Transaction Status</th>
                                            <td><label id="lbl_transaction_status" style="text-align:left;">N/A</label>
                                            </td>
                                        </tr>
                                    </table>

                                </div>

                                <div class="col-md-8">
                                    <table class="table table-bordered">
                                        <tr>
                                            <th colspan="2">PURCHASE SUMMARY</th>
                                        </tr>
                                        <tr>
                                            <th>Total Transaction Amount</th>
                                            <td>P <label id="lbl_total_transaction"
                                                         style="text-align:left;">0.00</label></td>
                                        </tr>
                                        <tr>
                                            <th>Total Good Payment</th>
                                            <td>P <label id="lbl_total_payment2" style="text-align:left;">0.00</label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Outstanding Balance</th>
                                            <td>P <label id="lbl_total_balance" style="text-align:left;">0.00</label>
                                            </td>
                                        </tr>
                                        </tr>
                                    </table>

                                </div>
                            </div>

                        </div>
                        <!-- /.box-body -->
                    </form>
                    <!-- /form-end -->
                </div>
                <!-- /.box -->
                <!--/.col (left) -->
            </div>
            <!-- search form -->
        </div>
        <!-- /Purchased Items -->
        <input type="hidden" id="txt_total_price">
        <input type="hidden" id="txt_total_payment">

        <div id="verify_complete_payment" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-orange-active">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><i class="ion ion-search"></i> Verify Payment</h4>
                    </div>
                    <div class="modal-body">
                        Payment is already sufficient. Do you want to mark this transaction as paid?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" id="btn_verify_payment" class="btn btn-warning" data-dismiss="modal"><i
                                    class="ion ion-check-round"></i>
                            Mark Paid
                        </button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->


        <div id="verify_remove_payment" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-orange-active">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><i class="ion ion-search"></i> Verify Remove Payment</h4>
                    </div>
                    <div class="modal-body">
                        Do you want to remove these payment/s?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" id="btn_verify_remove_payment" class="btn btn-warning"
                                data-dismiss="modal"><i class="ion ion-check-round"></i>
                            Remove Payment/s
                        </button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

        {{--MODAL--}}
        <div id="add-payment-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-blue-gradient">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><i class="ion ion-check"></i> Add Payment</h4>
                    </div>
                    <div class="modal-body">
                        Payment has been added.
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" id="btn_close_modal" data-dismiss="modal">Close
                        </button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        {{--MOdal End--}}

        {{--MODAL--}}
        <div id="remove-payment-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-blue-gradient">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><i class="fa fa-times"></i> Remove Payment</h4>
                    </div>
                    <div class="modal-body">
                        Payment has been removed.
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" id="btn_close_modal" data-dismiss="modal">Close
                        </button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        {{--MOdal End--}}

        <div id="update_payment_status" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-blue-active">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><i class="fa fa-edit"></i> Update Payment Status</h4>
                    </div>
                    <div class="modal-body">
                        Mark payment record as <label id="lbl_status_update"></label>?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                        <button type="button" id="btn_update_status_payment" class="btn btn-success"
                                data-dismiss="modal">Yes

                        </button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

        <div id="update_payment_remarks" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-blue-active">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><i class="fa fa-edit"></i> Update Payment Remarks</h4>
                    </div>
                    <div class="modal-body">
                        <textarea class="form-control" id="txt_remarks_update"></textarea>
                        <input type="hidden" id="hdn_payment_id">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                        <button type="button" id="btn_update_payment_status" class="btn btn-success"
                                data-dismiss="modal">Yes

                        </button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

        <div id="update_payment_deposit_date" class="modal fade" tabindex="-1" role="dialog"
             aria-labelledby="myModalLabel">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-blue-active">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><i class="fa fa-edit"></i> Update Payment Deposit Date</h4>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="payment_id">

                        <form class="form-horizontal">
                            <!-- Check No -->
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="txt_payment_date" class="col-sm-3 control-label">Check No</label>

                                    <div class="col-sm-9">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-rub"></i>
                                            </div>
                                            <label class="form-control pull-right" id="lbl_check_no"></label>
                                        </div>

                                        <!-- /.input group -->
                                    </div>
                                    <!-- /.col-sm-8 -->
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- Check No -->

                            <!-- Check Amount -->
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="txt_payment_date" class="col-sm-3 control-label">Check Amount</label>

                                    <div class="col-sm-9">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-rub"></i>
                                            </div>
                                            <label class="form-control pull-right" id="lbl_check_amount"></label>

                                        </div>

                                        <!-- /.input group -->
                                    </div>
                                    <!-- /.col-sm-8 -->
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- Check Amount -->

                            <!-- Check Date -->
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="txt_payment_date" class="col-sm-3 control-label">Check Date</label>

                                    <div class="col-sm-9">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <label class="form-control pull-right" id="lbl_check_date"></label>

                                        </div>

                                        <!-- /.input group -->
                                    </div>
                                    <!-- /.col-sm-8 -->
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- Check Date -->

                            <!-- Check Date -->
                            <div class="col-md-12" id="div_dep_date_1">
                                <div class="form-group">
                                    <label for="txt_payment_date" class="col-sm-3 control-label">Deposit Date 1</label>

                                    <div class="col-sm-9">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <label class="form-control pull-right" id="lbl_dep_date_1"></label>

                                        </div>

                                        <!-- /.input group -->
                                    </div>
                                    <!-- /.col-sm-8 -->
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- Check Date -->

                            <!-- Check Date -->
                            <div class="col-md-12" id="div_dep_date_2">
                                <div class="form-group">
                                    <label for="txt_payment_date" class="col-sm-3 control-label">Deposit Date 2</label>

                                    <div class="col-sm-9">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <label class="form-control pull-right" id="lbl_dep_date_2"></label>

                                        </div>

                                        <!-- /.input group -->
                                    </div>
                                    <!-- /.col-sm-8 -->
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- Check Date -->


                            <!-- Payment Date -->
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="txt_payment_date" class="col-sm-3 control-label">New Deposit
                                        Date</label>

                                    <div class="col-sm-9">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text"
                                                   class="form-control pull-right"
                                                   id="txt_new_deposit_date">
                                            <input type="hidden" id="new_dep_date">

                                        </div>

                                        <!-- /.input group -->`
                                    </div>
                                    <!-- /.col-sm-8 -->
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- /Payment Date -->
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" id="btn_update_deposit_date" class="btn btn-info" data-dismiss="modal">
                            Save

                        </button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>


    </section>
@endsection

@section('additional_footer')

    <script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/jquery.dataTables.min.js")}}'></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js")}}'></script>

    <script src='{{ asset("/bower_components/AdminLTE/plugins/jQueryUI/jquery-ui.min.js")}}'></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.full.min.js")}}'></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker.js")}}'></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/iCheck/icheck.min.js")}}'></script>

    <script>
        $(document).ready(function () {
            var new_deposit_date_value = '';
            $('#verify_complete_payment').modal({show: false});

            $('#txt_date_received').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                startDate: new Date(),
                locale: {
                    format: 'YYYY-MM-DD'
                }
            });

            $("#slct_bank").select2();

            $("#slct_payment_type").change(function () {
                if ($(this).val() == 'cash') {
                    $("#div_cash").show();
                    $("#div_check").hide();
                } else {
                    $("#div_cash").hide();
                    $("#div_check").show();
                }
            });

            //iCheck for checkbox and radio inputs
            $('input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass: 'iradio_minimal-blue'
            });

            var table_data = $('#tbl_payments_list').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true,
                "scrollX": true
            });
            $('#slct_client').select2({
                ajax: {
                    url: "/purchases/searchClient",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term // search term
                        };
                    },
                    processResults: function (data) {
                        // parse the results into the format expected by Select2.
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data
                        return {
                            results: data
                        };
                    },
                    cache: true
                },
                minimumInputLength: 2
            });

            $('#slct_client').on("select2:select", function (e) {
                $.get("/payments/getClientPO", {
                    client_id: $('#slct_client').val()
                }).done(function (data) {
                    $("#po_list").empty();
                    $("#po_list").append($('<option>', {value: ''})
                            .text('Select PO Number'));
                    $("#po_list").select2({
                        data: data
                    });
                });
            });

            $('#po_list').on("select2:select", function (e) {
                updatePaymentTable();
            });

            $("#btn_add_to_list").click(function () {

                if ($("#slct_client").select2("val") == null) {
                    alert('Client is required');
                    $("#slct_client").focus();
                    $("#slct_client").select2("open");
                    return false;
                }

                if ($("#po_list").select2("val") == '') {
                    alert('PO Number is required');
                    $("#po_list").focus();
                    $("#po_list").select2("open");
                    return false;
                }

                if ($("#txt_amount_cash").val() == "" && $("#txt_check_amount").val() == "") {
                    alert('Amount is required');
                    return false;
                }


                $.get("/payments/addClientPayment", {
                    po_id: $('#po_list').val(),
                    date_received: $("#txt_date_received").val(),
                    check_date: $("#txt_year").val() + '-' + $("#txt_month").val() + '-' + $("#txt_day").val(),
                    payment_type: $("#slct_payment_type").val(),
                    cash_amount: $("#txt_amount_cash").val(),
                    bank: $("#slct_bank").val(),
                    branch: $("#txt_branch").val(),
                    check_no: $("#txt_check_no").val(),
                    check_amount: $("#txt_check_amount").val(),
                    remarks: $("#txt_check_remarks").val()
                }).done(function (data) {
                    updatePaymentTable();
                });
            });
            $("#btn_verify_payment").click(function () {
                $.get("/payments/setTransactionPaid", {
                    transaction_id: $('#po_list').val()
                }).done(function (data) {
                    $("#div_add_payment").hide();
                    $("#lbl_transaction_status").html("Paid");
                });
            });

            $("#btn_remove_from_list").click(function () {
                $('#verify_remove_payment').modal({show: true});
            });

            $("#btn_verify_remove_payment").click(function () {
                var items = $("input[type=checkbox][name=po_payment]:checked");
                var for_removal = [];
                var amount_to_remove = 0;
                $("input[type=checkbox][name=po_payment]:checked").each(function () {
                    for_removal.push(this.value);
                    amount_to_remove += parseFloat($(this).next().val());
                });

                $.get("/payments/removePayments/", {
                    remove_items: for_removal
                }).done(function (data) {

                    updatePaymentTable();


                });

                $("#remove-payment-modal").modal('show');
            });

            $("#btn_update_payments").click(function () {
                if ($("#slct_status").val() == '--Select Action--') {
                    return false;
                }
                $("#update_payment_status").modal('show');
                $("#lbl_status_update").html($("#slct_status").val());

            });
            $("#btn_update_status_payment").click(function () {
                var items = $("input[type=checkbox][name=po_payment]:checked");
                var for_removal = [];
                $("input[type=checkbox][name=po_payment]:checked").each(function () {
                    for_removal.push(this.value);
                });

                $.get("/payments/updatePaymentStatus/", {
                    update_items: for_removal,
                    status: $("#slct_status").val(),
                    po_id: $('#po_list').val()
                }).done(function (data) {
                    updatePaymentTable();
                });
            });

            function updatePaymentTable() {

                var total_received_check = 0;
                //total_check_received
                $.get("/payments/getClientPayment", {
                    po_id: $('#po_list').val()
                }).done(function (data) {
                    var total_check = 0;
                    var total_cash = 0;
                    var total_bad_check = 0;
                    var total_check_received_w_replacement = 0;
                    table_data.clear().draw();
                    console.log(data.length);
                    if (data.length > 0) {
                        var ctr = 0;
                        for (ctr = 0; ctr < data.length; ctr++) {
                            console.log(data[ctr].status)
                            //alert(data[ctr].status);
                            if (data[ctr].type == 'cash') {
                                if (data[ctr].status == 'Good' || data[ctr].status == 'Verified') {
                                    total_cash += parseFloat(data[ctr].amount);
                                }

                                table_data.row.add(
                                        [
                                            "&nbsp;",
                                            data[ctr].remittance_date,
                                            data[ctr].purchase_order_no,
                                            data[ctr].type,
                                            "N/A",
                                            "N/A",
                                            "N/A",
                                            "N/A",
                                            "N/A",
                                            formatToNumberJS(data[ctr].amount),
                                            "N/A",
                                            data[ctr].status,
                                            "&nbsp;"
                                        ]).draw();

                            } else {
                                if (data[ctr].status != 'Replaced') {
                                    if(data[ctr].status != 'Cancelled'){
                                    total_received_check += parseFloat(data[ctr].amount);}
                                }
                                if (data[ctr].status == 'Good' || data[ctr].status == 'Verified') {
                                    total_check += parseFloat(data[ctr].amount);
                                    total_check_received_w_replacement += parseFloat(data[ctr].amount);
                                } else if (data[ctr].status == 'For Replacement') {
                                    total_check_received_w_replacement += parseFloat(data[ctr].amount);
                                    total_bad_check += parseFloat(data[ctr].amount);
                                }
                                else if (data[ctr].status != 'Good'
                                        && data[ctr].status != 'Verified'
                                        && data[ctr].status != 'Replaced'
                                        && data[ctr].status != 'Cancelled'
                                        && data[ctr].status != 'For Deposit') {
                                    total_bad_check += parseFloat(data[ctr].amount);
                                }
                                else {

                                }
                                if (data[ctr].status == 'Daif') {
                                    table_data.row.add(
                                            [
                                                "<input type=checkbox name='po_payment' value='" + data[ctr].id + "'><input type='hidden' id='" + data[ctr].id + "' value='" + data[ctr].amount + "'>",
                                                data[ctr].remittance_date,
                                                data[ctr].purchase_order_no,
                                                data[ctr].type,
                                                data[ctr].check_number,
                                                data[ctr].check_date,
                                                data[ctr].deposit_date1,
                                                data[ctr].bank,
                                                data[ctr].branch,
                                                formatToNumberJS(data[ctr].amount),
                                                data[ctr].remarks,
                                                data[ctr].status,


                                            ]).draw();
                                } else if (data[ctr].status == 'Daif2') {
                                    table_data.row.add(
                                            [
                                                "<input type=checkbox name='po_payment' value='" + data[ctr].id + "'><input type='hidden' id='" + data[ctr].id + "' value='" + data[ctr].amount + "'>",
                                                data[ctr].remittance_date,
                                                data[ctr].purchase_order_no,
                                                data[ctr].type,
                                                data[ctr].check_number,
                                                data[ctr].check_date,
                                                data[ctr].deposit_date2,
                                                data[ctr].bank,
                                                data[ctr].branch,
                                                formatToNumberJS(data[ctr].amount),
                                                data[ctr].remarks,
                                                data[ctr].status,

                                            ]).draw();
                                }
                                else if (data[ctr].status == 'On Hold') {
                                    table_data.row.add(
                                            [
                                                "<input type=checkbox name='po_payment' value='" + data[ctr].id + "'><input type='hidden' id='" + data[ctr].id + "' value='" + data[ctr].amount + "'>",
                                                data[ctr].remittance_date,
                                                data[ctr].purchase_order_no,
                                                data[ctr].type,
                                                data[ctr].check_number,
                                                data[ctr].check_date,
                                                data[ctr].deposit_date2,
                                                data[ctr].bank,
                                                data[ctr].branch,
                                                formatToNumberJS(data[ctr].amount),
                                                data[ctr].remarks,
                                                data[ctr].status,

                                            ]).draw();
                                }

                                else {

                                    table_data.row.add(
                                            [
                                                "<input type=checkbox name='po_payment' value='" + data[ctr].id + "'><input type='hidden' id='" + data[ctr].id + "' value='" + data[ctr].amount + "'>",
                                                data[ctr].remittance_date,
                                                data[ctr].purchase_order_no,
                                                data[ctr].type,
                                                data[ctr].check_number,
                                                data[ctr].check_date,
                                                data[ctr].deposit_date1,
                                                data[ctr].bank,
                                                data[ctr].branch,
                                                formatToNumberJS(data[ctr].amount),
                                                data[ctr].remarks,
                                                data[ctr].status,


                                            ]).draw();
                                }


                            }

                        }
                    } else {

                    }
                    $("#total_check_amount_paid").val(total_check_received_w_replacement);
                    $("#total_check_received").val(total_received_check);


                    $("#total_cash_amount_paid").val(total_cash);
                    $("#total_amount_paid").val(total_cash + total_check_received_w_replacement);

                    $("#total_bad_checks").val(total_bad_check);

                    $("#lbl_total_check").html(total_check.toFixed(2).toString().replace(/(\d)(?=(\d{3})+\.)/g, '$1,'));
                    $("#lbl_total_cash").html(total_cash.toFixed(2).toString().replace(/(\d)(?=(\d{3})+\.)/g, '$1,'));
                    $("#lbl_check_received").html(total_received_check.toFixed(2).toString().replace(/(\d)(?=(\d{3})+\.)/g, '$1,'));
                    $("#lbl_bad_checks").html(total_bad_check.toFixed(2).toString().replace(/(\d)(?=(\d{3})+\.)/g, '$1,'))

                    $("#lbl_total_payment").html((total_cash + total_received_check).toFixed(2).toString().replace(/(\d)(?=(\d{3})+\.)/g, '$1,'));
                    $("#lbl_total_payment2").html((total_cash + total_check).toFixed(2).toString().replace(/(\d)(?=(\d{3})+\.)/g, '$1,'));
                    $("#txt_total_payment").val(total_cash + total_check);

                    var total = total_cash + total_check;
                    var total_price = 0;

                    $.get("/payments/getTransactionDetails", {
                        transaction_id: $('#po_list').val()
                    }).done(function (data) {
                        console.log(data);
                        if (data[0].total_price != null) {
                            $("#lbl_total_transaction").html((parseFloat(data[0].total_price).toFixed(2).toString().replace(/(\d)(?=(\d{3})+\.)/g, '$1,')));
                            total_price = data[0].total_price;
                            $("#txt_total_price").val(total_price);
                            $("#lbl_total_balance").html((total_price - total).toFixed(2).toString().replace(/(\d)(?=(\d{3})+\.)/g, '$1,'));
                            if (total_price <= total) {
                                @if(!isAccessModuleAllowed('payments_edit'))
                                     $("#div_add_payment").hide();
                                @endif
                                    $("#lbl_transaction_status").html("Paid");
                            } else {
                                @if(!isAccessModuleAllowed('payments_edit'))
                                    $("#div_add_payment").show();
                                @endif
                                    $("#lbl_transaction_status").html("Incomplete");
                            }
                        } else {
                            @if(!isAccessModuleAllowed('payments_edit'))
                            $("#div_add_payment").show();
                            @endif

                            $("#lbl_transaction_status").html("Incomplete");
                            $("#lbl_total_balance").html("0.00");
                            $("#lbl_total_transaction").html("0.00");
                            $("#txt_total_price").html("0.00");
                        }

                    });


                });
            }


            $(document).on("click", ".open-AddBookDialog", function () {

                var payment_id = $(this).data('id');

                $.get("/payments/getPaymentDetail/", {
                    id: payment_id
                }).done(function (data) {
                    console.log(data);
                    var payment = data[0];

                    $("#lbl_check_no").html(payment.check_number);
                    $("#lbl_check_amount").html(payment.amount);
                    $("#lbl_check_date").html(payment.check_date);

                    if (payment.status == 'Daif' || payment.status == 'On Hold') {
                        $("#div_dep_date_1").hide();
                        $("#div_dep_date_2").hide();
                    } else if (payment.status == 'Daif2') {
                        $("#lbl_dep_date_1").html(payment.deposit_date1);
                        $("#div_dep_date_2").hide();
                    } else {
                        $("#lbl_dep_date_1").hide();
                        $("#div_dep_date_2").hide();
                    }


                    var formatted = "";

                    if (payment.status == 'Daif') {

                        $("#txt_new_deposit_date").val(payment.check_date);
                        var check_date = payment.check_date;
                        console.log(check_date);


                        var year = check_date.substring(0, 4);
                        var month = check_date.substring(5, 7);
                        var day = check_date.substring(8, 10);


                        $('#txt_new_deposit_date').daterangepicker({
                            singleDatePicker: true,
                            showDropdowns: true,
                            startDate: new Date(),
                            locale: {
                                format: 'YYYY-MM-DD'
                            },
                            minDate: new Date(year, (month - 1), day)
                        }, function (start, end, label) {
                            new_deposit_date_value = start.format('YYYY-MM-DD');
                        });

                    } else if (payment.status == 'Daif2') {

                        $("#txt_new_deposit_date").val(payment.deposit_date1);
                        var deposit_date1 = payment.deposit_date1;

                        var year = deposit_date1.substring(0, 4);
                        var month = deposit_date1.substring(5, 7);
                        var day = deposit_date1.substring(8, 10);

                        console.log(year);
                        console.log(month);
                        console.log(day);

                        $('#txt_new_deposit_date').daterangepicker({
                            singleDatePicker: true,
                            showDropdowns: true,
                            startDate: new Date(),
                            locale: {
                                format: 'YYYY-MM-DD'
                            },
                            minDate: new Date(year, (month - 1), day)
                        }, function (start, end, label) {
                            new_deposit_date_value = start.format('YYYY-MM-DD');
                        });
                    }

                });


                $(".modal-body #payment_id").val(payment_id);


            });

            $(document).on("click", ".open-UpdateRemarks", function () {

                var payment_id = $(this).data('id');


                $(".modal-body #hdn_payment_id").val(payment_id);


            });

            $("#btn_update_deposit_date").click(function () {
                var payment_id = $("#payment_id").val();

                $.get("/payments/updateDepositDate/", {
                    id: payment_id,
                    new_deposit_date: new_deposit_date_value
                }).done(function (data) {
                    console.log(data)
                    updatePaymentTable();
                });

            });

            $("#btn_update_payment_status").click(function () {
                var payment_id = $("#hdn_payment_id").val();

                $.get("/payments/updatePaymentRemarks/", {
                    id: payment_id,
                    remarks: $("#txt_remarks_update").val()
                }).done(function (data) {
                    console.log(data)
                    updatePaymentTable();
                });
            });

            if (parseInt($('#po_list').val()) != 0) {
                updatePaymentTable();
            }


        });
    </script>
@endsection



