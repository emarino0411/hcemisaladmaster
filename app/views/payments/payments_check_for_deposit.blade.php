@extends('admin_template')

@section('additional_header')

        <!-- DataTables -->
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.min.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker-bs3.css")}}'>

@endsection

@section('content')
        <!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <!-- general form elements -->
            <div class="com-md-12 box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Display Checks for Deposit</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <div class="box-body">
                    <div class="row">
                        <form role="form" class="form-horizontal">
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <label for="txt_payment_date" class="col-sm-6 control-label">Check Date / Deposit Date</label>

                                    <div class="col-sm-6">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text"
                                                   class="form-control pull-right"
                                                   id="txt_date_received">

                                        </div>

                                        <!-- /.input group -->
                                    </div>
                                    <!-- /.col-sm-8 -->
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <div class="col-md-2 col-sm-2">
                                <button id="btn_search" type="button" class="btn btn-primary"><i class="ion ion-search"></i> Search Checks
                                </button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
            <!-- /.box -->
            <!--/.col (left) -->
        </div>
        <!-- search form -->
    </div>
    <!-- /.row (main row) -->

    <div class="row">
        <div class=" col-md-12 col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-shopping-cart"></i> Checks for Deposit</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>&nbsp;</th>
                                <th>Deposit Date</th>
                                <th>Check Date</th>
                                <th>Bank</th>
                                <th>Check No</th>
                                <th>Amount</th>
                                <th>PO No</th>
                                <th>Client Name</th>
                                <th>Remarks</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                            <tfoot>
                            <tr>
                                <th>&nbsp;</th>
                                <th>Deposit Date</th>
                                <th>Check Date</th>
                                <th>Bank</th>
                                <th>Check No</th>
                                <th>Amount</th>
                                <th>PO No</th>
                                <th>Client Name</th>
                                <th>Remarks</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
    <!-- /Purchase Item Table -->

    <div class="row">
        <div class=" col-md-12 col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-edit"></i> Update Check Status</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <!-- Remove From List -->
                    <div class="row">

                        <div class="col-md-4">
                            <div class="input-group">
                                <select id="slct_status" class="form-control">
                                    <option>--Select Action--</option>
                                    <option value="For Deposit">For Deposit</option>
                                    <option value="Good">Good / Deposited</option>
                                    <option value="On Hold">On Hold</option>
                                    <option value="Closed">Accounts Closed</option>
                                    <option value="Daif">Daif</option>
                                    <option value="Daif2">Daif 2x</option>
                                    <option value="SPO">Stop Payment Order</option>
                                    <option value="Under Garnishment">Under Garnishment</option>
                                    <option value="Replaced">Replaced</option>
                                    <option value="For Replacement">For Replacement</option>
                                    <option value="Cancelled">Cancelled</option>
                                </select>

                                <div class="input-group-btn">
                                    <button type="button" id="btn_update_payments" class="btn btn-info">Go</button>
                                </div>
                                <!-- /btn-group -->
                            </div>
                        </div>

                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-md-12">

                                    {{--<button class="btn btn-success pull-right"> Print Invoice</button>--}}
                                    <button type="button"
                                            class="btn btn-warning pull-right @if(!isAccessModuleAllowed('payments_edit')) hidden @endif"
                                            style="margin-right:1%" id="btn_remove_from_list"> Remove
                                        from List
                                    </button>

                                </div>
                            </div>
                            <!-- /btn-group -->
                        </div>

                    </div>
                    <!-- /. Remove From List -->
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
        
    <div id="no-record-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-blue-gradient">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><i class="fa fa-close"></i> No Record To Display</h4>
                </div>
                <div class="modal-body">
                    There are no checks for deposit today.
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" id="btn_close_modal" data-dismiss="modal">Close
                    </button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

        <div id="update_payment_status" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-blue-active">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><i class="fa fa-edit"></i> Update Payment Status</h4>
                    </div>
                    <div class="modal-body">
                        Mark payment record as <label id="lbl_status_update"></label>?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                        <button type="button" id="btn_update_status_payment" class="btn btn-success" data-dismiss="modal">Yes
                            
                        </button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

        <div id="update_payment_remarks" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-blue-active">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><i class="fa fa-edit"></i> Update Payment Remarks</h4>
                    </div>
                    <div class="modal-body">
                        <textarea class="form-control" id="txt_remarks_update"></textarea>
                        <input type="hidden" id="hdn_payment_id">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                        <button type="button" id="btn_update_payment_status" class="btn btn-success" data-dismiss="modal">Yes
                            
                        </button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

         <div id="update_payment_deposit_date" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-blue-active">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><i class="fa fa-edit"></i> Update Payment Deposit Date</h4>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="payment_id">
                        <form class="form-horizontal">
                            <!-- Check No -->
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="txt_payment_date" class="col-sm-3 control-label">Check No</label>
                                    
                                    <div class="col-sm-9">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-rub"></i>
                                            </div>
                                            <label class="form-control pull-right" id="lbl_check_no"></label>
                                        </div>

                                        <!-- /.input group -->
                                    </div>
                                    <!-- /.col-sm-8 -->
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- Check No -->

                            <!-- Check Amount -->
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="txt_payment_date" class="col-sm-3 control-label">Check Amount</label>
                                    
                                    <div class="col-sm-9">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-rub"></i>
                                            </div>
                                            <label class="form-control pull-right" id="lbl_check_amount"></label>

                                        </div>

                                        <!-- /.input group -->
                                    </div>
                                    <!-- /.col-sm-8 -->
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- Check Amount -->

                            <!-- Check Date -->
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="txt_payment_date" class="col-sm-3 control-label">Check Date</label>
                                    
                                    <div class="col-sm-9">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <label class="form-control pull-right" id="lbl_check_date"></label>

                                        </div>

                                        <!-- /.input group -->
                                    </div>
                                    <!-- /.col-sm-8 -->
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- Check Date -->

                            <!-- Check Date -->
                            <div class="col-md-12" id="div_dep_date_1">
                                <div class="form-group">
                                    <label for="txt_payment_date" class="col-sm-3 control-label">Deposit Date 1</label>
                                    
                                    <div class="col-sm-9">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <label class="form-control pull-right" id="lbl_dep_date_1"></label>

                                        </div>

                                        <!-- /.input group -->
                                    </div>
                                    <!-- /.col-sm-8 -->
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- Check Date -->

                               <!-- Check Date -->
                            <div class="col-md-12" id="div_dep_date_2">
                                <div class="form-group">
                                    <label for="txt_payment_date" class="col-sm-3 control-label">Deposit Date 2</label>
                                    
                                    <div class="col-sm-9">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <label class="form-control pull-right" id="lbl_dep_date_2"></label>

                                        </div>

                                        <!-- /.input group -->
                                    </div>
                                    <!-- /.col-sm-8 -->
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- Check Date -->


                            <!-- Payment Date -->
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="txt_payment_date" class="col-sm-3 control-label">New Deposit Date</label>
                                    
                                    <div class="col-sm-9">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text"
                                                   class="form-control pull-right"
                                                   id="txt_new_deposit_date">
                                            <input type="hidden" id="new_dep_date">

                                        </div>

                                        <!-- /.input group -->`
                                    </div>
                                    <!-- /.col-sm-8 -->
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- /Payment Date -->
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" id="btn_update_deposit_date" class="btn btn-info" data-dismiss="modal">Save
                            
                        </button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>


        <div id="update_payment_status" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-blue-active">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><i class="fa fa-edit"></i> Update Payment Status</h4>
                    </div>
                    <div class="modal-body">
                        Mark payment record as <label id="lbl_status_update"></label>?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                        <button type="button" id="btn_update_status_payment" class="btn btn-success" data-dismiss="modal">Yes
                            
                        </button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

</section>

@endsection

@section('additional_footer')

    <script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/jquery.dataTables.min.js")}}'></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js")}}'></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.full.min.js")}}'></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker.js")}}'></script>


    <script>
        var date_from;
        var date_to;
        var new_deposit_date_value = '';
        $(document).ready(function () {
           
            var table_data = $('#example1').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true
            });
            
            $('#txt_date_received').daterangepicker({
                showDropdowns: true,
                startDate: new Date(),
                locale: {
                    format: 'YYYY-MM-DD'
                }
            });

            $('#txt_date_received').on('apply.daterangepicker', function (ev, picker) {
                date_from = picker.startDate.format('YYYY-MM-DD');
                date_to = picker.endDate.format('YYYY-MM-DD');
                
            });


            $("#btn_search").click(function(){
                updatePaymentTable();
            });
            

            $(document).on("click", ".open-AddBookDialog", function () {

                var payment_id = $(this).data('id');

                $.get("/payments/getPaymentDetail/", {
                    id: payment_id
                }).done(function (data) {
                    console.log(data);
                    var payment = data[0];

                    $("#lbl_check_no").html(payment.check_number);
                    $("#lbl_check_amount").html(payment.amount);
                    $("#lbl_check_date").html(payment.check_date);

                    if(payment.status=='Daif' || payment.status=='On Hold' ){
                        $("#div_dep_date_1").hide();
                        $("#div_dep_date_2").hide();
                    }else if(payment.status=='Daif2'){
                        $("#lbl_dep_date_1").html(payment.deposit_date1);
                        $("#div_dep_date_2").hide();
                    }else{
                        $("#lbl_dep_date_1").hide();
                        $("#div_dep_date_2").hide();
                    }
                    


                    var formatted = "";

                    if( payment.status == 'Daif'){
                        
                        $("#txt_new_deposit_date").val(payment.check_date);
                        var check_date = payment.check_date;
                        console.log(check_date);


                        var year = check_date.substring(0, 4);
                        var month = check_date.substring(5, 7);
                        var day = check_date.substring(8, 10);


                        $('#txt_new_deposit_date').daterangepicker({
                            singleDatePicker: true,
                            showDropdowns: true,
                            startDate: new Date(),
                            locale: {
                                format: 'YYYY-MM-DD'
                            },
                            minDate: new Date(year, (month-1), day)
                        }, function(start, end, label) {
                            new_deposit_date_value = start.format('YYYY-MM-DD');
                        });
                       
                    }else if( payment.status == 'Daif2'){

                        $("#txt_new_deposit_date").val(payment.deposit_date1);
                        var deposit_date1 = payment.deposit_date1;

                        var year = deposit_date1.substring(0, 4);
                        var month = deposit_date1.substring(5, 7);
                        var day = deposit_date1.substring(8, 10);

                        console.log(year);
                        console.log(month);
                        console.log(day);

                        $('#txt_new_deposit_date').daterangepicker({
                            singleDatePicker: true,
                            showDropdowns: true,
                            startDate: new Date(),
                            locale: {
                                format: 'YYYY-MM-DD'
                            },
                            minDate: new Date(year, (month-1), day)
                        }, function(start, end, label) {
                            new_deposit_date_value = start.format('YYYY-MM-DD');
                        });
                     }else if( payment.status == 'On Hold'){

                        
                        $("#txt_new_deposit_date").val(payment.check_date);
                        var check_date = payment.check_date;
                        console.log(check_date);


                        var year = check_date.substring(0, 4);
                        var month = check_date.substring(5, 7);
                        var day = check_date.substring(8, 10);

                        $('#txt_new_deposit_date').daterangepicker({
                            singleDatePicker: true,
                            showDropdowns: true,
                            startDate: new Date(),
                            locale: {
                                format: 'YYYY-MM-DD'
                            },
                            minDate: new Date(year, (month-1), day)
                        }, function(start, end, label) {
                            new_deposit_date_value = start.format('YYYY-MM-DD');
                        });
                    }
                    
                });

                
                $(".modal-body #payment_id").val( payment_id );


            });

            $(document).on("click", ".open-UpdateRemarks", function () {

                var payment_id = $(this).data('id');

                
                $(".modal-body #hdn_payment_id").val( payment_id );


            });

            $("#btn_update_deposit_date").click(function(){
                var payment_id = $("#payment_id").val();
                $.get("/payments/updateDepositDate/", {
                    id: payment_id,
                    new_deposit_date: new_deposit_date_value
                }).done(function (data) {
                    console.log(data)
                    updatePaymentTable();
                });

            });

            $("#btn_update_payment_status").click(function(){
                var payment_id = $("#hdn_payment_id").val();

                $.get("/payments/updatePaymentRemarks/", {
                    id: payment_id,
                    remarks: $("#txt_remarks_update").val()
                }).done(function (data) {
                    console.log(data)
                    updatePaymentTable();
                    $("#txt_remarks_update").val("");
                });
            });

            function updatePaymentTable(){
                $.get("/payments/getCheckForDeposit", {
                    _date_from: date_from,
                    _date_to: date_to
                }).done(function (data) {
                    table_data.clear().draw();

                    if(data.length==0){
                        $("#no-record-modal").modal('show');
                        return false;    
                    }else{
                        for(ctr=0;ctr<data.length;ctr++){
                            if(data[ctr].status == 'Daif'){
                                table_data.row.add(
                                    [   
                                        "<input type=checkbox name='po_payment' value='" + data[ctr].id + "'><input type='hidden' id='" + data[ctr].id + "' value='" + data[ctr].amount + "'>",
                                        data[ctr].deposit_date1,
                                        data[ctr].check_date,
                                        data[ctr].bank,
                                        data[ctr].check_number,
                                        formatToNumberJS(data[ctr].amount),
                                        data[ctr].purchase_order_no,
                                        data[ctr].first_name+ " "+data[ctr].last_name,
                                        data[ctr].remarks,
                                        data[ctr].status,
                                        "<button type='button' class='btn btn-xs btn-primary open-update-date-modal open-AddBookDialog btn btn-primary fa fa-calendar'  data-toggle=\"modal\" data-id='"+data[ctr].id+"' title=\"Update Deposit Date\" href=\"#update_payment_deposit_date\"></button> &nbsp;" +
                                        "<button type='button' class='btn btn-xs btn-info open-update-date-modal open-UpdateRemarks btn btn-info fa fa-edit'  data-toggle=\"modal\" data-id='"+data[ctr].id+"' title=\"Update Payment Remarks\" href=\"#update_payment_remarks\"></button>"
                                    ]
                                ).draw();
                            }

                            else if(data[ctr].status == 'On Hold'){
                                table_data.row.add(
                                    [
                                    "<input type=checkbox name='po_payment' value='" + data[ctr].id + "'><input type='hidden' id='" + data[ctr].id + "' value='" + data[ctr].amount + "'>",
                                        data[ctr].deposit_date1,
                                        data[ctr].check_date,
                                        data[ctr].bank,
                                        data[ctr].check_number,
                                        formatToNumberJS(data[ctr].amount),
                                        data[ctr].purchase_order_no,
                                        data[ctr].first_name+ " "+data[ctr].last_name,
                                        data[ctr].remarks,
                                        data[ctr].status,
                                        "<button type='button' class='btn btn-xs btn-primary open-update-date-modal open-AddBookDialog btn btn-primary fa fa-calendar'  data-toggle=\"modal\" data-id='"+data[ctr].id+"' title=\"Update Deposit Date\" href=\"#update_payment_deposit_date\"></button> &nbsp;" +
                                        "<button type='button' class='btn btn-xs btn-info open-update-date-modal open-UpdateRemarks btn btn-info fa fa-edit'  data-toggle=\"modal\" data-id='"+data[ctr].id+"' title=\"Update Payment Remarks\" href=\"#update_payment_remarks\"></button>"
                                    ]
                                ).draw();
                            }

                            else if(data[ctr].status == 'Daif2'){
                                table_data.row.add(
                                    [
                                    "<input type=checkbox name='po_payment' value='" + data[ctr].id + "'><input type='hidden' id='" + data[ctr].id + "' value='" + data[ctr].amount + "'>",
                                        data[ctr].deposit_date2,
                                        data[ctr].check_date,
                                        data[ctr].bank,
                                        data[ctr].check_number,
                                        formatToNumberJS(data[ctr].amount),
                                        data[ctr].purchase_order_no,
                                        data[ctr].first_name+ " "+data[ctr].last_name,
                                        data[ctr].remarks,
                                        data[ctr].status,
                                        "<button type='button' class='btn btn-xs btn-primary open-update-date-modal open-AddBookDialog btn btn-primary fa fa-calendar'  data-toggle=\"modal\" data-id='"+data[ctr].id+"' title=\"Update Deposit Date\" href=\"#update_payment_deposit_date\"></button> &nbsp;" +
                                        "<button type='button' class='btn btn-xs btn-info open-update-date-modal open-UpdateRemarks btn btn-info fa fa-edit'  data-toggle=\"modal\" data-id='"+data[ctr].id+"' title=\"Update Payment Remarks\" href=\"#update_payment_remarks\"></button>"
                                    ]
                                ).draw();
                            }else{
                                table_data.row.add(
                                    [
                                    "<input type=checkbox name='po_payment' value='" + data[ctr].id + "'><input type='hidden' id='" + data[ctr].id + "' value='" + data[ctr].amount + "'>",
                                        data[ctr].deposit_date1,
                                        data[ctr].check_date,
                                        data[ctr].bank,
                                        data[ctr].check_number,
                                        formatToNumberJS(data[ctr].amount),
                                        data[ctr].purchase_order_no,
                                        data[ctr].first_name+ " "+data[ctr].last_name,
                                        data[ctr].remarks,
                                        data[ctr].status,
                                         "<button type='button' class='btn btn-xs btn-info open-update-date-modal open-UpdateRemarks btn btn-info fa fa-edit'  data-toggle=\"modal\" data-id='"+data[ctr].id+"' title=\"Update Payment Remarks\" href=\"#update_payment_remarks\"></button>"
                                    ]
                                ).draw();
                            }
                            
                        }
                    }
                    
                    
                });
            }
            $("#btn_update_status_payment").click(function(){
                var items = $("input[type=checkbox][name=po_payment]:checked");
                var for_removal = [];
                $("input[type=checkbox][name=po_payment]:checked").each(function () {
                    for_removal.push(this.value);
                });

                $.get("/payments/updatePaymentStatus/", {
                    update_items: for_removal,
                    status: $("#slct_status").val(),
                    po_id: $('#po_list').val()
                }).done(function (data) {
                    console.log(data);
                    updatePaymentTable();
                });
            });

            $("#btn_update_payments").click(function () {
                if($("#slct_status").val()=='--Select Action--'){
                    return false;
                }
                $("#update_payment_status").modal('show');
                $("#lbl_status_update").html($("#slct_status").val());

            });

            
        });

        
    </script>
@endsection