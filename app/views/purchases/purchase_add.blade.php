@extends('admin_template')

@section('additional_header')

    <link rel="stylesheet"
          href='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker-bs3.css")}}'>
    <link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/iCheck/all.css")}}'>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.css")}}'>
    <link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.min.css")}}'>

    @endsection

    @section('content')
            <!-- Main content -->
    <section class="content">
        <!-- Purchase Order -->
        <div class="row">
            <div class="col-md-12 col-xs-12">

                <div class="com-md-12 box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-shopping-cart"></i> Purchase Order</h3>
                    </div>
                    <!-- /.box-header -->

                    <!-- form start -->
                    <form class="form-horizontal">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <!-- PO Number -->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="txt_po_no" class="col-sm-3 control-label">PO No</label>

                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-edit"></i>
                                                    </div>

                                                    <input type="text" class="form-control pull-right"
                                                           value=""
                                                           id="txt_purchase_order_no">

                                                    <input type="hidden" class="form-control pull-right"
                                                           value="{{$purchase_id}}" id="txt_po_no">
                                                    <input type="hidden" class="form-control pull-right"
                                                           value="{{$purchase_id}}" id="hdn_po_no">
                                                </div>
                                                <!-- /.input group -->
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /PO Number -->
                                    <!-- Purchase Date -->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="txt_purchase_date" class="col-sm-3 control-label">Purchase
                                                Date</label>

                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </div>
                                                    <input type="text" class="form-control pull-right"
                                                           id="txt_purchase_date">
                                                </div>

                                                <!-- /.input group -->
                                            </div>
                                            <!-- /.col-sm-8 -->
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                    <!-- /.col-md-6 -->

                                    <!-- Delivery Date -->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="txt_delivery_date" class="col-sm-3 control-label">Delivery
                                                Date</label>

                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </div>
                                                    <input type="text" class="form-control pull-right"
                                                           id="txt_delivery_date">
                                                </div>

                                                <!-- /.input group -->
                                            </div>
                                            <!-- /.col-sm-9 -->
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                    <!-- /Delivery Date -->

                                    <!-- Terms -->
                                    <div class="col-md-6">
                                        <div class="form-group" id="set">
                                            <label for="slct_terms" class="col-sm-3 control-label">Terms</label>

                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-rub"></i>
                                                    </div>
                                                    <select id="slct_terms" class="form-control select2">
                                                        <option value="cod">Cash On Delivery</option>
                                                        <option value="1">30 Days Installment</option>
                                                        <option value="2">2 Months Installment</option>
                                                        <option value="3">3 Months Installment</option>
                                                        <option value="4">4 Months Installment</option>
                                                        <option value="5">5 Months Installment</option>
                                                        <option value="6">6 Months Installment</option>
                                                        <option value="7">7 Months Installment</option>
                                                        <option value="8">8 Months Installment</option>
                                                        <option value="9">9 Months Installment</option>
                                                        <option value="10">10 Months Installment</option>
                                                        <option value="11">11 Months Installment</option>
                                                        <option value="12">12 Months Installment</option>
                                                        <option value="13">13 Months Installment</option>
                                                        <option value="14">14 Months Installment</option>
                                                        <option value="15">15 Months Installment</option>
                                                        <option value="16">16 Months Installment</option>
                                                        <option value="17">17 Months Installment</option>
                                                        <option value="18">18 Months Installment</option>
                                                        <option value="19">19 Months Installment</option>
                                                        <option value="20">20 Months Installment</option>
                                                        <option value="21">21 Months Installment</option>
                                                        <option value="22">22 Months Installment</option>
                                                        <option value="23">23 Months Installment</option>
                                                        <option value="24">24 Months Installment</option>

                                                    </select>
                                                </div>
                                                <!-- /.input group -->
                                            </div>
                                            <!-- /.col-sm-9 -->
                                        </div>
                                        <!-- /. form-group -->
                                    </div>
                                    <!-- /.Terms -->

                                    <!-- Client -->
                                    <div class="col-md-6">
                                        <div class="form-group" id="client">
                                            <label for="slct_terms" class="col-sm-3 control-label">Client</label>

                                            <div class="col-sm-9">
                                                <select id="slct_clients" class="form-control"
                                                        style="width: 100%;height:100%">
                                                </select>
                                                <!-- /.input group -->
                                            </div>
                                            <!-- /.col-sm-9 -->
                                        </div>
                                        <!-- /. form-group -->
                                    </div>
                                    <!-- /.Client -->

                                    <!-- Client -->
                                    <div class="col-md-6">
                                        <div class="form-group" id="client">
                                            <label for="slct_terms" class="col-sm-3 control-label">New Client?</label>

                                            <div class="col-sm-9">
                                                <a target="_blank" href="/clients/addNew?purchase=true" id="btn_search"
                                                   type="button"
                                                   class="btn btn-primary"><i class="ion ion-android-person-add"></i>
                                                    Add
                                                    Client</a>
                                            </div>
                                            <!-- /.col-sm-9 -->
                                        </div>
                                        <!-- /. form-group -->
                                    </div>
                                    <!-- /.Client -->

                                </div>
                                <!-- ./col-md-12 -->
                            </div>
                            <!-- /.row-->
                        </div>
                        <!-- /.box-body -->
                    </form>
                    <!-- /form-end -->
                </div>
                <!-- /.box -->
                <!--/.col (left) -->
            </div>
            <!-- search form -->
        </div>
        <!-- /Purchase Order -->

        <!-- Client Information -->
        <div class="row hidden">
            <div class=" col-md-12 col-xs-12">
                <div class="row">
                    <!-- client -->
                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="box-header">
                                <h3 class="box-title"><i class="fa fa-user"></i> Client Information</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="col-md-12">
                                    <div class="row">
                                        <!-- client information -->
                                        <div class="pull-right">
                                            <!-- Buttons -->
                                            <button id="btn_search" type="button" class="btn bg-olive"><i
                                                        class="ion ion-search"></i> Search Client
                                            </button>
                                            <a href="/clients/addNew?purchase=true" id="btn_search" type="button"
                                               class="btn btn-primary"><i class="ion ion-android-person-add"></i> New
                                                Client</a>
                                        </div>
                                        <!-- /.col-md-4 -->
                                    </div>
                                    <hr style="margin:2% 0 0 0">
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-2">
                                        <!-- Profile Image -->
                                        <img class="profile-user-img img-responsive img-circle"
                                             src='{{ asset ("/bower_components/AdminLTE/dist/img/user4-128x128.jpg") }}'
                                             style="margin-top:3%" alt="User profile picture">

                                        <h3 class="profile-username text-center">Nina Mcintire</h3>
                                        <a href="#" class="btn btn-primary btn-block"><b>View Profile</b></a>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="box-header with-border">
                                            <h3 class="box-title">Personal Information</h3>
                                        </div>
                                        <!-- /.box-header -->
                                        <div class="box-body">
                                            <strong><i class="fa fa-home margin-r-5"></i><i
                                                        class="fa fa-book marker-r-5"></i> Home Address</strong>

                                            <p class="text-muted">
                                                74 San Isidro St. Brgy. Sto. Niño, Quezon City Philippines 1113
                                            </p>

                                            <strong><i class="fa fa-home margin-r-5"></i><i
                                                        class="fa fa-map- fa-phone margin-r-5"></i> Contact
                                                Number</strong>

                                            <p class="text-muted">09258050517</p>

                                            <strong><i class="fa fa-home margin-r-5"></i><i
                                                        class="fa fa-envelope-o margin-r-5"></i> Email</strong>

                                            <p>emaymarino@gmail.com</p>
                                        </div>
                                        <!-- /.box-body -->
                                    </div>
                                    <div class="col-md-5">
                                        <div class="box-header with-border">
                                            <h3 class="box-title">Business Information</h3>
                                        </div>
                                        <!-- /.box-header -->
                                        <div class="box-body">
                                            <strong><i class="fa fa-briefcase margin-r-5"></i><i
                                                        class="fa fa-marker margin-r-5"></i> Business Address</strong>

                                            <p class="text-muted">
                                                74 San Isidro St. Brgy. Sto. Niño, Quezon City Philippines 1113
                                            </p>

                                            <strong><i class="fa fa-briefcase margin-r-5"></i><i
                                                        class="fa fa-map- fa-phone margin-r-5"></i> Contact
                                                Number</strong>

                                            <p class="text-muted">09258050517</p>

                                            <strong><i class="fa fa-briefcase margin-r-5"></i><i
                                                        class="fa fa-envelope-o margin-r-5"></i> Email</strong>

                                            <p>emaymarino@gmail.com</p>
                                        </div>
                                        <!-- /.box-body -->
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                    <!-- /client -->

                </div>
            </div>
        </div>
        <!-- / client information -->

        <!-- Purchased Items -->
        <div class="row">
            <div class="col-md-12 col-xs-12">

                <div class="com-md-12 box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-cutlery"></i> Purchase Items</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form class="form-horizontal">
                        <div class="box-body">

                            <div class="row">
                                <div class="col-md-12">
                                    <!-- Purchase Type -->
                                    <div class="col-md-6 hidden">
                                        <div class="form-group">
                                            <label for="slct_purchase_type" class="col-sm-3 control-label">Purchase
                                                Type</label>

                                            <div class="col-sm-9">

                                                <select id="slct_purchase_type" class="form-control select2"
                                                        style="width: 100%;height:100%">
                                                    <option value="">Select Item Type</option>
                                                    <option value="item">Per Item</option>
                                                    <option value="set">Per Set</option>
                                                </select>
                                            </div>
                                            <!-- /.col-sm-9 -->
                                        </div>
                                    </div>
                                    <!-- /Purchase Type -->

                                    <!-- Item / Set -->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="slct_purchase_item" id="lbl_item_set"
                                                   class="col-sm-3 control-label">Item/Set</label>

                                            <div class="col-sm-9">
                                                <select id="slct_purchase_item" class="set form-control select2"
                                                        style="width: 100%;height:100%;">
                                                </select>

                                            </div>
                                            <!-- /.col-sm-9 -->
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                    <!-- /Item / Set -->

                                    <!-- Qty -->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="txt_qty" class="col-sm-3 control-label">Quantity</label>

                                            <div class="col-sm-9">
                                                <input type="text" value="1" class="form-class" id="txt_qty">
                                            </div>
                                            <!-- /.col-sm-8 -->
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                    <!-- /.Qty -->

                                    <!-- Gift -->
                                    <div class="col-md-6">
                                        <div class="form-group" id="set">
                                            <label for="rdo_gift" class="col-sm-3 control-label">Is this a gift?</label>

                                            <div class="col-sm-9">
                                                <label style="margin-top:2%;margin-left:2%">
                                                    <input type="radio" name="is_gift" value="Yes" class="minimal"
                                                    >
                                                    Yes
                                                </label>
                                                <label style="margin-top:2%;margin-left:2%">
                                                    <input type="radio" name="is_gift" value="No" class="minimal"
                                                           checked>
                                                    No
                                                </label>

                                            </div>
                                            <!-- /.col-sm-9 -->
                                        </div>
                                        <!-- /. form-group -->
                                    </div>
                                    <!-- //Gift -->

                                    <!-- Price -->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="lbl_item_set_price" class="col-sm-3 control-label">Price</label>

                                            <div class="col-sm-9">
                                                <label for="lbl_item_set" id="lbl_item_set_price" class="control-label"><i
                                                            class="fa fa-rub"></i> 0.00</label>
                                                <input type="hidden" id="total_item_price">
                                                <input type="hidden" id="unit_cost">
                                            </div>
                                            <!-- /.col-sm-9 -->
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                    <!-- /Price -->

                                    <!-- Add to List -->
                                    <div class="col-md-6">
                                        <button class="btn btn-danger pull-right" style="margin-left:3%"> Reset</button>
                                        <button type="button" id="btn_add_purchase_item"
                                                class="btn btn-primary pull-right"><i class="fa fa-plus"></i>Add to List
                                        </button>

                                    </div>
                                    <!-- /Add to List -->

                                </div>
                                <!-- ./col-md-12 -->
                            </div>
                            <!-- /.row-->

                            <hr style="margin-top:1%">

                            <!-- Total Purchase Price -->
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-md-4 col-md-offset-5">
                                            <h2 class="text-danger">
                                                <label class="control-label">Total Purchase
                                                    Price: </label>

                                            </h2>
                                        </div>
                                        <div class="col-md-3">
                                            <h2 class="text-danger">

                                                <label for="lbl_total_purchase_price" id="lbl_total_purchase_price"
                                                       class="control-label "><i
                                                            class="fa fa-rub"></i> 0.00</label>
                                            </h2>
                                            <input type="hidden" id="total_purchase_price" value="0">

                                        </div>


                                        <!-- /.col-sm-9 -->
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                            </div>

                            <!-- Purchase Item Table -->
                            <div class="row">
                                <div class="col-md-12">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>&nbsp;</th>
                                            <th>Item/Set</th>
                                            <th>Qty</th>
                                            <th>Unit Cost</th>
                                            <th>Is Gift</th>
                                            <th>Total Amount</th>
                                        </tr>
                                        </thead>
                                        <tbody id="purchase_item_list">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /Purchase Item Table -->

                            <!-- Remove From List -->
                            <div class="row">
                                <div class="col-md-12">
                                    <button class="btn bg-orange pull-right" type="button" id="btn_remove_from_list">
                                        Remove From List
                                    </button>
                                </div>
                            </div>
                            <!-- /. Remove From List -->
                        </div>
                        <!-- /.box-body -->
                    </form>
                    <!-- /form-end -->
                </div>
                <!-- /.box -->
                <!--/.col (left) -->
            </div>
            <!-- search form -->
        </div>
        <!-- /Purchased Items -->

        <div class="row">
            <div class=" col-md-12">
                <!-- Representative -->
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title"><i class="fa fa-users"></i> Representative</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">

                        <!-- form start -->
                        <form class="form-horizontal">
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-12">


                                        <!-- Sponsor -->
                                        <div class="col-md-6">
                                            <div class="form-group" id="client">
                                                <label for="txt_sponsor" class="col-sm-3 control-label">Sponsor</label>

                                                <div class="col-sm-9">
                                                    <select id="txt_sponsor" class="form-control"
                                                            style="width: 100%;height:100%">
                                                    </select>
                                                    <!-- /.input group -->
                                                </div>
                                                <!-- /.col-sm-9 -->
                                            </div>
                                            <!-- /. form-group -->
                                        </div>
                                        <!-- /.Sponsor -->

                                        <!-- Consultant -->
                                        <div class="col-md-6">
                                            <div class="form-group" id="set">
                                                <label for="txt_consultant"
                                                       class="col-sm-3 control-label">Presenter</label>

                                                <div class="col-sm-9">
                                                    <select id="txt_consultant" class="form-control"
                                                            style="width: 100%;height:100%">
                                                    </select>
                                                    <!-- /.input group -->
                                                </div>
                                                <!-- /.col-sm-9 -->
                                                <!-- /.col-sm-9 -->
                                            </div>
                                            <!-- /. form-group -->
                                        </div>
                                        <!-- /.Consultant -->


                                        <!-- Associate -->
                                        <div class="col-md-6">
                                            <div class="form-group" id="set">
                                                <label for="txt_associate"
                                                       class="col-sm-3 control-label">Associate</label>

                                                <div class="col-sm-9">
                                                    <select id="txt_associate" class="form-control"
                                                            style="width: 100%;height:100%">
                                                    </select>
                                                    <!-- /.input group -->
                                                </div>
                                                <!-- /.col-sm-9 -->
                                                <!-- /.col-sm-9 -->
                                            </div>
                                            <!-- /. form-group -->
                                        </div>
                                        <!-- /.Associate -->

                                        <!-- Fast Track -->
                                        <div class="col-md-6">
                                            <div class="form-group" id="set">
                                                <label for="rdo_gift" class="col-sm-3 control-label">Is Phase
                                                    II?</label>

                                                <div class="col-sm-9">
                                                    <label style="margin-top:2%;margin-left:2%">
                                                        <input type="radio" name="is_fast_track" value="Yes"
                                                               class="minimal">
                                                        Yes
                                                    </label>
                                                    <label style="margin-top:2%;margin-left:2%">
                                                        <input type="radio" name="is_fast_track" value="No"
                                                               class="minimal" checked>
                                                        No
                                                    </label>

                                                </div>
                                                <!-- /.col-sm-9 -->
                                            </div>
                                            <!-- /. form-group -->
                                        </div>
                                        <!-- // Fast Track -->

                                        <!-- Reset Button -->
                                        <div class="col-md-6">
                                            <button type="button" class="btn btn-warning pull-right"
                                                    id="btn_reset_agents">Reset Representatives
                                            </button>
                                        </div>
                                        <!-- // Reset Button -->


                                    </div>
                                    <!-- ./col-md-12 -->
                                </div>
                                <!-- /.row-->
                            </div>
                            <!-- /.box-body -->
                        </form>
                        <!-- /form-end -->

                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box-primary -->
            </div>
        </div>

        <!-- Remarks -->
        <div class="row">
            <div class=" col-md-12">
                <!-- Representative -->
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title"><i class="fa  fa-pencil-square-o"></i> Details</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">

                        <!-- form start -->
                        <form class="form-horizontal">
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <!-- Remarks -->
                                        <div class="col-md-6">
                                            <div class="form-group" id="set">
                                                <label for="txtarea_remarks"
                                                       class="col-sm-3 control-label">Remarks</label>

                                                <div class="col-sm-9">
                                                    <div class="form-group">
                                                        <textarea id="txtarea_remarks" class="form-control" rows="3"
                                                                  placeholder="Enter ..."></textarea>
                                                    </div>
                                                </div>
                                                <!-- /.col-sm-9 -->
                                            </div>
                                            <!-- /. form-group -->
                                        </div>
                                        <!-- /.Remarks -->

                                        <!-- Transaction Status -->
                                        <div class="col-md-6">
                                            <div class="form-group" id="set">
                                                <label for="slct_trans_status" class="col-sm-3 control-label">Purchase
                                                    Status</label>

                                                <div class="col-sm-9">
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-heartbeat"></i>
                                                        </div>
                                                        <select id="slct_trans_status" class="form-control select2"
                                                                style="width: 100%;height:100%">
                                                            <option>Complete</option>
                                                            <option>With Payment Problem</option>
                                                            <option>Cancelled</option>
                                                        </select>
                                                    </div>
                                                    <!-- /.input group -->

                                                </div>
                                                <!-- /.col-sm-9 -->
                                            </div>
                                            <!-- /. form-group -->
                                        </div>
                                        <!-- /.Transaction Status -->

                                        <div class="row">
                                            <div class="col-md-12">
                                                <button type="button" id="btn_save_and_print"
                                                        class="btn btn-success pull-right"
                                                        style="margin-left:1%"> Save Purchase Order
                                                </button>
                                                <button type="button" id="btn_cancel" class="btn bg-orange pull-right">
                                                    Cancel
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- ./col-md-12 -->

                                    <!-- /. Remove From List -->
                                </div>
                                <!-- /.row-->
                            </div>
                            <!-- /.box-body -->
                        </form>
                        <!-- /form-end -->
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box-primary -->
            </div>
        </div>
        <!-- /.Remarks -->

        <input type="hidden" id="is_updated">

        {{--MODAL--}}
        <div id="save-purchase-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-blue-gradient">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><i class="ion ion-check"></i> Save Purchase Order</h4>
                    </div>
                    <div class="modal-body">
                        Record has been saved.
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" id="btn_close_modal" data-dismiss="modal">Close
                        </button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        {{--MOdal End--}}

    </section>
@endsection

@section('additional_footer')

    <script src='{{ asset("/bower_components/AdminLTE/plugins/jQueryUI/jquery-ui.min.js")}}'></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.full.min.js")}}'></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker.js")}}'></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/iCheck/icheck.min.js")}}'></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/jquery.dataTables.min.js")}}'></script>

    <script>
        $(document).ready(function () {
            var product_obj;
            var date_from = '';
            var date_to = '';
            $('#txt_purchase_date').daterangepicker({

                        showDropdowns: true,
                        autoApply: true,
                        singleDatePicker: true,

                        locale: {
                            format: 'YYYY/mm/dd'
                        }
                    },
                    function (start, end, label) {
                        date_from = start.format('YYYY-MM-DD');
                        date_to = end.format('YYYY-MM-DD');
                        console.log(date_from);
                    });
            $('#txt_delivery_date').daterangepicker({

                        showDropdowns: true,
                        autoApply: true,
                        singleDatePicker: true,

                        locale: {
                            format: 'YYYY/mm/dd'
                        }
                    },
                    function (start, end, label) {
                        date_to = end.format('YYYY-MM-DD');
                    });

            $('#txt_purchase_date').on('apply.daterangepicker', function (ev, picker) {
                date_from = picker.startDate.format('YYYY-MM-DD');
                date_to = picker.startDate.format('YYYY-MM-DD');

                $('#txt_delivery_date').daterangepicker({
                    singleDatePicker: true,
                    showDropdowns: true,
                    locale: {
                        format: 'YYYY/mm/dd'
                    },
                    minDate: $('#txt_purchase_date').val()
                });
                $('#txt_delivery_date').on('apply.daterangepicker', function (ev, picker) {
                    date_to = picker.startDate.format('YYYY-MM-DD');

                    $('#txt_purchase_date').daterangepicker({
                        singleDatePicker: true,
                        showDropdowns: true,
                        locale: {
                            format: 'YYYY/mm/dd'
                        },
                        minDate: $('#txt_delivery_date').val()
                    });

                });

                $('#txt_delivery_date').val($('#txt_purchase_date').val());
            });
            $('#txt_delivery_date').on('apply.daterangepicker', function (ev, picker) {
                date_to = picker.startDate.format('YYYY-MM-DD');

                $('#txt_purchase_date').daterangepicker({
                    singleDatePicker: true,
                    showDropdowns: true,
                    locale: {
                        format: 'YYYY/mm/dd'
                    },
                    minDate: $('#txt_delivery_date').val()
                });
                $('#txt_purchase_date').on('apply.daterangepicker', function (ev, picker) {
                    date_from = picker.startDate.format('YYYY-MM-DD');

                    $('#txt_delivery_date').daterangepicker({
                        singleDatePicker: true,
                        showDropdowns: true,
                        locale: {
                            format: 'YYYY/mm/dd'
                        },
                        minDate: $('#txt_purchase_date').val()
                    });
                    $('#txt_delivery_date').val($('#txt_purchase_date').val());
                });

            });

            $('#txt_purchase_date').on('cancel.daterangepicker', function (ev, picker) {
                $('#txt_delivery_date').val('');
            });

            $('#txt_purchase_date').on('cancel.daterangepicker', function (ev, picker) {
                $('#txt_delivery_date').val('');
            });

            $("#txt_qty").spinner({
                min: 1,
                change: function (event, ui) {
                    // alert();
                    updatePrice();
                }
            });


            //iCheck for checkbox and radio inputs
            $('input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass: 'iradio_minimal-blue'
            });

            var table_data = $('#example1').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "info": false,
                "autoWidth": true
            });

            /*  $("#slct_purchase_type").change(function () {
             var product_type = $("#slct_purchase_type").val();
             if (product_type == "") {
             $("#slct_purchase_item").empty();
             return false;
             }

             $.post("/products/getProductsByType", {
             product_type: product_type
             }).done(function (data) {
             product_obj = data;
             $('#slct_purchase_item').empty();
             $('#slct_purchase_item').append($('<option>', {
             value: '',
             text: 'Select One'
             }));
             $.each(data, function (i, item) {
             console.log(item);
             $('#slct_purchase_item').append($('<option>', {
             value: item.id,
             text: item.item_code
             }));

             });
             });

             });*/

            $("#slct_purchase_item").change(function () {
                updatePrice();

            });

            $("#btn_add_purchase_item").click(function () {

                if ($("#slct_clients").select2("val") == null) {
                    alert('Client is required');
                    $("#slct_clients").focus();
                    $("#slct_clients").select2("open");
                    return false;
                }


                $.get("/purchases/createPOItem", {
                    hdn_po_no: $("#hdn_po_no").val(),
                    slct_client: $("#slct_clients").select2("val"),
                    txt_po_no: $("#txt_po_no").val(),
                    slct_terms: $("#slct_terms").val(),
                    txt_delivery_date: date_to,
                    txt_purchase_date: date_from,
                    slct_purchase_item: $("#slct_purchase_item").val(),
                    txt_qty: $("#txt_qty").val(),
                    is_gift: $("input[name=is_gift]:checked").val(),
                    slct_consultant: $("#txt_consultant").val(),
                    slct_sponsor: $("#txt_sponsor").val(),
                    slct_associate: $("#txt_associate").val(),
                    is_fast_track: $("input[name=is_fast_track]:checked").val(),
                    txtarea_remarks: $("#txtarea_remarks").val(),
                    slct_trans_status: $("#slct_trans_status").val(),
                    total_item_price: $("#unit_cost").val(),
                    slct_purchase_type: $("#slct_purchase_type").val()
                }).done(function (data) {

                    $("#is_updated").val(1);
                    var total_price = 0;
                    var is_update = data[1]
                    var po_item = data[0]
                    $("#hdn_po_no").val(data[2].id);
                    $("#txt_po_no").val(data[2].id);

                    if (is_update) {
                        $("#" + data[0].id + "_qty").val(data[0].qty);
                        $("#" + data[0].id + "_amt").val(data[0].item_price);
                        $("#" + data[0].id + "_qty").siblings("span").html(data[0].qty);
                        $("#" + data[0].id + "_amt").siblings("span").html('<i class="fa fa-rub"></i> ' + parseFloat(data[0].item_price * data[0].qty).toFixed(2));
                        total_price = parseFloat(data[0].item_price) +
                                parseFloat($("#total_purchase_price").val());
                    } else {
                        table_data.row.add(
                                [
                                    "<input type=checkbox name='po_items' value='" + po_item.id + "'>",
                                    $("#slct_purchase_item :selected").text(),
                                    "<input type='hidden' id='" + po_item.id + "_qty'>" + "<span>" + $("#txt_qty").val() + "</span>",
                                    $("#unit_cost").val(),
                                    $("input[name=is_gift]:checked").val(),
                                    "<input type='hidden' id='" + po_item.id + "_amt' value=" + $("#total_item_price").val() + ">" + "<span>" + '<i class="fa fa-rub"></i> ' + parseFloat($("#total_item_price").val()).toFixed(2) + "</span>"
                                ]
                        ).draw();
                        total_price = parseFloat($("#total_item_price").val()) +
                                parseFloat($("#total_purchase_price").val());
                    }

                    $("#total_purchase_price").val(total_price);
                    $("#lbl_total_purchase_price").html('<i class="fa fa-rub"></i> ' + parseFloat(total_price).toFixed(2));


                    $('#slct_purchase_item').append($('<tr>'));
                    $('#slct_purchase_item').children('tr').last().append($('<td>', {
                        text: $("#slct_purchase_type").val()
                    }));
                });
            });

            $("input[type=radio][name=is_gift]").on('ifChanged', function () {
                updatePrice();
            });

            function updatePrice() {


                var qty = $("#txt_qty").val();
                if ($("#slct_purchase_item").children("option").length == 0) {
                    return;
                }
                ;
                console.log($("#slct_purchase_item").select2('val'));
                $.post("/purchases/getProductDetail", {
                    id: $("#slct_purchase_item").select2('val')
                }).done(function (data) {
                    product_obj = data;
                    console.log(product_obj);

                    var result = $.grep(product_obj, function (e) {
                        return e.id == $("#slct_purchase_item").val();
                    });


                    if (result.length == 0) {
                        // not found
                    } else if (result.length == 1) {
                        // access the foo property using result[0].foo
                        $("#lbl_item_set_price").html('<i class="fa fa-rub"></i> ' + parseFloat(result[0].retail_price * qty).toFixed(2));
                        $("#total_item_price").val(parseFloat(result[0].retail_price * qty));
                        $("#unit_cost").val(parseFloat(result[0].retail_price));
                    } else {
                        // multiple items found
                    }
                    if ($("input[name=is_gift]:checked").val() == 'Yes') {
                        $("#lbl_item_set_price").html('<i class="fa fa-rub"></i> 0.00');
                        $("#total_item_price").val(0);
                        $("#unit_cost").val(result[0].retail_price);
                    }

                });
            }

            $("#btn_save_and_print").click(function () {

                if ($("#slct_clients").select2("val") == null) {
                    alert('Client is required');
                    $("#slct_clients").focus();
                    $("#slct_clients").select2("open");
                    return false;
                }

                if ($("#txt_sponsor").select2("val") == null) {
                    alert('Sponsor is required');
                    $("#txt_sponsor").focus();
                    $("#txt_sponsor").select2("open");
                    return false;
                }

                $.get("/purchases/savePO", {
                    purchase_order_no: $("#txt_purchase_order_no").val(),
                    hdn_po_no: $("#hdn_po_no").val(),
                    slct_client: $("#slct_clients").select2('val'),
                    txt_po_no: $("#txt_po_no").val(),
                    slct_terms: $("#slct_terms").val(),
                    txt_delivery_date: date_to,
                    txt_purchase_date: date_from,
                    slct_purchase_item: $("#slct_purchase_item").val(),
                    txt_qty: $("#txt_qty").val(),
                    is_gift: $("input[name=is_gift]:checked").val(),
                    slct_consultant: $("#txt_consultant").val(),
                    slct_sponsor: $("#txt_sponsor").val(),
                    slct_associate: $("#txt_associate").val(),
                    is_fast_track: $("input[name=is_fast_track]:checked").val(),
                    txtarea_remarks: $("#txtarea_remarks").val(),
                    slct_trans_status: $("#slct_trans_status").val(),
                    total_item_price: $("#total_item_price").val(),
                    slct_purchase_type: $("#slct_purchase_type").val()
                }).done(function (data) {
                    $("#save-purchase-modal").modal('show');
                });
            });

            $("#btn_close_modal").click(function () {
                window.location = '/purchases?approved=true';
            });

            $("#btn_cancel").click(function () {

            });

            $('#txt_sponsor').select2({
                ajax: {
                    url: "/bookings/searchConsultantSponsor",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term, // search term,
                            not: $('#txt_consultant').val()
                        };
                    },
                    processResults: function (data) {
                        // parse the results into the format expected by Select2.
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data
                        return {
                            results: data
                        };
                    },
                    cache: true
                },
                minimumInputLength: 2
            });

            $('#txt_consultant').select2({
                ajax: {
                    url: "/bookings/searchConsultantSponsor",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term, // search term,
                            not: $('#txt_sponsor').val()
                        };
                    },
                    processResults: function (data) {
                        // parse the results into the format expected by Select2.
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data
                        return {
                            results: data
                        };
                    },
                    cache: true
                },
                minimumInputLength: 2
            });

            $('#txt_associate').select2({
                ajax: {
                    url: "/bookings/searchAssociate",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term // search term
                        };
                    },
                    processResults: function (data) {
                        // parse the results into the format expected by Select2.
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data
                        return {
                            results: data
                        };
                    },
                    cache: true
                },
                minimumInputLength: 2
            });


            $('#slct_clients').select2({
                ajax: {
                    url: "/purchases/searchClient",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term // search term
                        };
                    },
                    processResults: function (data) {
                        // parse the results into the format expected by Select2.
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data
                        return {
                            results: data
                        };
                    },
                    cache: true
                },
                minimumInputLength: 2
            });


            $('#slct_purchase_item').select2({
                ajax: {
                    url: "/products/getProductsByType",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term // search term
                        };
                    },
                    processResults: function (data) {
                        // parse the results into the format expected by Select2.
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data
                        return {
                            results: data
                        };
                    },
                    cache: true
                },
                minimumInputLength: 2
            });


            $("#btn_remove_from_list").click(function () {

                var items = $("input[type=checkbox][name=po_items]:checked");
                var for_removal = [];

                $("input[type=checkbox][name=po_items]:checked").each(function () {
                    for_removal.push(this.value);
                });

                $.get("/purchases/removeItems/", {
                    remove_items: for_removal
                }).done(function (data) {
                    var total_less = 0;
                    var total_purchase_price = parseFloat($("#total_purchase_price").val());
                    $("input[type=checkbox][name=po_items]:checked").each(function () {
                        console.log($(this));
                        total_less += parseFloat($("#" + $(this)[0].value + "_amt").val());

                        table_data.destroy();
                        $(this).parent().parent().detach();

                        table_data = $('#example1').DataTable({
                            "paging": true,
                            "lengthChange": true,
                            "searching": true,
                            "info": false,
                            "autoWidth": true
                        });
                    });
                    var remaining_price = total_purchase_price - total_less;
                    $("#total_purchase_price").val(remaining_price.toFixed(2));
                    $("#lbl_total_purchase_price").html('<i class="fa fa-rub"></i> ' + parseFloat(remaining_price).toFixed(2));


                });
            });

            $("#btn_reset_agents").click(function () {
                $("#txt_associate").select2('val', '');
                $("#txt_consultant").select2('val', '');
                $("#txt_sponsor").select2('val', '');
            });


        });
    </script>
@endsection