@extends('admin_template')

@section('additional_header')

<!-- DataTables -->
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.min.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker-bs3.css")}}'>

@endsection

@section('content')
    <!-- Main content -->
    <section class="content">
      <div class="row"> 
        <div class="col-md-12">
           <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title"><i class="fa fa-user"></i> Role Information</h3>
                </div>
                <!-- /.box-header -->
                 <div class="box-body">
                    <div class="row">
                      
                      <div class="col-md-12">
                        <form class="form-horizontal">
                          <!-- Account Balance -->
                          <div class="col-md-12">
                            <div class="form-group">
                              <label for="txt_account_balance" class="col-sm-2 control-label"> Role Name</label>
                              <div class="col-sm-4">
                                <input type="text" class="form-control pull-right" value="McIntire" id="txt_account_balance">
                              </div>
                              <label for="txt_account_balance" class="col-sm-2 control-label"> Role Description</label>
                              <div class="col-sm-4">
                                <input type="text" class="form-control pull-right" value="Nina" id="txt_account_balance">
                              </div>
                            
                            </div>
                          </div>

                          <!-- buttons  -->
                          <div class="col-md-2 col-md-offset-10 ">
                            <div class="row">
                              <div class="col-md-12">
                                <button class="btn btn-info pull-right"> Save</button>
                                <button class="btn btn-danger"> Cancel</button>
                              </div>
                            </div>
                            <!-- /btn-group -->
                          </div>
                          <!-- /.buttons  -->

                        </form>


                     
                      </div><!-- col-md-12 -->
                    </div><!-- /row -->
                  </div>
                <!-- /.box-body -->
              </div>  
        </div>
        <!-- /.col -->
      </div>
    </section>

@endsection

@section('additional_footer')

<script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/jquery.dataTables.min.js")}}'></script>
<script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js")}}'></script>
<script src='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.full.min.js")}}'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker.js")}}'></script>



@endsection