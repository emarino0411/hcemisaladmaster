@extends('admin_template')

@section('additional_header')

        <!-- DataTables -->
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.min.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker-bs3.css")}}'>

@endsection

@section('content')
        <!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <!-- general form elements -->
            <div class="com-md-12 box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Display User Role Configuration</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <div class="box-body">
                    <div class="row">
                        <form role="form">
                            <div class="col-md-3">
                                <div class="input-group">
                                    <select id="slct_user_role" class="form-control">
                                        @if(count($roles)==0)
                                            <option value="0">--Add User Role--</option>
                                        @else
                                            <option value="0">--Display User Role--</option>
                                            @foreach($roles as $role)
                                                <option value="{{$role['name']}}">{{ucwords($role['name'])}}</option>
                                            @endforeach
                                        @endif
                                    </select>

                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-info">Go</button>
                                    </div>
                                    <!-- /btn-group -->
                                </div>
                            </div>
                            <div class="col-md-2 col-md-offset-7">
                                @if(isAccessModuleAllowed('roles_add'))
                                    <button id="btn_search" type="button" class="btn btn-primary" data-toggle="modal"
                                            data-target="#add-role"><i class="fa fa-key"></i> New User Role
                                    </button>
                                @endif
                            </div>


                        </form>
                    </div>
                    <!-- /row -->

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
            <!--/.col (left) -->
        </div>
        <!-- search form -->
    </div>
    <!-- /.row (main row) -->

    <div class="row hidden">
        <div class="col-md-12 col-xs-12">
            <!-- general form elements -->
            <div class="com-md-12 box">

                <!-- /.box-header -->
                <!-- form start -->
                <div class="box-body">
                    <div class="row">
                        <form role="form" class="form-horizontal">
                            <div class="col-md-3">
                                <label class="control-label pull-left"> No. Of Users: 0</label>
                            </div>
                            <div class="col-md-2 col-md-offset-7">
                                <a href="#" class="btn btn-success">View Users</a>
                            </div>


                        </form>
                    </div>
                    <!-- /row -->

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
            <!--/.col (left) -->
        </div>
        <!-- search form -->
    </div>

    <div class="row">
        <div class=" col-md-12 col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-shopping-cart"></i> Access Rights</h3>
                </div>
                <!-- /.box-header -->
                <style>
                    td, th {
                        text-align: center
                    }
                </style>
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped" style="text-align:center">
                        <thead>
                        <tr>
                            <th>Module</th>
                            <th>View</th>
                            <th>Add</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Purchase</td>
                            <td><input id="purchase_view" type="checkbox"></td>
                            <td><input id="purchase_add" type="checkbox"></td>
                            <td><input id="purchase_edit" type="checkbox"></td>
                            <td><input id="purchase_delete" type="checkbox"><br>(Cancel PO)</td>
                        </tr>
                        <tr>
                            <td>Payments</td>
                            <td><input id="payments_view" type="checkbox"></td>
                            <td><input id="payments_add" type="checkbox"></td>
                            <td><input id="payments_edit" type="checkbox"></td>
                            <td><input id="payments_delete" type="checkbox"></td>
                        </tr>
                        <tr>
                            <td>Bookings</td>
                            <td><input id="bookings_view" type="checkbox"></td>
                            <td><input id="bookings_add" type="checkbox"></td>
                            <td><input id="bookings_edit" type="checkbox"></td>
                            <td><input id="bookings_delete" type="checkbox"></td>
                        </tr>
                        <tr>
                            <td>Agents</td>
                            <td><input id="agents_view" type="checkbox"></td>
                            <td><input id="agents_add" type="checkbox"></td>
                            <td><input id="agents_edit" type="checkbox"></td>
                            <td><input id="agents_delete" type="checkbox"></td>
                        </tr>
                        <tr>
                            <td>Clients</td>
                            <td><input id="clients_view" type="checkbox"></td>
                            <td><input id="clients_add" type="checkbox"></td>
                            <td><input id="clients_edit" type="checkbox"></td>
                            <td><input id="clients_delete" type="checkbox"></td>
                        </tr>
                        <tr>
                            <td>Users</td>
                            <td><input id="users_view" type="checkbox"></td>
                            <td><input id="users_add" type="checkbox"></td>
                            <td><input id="users_edit" type="checkbox"></td>
                            <td><input id="users_delete" type="checkbox"></td>
                        </tr>
                        <tr>
                            <td>User Roles</td>
                            <td><input id="roles_view" type="checkbox"></td>
                            <td><input id="roles_add" type="checkbox"></td>
                            <td><input id="roles_edit" type="checkbox"></td>
                            <td><input id="roles_delete" type="checkbox"></td>
                        </tr>
                        <tr>
                            <td>Promos</td>
                            <td><input id="promos_view" type="checkbox"></td>
                            <td><input id="promos_add" type="checkbox"></td>
                            <td><input id="promos_edit" type="checkbox"></td>
                            <td><input id="promos_delete" type="checkbox"></td>
                        </tr>
                        <tr>
                            <td>Reports</td>
                            <td><input id="reports_view" type="checkbox"></td>
                            <td><input id="reports_add" type="checkbox"></td>
                            <td><input id="reports_edit" type="checkbox"></td>
                            <td><input id="reports_delete" type="checkbox"></td>
                        </tr>
                        <tr>
                            <td>Products</td>
                            <td><input id="products_view" type="checkbox"></td>
                            <td><input id="products_add" type="checkbox"></td>
                            <td><input id="products_edit" type="checkbox"></td>
                            <td><input id="products_delete" type="checkbox"></td>
                        </tr>
                        <tr>
                            <td>Multilevel and Promotions</td>
                            <td><input id="multilevel_view" type="checkbox"></td>
                            <td>&nbsp;</td>
                            <td><input id="multilevel_update" type="checkbox"></td>
                            <td>&nbsp;</td>
                        </tr>
                         <tr>
                            <td>Collections</td>
                            <td><input id="collections_view" type="checkbox"></td>
                            <td><input id="collections_add" type="checkbox"></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>


                        </tbody>

                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>

    <div class="row">
        <div class=" col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-rub"></i> Commissions</h3>
                </div>
                <!-- /.box-header -->

                <div class="box-body">
                    <div class="col-md-6 col-md-offset-3">

                        <!-- Commissions List-->
                        <div class="col-md-12">
                            <div class="form-group" id="set">
                                <label for="rdo_gift" class="col-sm-6 control-label">View Commissions</label>

                                <div class="col-sm-6">
                                    <div class="col-sm-6">
                                        <label style="margin-top:2%;margin-left:2%">
                                            <input type="radio" name="view_commission" value="Yes"
                                                   class="minimal">
                                            Yes
                                        </label>
                                    </div>
                                    <div class="col-sm-6">
                                        <label style="margin-top:2%;margin-left:2%">
                                            <input type="radio" name="view_commission" value="No" class="minimal"
                                                   checked>
                                            No
                                        </label>
                                    </div>
                                </div>
                                <!-- /.col-sm-9 -->
                            </div>
                            <!-- /. form-group -->
                        </div>
                        <!-- //Commissions List-->

                        <!-- Commissions -->
                        <div class="col-md-12">
                            <div class="form-group" id="set">
                                <label for="rdo_gift" class="col-sm-6 control-label">Distribute Commissions</label>

                                <div class="col-sm-6">
                                    <div class="col-sm-6">
                                        <label style="margin-top:2%;margin-left:2%">
                                            <input type="radio" name="distribute_commission" value="Yes"
                                                   class="minimal">
                                            Yes
                                        </label>
                                    </div>
                                    <div class="col-sm-6">
                                        <label style="margin-top:2%;margin-left:2%">
                                            <input type="radio" name="distribute_commission" value="No" class="minimal"
                                                   checked>
                                            No
                                        </label>
                                    </div>
                                </div>
                                <!-- /.col-sm-9 -->
                            </div>
                            <!-- /. form-group -->
                        </div>
                        <!-- //Commissions -->

                        <!-- Print Voucher -->
                        <div class="col-md-12">
                            <div class="form-group" id="set">
                                <label for="rdo_gift" class="col-sm-6 control-label">Print Voucher</label>

                                <div class="col-sm-6">
                                    <div class="col-sm-6">
                                        <label style="margin-top:2%;margin-left:2%">
                                            <input type="radio" name="print_voucher" value="Yes" class="minimal">
                                            Yes
                                        </label>
                                    </div>
                                    <div class="col-sm-6">
                                        <label style="margin-top:2%;margin-left:2%">
                                            <input type="radio" name="print_voucher" value="No" class="minimal" checked>
                                            No
                                        </label>
                                    </div>
                                </div>
                                <!-- /.col-sm-9 -->
                            </div>
                            <!-- /. form-group -->
                        </div>
                        <!-- //Print Voucher -->

                        <!-- Print Cheque -->
                        <div class="col-md-12">
                            <div class="form-group" id="set">
                                <label for="rdo_gift" class="col-sm-6 control-label">Print Cheque</label>

                                <div class="col-sm-6">
                                    <div class="col-sm-6">
                                        <label style="margin-top:2%;margin-left:2%">
                                            <input type="radio" name="print_check" value="Yes" class="minimal">
                                            Yes
                                        </label>
                                    </div>
                                    <div class="col-sm-6">
                                        <label style="margin-top:2%;margin-left:2%">
                                            <input type="radio" name="print_check" value="No" class="minimal" checked>
                                            No
                                        </label>
                                    </div>
                                </div>
                                <!-- /.col-sm-9 -->
                            </div>
                            <!-- /. form-group -->
                        </div>
                        <!-- //Print Cheque -->


                        <!-- Print Cheque -->
                        <div class="col-md-12">
                            <div class="form-group" id="set">
                                <label for="rdo_gift" class="col-sm-6 control-label">Commissions Update</label>

                                <div class="col-sm-6">
                                    <div class="col-sm-6">
                                        <label style="margin-top:2%;margin-left:2%">
                                            <input type="radio" name="update_commission" value="Yes" class="minimal">
                                            Yes
                                        </label>
                                    </div>
                                    <div class="col-sm-6">
                                        <label style="margin-top:2%;margin-left:2%">
                                            <input type="radio" name="update_commission" value="No" class="minimal" checked>
                                            No
                                        </label>
                                    </div>
                                </div>
                                <!-- /.col-sm-9 -->
                            </div>
                            <!-- /. form-group -->
                        </div>
                        <!-- //Print Cheque -->
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>

    <div class="row">
        <div class=" col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-dashboard"></i> Dashboard</h3>
                </div>
                <!-- /.box-header -->

                <div class="box-body">
                    <div class="col-md-6 col-md-offset-3">
                        <!-- new_po -->
                        <div class="col-md-12">
                            <div class="form-group" id="set">
                                <label for="rdo_gift" class="col-sm-6 control-label">New PO</label>

                                <div class="col-sm-6">
                                    <div class="col-sm-6">
                                        <label style="margin-top:2%;margin-left:2%">
                                            <input type="radio" name="new_po" value="Yes" class="minimal">
                                            Yes
                                        </label>
                                    </div>
                                    <div class="col-sm-6">
                                        <label style="margin-top:2%;margin-left:2%">
                                            <input type="radio" name="new_po" value="No" class="minimal" checked>
                                            No
                                        </label>
                                    </div>
                                </div>
                                <!-- /.col-sm-9 -->
                            </div>
                            <!-- /. form-group -->
                        </div>
                        <!-- //new_po -->

                        <!-- new_agent -->
                        <div class="col-md-12">
                            <div class="form-group" id="set">
                                <label for="rdo_gift" class="col-sm-6 control-label">New Agent</label>

                                <div class="col-sm-6">
                                    <div class="col-sm-6">
                                        <label style="margin-top:2%;margin-left:2%">
                                            <input type="radio" name="new_agent" value="Yes" class="minimal">
                                            Yes
                                        </label>
                                    </div>
                                    <div class="col-sm-6">
                                        <label style="margin-top:2%;margin-left:2%">
                                            <input type="radio" name="new_agent" value="No" class="minimal" checked>
                                            No
                                        </label>
                                    </div>
                                </div>
                                <!-- /.col-sm-9 -->
                            </div>
                            <!-- /. form-group -->
                        </div>
                        <!-- //new_agent -->

                        <!-- Bookings-->
                        <div class="col-md-12">
                            <div class="form-group" id="set">
                                <label for="rdo_gift" class="col-sm-6 control-label">Bookings</label>

                                <div class="col-sm-6">
                                    <div class="col-sm-6">
                                        <label style="margin-top:2%;margin-left:2%">
                                            <input type="radio" name="bookings" value="Yes" class="minimal">
                                            Yes
                                        </label>
                                    </div>
                                    <div class="col-sm-6">
                                        <label style="margin-top:2%;margin-left:2%">
                                            <input type="radio" name="bookings" value="No" class="minimal" checked>
                                            No
                                        </label>
                                    </div>
                                </div>
                                <!-- /.col-sm-9 -->
                            </div>
                            <!-- /. form-group -->
                        </div>
                        <!-- //Bookings -->


                        <!-- Bookings-->
                        <div class="col-md-12">
                            <div class="form-group" id="set">
                                <label for="rdo_gift" class="col-sm-6 control-label">Due Payment</label>

                                <div class="col-sm-6">
                                    <div class="col-sm-6">
                                        <label style="margin-top:2%;margin-left:2%">
                                            <input type="radio" name="payment" value="Yes" class="minimal">
                                            Yes
                                        </label>
                                    </div>
                                    <div class="col-sm-6">
                                        <label style="margin-top:2%;margin-left:2%">
                                            <input type="radio" name="payment" value="No" class="minimal" checked>
                                            No
                                        </label>
                                    </div>
                                </div>
                                <!-- /.col-sm-9 -->
                            </div>
                            <!-- /. form-group -->
                        </div>
                        <!-- //Bookings -->

                        <!-- approved_commissions-->
                        <div class="col-md-12">
                            <div class="form-group" id="set">
                                <label for="rdo_gift" class="col-sm-6 control-label">Approved Commissions</label>

                                <div class="col-sm-6">
                                    <div class="col-sm-6">
                                        <label style="margin-top:2%;margin-left:2%">
                                            <input type="radio" name="approved_commissions" value="Yes" class="minimal">
                                            Yes
                                        </label>
                                    </div>
                                    <div class="col-sm-6">
                                        <label style="margin-top:2%;margin-left:2%">
                                            <input type="radio" name="approved_commissions" value="No" class="minimal"
                                                   checked>
                                            No
                                        </label>
                                    </div>
                                </div>
                                <!-- /.col-sm-9 -->
                            </div>
                            <!-- /. form-group -->
                        </div>
                        <!-- //approved_commissions -->

                        <!-- promo_qualifiers-->
                        <div class="col-md-12">
                            <div class="form-group" id="set">
                                <label for="rdo_gift" class="col-sm-6 control-label">Promo Qualifiers</label>

                                <div class="col-sm-6">
                                    <div class="col-sm-6">
                                        <label style="margin-top:2%;margin-left:2%">
                                            <input type="radio" name="promo_qualifiers" value="Yes" class="minimal">
                                            Yes
                                        </label>
                                    </div>
                                    <div class="col-sm-6">
                                        <label style="margin-top:2%;margin-left:2%">
                                            <input type="radio" name="promo_qualifiers" value="No" class="minimal"
                                                   checked>
                                            No
                                        </label>
                                    </div>
                                </div>
                                <!-- /.col-sm-9 -->
                            </div>
                            <!-- /. form-group -->
                        </div>
                        <!-- //promo_qualifiers -->

                        <!-- pull_out-->
                        <div class="col-md-12">
                            <div class="form-group" id="set">
                                <label for="rdo_gift" class="col-sm-6 control-label">For Pull Out</label>

                                <div class="col-sm-6">
                                    <div class="col-sm-6">
                                        <label style="margin-top:2%;margin-left:2%">
                                            <input type="radio" name="pull_out" value="Yes" class="minimal">
                                            Yes
                                        </label>
                                    </div>
                                    <div class="col-sm-6">
                                        <label style="margin-top:2%;margin-left:2%">
                                            <input type="radio" name="pull_out" value="No" class="minimal" checked>
                                            No
                                        </label>
                                    </div>
                                </div>
                                <!-- /.col-sm-9 -->
                            </div>
                            <!-- /. form-group -->
                        </div>
                        <!-- //pull_out -->

                        <!-- Follow Up-->
                        <div class="col-md-12">
                            <div class="form-group" id="set">
                                <label for="rdo_gift" class="col-sm-6 control-label">Checks Follow Up</label>

                                <div class="col-sm-6">
                                    <div class="col-sm-6">
                                        <label style="margin-top:2%;margin-left:2%">
                                            <input type="radio" name="follow_up" value="Yes" class="minimal">
                                            Yes
                                        </label>
                                    </div>
                                    <div class="col-sm-6">
                                        <label style="margin-top:2%;margin-left:2%">
                                            <input type="radio" name="follow_up" value="No" class="minimal" checked>
                                            No
                                        </label>
                                    </div>
                                </div>
                                <!-- /.col-sm-9 -->
                            </div>
                            <!-- /. form-group -->
                        </div>
                        <!-- //Follow Up -->

                        <!-- calendar-->
                        <div class="col-md-12">
                            <div class="form-group" id="set">
                                <label for="rdo_gift" class="col-sm-6 control-label">Calendar</label>

                                <div class="col-sm-6">
                                    <div class="col-sm-6">
                                        <label style="margin-top:2%;margin-left:2%">
                                            <input type="radio" name="calendar" value="Yes" class="minimal">
                                            Yes
                                        </label>
                                    </div>
                                    <div class="col-sm-6">
                                        <label style="margin-top:2%;margin-left:2%">
                                            <input type="radio" name="calendar" value="No" class="minimal" checked>
                                            No
                                        </label>
                                    </div>
                                </div>
                                <!-- /.col-sm-9 -->
                            </div>
                            <!-- /. form-group -->
                        </div>
                        <!-- //calendar -->

                        <!-- Orders-->
                        <div class="col-md-12">
                            <div class="form-group" id="set">
                                <label for="rdo_gift" class="col-sm-6 control-label">Orders</label>

                                <div class="col-sm-6">
                                    <div class="col-sm-6">
                                        <label style="margin-top:2%;margin-left:2%">
                                            <input type="radio" name="orders" value="Yes" class="minimal">
                                            Yes
                                        </label>
                                    </div>
                                    <div class="col-sm-6">
                                        <label style="margin-top:2%;margin-left:2%">
                                            <input type="radio" name="orders" value="No" class="minimal" checked>
                                            No
                                        </label>
                                    </div>
                                </div>
                                <!-- /.col-sm-9 -->
                            </div>
                            <!-- /. form-group -->
                        </div>
                        <!-- //Orders -->


                        <!-- Top Agents-->
                        <div class="col-md-12">
                            <div class="form-group" id="set">
                                <label for="rdo_gift" class="col-sm-6 control-label">Top Agents</label>

                                <div class="col-sm-6">
                                    <div class="col-sm-6">
                                        <label style="margin-top:2%;margin-left:2%">
                                            <input type="radio" name="top_agents" value="Yes" class="minimal">
                                            Yes
                                        </label>
                                    </div>
                                    <div class="col-sm-6">
                                        <label style="margin-top:2%;margin-left:2%">
                                            <input type="radio" name="top_agents" value="No" class="minimal" checked>
                                            No
                                        </label>
                                    </div>
                                </div>
                                <!-- /.col-sm-9 -->
                            </div>
                            <!-- /. form-group -->
                        </div>
                        <!-- //Orders -->

                    </div>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>

    <div class="row">
        <div class=" col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-line-chart"></i> Reports</h3>
                </div>
                <!-- /.box-header -->

                <div class="box-body">
                    <div class="col-md-6 col-md-offset-3">
                        <!-- new_po -->
                        <div class="col-md-12">
                            <div class="form-group" id="set">
                                <label for="rdo_gift" class="col-sm-6 control-label">Checks for Deposit</label>

                                <div class="col-sm-6">
                                    <div class="col-sm-6">
                                        <label style="margin-top:2%;margin-left:2%">
                                            <input type="radio" name="checks_for_deposit" value="Yes" class="minimal">
                                            Yes
                                        </label>
                                    </div>
                                    <div class="col-sm-6">
                                        <label style="margin-top:2%;margin-left:2%">
                                            <input type="radio" name="checks_for_deposit" value="No" class="minimal" checked>
                                            No
                                        </label>
                                    </div>
                                </div>
                                <!-- /.col-sm-9 -->
                            </div>
                            <!-- /. form-group -->
                        </div>
                        <!-- //new_po -->

                        <!-- new_agent -->
                        <div class="col-md-12">
                            <div class="form-group" id="set">
                                <label for="rdo_gift" class="col-sm-6 control-label">Commissions for Release</label>

                                <div class="col-sm-6">
                                    <div class="col-sm-6">
                                        <label style="margin-top:2%;margin-left:2%">
                                            <input type="radio" name="commissions_for_release" value="Yes" class="minimal">
                                            Yes
                                        </label>
                                    </div>
                                    <div class="col-sm-6">
                                        <label style="margin-top:2%;margin-left:2%">
                                            <input type="radio" name="commissions_for_release" value="No" class="minimal" checked>
                                            No
                                        </label>
                                    </div>
                                </div>
                                <!-- /.col-sm-9 -->
                            </div>
                            <!-- /. form-group -->
                        </div>
                        <!-- //new_agent -->

                        <!-- Bookings-->
                        <div class="col-md-12">
                            <div class="form-group" id="set">
                                <label for="rdo_gift" class="col-sm-6 control-label">On Hold Commissions</label>

                                <div class="col-sm-6">
                                    <div class="col-sm-6">
                                        <label style="margin-top:2%;margin-left:2%">
                                            <input type="radio" name="on_hold_commissions" value="Yes" class="minimal">
                                            Yes
                                        </label>
                                    </div>
                                    <div class="col-sm-6">
                                        <label style="margin-top:2%;margin-left:2%">
                                            <input type="radio" name="on_hold_commissions" value="No" class="minimal" checked>
                                            No
                                        </label>
                                    </div>
                                </div>
                                <!-- /.col-sm-9 -->
                            </div>
                            <!-- /. form-group -->
                        </div>
                        <!-- //Bookings -->


                        <!-- Bookings-->
                        <div class="col-md-12">
                            <div class="form-group" id="set">
                                <label for="rdo_gift" class="col-sm-6 control-label">Sales By Agent</label>

                                <div class="col-sm-6">
                                    <div class="col-sm-6">
                                        <label style="margin-top:2%;margin-left:2%">
                                            <input type="radio" name="sales_by_agent" value="Yes" class="minimal">
                                            Yes
                                        </label>
                                    </div>
                                    <div class="col-sm-6">
                                        <label style="margin-top:2%;margin-left:2%">
                                            <input type="radio" name="sales_by_agent" value="No" class="minimal" checked>
                                            No
                                        </label>
                                    </div>
                                </div>
                                <!-- /.col-sm-9 -->
                            </div>
                            <!-- /. form-group -->
                        </div>
                        <!-- //Bookings -->

                        <!-- approved_commissions-->
                        <div class="col-md-12">
                            <div class="form-group" id="set">
                                <label for="rdo_gift" class="col-sm-6 control-label">Total Sales by Month</label>

                                <div class="col-sm-6">
                                    <div class="col-sm-6">
                                        <label style="margin-top:2%;margin-left:2%">
                                            <input type="radio" name="total_sales_by_month" value="Yes" class="minimal">
                                            Yes
                                        </label>
                                    </div>
                                    <div class="col-sm-6">
                                        <label style="margin-top:2%;margin-left:2%">
                                            <input type="radio" name="total_sales_by_month" value="No" class="minimal"
                                                   checked>
                                            No
                                        </label>
                                    </div>
                                </div>
                                <!-- /.col-sm-9 -->
                            </div>
                            <!-- /. form-group -->
                        </div>
                        <!-- //approved_commissions -->

                        <!-- promo_qualifiers-->
                        <div class="col-md-12">
                            <div class="form-group" id="set">
                                <label for="rdo_gift" class="col-sm-6 control-label">Contest Status</label>

                                <div class="col-sm-6">
                                    <div class="col-sm-6">
                                        <label style="margin-top:2%;margin-left:2%">
                                            <input type="radio" name="contest_status" value="Yes" class="minimal">
                                            Yes
                                        </label>
                                    </div>
                                    <div class="col-sm-6">
                                        <label style="margin-top:2%;margin-left:2%">
                                            <input type="radio" name="contest_status" value="No" class="minimal"
                                                   checked>
                                            No
                                        </label>
                                    </div>
                                </div>
                                <!-- /.col-sm-9 -->
                            </div>
                            <!-- /. form-group -->
                        </div>
                        <!-- //promo_qualifiers -->

                        <!-- pull_out-->
                        <div class="col-md-12">
                            <div class="form-group" id="set">
                                <label for="rdo_gift" class="col-sm-6 control-label">Total Sales Incomplete Payment</label>

                                <div class="col-sm-6">
                                    <div class="col-sm-6">
                                        <label style="margin-top:2%;margin-left:2%">
                                            <input type="radio" name="total_sales_incomplete_payment" value="Yes" class="minimal">
                                            Yes
                                        </label>
                                    </div>
                                    <div class="col-sm-6">
                                        <label style="margin-top:2%;margin-left:2%">
                                            <input type="radio" name="total_sales_incomplete_payment" value="No" class="minimal" checked>
                                            No
                                        </label>
                                    </div>
                                </div>
                                <!-- /.col-sm-9 -->
                            </div>
                            <!-- /. form-group -->
                        </div>
                        <!-- //pull_out -->

                        <!-- Follow Up-->
                        <div class="col-md-12">
                            <div class="form-group" id="set">
                                <label for="rdo_gift" class="col-sm-6 control-label">List of Checks By Status</label>

                                <div class="col-sm-6">
                                    <div class="col-sm-6">
                                        <label style="margin-top:2%;margin-left:2%">
                                            <input type="radio" name="list_of_checks_by_status" value="Yes" class="minimal">
                                            Yes
                                        </label>
                                    </div>
                                    <div class="col-sm-6">
                                        <label style="margin-top:2%;margin-left:2%">
                                            <input type="radio" name="list_of_checks_by_status" value="No" class="minimal" checked>
                                            No
                                        </label>
                                    </div>
                                </div>
                                <!-- /.col-sm-9 -->
                            </div>
                            <!-- /. form-group -->
                        </div>
                        <!-- //Follow Up -->

                        <!-- calendar-->
                        <div class="col-md-12">
                            <div class="form-group" id="set">
                                <label for="rdo_gift" class="col-sm-6 control-label">List of Clients for the Month</label>

                                <div class="col-sm-6">
                                    <div class="col-sm-6">
                                        <label style="margin-top:2%;margin-left:2%">
                                            <input type="radio" name="list_of_clients_for_month" value="Yes" class="minimal">
                                            Yes
                                        </label>
                                    </div>
                                    <div class="col-sm-6">
                                        <label style="margin-top:2%;margin-left:2%">
                                            <input type="radio" name="list_of_clients_for_month" value="No" class="minimal" checked>
                                            No
                                        </label>
                                    </div>
                                </div>
                                <!-- /.col-sm-9 -->
                            </div>
                            <!-- /. form-group -->
                        </div>
                        <!-- //calendar -->

                            <!-- calendar-->
                        <div class="col-md-12">
                            <div class="form-group" id="set">
                                <label for="rdo_gift" class="col-sm-6 control-label">Client Purchases</label>

                                <div class="col-sm-6">
                                    <div class="col-sm-6">
                                        <label style="margin-top:2%;margin-left:2%">
                                            <input type="radio" name="client_purchases" value="Yes" class="minimal">
                                            Yes
                                        </label>
                                    </div>
                                    <div class="col-sm-6">
                                        <label style="margin-top:2%;margin-left:2%">
                                            <input type="radio" name="client_purchases" value="No" class="minimal" checked>
                                            No
                                        </label>
                                    </div>
                                </div>
                                <!-- /.col-sm-9 -->
                            </div>
                            <!-- /. form-group -->
                        </div>
                        <!-- //calendar -->

                        <!-- Orders-->
                        <div class="col-md-12">
                            <div class="form-group" id="set">
                                <label for="rdo_gift" class="col-sm-6 control-label">Top Selling Products</label>

                                <div class="col-sm-6">
                                    <div class="col-sm-6">
                                        <label style="margin-top:2%;margin-left:2%">
                                            <input type="radio" name="top_selling_products" value="Yes" class="minimal">
                                            Yes
                                        </label>
                                    </div>
                                    <div class="col-sm-6">
                                        <label style="margin-top:2%;margin-left:2%">
                                            <input type="radio" name="top_selling_products" value="No" class="minimal" checked>
                                            No
                                        </label>
                                    </div>
                                </div>
                                <!-- /.col-sm-9 -->
                            </div>
                            <!-- /. form-group -->
                        </div>
                        <!-- //Orders -->


                        <!-- Top Agents-->
                        <div class="col-md-12">
                            <div class="form-group" id="set">
                                <label for="rdo_gift" class="col-sm-6 control-label">Top Seller / Top Sponsor</label>

                                <div class="col-sm-6">
                                    <div class="col-sm-6">
                                        <label style="margin-top:2%;margin-left:2%">
                                            <input type="radio" name="top_seller_sponsor" value="Yes" class="minimal">
                                            Yes
                                        </label>
                                    </div>
                                    <div class="col-sm-6">
                                        <label style="margin-top:2%;margin-left:2%">
                                            <input type="radio" name="top_seller_sponsor" value="No" class="minimal" checked>
                                            No
                                        </label>
                                    </div>
                                </div>
                                <!-- /.col-sm-9 -->
                            </div>
                            <!-- /. form-group -->
                        </div>
                        <!-- //Orders -->

                    </div>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 col-xs-12">
            <!-- general form elements -->
            <div class="com-md-12 box">
                <!-- form start -->
                <div class="box-body">
                    <div class="row">
                        <form role="form">
                            <div class="col-md-3 hidden">
                                <div class="input-group">
                                    <select class="form-control">
                                        <option>--Select Action--</option>
                                        <option>Activate</option>
                                        <option>Deactivate</option>
                                        <option>Delete</option>
                                    </select>

                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-info">Go</button>
                                    </div>
                                    <!-- /btn-group -->
                                </div>
                            </div>
                            @if(isAccessModuleAllowed('roles_edit'))
                                <div class="col-md-1 pull-right">

                                    <button type="button" class="btn btn-primary pull-right" id="btn_update_role"><i
                                                class="fa fa-save"></i> Update
                                    </button>
                                </div>
                                <div class="col-md-1 pull-right">

                                    <a href="#" class="btn btn-warning  pull-right">Cancel</a>
                                </div>
                            @endif


                        </form>
                    </div>
                    <!-- /row -->

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
            <!--/.col (left) -->
        </div>
        <!-- search form -->
    </div>
    {{--MODAL--}}
    <div id="save-role-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-blue-gradient">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><i class="ion ion-check"></i> Save Role OK</h4>
                </div>
                <div class="modal-body">
                    Record has been saved.
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" id="btn_close_modal" data-dismiss="modal">Close
                    </button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--MOdal End--}}
</section>

@endsection

@section('additional_footer')

    <script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/jquery.dataTables.min.js")}}'></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js")}}'></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.full.min.js")}}'></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker.js")}}'></script>


    <script>
        $(document).ready(function () {
            $("input[type=checkbox]").attr('disabled', 'true');
            $("input[type=radio]").attr('disabled', 'true');
            $("#btn_update_role").attr('disabled', 'true');

            $('#example1').DataTable({
                "paging": false,
                "lengthChange": true,
                "searching": false,
                "ordering": true,
                "info": false,
                "autoWidth": true
            });

            $("#btn_search").click(function () {
                $(".select2").select2({
                    theme: "classic"
                });
                $('#txt_daterange').daterangepicker();

            });

            $(".hide-filter").each(function (index) {
                $(this).parent().parent().hide();
                $(this).on("click", function () {
                    $(this).parent().parent().hide();
                    $("." + $(this).parent().parent().attr('id')).show();
                });
            });

            $(".dropdown-menu li").each(function (index) {
                $(this).on("click", function () {
                    $("#" + $(this).children("input").val()).show();
                    $(this).hide();
                });
            });

            $("#btn_save_role").click(function () {
                if ($("#txt_role_name").val().trim() == "") {
                    $("#lbl_error_role").removeClass('hidden');
                    $("#lbl_error_role").html('<small>Role Name is required.</small>');
                    $("#txt_role_name").parent().addClass('has-error');
                    $("#txt_role_name").focus();
                    return false;
                }

                var patt = /^[a-zA-Z\-]+$/;
                var result = patt.test($("#txt_role_name").val());
                console.log(result);
                if (!result) {
                    $("#lbl_error_role").removeClass('hidden');
                    $("#lbl_error_role").html('<small>No special characters allowed.</small>');
                    $("#txt_role_name").parent().addClass('has-error');
                    $("#txt_role_name").focus();
                    return false;
                }

                $.post("/roles/addNew", {name: $("#txt_role_name").val(), description: $("#txt_role_desc").val()})
                        .done(function (data) {
                            console.log(data + "<<<");
                            if (data.match('ROLE_EXISTING')) {
                                $("#txt_role_name").parent().addClass('has-error');
                                $("#txt_role_name").focus();
                                $("#lbl_error_role").removeClass('hidden');
                                $("#lbl_error_role").html('<small>Role Name already exists.</small>');
                                return;
                            } else {


                                $('#add-role').modal('hide');
                                $('#slct_user_role').append($('<option/>', {
                                    value: $("#txt_role_name").val(),
                                    text: $("#txt_role_name").val()
                                }));
                                $('input[type=checkbox]').removeAttr('disabled');
                                $('input[type=radio]').removeAttr('disabled');
                                $("#slct_user_role").val($("#txt_role_name").val());

                                $("#txt_role_name").parent().removeClass('has-error');
                                $("#txt_role_name").val('');
                                $("#lbl_error_role").addClass('hidden');

                                $("#btn_update_role").removeAttr('disabled');
                                return;
                            }
                        });
                $("#save-role-modal").modal('show');
            });
            $("#btn_update_role").click(function () {
                $.get("/roles/update", {
                            purchase_view: $("#purchase_view").is(':checked'),
                            purchase_add: $("#purchase_add").is(':checked'),
                            purchase_edit: $("#purchase_edit").is(':checked'),
                            purchase_delete: $("#purchase_delete").is(':checked'),
                            payments_view: $("#payments_view").is(':checked'),
                            payments_add: $("#payments_add").is(':checked'),
                            payments_edit: $("#payments_edit").is(':checked'),
                            payments_delete: $("#payments_delete").is(':checked'),
                            bookings_view: $("#bookings_view").is(':checked'),
                            bookings_add: $("#bookings_add").is(':checked'),
                            bookings_edit: $("#bookings_edit").is(':checked'),
                            bookings_delete: $("#bookings_delete").is(':checked'),
                            agents_view: $("#agents_view").is(':checked'),
                            agents_add: $("#agents_add").is(':checked'),
                            agents_edit: $("#agents_edit").is(':checked'),
                            agents_delete: $("#agents_delete").is(':checked'),
                            clients_view: $("#clients_view").is(':checked'),
                            clients_add: $("#clients_add").is(':checked'),
                            clients_edit: $("#clients_edit").is(':checked'),
                            clients_delete: $("#clients_delete").is(':checked'),
                            users_view: $("#users_view").is(':checked'),
                            users_add: $("#users_add").is(':checked'),
                            users_edit: $("#users_edit").is(':checked'),
                            users_delete: $("#users_delete").is(':checked'),
                            roles_view: $("#roles_view").is(':checked'),
                            roles_add: $("#roles_add").is(':checked'),
                            roles_edit: $("#roles_edit").is(':checked'),
                            roles_delete: $("#roles_delete").is(':checked'),
                            promos_view: $("#promos_view").is(':checked'),
                            promos_add: $("#promos_add").is(':checked'),
                            promos_edit: $("#promos_edit").is(':checked'),
                            promos_delete: $("#promos_delete").is(':checked'),
                            reports_view: $("#reports_view").is(':checked'),
                            reports_add: $("#reports_add").is(':checked'),
                            reports_edit: $("#reports_edit").is(':checked'),
                            reports_delete: $("#reports_delete").is(':checked'),
                            products_view: $("#products_view").is(':checked'),
                            products_add: $("#products_add").is(':checked'),
                            products_edit: $("#products_edit").is(':checked'),
                            products_delete: $("#products_delete").is(':checked'),
                            distribute_commission: $("input[name=distribute_commission]:checked").val(),
                            print_voucher: $("input[name=print_voucher]:checked").val(),
                            print_check: $("input[name=print_check]:checked").val(),

                            view_commission: $("input[name=view_commission]:checked").val(),
                            update_commission: $("input[name=update_commission]:checked").val(),


                            new_po: $("input[name=new_po]:checked").val(),
                            new_agent: $("input[name=new_agent]:checked").val(),
                            bookings: $("input[name=bookings]:checked").val(),
                            payment: $("input[name=payment]:checked").val(),
                            approved_commissions: $("input[name=approved_commissions]:checked").val(),
                            promo_qualifiers: $("input[name=promo_qualifiers]:checked").val(),
                            pull_out: $("input[name=pull_out]:checked").val(),
                            follow_up: $("input[name=follow_up]:checked").val(),
                            calendar: $("input[name=calendar]:checked").val(),
                            orders: $("input[name=orders]:checked").val(),
                            top_agents: $("input[name=top_agents]:checked").val(),
                            multilevel_view : $("#multilevel_view").is(':checked'), 
                            multilevel_update : $("#multilevel_update").is(':checked'), 
                            collections_add : $("#collections_add").is(':checked'), 
                            collections_view : $("#collections_view").is(':checked'), 
                            role_name: $("#slct_user_role").val(),

                            checks_for_deposit: $("input[name=checks_for_deposit]:checked").val(),
                            commissions_for_release: $("input[name=commissions_for_release]:checked").val(),
                            on_hold_commissions: $("input[name=on_hold_commissions]:checked").val(),
                            sales_by_agent: $("input[name=sales_by_agent]:checked").val(),
                            total_sales_by_month: $("input[name=total_sales_by_month]:checked").val(),
                            contest_status: $("input[name=contest_status]:checked").val(),
                            total_sales_incomplete_payment: $("input[name=total_sales_incomplete_payment]:checked").val(),
                            list_of_checks_by_status: $("input[name=list_of_checks_by_status]:checked").val(),
                            list_of_clients_for_month: $("input[name=list_of_clients_for_month]:checked").val(),
                            client_purchases: $("input[name=client_purchases]:checked").val(),
                            top_selling_products: $("input[name=top_selling_products]:checked").val(),
                            top_seller_sponsor: $("input[name=top_seller_sponsor]:checked").val()

                        }
                ).done(function (data) {
                    if (data == 'ROLE_SAVED') {
                        console.log('role updated');
                    }
                    console.log(data);

                    $("#save-role-modal").modal('show');
                });
            });

            $("#slct_user_role").change(function () {
                if ($("#slct_user_role").val() == 0) {
                    return;
                }

                $('input[type=checkbox]').removeAttr('disabled');
                $('input[type=radio]').removeAttr('disabled');
                $('#btn_update_role').removeAttr('disabled');

                $.post("/roles/getConfig", {role_name: $("#slct_user_role").val()})
                        .done(function (data) {
                            console.log(data);
                            $("#purchase_view").prop('checked', data['purchase_view'] != 1 ? false : true);
                            $("#purchase_add").prop('checked', data['purchase_add'] != 1 ? false : true);
                            $("#purchase_edit").prop('checked', data['purchase_edit'] != 1 ? false : true);
                            $("#purchase_delete").prop('checked', data['purchase_delete'] != 1 ? false : true);
                            $("#payments_view").prop('checked', data['payments_view'] != 1 ? false : true);
                            $("#payments_add").prop('checked', data['payments_add'] != 1 ? false : true);
                            $("#payments_edit").prop('checked', data['payments_edit'] != 1 ? false : true);
                            $("#payments_delete").prop('checked', data['payments_delete'] != 1 ? false : true);
                            $("#bookings_view").prop('checked', data['bookings_view'] != 1 ? false : true);
                            $("#bookings_add").prop('checked', data['bookings_add'] != 1 ? false : true);
                            $("#bookings_edit").prop('checked', data['bookings_edit'] != 1 ? false : true);
                            $("#bookings_delete").prop('checked', data['bookings_delete'] != 1 ? false : true);
                            $("#agents_view").prop('checked', data['agents_view'] != 1 ? false : true);
                            $("#agents_add").prop('checked', data['agents_add'] != 1 ? false : true);
                            $("#agents_edit").prop('checked', data['agents_edit'] != 1 ? false : true);
                            $("#agents_delete").prop('checked', data['agents_delete'] != 1 ? false : true);
                            $("#clients_view").prop('checked', data['clients_view'] != 1 ? false : true);
                            $("#clients_add").prop('checked', data['clients_add'] != 1 ? false : true);
                            $("#clients_edit").prop('checked', data['clients_edit'] != 1 ? false : true);
                            $("#clients_delete").prop('checked', data['clients_delete'] != 1 ? false : true);
                            $("#users_view").prop('checked', data['users_view'] != 1 ? false : true);
                            $("#users_add").prop('checked', data['users_add'] != 1 ? false : true);
                            $("#users_edit").prop('checked', data['users_edit'] != 1 ? false : true);
                            $("#users_delete").prop('checked', data['users_delete'] != 1 ? false : true);
                            $("#roles_view").prop('checked', data['roles_view'] != 1 ? false : true);
                            $("#roles_add").prop('checked', data['roles_add'] != 1 ? false : true);
                            $("#roles_edit").prop('checked', data['roles_edit'] != 1 ? false : true);
                            $("#roles_delete").prop('checked', data['roles_delete'] != 1 ? false : true);
                            $("#promos_view").prop('checked', data['promos_view'] != 1 ? false : true);
                            $("#promos_add").prop('checked', data['promos_add'] != 1 ? false : true);
                            $("#promos_edit").prop('checked', data['promos_edit'] != 1 ? false : true);
                            $("#promos_delete").prop('checked', data['promos_delete'] != 1 ? false : true);
                            $("#reports_view").prop('checked', data['reports_view'] != 1 ? false : true);
                            $("#reports_add").prop('checked', data['reports_add'] != 1 ? false : true);
                            $("#reports_edit").prop('checked', data['reports_edit'] != 1 ? false : true);
                            $("#reports_delete").prop('checked', data['reports_delete'] != 1 ? false : true);


                            $("#multilevel_view").prop('checked', data['multilevel_view'] != 1 ? false : true);
                            $("#multilevel_update").prop('checked', data['multilevel_update'] != 1 ? false : true);


                            $("#collections_view").prop('checked', data['collections_view'] != 1 ? false : true);
                            $("#collections_add").prop('checked', data['collections_add'] != 1 ? false : true);

                            $("#products_view").prop('checked', data['products_view'] != 1 ? false : true);
                            $("#products_add").prop('checked', data['products_add'] != 1 ? false : true);
                            $("#products_edit").prop('checked', data['products_edit'] != 1 ? false : true);
                            $("#products_delete").prop('checked', data['products_delete'] != 1 ? false : true);

                            data['distribute_commission'] != 1 ? $("input[name=distribute_commission][value=No]").prop('checked', true) :
                                    $("input[name=distribute_commission][value=Yes]").prop('checked', true);
                            data['print_voucher'] != 1 ? $("input[name=print_voucher][value=No]").prop('checked', true) :
                                    $("input[name=print_voucher][value=Yes]").prop('checked', true);
                            data['print_check'] != 1 ? $("input[name=print_check][value=No]").prop('checked', true) :
                                    $("input[name=print_check][value=Yes]").prop('checked', true);

                            data['view_commissions'] != 1 ? $("input[name=view_commission][value=No]").prop('checked', true) :
                                    $("input[name=view_commission][value=Yes]").prop('checked', true);

                            data['update_commissions'] != 1 ? $("input[name=update_commission][value=No]").prop('checked', true) :
                                    $("input[name=update_commission][value=Yes]").prop('checked', true);

                            data['dashboard_new_po'] != 1 ? $("input[name=new_po][value=No]").prop('checked', true) :
                                    $("input[name=new_po][value=Yes]").prop('checked', true);
                            data['dashboard_new_agent'] != 1 ? $("input[name=new_agent][value=No]").prop('checked', true) :
                                    $("input[name=new_agent][value=Yes]").prop('checked', true);
                            data['dashboard_bookings'] != 1 ? $("input[name=bookings][value=No]").prop('checked', true) :
                                    $("input[name=bookings][value=Yes]").prop('checked', true);
                            data['dashboard_due_payments'] != 1 ? $("input[name=payment][value=No]").prop('checked', true) :
                                    $("input[name=payment][value=Yes]").prop('checked', true);
                            data['dashboard_approved_commissions'] != 1 ? $("input[name=approved_commissions][value=No]").prop('checked', true) :
                                    $("input[name=approved_commissions][value=Yes]").prop('checked', true);
                            data['dashboard_promo_qualifiers'] != 1 ? $("input[name=promo_qualifiers][value=No]").prop('checked', true) :
                                    $("input[name=promo_qualifiers][value=Yes]").prop('checked', true);
                            data['dashboard_for_pull_out'] != 1 ? $("input[name=pull_out][value=No]").prop('checked', true) :
                                    $("input[name=pull_out][value=Yes]").prop('checked', true);
                            data['dashboard_checks_follow_up'] != 1 ? $("input[name=follow_up][value=No]").prop('checked', true) :
                                    $("input[name=follow_up][value=Yes]").prop('checked', true);
                            data['dashboard_calendar'] != 1 ? $("input[name=calendar][value=No]").prop('checked', true) :
                                    $("input[name=calendar][value=Yes]").prop('checked', true);
                            data['dashboard_orders'] != 1 ? $("input[name=orders][value=No]").prop('checked', true) :
                                    $("input[name=orders][value=Yes]").prop('checked', true);
                            data['dashboard_top_agents'] != 1 ? $("input[name=top_agents][value=No]").prop('checked', true) :
                                    $("input[name=top_agents][value=Yes]").prop('checked', true);

                            data['checks_for_deposit'] != 1 ? $("input[name=checks_for_deposit][value=No]").prop('checked', true) :
                                    $("input[name=checks_for_deposit][value=Yes]").prop('checked', true);
                            data['commissions_for_release'] != 1 ? $("input[name=commissions_for_release][value=No]").prop('checked', true) :
                                    $("input[name=commissions_for_release][value=Yes]").prop('checked', true);
                            data['on_hold_commissions'] != 1 ? $("input[name=on_hold_commissions][value=No]").prop('checked', true) :
                                    $("input[name=on_hold_commissions][value=Yes]").prop('checked', true);
                            data['sales_by_agent'] != 1 ? $("input[name=sales_by_agent][value=No]").prop('checked', true) :
                                    $("input[name=sales_by_agent][value=Yes]").prop('checked', true);
                            data['total_sales_by_month'] != 1 ? $("input[name=total_sales_by_month][value=No]").prop('checked', true) :
                                    $("input[name=total_sales_by_month][value=Yes]").prop('checked', true);
                            data['contest_status'] != 1 ? $("input[name=contest_status][value=No]").prop('checked', true) :
                                    $("input[name=contest_status][value=Yes]").prop('checked', true);
                            data['total_sales_incomplete_payment'] != 1 ? $("input[name=total_sales_incomplete_payment][value=No]").prop('checked', true) :
                                    $("input[name=total_sales_incomplete_payment][value=Yes]").prop('checked', true);
                            data['list_of_checks_by_status'] != 1 ? $("input[name=list_of_checks_by_status][value=No]").prop('checked', true) :
                                    $("input[name=list_of_checks_by_status][value=Yes]").prop('checked', true);
                            data['list_of_clients_for_month'] != 1 ? $("input[name=list_of_clients_for_month][value=No]").prop('checked', true) :
                                    $("input[name=list_of_clients_for_month][value=Yes]").prop('checked', true);
                                    
                            data['client_purchases'] != 1 ? $("input[name=client_purchases][value=No]").prop('checked', true) :
                                    $("input[name=client_purchases][value=Yes]").prop('checked', true);
                            
                            data['top_selling_products'] != 1 ? $("input[name=top_selling_products][value=No]").prop('checked', true) :
                                    $("input[name=top_selling_products][value=Yes]").prop('checked', true);
                            data['top_seller_sponsor'] != 1 ? $("input[name=top_seller_sponsor][value=No]").prop('checked', true) :
                                    $("input[name=top_seller_sponsor][value=Yes]").prop('checked', true);

                        });
            });


        });
    </script>
@endsection