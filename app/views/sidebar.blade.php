<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset( Session::get('img')) }}" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p>{{  Session::get('name') }}</p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form (Optional) -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
				<span class="input-group-btn">
				  <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                  </button>
				</span>
            </div>
        </form>
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>

            <li>
                <a href="/dashboard">
                    <i class="fa fa-dashboard"></i> <span>HCEMI Dashboard</span></i>
                </a>
            </li>
            @if(isAccessModuleAllowed('purchase_view'))
                <li>
                    <a href="/purchases">
                        <i class="fa fa-shopping-cart"></i> <span>Purchase Orders</span></i>
                    </a>
                </li>
            @endif
            @if(isAccessModuleAllowed('payments_view'))
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-money"></i> <span>Payments</span> <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        @if(isAccessModuleAllowed('payments_view'))
                            <li><a href="/payments"><i class="fa fa-list"></i> Payments List</a></li>
                        @endif
                        @if(isAccessModuleAllowed('payments_add'))
                            <li><a href="/payments/addNew"><i class="fa fa-plus"></i> Add Payments</a></li>
                        @endif
                        @if(isAccessModuleAllowed('payments_view'))
                            <li><a href="/payments/viewPayments"><i class="fa fa-user-plus"></i> View Payments</a></li>
                        @endif
                        @if(isAccessModuleAllowed('payments_edit'))
                            <li><a href="/payments/checksForDeposit"><i class="fa fa-user-plus"></i> Checks for Deposit</a></li>
                        @endif
                    </ul>
                </li>
            @endif
            @if(isAccessModuleAllowed('collections_view'))
                <li>
                    <a href="/collections">
                        <i class="fa fa-calculator"></i> <span>Collections</span></i>
                    </a>
                </li>
            @endif
            @if(isAccessModuleAllowed('distribute_commission') || isAccessModuleAllowed('print_voucher'))
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-rub"></i> <span>Commissions</span> <i class="fa fa-angle-left pull-right"></i>
                    </a>

                    <ul class="treeview-menu">
                        @if(isAccessModuleAllowed('view_commission'))
                            <li><a href="/commissions/list"><i class="fa fa-users"></i> List of Commissions</a></li>
                        @endif
                        @if(isAccessModuleAllowed('distribute_commission'))
                            <li><a href="/commissions/"><i class="fa fa-users"></i> Distribute Commissions</a></li>
                        @endif
                        @if(isAccessModuleAllowed('print_voucher'))
                            <li><a href="/commissions/voucher"><i class="fa fa-money"></i> Vouchers</a></li>
                        @endif
                        @if(isAccessModuleAllowed('update_commission'))
                            <li><a href="/commissions/commissionForRelease"><i class="fa fa-industry"></i> Commissions Update</a></li>
                        @endif
                    </ul>
                </li>
            @endif

            @if(isAccessModuleAllowed('bookings_view'))
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-book"></i> <span>Bookings</span> <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="/bookings"><i class="fa fa-calendar"></i> Booking Calendar</a></li>
                        <li><a href="/bookings/list"><i class="fa fa-list"></i> Booking List</a></li>
                    </ul>
                </li>
            @endif

            @if(isAccessModuleAllowed('clients_view'))
                <li>
                    <a href="/clients">
                        <i class="fa fa-briefcase"></i> <span>Clients</span></i>
                    </a>
                </li>
            @endif

            @if(isAccessModuleAllowed('agents_view'))
                <li>
                    <a href="/agents">
                        <i class="fa fa-users"></i> <span>Agents</span></i>
                    </a>
                </li>
            @endif
            @if(isAccessModuleAllowed('products_view'))
                <li>
                    <a href="/products">
                        <i class="fa fa-cutlery"></i> <span>Products and Sets</span></i>
                    </a>
                </li>
            @endif
            @if(isAccessModuleAllowed('promos_view'))
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-gift"></i> <span>Contests</span> <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        @if(isAccessModuleAllowed('promos_view'))
                            <li><a href="/promos"><i class="fa fa-plus"></i> New Contests</a></li>
                        @endif
                        @if(isAccessModuleAllowed('promos_view'))
                            <li><a href="/promos/update"><i class="fa fa-edit"></i> Contest Update</a></li>
                        @endif
                        @if(isAccessModuleAllowed('promos_edit'))
                            <li><a href="/promos/register_po"><i class="fa fa-user-plus"></i> Register PO by Agent</a></li>
                            <li><a href="/promos/register_po_by_po"><i class="fa fa-shopping-cart"></i> Register PO by Purchase</a></li>
                        @endif
                    </ul>
                </li>
            @endif
            @if(isAccessModuleAllowed('multilevel_view'))
                <li>
                    <a href="/multilevel">
                        <i class="fa fa-arrow-up"></i> <span>Multi-level and Promotions</span></i>
                  
                    </a>
                </li>
            @endif
            <li>
                <a href="/sales_override">
                    <i class="fa fa-heart"></i> <span> Sales Override</span></i>

                </a>
            </li>
            @if(
                isAccessModuleAllowed('checks_for_deposit') ||
                isAccessModuleAllowed('commissions_for_release') || 
                isAccessModuleAllowed('on_hold_commissions')|| 
                isAccessModuleAllowed('sales_by_agent')|| 
                isAccessModuleAllowed('total_sales_by_month') ||
                isAccessModuleAllowed('contest_status') || 
                isAccessModuleAllowed('total_sales_incomplete_payment') ||
                isAccessModuleAllowed('list_of_checks_by_status')|| 
                isAccessModuleAllowed('list_of_clients_for_month')|| 
                isAccessModuleAllowed('top_selling_products') || 
                isAccessModuleAllowed('top_seller_sponsor')
            )

                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-line-chart"></i> <span>Reports</span> <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        @if(isAccessModuleAllowed('checks_for_deposit'))
                        <li><a href="/reports/checkForDeposit"><i class="fa fa-bank"></i> Checks for Deposit</a></li>
                        @endif
                        @if(isAccessModuleAllowed('commissions_for_release'))
                        <li><a href="/reports/commissionForRelease"><i class="fa fa-rub"></i> Commissions for Release</a></li>
                        @endif
                        @if(isAccessModuleAllowed('on_hold_commissions'))
                        <li><a href="/reports/onHoldCommission"><i class="fa fa-rub"></i> On Hold Commissions</a></li>
                        @endif
                        @if(isAccessModuleAllowed('sales_by_agent'))
                        <li><a href="/reports/salesByAgent"><i class="fa fa-user"></i> Sales By Agent</a></li>
                        @endif
                        @if(isAccessModuleAllowed('total_sales_by_month'))
                        <li><a href="/reports/totalSalesByMonth"><i class="fa fa-dollar"></i> Total Sales by Month</a></li>
                        @endif
                        @if(isAccessModuleAllowed('contest_status'))
                        <li><a href="/reports/contestStatus"><i class="fa fa-gift"></i> Contest Status </a></li>
                        @endif
                        @if(isAccessModuleAllowed('total_sales_incomplete_payment'))
                        <li><a href="/reports/totalSalesIncompletePayment"><i class="fa fa-dollar"></i> List of Accounts for Follow Up </a></li>
                        @endif
                        @if(isAccessModuleAllowed('list_of_checks_by_status'))
                        <li><a href="/reports/listOfCheckByStatus"><i class="fa fa-bank"></i> List of Checks By Status </a></li>
                        @endif
                        @if(isAccessModuleAllowed('list_of_clients_for_month'))
                        <li><a href="/reports/listOfClient"><i class="fa fa-users"></i> List of Clients for the Month </a></li>
                        @endif

                        @if(isAccessModuleAllowed('client_purchases'))
                        <li><a href="/reports/clientPurchases"><i class="fa fa-shopping-cart"></i> Client Purchases </a></li>
                        @endif

                        @if(isAccessModuleAllowed('top_selling_products'))
                        <li><a href="/reports/topSellingProducts"><i class="fa fa-cutlery"></i> Top Selling Products </a></li>
                        @endif
                        @if(isAccessModuleAllowed('top_seller_sponsor'))
                        <li><a href="/reports/topSeller"><i class="fa fa-users"></i> Top Seller / Top Sponsor</a> </li>
                        @endif
                        
                       
                    </ul>
                </li>
            @endif
            @if(isAccessModuleAllowed('users_view'))
                <li>
                    <a href="/users">
                        <i class="fa fa-street-view"></i> <span>Users</span></i>
                    </a>
                </li>
            @endif
            @if(isAccessModuleAllowed('roles_view'))

                <li>
                    <a href="/roles">
                        <i class="fa fa-key"></i> <span>User Roles</span></i>
                    </a>
                </li>
            @endif


        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>