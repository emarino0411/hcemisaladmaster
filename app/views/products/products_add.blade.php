@extends('admin_template')

@section('additional_header')

        <!-- DataTables -->
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.min.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker-bs3.css")}}'>

@endsection

@section('content')
        <!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-cutlery fa-fw"></i> Product Information</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12 hidden" id="div_error">
                            <div class="alert alert-warning">
                                <strong><i class="fa fa-warning"></i></strong> There were some problems with your input.
                            </div>
                        </div>

                        <div class="col-md-12">
                            <form class="form-horizontal">
                                <!-- Account Balance -->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="txt_product_name" class="col-sm-4 control-label">
                                            Product Name</label>

                                        <div class="col-sm-8">
                                            <input type="text" class="form-control pull-right" id="txt_product_name"
                                                   data-toggle="tooltip"
                                                   title="Alphanumeric only.">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="slct_product_type" class="col-sm-4 control-label"> Type </label>

                                        <div class="col-sm-8">
                                            <select id="slct_product_type" class="form-control select2"
                                                    style="width: 100%;height:100%;background-color:white"
                                                    data-toggle="tooltip"
                                                    title="Product Type">
                                                <option value="Single">Loose Item</option>
                                                <option value="Set">Product Set</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="txt_product_code" class="col-sm-4 control-label">
                                            Product Code</label>

                                        <div class="col-sm-8">
                                            <input type="text" class="form-control pull-right" id="txt_product_code"
                                                   data-toggle="tooltip"
                                                   title="Alphanumeric only.">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6 hidden">
                                    <div class="form-group">
                                        <label for="slct_product_category" class="col-sm-4 control-label">
                                            Category </label>

                                        <div class="col-sm-8">
                                            <select id="slct_product_category" class="form-control select2"
                                                    style="width: 100%;height:100%;background-color:white"
                                                    data-toggle="tooltip"
                                                    title="Product Category">
                                                <option>Caserole</option>
                                                <option>Machine Processing</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="txt_retail_price" class="col-sm-4 control-label">
                                            Retail Price</label>

                                        <div class="col-sm-8">
                                            <input type="text" class="form-control pull-right" id="txt_retail_price"
                                                   data-toggle="tooltip"
                                                   title="Alphanumeric only.">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="txt_inventory_price" class="col-sm-4 control-label">
                                            Inventory Price</label>

                                        <div class="col-sm-8">
                                            <input type="text" class="form-control pull-right" id="txt_inventory_price"
                                                   data-toggle="tooltip"
                                                   title="Alphanumeric only.">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="txt_production_point" class="col-sm-4 control-label">
                                            Production Points</label>

                                        <div class="col-sm-8">
                                            <input type="text" class="form-control pull-right" id="txt_production_point"
                                                   data-toggle="tooltip"
                                                   title="Alphanumeric only.">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="txt_production_point_associate" class="col-sm-4 control-label">
                                            Production Points<small>(Associate)</small></label>

                                        <div class="col-sm-8">
                                            <input type="text" class="form-control pull-right" id="txt_production_point_associate"
                                                   data-toggle="tooltip"
                                                   title="Alphanumeric only.">
                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="txt_spv" class="col-sm-4 control-label">
                                            SPV</label>

                                        <div class="col-sm-8">
                                            <input type="text" class="form-control pull-right" id="txt_spv"
                                                   data-toggle="tooltip"
                                                   title="Alphanumeric only.">
                                        </div>
                                    </div>
                                </div>
                            </form>

                            <!-- buttons  -->
                            <div class="col-md-2 col-md-offset-10 ">
                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="button" id="btn_save_product" class="btn btn-info pull-right">
                                            Save
                                        </button>

                                        <button class="btn btn-danger"> Cancel</button>
                                    </div>
                                </div>
                                <!-- /btn-group -->
                            </div>
                            <!-- /.buttons  -->

                        </div><!-- col-md-12 -->
                    </div><!-- /row -->
                </div>
                <!-- /.box-body -->
            </div>
        </div>
        <!-- /.col -->
    </div>


    {{--MODAL--}}
    <div id="save-product-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-blue-gradient">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><i class="ion ion-check"></i> Save Product</h4>
                </div>
                <div class="modal-body">
                    Record has been saved.
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" id="btn_close_modal" data-dismiss="modal">Close
                    </button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--MOdal End--}}


</section>

@endsection

@section('additional_footer')

    <script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/jquery.dataTables.min.js")}}'></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js")}}'></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.full.min.js")}}'></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker.js")}}'></script>

    <script>
        $(document).ready(function () {
            $(".select2").select2();
            $("#txt_user_birthdate").daterangepicker({
                singleDatePicker: true,
                showDropdowns: true
            });

            $("#btn_save_product").click(function () {


                $.post("/products/create", {
                    item_code: $("#txt_product_code").val(),
                    product_type: $("#slct_product_type").val(),
                    description: $("#txt_product_name").val(),
                    retail_price: $("#txt_retail_price").val(),
                    inventory_price: $("#txt_inventory_price").val(),
                    category: $("#slct_product_category").val(),
                    product_type: $("#slct_product_type").val(),
                    production_points: $("#txt_production_point").val(),
                    production_points_associate: $("#txt_production_point_associate").val(),
                    spv: $("#txt_spv").val()
                }).done(function (data) {
                    
                    $("#save-product-modal").modal('show');
                });
            });
            $("#btn_close_modal").click(function(){
                window.location = '/products?save=success';
            });

        });
    </script>

@endsection