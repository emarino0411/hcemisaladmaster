@extends('admin_template')

@section('additional_header')

        <!-- DataTables -->
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.min.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker-bs3.css")}}'>

@endsection

@section('content')
        <!-- Main content -->
<section class="content">

    <div class="row">
        <div class="col-md-3">

            <!-- Profile Image -->
            <div class="box box-primary">
                <div class="box-body box-profile">
                    
                    <h3 class="profile-username text-center">{{$product->description}}</h3>
                    <hr>
                    <p class="text-muted text-center"><b>Item Code</b><br>{{$product->item_code}}</p>
                    <hr>
                    <p class="text-muted text-center"><b>Item Status</b><br>{{$product->status}}</p>
                    @if(isAccessModuleAllowed('products_edit'))
                        <center>
                            <button class="btn btn-primary" id="btn_update">Update Information</button>
                        </center>
                    @endif

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

        </div>
        <!-- /.col -->
        <div class="col-md-9">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header">
                            <h3 class="box-title"><i class="fa fa-cutlery fa-fw"></i> Product Information</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12 hidden" id="div_error">
                                    <div class="alert alert-warning">
                                        <strong><i class="fa fa-warning"></i></strong> There were some problems with
                                        your input.
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <form class="form-horizontal">
                                        <!-- Account Balance -->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="txt_product_name" class="col-sm-4 control-label">
                                                    Product Name</label>

                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control pull-right"
                                                           id="txt_product_name" data-toggle="tooltip"
                                                           value="{{$product->description}}" title="Alphanumeric only.">
                                                    <input type="hidden" id="id" value="{{$product->id}}">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="slct_product_type" class="col-sm-4 control-label">
                                                    Type </label>

                                                <div class="col-sm-8">
                                                    <select id="slct_product_type" class="form-control select2"
                                                            style="width: 100%;height:100%;background-color:white"
                                                            data-toggle="tooltip"
                                                            title="Civil Status">
                                                        <option @if($product->type=='Single Product') selected @endif>
                                                            Single Product
                                                        </option>
                                                        <option @if($product->type=='Product Set') selected @endif>
                                                            Product Set
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="txt_product_code" class="col-sm-4 control-label">
                                                    Product Code</label>

                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control pull-right"
                                                           id="txt_product_code" data-toggle="tooltip"
                                                           value="{{$product->item_code}}" title="Alphanumeric only.">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6 hidden">
                                            <div class="form-group">
                                                <label for="slct_product_category" class="col-sm-4 control-label">
                                                    Category </label>

                                                <div class="col-sm-8">
                                                    <select id="slct_product_category" class="form-control select2"
                                                            style="width: 100%;height:100%;background-color:white"
                                                            data-toggle="tooltip"
                                                            title="Product Category">
                                                        <option @if($product->category=='Caserole') selected @endif>
                                                            Caserole
                                                        </option>
                                                        <option @if($product->category=='Machine Processing') selected @endif>
                                                            Machine Processing
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="txt_retail_price" class="col-sm-4 control-label">
                                                    Retail Price</label>

                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control pull-right"
                                                           id="txt_retail_price" data-toggle="tooltip"
                                                           value="{{$product->retail_price}}"
                                                           title="Alphanumeric only.">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="txt_inventory_price" class="col-sm-4 control-label">
                                                    Inventory Price</label>

                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control pull-right"
                                                           id="txt_inventory_price" data-toggle="tooltip"
                                                           value="{{$product->inventory_price}}"
                                                           title="Alphanumeric only.">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="txt_production_point" class="col-sm-4 control-label">
                                                    Production Points</label>

                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control pull-right"
                                                           id="txt_production_point"
                                                           data-toggle="tooltip"
                                                           value="{{$product->production_points}}"
                                                           title="Alphanumeric only.">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="txt_production_point_associate"
                                                       class="col-sm-4 control-label">
                                                    Production Points
                                                    <small>(Associate)</small>
                                                </label>

                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control pull-right"
                                                           id="txt_production_point_associate"
                                                           data-toggle="tooltip"
                                                           value="{{$product->production_points_associate}}"
                                                           title="Alphanumeric only.">
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="txt_spv" class="col-sm-4 control-label">
                                                    SPV</label>

                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control pull-right" id="txt_spv"
                                                           data-toggle="tooltip"
                                                           value="{{$product->spv}}"
                                                           title="Alphanumeric only.">
                                                </div>
                                            </div>
                                        </div>
                                    </form>

                                    <!-- buttons  -->
                                    <div class="col-md-3 col-md-offset-9" id="div_update_product">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <button type="button" id="btn_save_product"
                                                        class="btn btn-info pull-right"> Update
                                                </button>
                                                <button class="btn btn-warning" id="btn_cancel_update"> Cancel</button>
                                            </div>
                                        </div>
                                        <!-- /btn-group -->
                                    </div>
                                    <!-- /.buttons  -->

                                </div><!-- col-md-12 -->
                            </div><!-- /row -->
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
                <!-- /.col -->
            </div>

        </div>
        <!-- /.col -->
    </div>

   {{--MODAL--}}
    <div id="save-product-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-blue-gradient">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><i class="ion ion-check"></i> Save Product</h4>
                </div>
                <div class="modal-body">
                    Record has been saved.
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" id="btn_close_modal" data-dismiss="modal">Close
                    </button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--MOdal End--}}


</section>

@endsection

@section('additional_footer')

    <script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/jquery.dataTables.min.js")}}'></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js")}}'></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.full.min.js")}}'></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker.js")}}'></script>


    <script>
        $(document).ready(function () {

            $('.select2').select2();
            $("#txt_user_birthdate").daterangepicker({
                singleDatePicker: true,
                showDropdowns: true
            });
            $('.select2').attr('height', '200%');
                @if(!$edit_mode){
                cancelEdit();
            }
            @endif


            $('#example1').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true
            });

            $("#btn_search").click(function () {
                $(".select2").select2({
                    theme: "classic"
                });
                $('#txt_daterange').daterangepicker();

            });

            $(".hide-filter").each(function (index) {
                $(this).parent().parent().hide();
                $(this).on("click", function () {
                    $(this).parent().parent().hide();
                    $("." + $(this).parent().parent().attr('id')).show();
                });
            });

            $(".dropdown-menu li").each(function (index) {
                $(this).on("click", function () {
                    $("#" + $(this).children("input").val()).show();
                    $(this).hide();
                });
            });

            $("#btn_update").click(function () {
                console.log('hahahah');

                $("#slct_product_type").removeAttr('disabled');
                $("#txt_product_code").removeAttr('disabled');
                $("#txt_product_name").removeAttr('disabled');
                $("#txt_retail_price").removeAttr('disabled');
                $("#txt_inventory_price").removeAttr('disabled');
                $("#slct_product_category").removeAttr('disabled');
                $("#slct_product_type").removeAttr('disabled');
                $("#div_update_product").show();

            });

            $("#btn_cancel_update").click(function () {
                cancelEdit();
            });

            function cancelEdit() {
                $("#div_update_product").hide();

                $("#slct_product_type").attr('disabled', 'true');
                $("#txt_product_code").attr('disabled', 'true');
                $("#txt_product_name").attr('disabled', 'true');
                $("#txt_retail_price").attr('disabled', 'true');
                $("#txt_inventory_price").attr('disabled', 'true');
                $("#slct_product_category").attr('disabled', 'true');
                $("#slct_product_type").attr('disabled', 'true');
                $("#txt_production_point").attr('disabled', 'true');
                $("#txt_production_point_associate").attr('disabled', 'true');
                $("#txt_spv").attr('disabled', 'true');

                $("#slct_product_type").css('background-color', 'white');
                $("#txt_product_code").css('background-color', 'white');
                $("#txt_product_name").css('background-color', 'white');
                $("#txt_retail_price").css('background-color', 'white');
                $("#txt_inventory_price").css('background-color', 'white');
                $("#slct_product_category").css('background-color', 'white');
                $("#slct_product_type").css('background-color', 'white');
                $("#txt_production_point").css('background-color', 'white');
                $("#txt_production_point_associate").css('background-color', 'white');
                $("#txt_spv").css('background-color', 'white');

                $('.select2').css('background-color', 'white');
            }

                @if(!$edit_mode){
                cancelEdit();
            }
            @endif

            $("#btn_update").click(function () {
                console.log('hahahah');
//                $("#txt_user_last_name").removeAttr('disabled');
//                $("#txt_user_first_name").removeAttr('disabled');
//                $("rdo_gender").removeAttr('disabled');
//                $("#txt_user_birthdate").removeAttr('disabled');
//                $("#slct_civil_status").removeAttr('disabled');
//                $("#slct_user_role").removeAttr('disabled');
//                $("#txt_user_street").removeAttr('disabled');
//                $("#slct_user_barangay").removeAttr('disabled');
//                $("#slct_town").removeAttr('disabled');
//                $("#slct_province").removeAttr('disabled');
//                $("#txt_user_landline").removeAttr('disabled');
//                $("#txt_user_mobile").removeAttr('disabled');
//                $("#txt_user_email").removeAttr('disabled');

                $("#slct_product_type").removeAttr('disabled');
                $("#txt_product_code").removeAttr('disabled');
                $("#txt_product_name").removeAttr('disabled');
                $("#txt_retail_price").removeAttr('disabled');
                $("#txt_inventory_price").removeAttr('disabled');
                $("#slct_product_category").removeAttr('disabled');
                $("#slct_product_type").removeAttr('disabled');

                $("#txt_production_point").removeAttr('disabled');
                $("#txt_production_point_associate").removeAttr('disabled');
                $("#txt_spv").removeAttr('disabled');


                $("#div_update_product").show();

            });

            $("#btn_cancel_update").click(function () {
                cancelEdit();
            });

            $("#btn_save_product").click(function () {
                $.post("/products/update", {
                    id: $("#id").val(),
                    item_code: $("#txt_product_code").val(),
                    product_type: $("#slct_product_type").val(),
                    description: $("#txt_product_name").val(),
                    retail_price: $("#txt_retail_price").val(),
                    inventory_price: $("#txt_inventory_price").val(),
                    category: $("#slct_product_category").val(),
                    product_type: $("#slct_product_type").val(),
                    production_points: $("#txt_production_point").val(),
                    production_points_associate: $("#txt_production_point_associate").val(),
                    spv: $("#txt_spv").val()
                }).done(function (data) {
                         $("#save-product-modal").modal('show');
                });
            });
            $("#btn_close_modal").click(function(){
                window.location = '/products?save=success';
            });
        });
    </script>
@endsection